buzzwords = ['Step 1', 'Step 2', 'Step 3', 'Step 4', 'Step 5', 'Step 10', 'Step 14', 'Step 15', 'Step 16'] 
buzzword_counts = Hash.new({ value: 0 })

SCHEDULER.every '2s' do
  random_buzzword = buzzwords.sample
  buzzword_counts[random_buzzword] = { label: random_buzzword, value: (buzzword_counts[random_buzzword][:value] + 1) % 30 }

  send_event('buzzwords', { items: buzzword_counts.values })
end
