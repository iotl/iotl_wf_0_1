package com;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.gt.bo.DocumentMetaDataCreate;
import com.gt.entity.Businesses;
import com.gt.entity.Material;
import com.gt.entity.RevenueProposal;
import com.gt.services.RuleEngineHelper;
import com.gt.utils.ExcelUtils;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = WfApplication.class)
public class WfApplicationTests {

	@Autowired
	private RuleEngineHelper ruleEngineHelper;	


	@Test
	public void testDOAForRawMaterial() {

		String filePath = System.getProperty("user.dir");
		filePath = filePath + "\\TestData\\Hirakud_TC.xls";
		List<Map<Integer, String>> excelList;
		try {
			excelList = ExcelUtils.getContent(filePath,
					true);

			if (excelList != null && !excelList.isEmpty()) {
				
				List<String> testNumber = new ArrayList<String>();
				for (Map<Integer, String> rowList : excelList) {
					DocumentMetaDataCreate dmdc = new DocumentMetaDataCreate();
					ArrayList<RevenueProposal> rps1 = new ArrayList<RevenueProposal>();
					RevenueProposal rp= new RevenueProposal();
					if(rowList.get(1)==null || rowList.get(1).isEmpty()){
						continue;
					}
					rp.setAmountNegotiated(Double.valueOf(rowList.get(1)));
					rp.setIsBudgeted(rowList.get(2));
					rps1.add(rp);
					dmdc.setRpData(rps1);
					Material mat= new Material();
					mat.setMaterialId(Integer.parseInt(rowList.get(3)));
					Businesses bs=new Businesses();
					bs.setBusinessName(rowList.get(4));
					dmdc.setMaterial(mat);
					dmdc.setBusiness(bs);
					dmdc.setPlantName(rowList.get(5));
					dmdc.setCreatedBy(Integer.parseInt(rowList.get(6)));

					System.out.println("test"+rowList);
					ruleEngineHelper.addBusinessRuleUsersUT(dmdc,null,null,null);

					//Compare the DOA chain provided by Rule Engine and the expected DOA chain (as mentioned in excel data)
					String doaOutPutFromRuleEngine = ruleEngineHelper.getDoaOutputFromRuleEngine();

					System.out.println("doaOutPutFromRuleEngine >>>>>>>>>"+doaOutPutFromRuleEngine);
					
					
					String expectedDOA = rowList.get(7);
					
					System.out.println("expectedDOA >>>>>>>>>"+expectedDOA);
					
					if(!doaOutPutFromRuleEngine.trim().equalsIgnoreCase(expectedDOA.trim()))
					{
						testNumber.add(rowList.get(0));
					}
				}
				
				for(String failedTestNumber :testNumber){
					System.out.println("The failedTestNumbers from sheet is: "+failedTestNumber);
				}
				
				assertEquals(false,!testNumber.isEmpty());	
					
			} else {
				throw new Exception(
						"Invalid XL File. Row or Column is empty");
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
