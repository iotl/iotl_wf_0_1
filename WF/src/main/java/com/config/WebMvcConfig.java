package com.config;

import java.util.Properties;

import javax.servlet.MultipartConfigElement;

import org.springframework.boot.context.embedded.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import org.springframework.web.servlet.mvc.WebContentInterceptor;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.gt.interceptor.LoginInterceptor;

@EnableWebMvc
@Configuration
class WebMvcConfig extends WebMvcConfigurerAdapter {
	 @Bean
	 public InternalResourceViewResolver configureInternalResourceViewResolver() {
	  InternalResourceViewResolver resolver = new InternalResourceViewResolver();
	  resolver.setPrefix("/UI1/");
	  resolver.setSuffix(".html");
	  return resolver;
	
}
	 
	 @Bean
	    public MultipartConfigElement multipartConfigElement() {
	        MultipartConfigFactory factory = new MultipartConfigFactory();
	        factory.setMaxFileSize("150MB");
	        factory.setMaxRequestSize("150MB");
	        return factory.createMultipartConfig();
	    }
	 
	 @Bean
	 public WebContentInterceptor webContentInterceptor() {
	     WebContentInterceptor interceptor = new WebContentInterceptor();
	     interceptor.setCacheSeconds(0);
	     interceptor.setUseExpiresHeader(true);;
	     interceptor.setUseCacheControlHeader(true);
	     interceptor.setUseCacheControlNoStore(true);

	     return interceptor;
	 }
	 
	 
	 @Bean
	    public SimpleMappingExceptionResolver createSimpleMappingExceptionResolver() {
	      SimpleMappingExceptionResolver resolver = new SimpleMappingExceptionResolver();
	      Properties errorMaps = new Properties();
	      //errorMaps.setProperty("com.gt.exception.NotSignedInException", "/loginC/isLoggedIn");
	     errorMaps.setProperty("com.gt.exception.NotSignedInException", "redirect: loginC/isLoggedIn");
	      resolver.setExceptionMappings(errorMaps);
	      //resolver.setDefaultErrorView("globalerror");
	      resolver.setExceptionAttribute("exc");
	      return resolver;
	   }
	 
	 @Bean
	 public LoginInterceptor loginInterceptor() {
	     return new LoginInterceptor();
	 }
	 
	 @Override
	    public void addInterceptors(InterceptorRegistry registry) {
	        registry.addInterceptor(loginInterceptor()).addPathPatterns("/**");
	 
	
	 }
	 
	  
}