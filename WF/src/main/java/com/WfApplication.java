package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.velocity.VelocityAutoConfiguration;

@SpringBootApplication
@EnableAutoConfiguration(exclude = {VelocityAutoConfiguration.class})
public class WfApplication {

    public static void main(String[] args) {
        SpringApplication.run(WfApplication.class, args);
    }
}
