package com.gt.utils;

/***
 * 
 * @author naval
 * List of statuses for DocUserAction 
 *
 */
public interface ActionStatus {
	
	/**
	 * Document created by Initiator
	 */
	public static final String INITIATED = "Initiated";
	
	/**
	 * Document endorsed by Endorser
	 */
	public static final String ENDORSED = "Endorsed";
	/**
	 * Advisory endorser added by Endorser
	 */
	public static final String ENDORSED_PARALLEL = "Endorsed_Parallel";
	/**
	 * An action item for either endorsement or reply or approval or something else.
	 */
	public static final String PENDING = "Pending";
	
	/**
	 * An action item for parallel advisory endorsers
	 */
	public static final String PENDING_PARALLEL = "Pending_Parallel";
	
	/**
	 * An action item for Controller for express documents
	 */
	public static final String PENDING_EXPRESS = "Pending_Express";
	
	/**
	 * An action item which is no longer valid
	 */
	public static final String PENDING_INVALID = "Pending_Invalid";
	
	/**
	 * Document referred to an endorser/initiator in the earlier chain
	 */
	public static final String REFERRED = "Referred";
	
	/**
	 * Reply sent to an 'Referred' action
	 */
	public static final String REPLIED = "Replied";
	
	/**
	 * Document approved by Approver
	 */
	public static final String APPROVED = "Approved";
	
	/**
	 * Document rejected by Approver
	 */
	public static final String REJECTED = "Rejected";
	
	/**
	 * Document released/closed by Initiator/Controller
	 */
	public static final String RELEASED = "Released";
	
	/**
	 * Document cancelled by Initiator
	 */
	public static final String CANCELLED = "Cancelled";
	
	/**
	 * Document Created by Initiator but not send for an endorsement
	 */
	public static final String DRAFT = "Draft";
	
}
