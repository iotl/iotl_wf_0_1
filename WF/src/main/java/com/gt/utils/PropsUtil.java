package com.gt.utils;

import java.util.ResourceBundle;

import org.apache.log4j.Logger;

public final class PropsUtil {

	/* private static ResourceBundle bundle = null; */

	private static String PROP_FILE = "appsettings";

	private static final Logger cat = Logger.getLogger(PropsUtil.class);

	public static String getValue(String key) {
		String rtnStr = "";
		try {
			ResourceBundle myBundle = ResourceBundle.getBundle(PROP_FILE);
			if (myBundle != null)
				rtnStr = myBundle.getString(key).trim();

		} catch (Exception e) {
			cat.fatal("getValue() Exception " + e.toString());
		}
		return rtnStr;
	}


	public static String getValue(String fileClassPath, String key) {
		String rtnStr = "";
		try {
			ResourceBundle myBundle = ResourceBundle.getBundle(fileClassPath);
			if (myBundle != null)
				rtnStr = myBundle.getString(key).trim();

		} catch (Exception e) {
			cat.fatal("getValue() Exception " + e.toString());
		}
		return rtnStr;
	}

}
