/**
 * 
 */
package com.gt.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.Part;

/**
 * @author Mohan
 *
 */
public class WFUtil {
	
	/**
	 * @param filePath
	 * @param inClassPath
	 * @return
	 */
	public static String getFileContent(String filePath, boolean inClassPath) {
		String fileContent = null;
		InputStream in = null;
		BufferedReader bReader = null;
		try {

			if (inClassPath) {
				// in = getFileFromClassPath(filePath);
				in = WFUtil.class.getResourceAsStream(filePath);
				bReader = new BufferedReader(new InputStreamReader(in));
			} else {
				bReader = new BufferedReader(new FileReader(filePath));
			}

			String str;
			StringBuilder sb = new StringBuilder();
			while ((str = bReader.readLine()) != null) {
				sb.append(str);
			}

			fileContent = sb.toString();
		} catch (Exception e) {
			System.out.println("Exception at getFileContent(1) >>>> "
					+ e.toString());
			// e.printStackTrace();
		} finally {
			try {
				if (bReader != null) {
					bReader.close();
				}
			} catch (Exception e) {
				//
			}

			try {
				if (in != null) {
					in.close();
				}
			} catch (Exception e) {
				//
			}
		}
		return fileContent;
	}
	
	/**
	 * @param dirPath
	 */
	public static void makeFilePath(String dirPath) {

		makeDirectories(dirPath);

	}
	
	/**
	 * 
	 * @param dirPath
	 */
	public static File makeDirectories(String dirPath) {
		File dir = new File(dirPath);
		// localInfo("<<<< DIR-PATH >>>> " + dirPath);
		//
		if (!dir.isDirectory()) {
			dir.mkdirs();
		}
		return dir;
	}
	
	public static  String getFileName(final Part part) {
		final String partHeader = part.getHeader("content-disposition");
		for (String content : part.getHeader("content-disposition").split(";")) {
			if (content.trim().startsWith("filename")) {
				String fileName = content.substring(content.indexOf('=') + 1)
						.trim().replace("\"", "");
				Path p = Paths.get(fileName);
				fileName = p.getFileName().toString();
				return fileName;
			}
		}
		return null;
	}
	
	public static Collection<String> docRoleList(){
	List<String> docRoleList= new ArrayList<String>();
	docRoleList.add(DocRole.APPROVER);
	docRoleList.add(DocRole.ENDORSER);
	docRoleList.add(DocRole.INITIATOR);
	docRoleList.add(DocRole.CONTROLLER);
	Collection<String> docRole = new ArrayList<String>(docRoleList);
	return docRole;
	}
	
	public static Collection<String> docRoleListForMainLine(){
		List<String> docRoleList= new ArrayList<String>();
		docRoleList.add(DocRole.APPROVER);
		docRoleList.add(DocRole.ENDORSER);
		docRoleList.add(DocRole.INITIATOR);
		docRoleList.add(DocRole.CONTROLLER);
		Collection<String> docRole = new ArrayList<String>(docRoleList);
		return docRole;
		}
	
	public static Collection<String> docRoleListForSupportingEndorser(){
		List<String> docRoleList= new ArrayList<String>();
		docRoleList.add(DocRole.APPROVER);
		docRoleList.add(DocRole.ENDORSER);
		docRoleList.add(DocRole.INITIATOR);
		docRoleList.add(DocRole.CONTROLLER);
		docRoleList.add(DocRole.EARLIER_APPROVER);
		docRoleList.add(DocRole.EARLIER_ENDORSER);
		Collection<String> docRole = new ArrayList<String>(docRoleList);
		return docRole;
		}
	
	
	
	public static Collection<String> getDocRoles(){
		List<String> docRoleList= new ArrayList<String>();
		docRoleList.add(DocRole.EARLIER_APPROVER);
		docRoleList.add(DocRole.EARLIER_ENDORSER);
		Collection<String> docRole = new ArrayList<String>(docRoleList);
		return docRole;
		}
	
	public static String getFormattedDisplayDate(Date dateValue,
			String requiredFormat, String timeZoneStr) {
		String formatedDate = "";
		// yyyy-MM-dd HH:mm:ss
		try {

			DateFormat outputFormat = new SimpleDateFormat(requiredFormat);
			TimeZone timeZone = TimeZone.getTimeZone(timeZoneStr);
			outputFormat.setTimeZone(timeZone);
			formatedDate = outputFormat.format(dateValue);
		} catch (Exception e) {
		}

		return formatedDate;
	}

	
	/**
	 * 
	 * @param dateValue
	 * @param givenFormat
	 * @return
	 */
	public static Date getStringToDate(String dateValue, String givenFormat) {
		// DateFormat formatter;
		Date date = null;
		try {
			// "MM/dd/yyyy"
			DateFormat existingFormat = new SimpleDateFormat(givenFormat);
			date = existingFormat.parse(dateValue);
		} catch (Exception e) {
		}

		return date;

	}


	
	
}
