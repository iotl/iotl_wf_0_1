package com.gt.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Fetch default password and hash it
 * @author ABGHIL development team
 *
 */
public class PasswordHashing {

	private static final String defaultPasswordKey="default.password";
	
	/**
	 * added on 7-12-15 to generate hash value of default password
	 *
	 * @return
	 */
	public static String PasswordEncoderGenerator(){
		
		String defaultPassword=PropsUtil.getValue(defaultPasswordKey);
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hashedPassword = passwordEncoder.encode(defaultPassword);
		
		return hashedPassword;
		
	}
	
	
	
}
