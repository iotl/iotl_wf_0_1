package com.gt.utils;

public interface Formats {
	
	/**
	 * Common Indian Date format upto seconds details 
	 */
	public static final String DATE_WITH_HOUR_MINUTE_SECOND = "dd/MM/yyyy HH:mm:ss";
	
	/**
	 * Commas after thousands and lakhs. Two decimals precision.
	 */
	public static final String INDIAN_NUMBER_FORMAT_WITH_TWO_DECIMALS = "#,##,##0.00";
	
	/**
	 * Commas after thousands and lakhs. Zero decimals precision.
	 */
	public static final String INDIAN_NUMBER_FORMAT_WITH_ZERO_DECIMALS = "#,##,##0";
	
	/**
	 * No commas  after thousands and lakhs. Zero decimals precision.
	 */
	public static final String INDIAN_NUMBER_FORMAT_WITH_TWO_DECIMALS_WITHOUT_SEPERATOR = "#####0.00";

}
