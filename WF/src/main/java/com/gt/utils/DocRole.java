package com.gt.utils;

/**
 * @author naval
 * User's role for a specific document -- as stored in DocUserMaster and DocUserAction
 *
 */
public interface DocRole {
	
	public static final String INITIATOR = "Initiator";
	
	public static final String ENDORSER = "Endorser";
	
	public static final String VIEWER = "Viewer";
	
	public static final String PO_VIEWER = "POViewer";
	
	public static final String CONTROLLER = "Controller";
	
	public static final String ASSISTANT_CONTROLLER = "Assistant-Controller";
	
	public static final String MINING_CONTROLLER = "SE-Mining & Minerals-Controller";
	
	public static final String MINING_COAL = "SE-Mining & Minerals-Coal Procurement";
	
	public static final String MINING_HEAD = "SE-Mining & Minerals-Head";
	
	public static final String APPROVER = "Approver";
	
	public static final String INITENDORSER = "InitEndorser";
	
    public static final String EARLIER_ENDORSER = "EARLIER_ENDORSER";
	
	public static final String EARLIER_APPROVER = "EARLIER_APPROVER";
	
	public static final String EARLIER_CONTROLLER = "EARLIER_CONTROLLER";
	
	public static final String TEMPORARY_DELEGATEE = "Temporary-Delegatee";

}
