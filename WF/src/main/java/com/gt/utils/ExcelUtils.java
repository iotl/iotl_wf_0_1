package com.gt.utils;


import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import jxl.Cell;
import jxl.CellView;
import jxl.Sheet;
import jxl.Workbook;
import jxl.write.Alignment;
import jxl.write.Border;
import jxl.write.BorderLineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.log4j.Logger;

/**
 * @author Vignesh
 * 
 *         This class is created to write content in Excel
 */

public final class ExcelUtils {

	private static final Logger logger = Logger.getLogger(ExcelUtils.class
			.getName());

	/**
	 * Property file name for Temporary class path.
	 */
	// private static final String TEMP_PROPERTY_FILE = "appsettings";
/**
	 * @param xlFilePath
	 * @return
	 * @throws Exception
	 */
	public static List<Map<Integer, String>> getContent(String xlFilePath,
			boolean ignoreFirstRow) throws Exception {
		List<Map<Integer, String>> contents = null;
		Workbook workbook = null;
		Sheet sheet = null;
		try {
			contents = new ArrayList<Map<Integer, String>>();
			workbook = Workbook.getWorkbook(new File(xlFilePath));
			if (workbook != null) {
				Sheet[] sheets = workbook.getSheets();
				if (sheets != null && sheets.length > 0) {
					sheet = sheets[0];
					int columns = sheet.getColumns();
					int rows = sheet.getRows();
					int startingRow = 0;
					if (ignoreFirstRow) {
						startingRow = 1;
					}
					for (int r = startingRow; r < rows; r++) {
						Map<Integer, String> contentRowMap = new LinkedHashMap<Integer, String>();
						boolean emptyRow = true;
						for (int i = 0; i < columns; i++) {
							Cell cell = sheet.getCell(i, r); // Column,Row
							String content = cell.getContents();
							content = content == null ? "" : content.trim();
							if (content.length() > 0) {
								emptyRow = false;
							}
							contentRowMap.put(Integer.valueOf(i), content);
						}
						if (!emptyRow) {
							contents.add(contentRowMap);
						}
					}
				} else {
					throw new Exception("Sheet(s) Not Found");
				}
			} else {
				throw new Exception("WorkBook Not Created");
			}
		} catch (Exception e) {
			logger.fatal("Exception at getContent() >>>> " + e.toString());
			throw new Exception(e);
		} finally {
			try {
				if (workbook != null) {
					workbook.close();
				}
			} catch (Exception e) {
				//
			}
		}
		return contents;
	}

	/**
	 * @param xlFilePath
	 * @param ignoreFirstRow
	 * @return
	 * @throws Exception
	 */
	public static List<List<String>> getXLContent(String xlFilePath,
			boolean ignoreFirstRow) throws Exception {
		List<List<String>> rowContents = null;
		Workbook workbook = null;
		Sheet sheet = null;
		try {
			rowContents = new ArrayList<List<String>>();
			workbook = Workbook.getWorkbook(new File(xlFilePath));
			if (workbook != null) {
				Sheet[] sheets = workbook.getSheets();
				if (sheets != null && sheets.length > 0) {
					sheet = sheets[0];
					int columns = sheet.getColumns();
					int rows = sheet.getRows();
					int startingRow = 0;
					if (ignoreFirstRow) {
						startingRow = 1;
					}
					for (int r = startingRow; r < rows; r++) {
						List<String> contentList = new ArrayList<String>();
						for (int i = 0; i < columns; i++) {
							Cell cell = sheet.getCell(i, r); // Column,Row
							String content = cell.getContents();
							content = content == null ? "" : content.trim();
							contentList.add(content);
						}

						rowContents.add(contentList);

					}
				} else {
					throw new Exception("Sheet(s) Not Found");
				}
			} else {
				throw new Exception("WorkBook Not Created");
			}
		} catch (Exception e) {
			logger.fatal("Exception at getXLContent() >>>> " + e.toString());
			throw new Exception(e);
		} finally {
			try {
				if (workbook != null) {
					workbook.close();
				}
			} catch (Exception e) {
				//
			}
		}
		return rowContents;
	}

	/**
	 * @param xlFilePath
	 * @return
	 */
	public static String[] getColumnNames(String xlFilePath) throws Exception {
		String[] columnNames = null;
		Workbook workbook = null;
		Sheet sheet = null;
		try {
			workbook = Workbook.getWorkbook(new File(xlFilePath));

			if (workbook != null) {
				Sheet[] sheets = workbook.getSheets();
				if (sheets != null && sheets.length > 0) {
					sheet = sheets[0];

					int columns = sheet.getColumns();
					columnNames = new String[columns];
					for (int i = 0; i < columns; i++) {
						Cell cell = sheet.getCell(i, 0); // Column,Row
						String content = cell.getContents();
						columnNames[i] = content;
					}

				} else {
					throw new Exception("Sheet(s) Not Found");
				}
			} else {
				throw new Exception("WorkBook Not Created");
			}
		} catch (Exception e) {
			logger.fatal("Exception at getColumnNames() >>>> " + e.toString());
			throw new Exception(e);
		} finally {
			try {
				if (workbook != null) {
					workbook.close();
				}
			} catch (Exception e) {
				//
			}
		}

		return columnNames;
	}
	
	
	public static void wirteCommonExcelReport(List<String> columnNameList,
			List<List<String>> rowList, String fileLocation, String sheetName) {
		try {

			WritableCellFormat columnTitleCellFormat = new WritableCellFormat();

			/* Set border style and color for title */
			columnTitleCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
			columnTitleCellFormat.setBackground(jxl.format.Colour.IVORY);

			/* Set Border for other cell */
			WritableCellFormat columnCellFormat = new WritableCellFormat();
			columnCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
			columnCellFormat.setAlignment(Alignment.LEFT);
			
			WritableCellFormat columnCellFormat1 = new WritableCellFormat();
			columnCellFormat1.setBorder(Border.ALL, BorderLineStyle.THIN);
			columnCellFormat1.setAlignment(Alignment.RIGHT);

			/* Create work book in a given file */
			WritableWorkbook workbook = Workbook.createWorkbook(new File(
					fileLocation));

			logger.info("<<<< sheetName >>>> " + sheetName);
			/*
			 * Create work sheet with the with sheet name in work book
			 */
			WritableSheet sheet = workbook.createSheet(sheetName, 0);

			/* Write title of the column */
			if (columnNameList != null && !columnNameList.isEmpty()) {
				int size = columnNameList.size();

				for (int i = 0; i < size; ++i) {
					/*
					 * Print all column Title value in current row
					 * 
					 * new Label(col, row, content, format)
					 */
					/*if(i == 3){
							sheet.setColumnView(i, 40);
							}else if(i ==6){
								sheet.setColumnView(i, 60);
								}else if(i !=0){
									sheet.setColumnView(i, 20);
								}*/
					//int maxLength = Math.max(columnNameList.get(i).getBytes().length, 0);
					//sheet.getColumnView(i).setSize(25 * 12 *maxLength);
					/*for (int j = 0; j < size; j++) {
						maxLength = Math.max(sheet.getCell(i, j).getContents().getBytes().length, maxLength);
					}

					cell.setSize(25 * 12 * maxLength);*/

				//	sheet.setColumnView(i, maxLength);
					CellView cell = sheet.getColumnView(i);
					cell.setAutosize(true);
					sheet.setColumnView(i,  cell);
					sheet.addCell(new Label(i, 0, columnNameList.get(i),
								columnTitleCellFormat));
					
				}
				

				int totalRowWritten = 0;
				/* Create row as per the size of rowList */
				if (rowList != null && !rowList.isEmpty()) {
					int rowSize = rowList.size();
					logger.info("<<<< row size >>>> " + rowSize);
					for (int row = 0; row < rowSize; row++) {
						List<String> coloumnList = rowList.get(row);

						if (coloumnList != null && !coloumnList.isEmpty()) {
							totalRowWritten++;
							int columnSize = coloumnList.size();
							for (int col = 0; col < columnSize; col++) {

								/*
								 * Write data with the specified column and row
								 * and with the specified format
								 */
								String data = coloumnList.get(col);
								data = data == null ? "" : data.trim();
								int maxLength = Math.max(data.getBytes().length, 0);
								/*
								 * localInfo("col: " + col + "  row: " + row +
								 * "  data: " + data);
								 */
								CellView cell = sheet.getColumnView(col);
								cell.setAutosize(true);
								sheet.setColumnView(col,  cell);
								if(col == 0 || col == 3 || col == 4 || col == 5 || col == 6 || col == 7 || col == 8|| col == 9){
									/*Number	l=new Number(col, row + 1, Double.valueOf(data),
											columnCellFormat1);*/
									if(data.equalsIgnoreCase("0.00") && col != 3){
										data =" ";
										Label l  = new Label(col, row + 1, data,
												columnCellFormat1);
											sheet.addCell(l);
									}else{
										jxl.write.Number l = new jxl.write.Number(col, row+1, Double.valueOf(data), columnCellFormat1);
										sheet.addCell(l);
									}
								}else{
									Label l  = new Label(col, row + 1, data,
										columnCellFormat);
									sheet.addCell(l);
								}
							}
						}

					}
				}

				logger.info("<<<< Total Row Written >>>> " + totalRowWritten);

				/* Write all column value in worksheet */
				workbook.write();

				/* If work book has written close the workbook */
				workbook.close();
			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.fatal("Exception at wirteCommonExcelSalesReport 1 >>> "
					+ e.toString());

		}
	}

	public static void main(String... args) {
		//
	}
}
