/**
 * 
 */
package com.gt.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.gt.entity.DocAttachment;
import com.gt.entity.DocumentMaster;
import com.gt.entity.RevenueProposal;
import com.gt.entity.Users;
import com.gt.repository.MasterMaterialDataRepository;
import com.gt.services.DocumentService;
import com.gt.services.UserService;

/**
 * @author GYANTECH
 *
 */
@Component("rpDetailsGeneration")
@Transactional
public class RPDetailsGeneration {
	
	@Autowired
	private DocumentService documentService;
	
	@Autowired
	private DocAttachmentUtil docAttachmentUtil;
	
	@Autowired
	private UserService userservice;
	
	@Autowired
	private MasterMaterialDataRepository masterMaterialRepo;
	
	
	public void generateRpDetailsAsExcel(){
		
		List<String> statusList= new ArrayList<String>();
		statusList.add(DocStatus.INITIATED);
		statusList.add(DocStatus.APPROVED);
		statusList.add(DocStatus.RELEASED);
		statusList.add(DocStatus.IN_PROCESS);
		statusList.add(DocStatus.IN_PROCESS_PARALLEL);
		Collection<String> status = new ArrayList<String>(statusList);
		List<DocumentMaster> documentList=documentService.findByStatusIn(status);
		
List<List<String>> rowList = new ArrayList<List<String>>();
		
		//objList.addAll(itemList);
		
		List<String> columnList=new ArrayList<String>();
		columnList.add("Sr.No");
		columnList.add("RP Created Date");
		columnList.add("RP Created By");
		columnList.add("RP Number");
		columnList.add("Sum Of Negotiated Amount(In Lakhs)");
		columnList.add("Sum Of Approved Amount(In Lakhs)");
		columnList.add("Title");
		columnList.add("Material Name");
		columnList.add("Attachment Name");
		
		String filePath = PropsUtil.getValue("temp.path");

		WFUtil.makeFilePath(filePath);

		filePath = filePath + "\\rpdetails";
		WFUtil.makeFilePath(filePath);
		
		int i=1;
		for(DocumentMaster documentMaster:documentList){
			
		Users users=userservice.findByUserid(documentMaster.getCreatedBy());
			List<RevenueProposal> rpList=documentService.findRPByDocumentId(documentMaster.getDocumentId());
			
			Double negotiatedAmt=0d;
			Double approvedAmt=0d;
			for(RevenueProposal rp:rpList){
				negotiatedAmt=negotiatedAmt+(rp.getAmountNegotiated() == null ? 0d : rp.getAmountNegotiated());
				approvedAmt=approvedAmt+(rp.getAmountApproved() == null ? 0d : rp.getAmountApproved());
			}
			
			if(negotiatedAmt>0){
				negotiatedAmt=negotiatedAmt/100000;
			}
			
			if(approvedAmt>0){
				approvedAmt=approvedAmt/100000;
			}
			String rpNumberFP=filePath + "\\"  + rpList.get(0).getRpNumber().replaceAll("/", "_");
			
			WFUtil.makeFilePath(rpNumberFP);
			
			List<DocAttachment> docAttachmentList = documentService.findByDocumentId(documentMaster.getDocumentId());
			
			try {
				docAttachmentUtil.writePdfForRPGeneration(rpNumberFP,rpList.get(0).getRpNumber(),String.valueOf(documentMaster.getCreatedBy()),String.valueOf(documentMaster.getDocumentId()),"true",null,false,"false");
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			String attachmentName="";
			for(DocAttachment docAttachment:docAttachmentList){
			Blob blob = docAttachment.getContent();
			try {
				if(attachmentName.isEmpty()){
					attachmentName=docAttachment.getFileName();
				}else{
				attachmentName =attachmentName+","+docAttachment.getFileName();
				}
				byte[] byteArray = blob.getBytes(1, (int)blob.length());
				File file = new File(rpNumberFP+"\\"+docAttachment.getFileName());
				if (file != null) {
					if (file.exists()) {
						file.delete();
					}
					file.createNewFile();
				}

				FileOutputStream fop = new FileOutputStream(file);
				fop.write(byteArray);
				fop.flush();
				fop.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
			List<String> tempList=new ArrayList<String>();
			tempList.add(String.valueOf(i));
			tempList.add(WFUtil.getFormattedDisplayDate(documentMaster.getCreatedTimeStamp(), "dd-MMM-yyyy HH:mm", "HH:mm:ss"));
			tempList.add(users.getFirstName()+" "+users.getLastName());
			tempList.add(rpList.get(0).getRpNumber());
			tempList.add(String.valueOf(negotiatedAmt));
			tempList.add(String.valueOf(approvedAmt));
			tempList.add(documentMaster.getTitle());
			tempList.add(masterMaterialRepo.findByMaterialId(documentMaster.getMaterialId()).getMaterialName());
			tempList.add(attachmentName);
			i++;
			rowList.add(tempList);
		}
		
		
		
		
		filePath = filePath + "\\rpdetails.xls";
		ExcelUtils.wirteCommonExcelReport(columnList, rowList, filePath, "rpdetails");
	}

	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		new RPDetailsGeneration().generateRpDetailsAsExcel();

	}

}
