package com.gt.utils;

import java.util.HashMap;

public class RomanNumberUtility {
	
	 private HashMap<Integer,String> romanHashMap;
	
	 private static class RomanLoader {
	        private static final RomanNumberUtility INSTANCE = new RomanNumberUtility();
	    }
	    private RomanNumberUtility() {
	        if (RomanLoader.INSTANCE != null) {
	            throw new IllegalStateException("Already instantiated");
	        }
	        romanHashMap = new HashMap<>();
	        romanHashMap.put(1, "I");
	        romanHashMap.put(2, "II");
	        romanHashMap.put(3, "III");
	        romanHashMap.put(4, "IV");
	    }
	    public static RomanNumberUtility getInstance() {
	        return RomanLoader.INSTANCE;
	    }
	    
	    public HashMap<Integer,String> getMap(){
	    	return romanHashMap;
	    }

}
