package com.gt.utils;

/***
 * 
 * @author naval
 * List of statuses for DocumentMaster 
 *
 */
public interface DocStatus {
	/**
	 * Initiator has created a draft document. Not yet sent for endorsement.
	 */
	public static final String INITIATED = "Initiated";
	
	/**
	 * Document is in the approval process with main line endorsers
	 */
	public static final String IN_PROCESS = "In-process";
	
	/**
	 * Document is in the approval process with advisory line endorsers in parallel mode
	 */
	public static final String IN_PROCESS_PARALLEL = "In-process-parallel";
	
	/**
	 * Document is approved by the Approver
	 */
	public static final String APPROVED = "Approved";
	
	/**
	 * Document is rejected by the Approver
	 */
	public static final String REJECTED = "Rejected";
	
	/**
	 * Document is released/closed by the Initiator/Controller
	 */
	public static final String RELEASED = "Released";
	
	/**
	 * Document is discarded by the Initiator
	 */
	public static final String CANCELLED = "Cancelled";

}
