package com.gt.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.controllers.DocumentController;
import com.gt.bo.DocumentMetaData;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

public class HeaderFooterPageEvent extends PdfPageEventHelper {
	
	private final static Logger LOGGER = Logger
			.getLogger(HeaderFooterPageEvent.class.getName());
	
	/**
	 * 
	 */
	private List<DocumentMetaData> documentList;
	private String isReleaseRP;
//	private List<DocumentMetaDataCP> documentListCP;
	
	/**
	 * @param documentList
	 */
	public HeaderFooterPageEvent(List<DocumentMetaData> documentList,String isReleaseRP){
		this.documentList=documentList;
		this.isReleaseRP=isReleaseRP;
	}
	
	/*public HeaderFooterPageEvent(List<DocumentMetaDataCP> documentListCP){
		this.documentListCP=documentListCP;
	}*/
    /* (non-Javadoc)
     * @see com.itextpdf.text.pdf.PdfPageEventHelper#onStartPage(com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document)
     */
    public void onStartPage(PdfWriter writer,Document document) {
    	Rectangle rect = writer.getBoxSize("art");
    	
    	 HTMLWorker htmlWorker = new HTMLWorker(document);
		  try {
				  htmlWorker.parse(new StringReader(getVelocityContent(writer.getCurrentPageNumber())));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        ColumnText.showTextAligned(writer.getDirectContent(),Element.ALIGN_TOP, new Phrase(htmlWorker.toString()), rect.getTop(), rect.getTop(), 0);
        
        //ColumnText.showTextAligned(writer.getDirectContent(),Element.ALIGN_CENTER, new Phrase("Top Right"), rect.getRight(), rect.getTop(), 0);
    }
   /* public void onEndPage(PdfWriter writer,Document document) {
    	Rectangle rect = writer.getBoxSize("art");
        ColumnText.showTextAligned(writer.getDirectContent(),Element.ALIGN_CENTER, new Phrase("Bottom Left"), rect.getLeft(), rect.getBottom(), 0);
        ColumnText.showTextAligned(writer.getDirectContent(),Element.ALIGN_CENTER, new Phrase("Bottom Right"), rect.getRight(), rect.getBottom(), 0);
    }*/
    
    /**
     * @param documentList
     * @return
     */
    public String getVelocityContent(int pageNumber) {

		String message = null;
		String htmlContent =null;
		try {
			
			if(documentList!=null && documentList.size() >0)
			{
			htmlContent = "/com/gt/templates/viewRPPDFTemplateHeader.html";
			}/*else{
				htmlContent = "/com/gt/templates/viewCPPDFTemplateHeader.html";
			}*/
			htmlContent = getFileContent(htmlContent, true);
			 
			 String path=System.getProperty("user.dir");
			 String appMode=PropsUtil.getValue("app.mode"); 
			 if(appMode.equals("debug")){
			 if(path!=null && path.lastIndexOf("WF")!=-1){
				 path=path.substring(0,path.lastIndexOf("WF"));
			 }
			 }
		       
		        String checkBoxYes=path+"\\UI\\images\\checkbox_yes.png";
		        String checkBoxNo=path+"\\UI\\images\\checkbox_no.png";
		        String abgLogo=path+"\\UI\\images\\abg_logo.jpg";
		        String black=path+"\\UI\\images\\black.jpg";	
		        LOGGER.info("<<<< checkBoxYes <<<<<"+checkBoxYes);
		        Map<String,Object> inputParams=new HashMap<String,Object>();
		        inputParams.put("checkBoxYes",checkBoxYes);
		        inputParams.put("checkBoxNo",checkBoxNo);
		        inputParams.put("abgLogo",abgLogo);
		        inputParams.put("pageNumber",pageNumber);
		        inputParams.put("isReleaseRP",isReleaseRP);
		        inputParams.put("black",black);
		        String pdfContent ="";
		        if(documentList!=null && documentList.size() >0)
				{
			 pdfContent = VelocityStringUtil.getPDFHeaderContent("VIEWRP",
					htmlContent, inputParams,documentList.get(0), "dmd");
				}/*else{
					 pdfContent = VelocityStringUtil.getPDFHeaderContent("VIEWRP",
							htmlContent, inputParams,documentListCP.get(0), "dmd");
				}*/
			message = pdfContent;
	        
	       
	        
		} catch (Exception e) {
			e.printStackTrace();
		}

		return message;
	}
    
    /**
	 * @param filePath
	 * @param inClassPath
	 * @return
	 */
	public String getFileContent(String filePath, boolean inClassPath) {
		String fileContent = null;
		InputStream in = null;
		BufferedReader bReader = null;
		try {

			if (inClassPath) {
				// in = getFileFromClassPath(filePath);
				in = DocumentController.class.getResourceAsStream(filePath);
				bReader = new BufferedReader(new InputStreamReader(in));
			} else {
				bReader = new BufferedReader(new FileReader(filePath));
			}

			String str;
			StringBuilder sb = new StringBuilder();
			while ((str = bReader.readLine()) != null) {
				sb.append(str);
			}

			fileContent = sb.toString();
		} catch (Exception e) {
			System.out.println("Exception at getFileContent(1) >>>> "
					+ e.toString());
			// e.printStackTrace();
		} finally {
			try {
				if (bReader != null) {
					bReader.close();
				}
			} catch (Exception e) {
				//
			}

			try {
				if (in != null) {
					in.close();
				}
			} catch (Exception e) {
				//
			}
		}
		return fileContent;
	}
} 
