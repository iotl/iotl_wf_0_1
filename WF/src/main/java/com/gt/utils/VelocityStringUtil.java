
package com.gt.utils;


import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.ExtendedProperties;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.resource.Resource;
import org.apache.velocity.runtime.resource.loader.ResourceLoader;
import org.apache.velocity.tools.generic.MathTool;
import org.apache.velocity.tools.generic.NumberTool;

/**
 * 
 * 
 */
public final class VelocityStringUtil extends ResourceLoader {

	public static final Logger logger = Logger
			.getLogger(VelocityStringUtil.class.getName());
	
	public static final String EMPTY_TEMPLATE_NAME = "VelocityStringUtil EMPTY TEMPLATE";

	class TemplateDetails {
		byte[] bodyBytes;
		long lastModified;
	}

	private HashMap<String, TemplateDetails> templates;

	/**
	 * Initialize the template loader with a a resources class.
	 */
	@Override
	public void init(ExtendedProperties arg0) {
		this.templates = new HashMap<String, TemplateDetails>();

		// this is a hack so that later, the Velocity client code

		// to retrieve a handle to this loader in order to add

		// See main() test method below.
		setTemplate(EMPTY_TEMPLATE_NAME, "");
	}

	/**
	 * Use this method to add a template String to this loader. If a template is
	 * already known with the given name then it will be overwritten.
	 */
	public void setTemplate(String name, String body) {
		TemplateDetails td = new TemplateDetails();
		td.bodyBytes = body.getBytes();
		td.lastModified = System.currentTimeMillis();
		this.templates.put(name, td);
	}

	public void removeTemplate(String name) {
		this.templates.remove(name);
	}

	public boolean hasTemplate(String name) {
		return this.templates.containsKey(name);
	}

	/**
	 * Get the InputStream that the Runtime will parse to create a template.
	 */
	@Override
	public InputStream getResourceStream(String name)
			throws ResourceNotFoundException {

		if (this.templates.containsKey(name)) {
			TemplateDetails td = this.templates.get(name);
			return new ByteArrayInputStream(td.bodyBytes);
		}

		throw new ResourceNotFoundException("Template " + name + " not found.");

	}

	/**
	 * Given a template, check to see if the source of InputStream has been
	 * modified.
	 */
	@Override
	public boolean isSourceModified(Resource resource) {
		TemplateDetails td = this.templates.get(resource.getName());
		if (td == null) {
			return false;
		}
		return (td.lastModified > resource.getLastModified());
	}

	/**
	 * Get the last modified time of the InputStream source that was used to
	 * create the template. We need the template here because we have to extract
	 * the name of the template in order to locate the InputStream source.
	 */
	@Override
	public long getLastModified(Resource resource) {
		TemplateDetails td = this.templates.get(resource.getName());
		if (td == null) {
			return -1;
		}
		return td.lastModified;
	}

	static {
		java.util.Properties props = new java.util.Properties();

		props.put("resource.loader", "srs");

		props.put("srs.resource.loader.public.name", "VelocityStringUtil");
		props.put("srs.resource.loader.class",

		"com.gt.utils.VelocityStringUtil");
		try {
			Velocity.init(props);

		} catch (Exception e) {
			System.out.println("Error Initializing	TemplateEngine: "
					+ e.getMessage());

		}
	}

	/**
	 * @param templateName
	 * @param content
	 * @param valueParams
	 * @return
	 */
	public static String getFinalContent(String templateName, String content,
			Map<String, Object> valueParams) {

		String rtnContent = null;
		try {
			VelocityStringUtil srs = (VelocityStringUtil)

			Velocity.getTemplate(VelocityStringUtil.EMPTY_TEMPLATE_NAME)
					.getResourceLoader();

			srs.setTemplate(templateName, content);

		} catch (Exception e) {
			System.out.println("Exception at getFinalContent(0)" + e.toString());
			// e.printStackTrace();
		}

		try {

			Template t = Velocity.getTemplate(templateName);

			if (t == null) {
				throw new Exception("Template is empty");
			}
			VelocityContext ctx = new VelocityContext();

			if (valueParams != null && !valueParams.isEmpty()) {
				Set<String> keys = valueParams.keySet();
				Iterator<String> iter = keys.iterator();
				while (iter.hasNext()) {
					String key = iter.next();
					Object value = valueParams.get(key);
					ctx.put(key, value);
				}
			}

			Writer w = new StringWriter();

			try {
				ctx.put("number", new NumberTool());
				t.merge(ctx, w);
				rtnContent = w.toString();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println("Exception at getFinalContent(1) >>>> "
						+ e.toString());
			}
		} catch (Exception e) {
			System.out.println("Exception at getFinalContent(2) >>>> " + e.toString());
		}

		return rtnContent;

	}
	public static String getPDFContent(
			String templateName, String templateContent, String listName,
			List list,String totalListName,List listForTotal,Map<String, Object> valueParams,Object paramObject,String paramName,
			String commentsListName,List commentsList,String documentUnitsName,List documentUnitsList,String endorsersListName,List endorsersList,String advisoryEndorsersListName,List advisoryEndorsersList,
			String plantMainListName,List plantMainList) {

		String rtnContent = null;
		try {
			VelocityStringUtil srs = (VelocityStringUtil)

			Velocity.getTemplate(VelocityStringUtil.EMPTY_TEMPLATE_NAME)
					.getResourceLoader();

			srs.setTemplate(templateName, templateContent);

		} catch (Exception e) {
			System.out.println("Exception at getFinalContent(1)"
					+ e.toString());
//			 e.printStackTrace();
		}

		try {
			Template template = Velocity.getTemplate(templateName);

			if (template == null) {
				throw new Exception("Template is empty");
			}
			VelocityContext ctx = new VelocityContext();

			ctx.put(paramName, paramObject);
			ctx.put(listName, list);
			ctx.put(totalListName, listForTotal);
			ctx.put(commentsListName, commentsList);
			ctx.put(documentUnitsName, documentUnitsList);
			ctx.put(endorsersListName, endorsersList);
			ctx.put(advisoryEndorsersListName, advisoryEndorsersList);
			ctx.put(plantMainListName,plantMainList);
			//ctx.put("number", new NumberTool());

			if (valueParams != null && !valueParams.isEmpty()) {
				Set<String> keys = valueParams.keySet();
				for (String key : keys) {
					ctx.put(key, valueParams.get(key));
				}
			}

			Writer w = new StringWriter();
			try {
				  ctx.put("math", new MathTool());
				    ctx.put("aNumber", new Double(2.2));
				    ctx.put("number", new NumberTool());
				template.merge(ctx, w);
				rtnContent = w.toString();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.info("Exception at getPDFContent(3) >>>> "
						+ e.toString());
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("Exception at getPDFContent(4) >>>> "
					+ e.toString());
			// e.printStackTrace();
		}

		return rtnContent;

	}


	public static String getPDFHeaderContent(
			String templateName, String templateContent,Map<String, Object> valueParams,Object paramObject,String paramName) {

		String rtnContent = null;
		try {
			VelocityStringUtil srs = (VelocityStringUtil)

			Velocity.getTemplate(VelocityStringUtil.EMPTY_TEMPLATE_NAME)
					.getResourceLoader();

			srs.setTemplate(templateName, templateContent);

		} catch (Exception e) {
			System.out.println("Exception at getFinalContent(1)"
					+ e.toString());
//			 e.printStackTrace();
		}

		try {
			Template template = Velocity.getTemplate(templateName);

			if (template == null) {
				throw new Exception("Template is empty");
			}
			VelocityContext ctx = new VelocityContext();

			ctx.put(paramName, paramObject);
			//ctx.put("number", new NumberTool());

			if (valueParams != null && !valueParams.isEmpty()) {
				Set<String> keys = valueParams.keySet();
				for (String key : keys) {
					ctx.put(key, valueParams.get(key));
				}
			}

			Writer w = new StringWriter();
			try {
				ctx.put("number", new NumberTool());
				template.merge(ctx, w);
				rtnContent = w.toString();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.info("Exception at getPDFContent(3) >>>> "
						+ e.toString());
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("Exception at getPDFContent(4) >>>> "
					+ e.toString());
			// e.printStackTrace();
		}

		return rtnContent;

	}



	public static void main(String args[]) {

	}

}
