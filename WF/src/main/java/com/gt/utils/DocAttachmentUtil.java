/**
 * 
 */
package com.gt.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.controllers.Status;
import com.gt.bo.DocumentData;
import com.gt.bo.DocumentMetaData;
import com.gt.bo.DocumentUserActionBO;
import com.gt.entity.DocUserAction;
import com.gt.entity.RevenueProposal;
import com.gt.services.DocUserActionService;
import com.gt.services.DocumentService;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.codec.Base64;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.exceptions.RuntimeWorkerException;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.AbstractImageProvider;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;

/**
 * @author Vignesh 
 *
 */
@Component("docAttachmentUtil")
public class DocAttachmentUtil {
	
	/**
	 * 
	 */
	@Autowired
	private DocumentService documentService;
	
	@Autowired
	private DocUserActionService docUserActionService;
	
	
	

	/**
	 * @return
	 */
	public String getVelocityContent(String documentId, String userId,
			String isViewRp,String isReleaseRP,String includeComments,String rpNum,Integer actionId,List<DocumentMetaData> documentList,String isReferredInitiator) {

		String message = null;

		try {
			DocumentMetaData dmd=null;
			if(documentList!=null && documentList.size()>0){
				dmd =documentList.get(0);
			}
			String htmlContent = "/com/gt/templates/viewRPAsPDFTemplate.html";
			if(documentList!=null && documentList.size()>0){
				
				if(dmd!=null && dmd.getIsDept()){
					htmlContent = "/com/gt/templates/corporateITRPAsPDFTemplate.html";
				}
				//System.out.println("dmd.getCategoryId() "+dmd.getCategoryId());
				
				if(dmd!=null && dmd.getCategoryId()!=null && dmd.getCategoryId().intValue() > 0){
					List<DocUserAction> isReleasedRPList = docUserActionService
							.findByDocumentIdAndActionStatus(Integer.parseInt(documentId), "Released");
					if(isReleasedRPList!=null && isReleasedRPList.size() > 0 &&dmd.getCategoryName()!=null&&  dmd.getCategoryName().equalsIgnoreCase("e-Auction") && !dmd.getInPrincipalApprocal()){
						
						dmd.setIsFinalApproval(true);
					}else{
						dmd.setIsFinalApproval(false);
					}
					
					if(rpNum!=null && (rpNum.indexOf("TRPT")!=-1 || rpNum.indexOf("Transportation")!=-1)){
						htmlContent = "/com/gt/templates/transportationRPAsPDFTemplate_DOM.html";
					}else{
						htmlContent = "/com/gt/templates/mnmCoalAsPDFTemplate.html";
					}
				}
			}
			
			
			
			includeComments="true";
			htmlContent = WFUtil.getFileContent(htmlContent, true);

			List<DocumentData> revenueProposalList = documentService
					.getDocumentData(Integer.parseInt(documentId));


			List<DocumentData> totalValueList = documentService
					.getDocumentTotal(Integer.parseInt(documentId));
			List<DocumentUserActionBO> tempCommentsList = new ArrayList<DocumentUserActionBO>();
			List<DocumentUserActionBO> commentsList = docUserActionService
					.getDocumentUserAction(Integer.parseInt(documentId),
							Integer.parseInt(userId));

			List<DocumentData> documentUnits = documentService
					.getDocumentUnits(Integer.parseInt(documentId));
			
			List<RevenueProposal> plantMainList=documentService.getDraftXportationData(Integer.parseInt(documentId));

			List<DocumentUserActionBO> endorsersList = null;

			List<DocumentUserActionBO> advisoryEndorsersList = null;

		/*	if ("true".equalsIgnoreCase(isViewRp)) {
				endorsersList = docUserActionService.getDocumentUserActionInQueue(
						Integer.parseInt(documentId),Integer.parseInt(userId), false);
				advisoryEndorsersList = docUserActionService
						.getDocumentUserActionInQueue(
								Integer.parseInt(documentId),Integer.parseInt(userId), true);
			} else {*/
			boolean isReferInitiator =false;
			if("true".equalsIgnoreCase(isReferredInitiator)){
				isReferInitiator =true;
			}
						/*endorsersList = docUserActionService
						.getDocumentUserActionDocumentAllUsersInQueue(
								Integer.parseInt(documentId), false,isReferInitiator);*/
			
			if(documentList!=null && documentList.size()>0){
//				DocumentMetaData dmd=documentList.get(0);
				if(dmd!=null && dmd.getIsDept()){
					if(dmd!=null && dmd.getCategoryId()!=null && dmd.getCategoryId().intValue() > 0){
						endorsersList = docUserActionService.geUsersInQueue(Integer.parseInt(documentId),null,false,false ,true);
					}else{
					endorsersList = docUserActionService.getDocumentUserMasterInQueue(Integer.parseInt(documentId),true );
					}
				}else{
					endorsersList = docUserActionService.geUsersInQueue(Integer.parseInt(documentId),null,false,false ,true);
				}
			}else{
				endorsersList = docUserActionService.geUsersInQueue(Integer.parseInt(documentId),null,false,false ,true);
			}
			
			advisoryEndorsersList = docUserActionService.geUsersInQueue(Integer.parseInt(documentId),null,true,false ,true);
				/*advisoryEndorsersList = docUserActionService
						.getDocumentUserActionDocumentAllUsersInQueue(
								Integer.parseInt(documentId), true,false);*/
//			}
			DocumentUserActionBO tmpDocumentUserActionBO = null;
			for(DocumentUserActionBO documentUserActionBO :commentsList){
				if(documentUserActionBO.getReferredTo()==null){
					documentUserActionBO.setReferredTo("0");
				}
				tmpDocumentUserActionBO = new DocumentUserActionBO();
				BeanUtils.copyProperties(documentUserActionBO, tmpDocumentUserActionBO);
				String safeHTML =documentUserActionBO.getComment();
				if(safeHTML!=null && !safeHTML.isEmpty()){
			    //safeHTML=safeHTML.replaceFirst("<table", "<table width='100%' border='1'");
			    safeHTML=safeHTML.replaceAll("style=\"border-width: 1px; border-style: solid;\"", "");
			    safeHTML=safeHTML.replaceAll("<tr>", "<tr><td>&nbsp;</td>");
			    safeHTML=safeHTML.replaceAll("<li>", "<p>");
			    safeHTML=safeHTML.replaceAll("</li>", "</p>");
			    safeHTML=safeHTML.replaceAll("<ul>", "");
			    safeHTML=safeHTML.replaceAll("</ul>", "");
			    safeHTML=safeHTML.replaceAll("<ol>", "");
			    safeHTML=safeHTML.replaceAll("</ol>", "");
			    safeHTML=safeHTML.replaceAll("â‚¹", " Rs");
				tmpDocumentUserActionBO.setComment(safeHTML);
				}
				tempCommentsList.add(tmpDocumentUserActionBO);
			}
			 String path=System.getProperty("user.dir");
			 String appMode=PropsUtil.getValue("app.mode"); 
			 if(appMode.equals("debug")){
			 if(path!=null && path.lastIndexOf("WF")!=-1){
				 path=path.substring(0,path.lastIndexOf("WF"));
			 }
			 }
		       
		        String attachmentImg=path+"\\UI\\images\\mail_attachment.png";
		        
		        Map<String,Object> valueParams=new HashMap<String,Object>();
		        valueParams.put("attachmentImg", attachmentImg);
		        valueParams.put("isReleaseRP",isReleaseRP);
		        valueParams.put("includeComments",includeComments);
			    valueParams.put("actionId",actionId);
			    valueParams.put("finalApproval",dmd.getIsFinalApproval());
			
			int plantId=0;
			String releaseMailContent="";
			System.out.println("isReleaseRP >>>>>"+isReleaseRP);
			System.out.println("dmd.getIsFinalApproval() >>>>>"+dmd.getIsFinalApproval());
			if("true".equals(isReleaseRP)){
//				if(rpNum!=null && actionId!=null){
//					plantId=Integer.valueOf(rpNum.substring(rpNum.lastIndexOf("_")+1));
				String releaseHtmlContent = null;
				if(dmd!=null && dmd.getIsDept()){
					if(dmd!=null && dmd.getCategoryId()!=null && dmd.getCategoryId().intValue() >0){
						System.out.println(("dmd.getCategoryId() >>>>>"+dmd.getCategoryId()));
						System.out.println(("rpNum.indexOf >>>>>"+rpNum.indexOf("TRPT")));
						if(rpNum!=null && (rpNum.indexOf("TRPT")!=-1 || rpNum.indexOf("Transportation")!=-1)){
							releaseHtmlContent = "/com/gt/templates/releaseRPAsPDFXportationTemplate.html";
						}else{
						 releaseHtmlContent = "/com/gt/templates/releaseRPForDomesticAsPDFTemplate.html";
						}
					}else{
						releaseHtmlContent = "/com/gt/templates/releaseCorporateITRPAsPDFTemplate.html";
					}
					
				}else{
					 releaseHtmlContent = "/com/gt/templates/releaseRPAsPDFTemplate.html";
				}
				
				
					if(releaseHtmlContent!=null){
					releaseHtmlContent = WFUtil.getFileContent(releaseHtmlContent, true);
					valueParams.put("plantId",plantId);
					releaseMailContent= VelocityStringUtil.getPDFContent("VIEWRP",
							releaseHtmlContent, "revenuelist", revenueProposalList,
							"revenuetotallist", totalValueList, valueParams, null, null,
							"commentsList", tempCommentsList, "documentUnitsList",
							documentUnits, "endorsersList", endorsersList,
							"advisoryEndorsersList", advisoryEndorsersList,"plantMainList",plantMainList);
					}
//				}
				
			}
			
		//	System.out.println("<<<< releaseHtmlContent >>>>"+releaseMailContent);
			

			String mailContent = VelocityStringUtil.getPDFContent("VIEWRP",
					htmlContent, "revenuelist", revenueProposalList,
					"revenuetotallist", totalValueList, valueParams, null, null,
					"commentsList", tempCommentsList, "documentUnitsList",
					documentUnits, "endorsersList", endorsersList,
					"advisoryEndorsersList", advisoryEndorsersList,"plantMainList",plantMainList);

			message =releaseMailContent + mailContent;
			
			System.out.println("<<<< message >>>>"+message);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return message;
	}
	
	/**
	 * @param request
	 * @return
	 * @throws IOException
	 */
	public synchronized Status writePdf(HttpServletRequest request,
			String rpNum,String userId,String documentId,String includeComments,Integer actionId,boolean isMail,String isReferredInitiator) throws IOException {
		// TODO Auto-generated method stub
		String isViewRP=null;
		String isPrint = null;
		String isReleaseRP="false";
				Document document = new Document(PageSize.LETTER);
				OutputStream file = null;
				String htmlContent = null;
				if(request!=null){
				if(userId == null || userId.isEmpty()){
					userId = request.getParameter("userId");
				}
					isPrint = request.getParameter("isPrint");
				if(documentId == null || documentId.isEmpty()){
					documentId = request.getParameter("documentId");
				}
				if(isViewRP == null || isViewRP.isEmpty()){
					isViewRP = request.getParameter("isViewRP");
				}
				}
				try {
					
					boolean isRPReleased=documentService.isRPReleased(Integer.parseInt(documentId));
					
					if(isRPReleased){
						isReleaseRP="true";
					}else{
						isReleaseRP="false";
					}
					rpNum=rpNum.replaceAll("/", "_");
					String fileName = rpNum + ".pdf";
					String filePath = PropsUtil.getValue("temp.path");
					WFUtil.makeFilePath(filePath);
					filePath = filePath + "\\pdf" + userId;
					WFUtil.makeFilePath(filePath);
					if(isRPReleased && isMail){
					fileName = rpNum + ".pdf";
					filePath = PropsUtil.getValue("temp.path");
					}
					filePath = filePath + "\\" + fileName;
					File f = new File(filePath);
					if (f.exists()) {
						f.delete();
					}
					if (!f.exists()) {
						f.createNewFile();
					}

					System.out.println("<<<< isViewRP >>>>" + isViewRP);
					System.out.println("<<<< documentId >>>>" + documentId);
					System.out.println("<<<< filePath >>>>" + filePath);
					Integer usr = Integer.parseInt(userId);
					List<DocumentMetaData> documentList = documentService
							.getDocumentMetada(Integer.parseInt(documentId),usr);
					
					
					htmlContent = getVelocityContent(documentId, userId, isViewRP,isReleaseRP,includeComments,rpNum,actionId,documentList,isReferredInitiator);
					//System.out.println("<<<< htmlContent >>>>" + htmlContent);
					file = new FileOutputStream(filePath);

					PdfWriter writer = PdfWriter.getInstance(document, file);
					if(isPrint!=null){
					if ("true".equals(isPrint)) {
						PdfAction action = new PdfAction(PdfAction.PRINTDIALOG);
						writer.setOpenAction(action);
					}
					}
					Rectangle rect = new Rectangle(30, 30, 550, 800);
					writer.setBoxSize("art", rect);
					HeaderFooterPageEvent event = new HeaderFooterPageEvent(
							documentList,isReleaseRP);
					writer.setPageEvent(event);
					document.open();
					// XMLWorkerHelper worker = XMLWorkerHelper.getInstance();
					// HTMLWorker htmlWorker = new HTMLWorker(document);
					// System.out.println("htmlWorker.newPage()");
					// htmlWorker.parse(new StringReader(htmlContent));
					// worker.parseXHtml(writer, document, new
					// StringReader(htmlContent));
					
					//System.out.println("<<<< htmlContent >>>>" + htmlContent);
					
					 
//					 InputStream is = new ByteArrayInputStream(htmlContent.getBytes());
//					    XMLWorkerHelper.getInstance().parseXHtml(writer, document, is);
				//	XMLWorkerHelper.getInstance().p

			        // CSS
			        CSSResolver cssResolver =
			                XMLWorkerHelper.getInstance().getDefaultCssResolver(true);
			 
			        // HTML
			        HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
			        htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());
			        htmlContext.setImageProvider(new Base64ImageProvider());
			 
			        // Pipelines
			        PdfWriterPipeline pdf = new PdfWriterPipeline(document, writer);
			        HtmlPipeline html = new HtmlPipeline(htmlContext, pdf);
			        CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);
			        
			        if(htmlContent!=null && !htmlContent.isEmpty()){
			        		    	  htmlContent=htmlContent.replaceAll("<p>", "<p><br/>");
			        		      }
			      
			        // XML Worker
			        XMLWorker worker = new XMLWorker(css, true);
			        XMLParser p = new XMLParser(worker);
			        p.parse(new ByteArrayInputStream(htmlContent.getBytes()));
			        
//			        InputStream is = new ByteArrayInputStream(htmlContent.getBytes());
//				    XMLWorkerHelper.getInstance().parseXHtml(writer, document, is);
			 
					System.out.println("PDF Created!");
				} catch (DocumentException e) {

				} catch (IOException e) {

				}catch(RuntimeWorkerException e){
					
				}catch (Exception e) {
					e.printStackTrace();
				}
				document.close();
				file.flush();
				file.close();
				return null;
	}
	
	class Base64ImageProvider extends AbstractImageProvider {
		public Base64ImageProvider(){
			//System.out.println("<<<< Inside  Base64ImageProvider>>>>");
		}
			
	        @Override
	        public Image retrieve(String src) {
	        	//System.out.println("<<<< Inside  retrieve>>>>");
	            int pos = src.indexOf("base64,");
	          //  System.out.println("src >>>>"+src);
	            try {
	                if (src.startsWith("data") && pos > 0) {
	                    byte[] img = Base64.decode(src.substring(pos + 7));
	                    return Image.getInstance(img);
	                }
	                else {
	                    return Image.getInstance(src);
	                }
	            } catch (BadElementException ex) {
	            	ex.printStackTrace();
	                return null;
	            } catch (IOException ex) {
	            	ex.printStackTrace();
	                return null;
	            }
	        }
	 
	        @Override
	        public String getImageRootPath() {
	        	//System.out.println("<<<< Inside  getImageRootPath>>>>");
	            return null;
	        }
	    }

	
	public synchronized Status writePdfForRPGeneration(String filePath,
			String rpNum,String userId,String documentId,String includeComments,Integer actionId,boolean isMail,String isReferredInitiator) throws IOException {
		// TODO Auto-generated method stub
		String isViewRP=null;
		String isPrint = null;
		String isReleaseRP="false";
				Document document = new Document(PageSize.LETTER);
				OutputStream file = null;
				String htmlContent = null;
				try {
					
					boolean isRPReleased=documentService.isRPReleased(Integer.parseInt(documentId));
					
					if(isRPReleased){
						isReleaseRP="true";
					}else{
						isReleaseRP="false";
					}
					rpNum=rpNum.replaceAll("/", "_");
					String fileName = rpNum + ".pdf";
					WFUtil.makeFilePath(filePath);
					filePath = filePath + "\\" + fileName;
					File f = new File(filePath);
					if (f.exists()) {
						f.delete();
					}
					if (!f.exists()) {
						f.createNewFile();
					}

					System.out.println("<<<< isViewRP >>>>" + isViewRP);
					System.out.println("<<<< documentId >>>>" + documentId);
					System.out.println("<<<< filePath >>>>" + filePath);
					Integer usr = Integer.parseInt(userId);
					List<DocumentMetaData> documentList = documentService
							.getDocumentMetada(Integer.parseInt(documentId),usr);
					
					
					htmlContent = getVelocityContent(documentId, userId, isViewRP,isReleaseRP,includeComments,rpNum,actionId,documentList,isReferredInitiator);
					//System.out.println("<<<< htmlContent >>>>" + htmlContent);
					file = new FileOutputStream(filePath);

					PdfWriter writer = PdfWriter.getInstance(document, file);
					Rectangle rect = new Rectangle(30, 30, 550, 800);
					writer.setBoxSize("art", rect);
					HeaderFooterPageEvent event = new HeaderFooterPageEvent(
							documentList,isReleaseRP);
					writer.setPageEvent(event);
					document.open();
					// XMLWorkerHelper worker = XMLWorkerHelper.getInstance();
					// HTMLWorker htmlWorker = new HTMLWorker(document);
					// System.out.println("htmlWorker.newPage()");
					// htmlWorker.parse(new StringReader(htmlContent));
					// worker.parseXHtml(writer, document, new
					// StringReader(htmlContent));
					
					//System.out.println("<<<< htmlContent >>>>" + htmlContent);
					
					 
//					 InputStream is = new ByteArrayInputStream(htmlContent.getBytes());
//					    XMLWorkerHelper.getInstance().parseXHtml(writer, document, is);
				//	XMLWorkerHelper.getInstance().p

			        // CSS
			        CSSResolver cssResolver =
			                XMLWorkerHelper.getInstance().getDefaultCssResolver(true);
			 
			        // HTML
			        HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
			        htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());
			        htmlContext.setImageProvider(new Base64ImageProvider());
			 
			        // Pipelines
			        PdfWriterPipeline pdf = new PdfWriterPipeline(document, writer);
			        HtmlPipeline html = new HtmlPipeline(htmlContext, pdf);
			        CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);
			        
			        if(htmlContent!=null && !htmlContent.isEmpty()){
			        		    	  htmlContent=htmlContent.replaceAll("<p>", "<p><br/>");
			        		      }
			      
			        // XML Worker
			        XMLWorker worker = new XMLWorker(css, true);
			        XMLParser p = new XMLParser(worker);
			        p.parse(new ByteArrayInputStream(htmlContent.getBytes()));
			        
//			        InputStream is = new ByteArrayInputStream(htmlContent.getBytes());
//				    XMLWorkerHelper.getInstance().parseXHtml(writer, document, is);
			 
					System.out.println("PDF Created!");
				} catch (DocumentException e) {

				} catch (IOException e) {

				}catch(RuntimeWorkerException e){
					
				}catch (Exception e) {
					e.printStackTrace();
				}
				document.close();
				file.flush();
				file.close();
				return null;
	}


}
