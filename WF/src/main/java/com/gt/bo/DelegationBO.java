package com.gt.bo;



import java.util.Date;

import com.gt.entity.Delegation;
import com.gt.entity.Users;

public class DelegationBO {
	
	private UsersBO users;
	private Date fromDate;
	private Date toDate;
	private Integer userId;
	private String mailId;
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public UsersBO getUsers() {
		return users;
	}
	public void setUsers(UsersBO users) {
		this.users = users;
	}
	public String getMailId() {
		return mailId;
	}
	public void setMailId(String mailId) {
		this.mailId = mailId;
	}

}
