package com.gt.bo;

public class ReferBackBO {
	
	private Integer referedBy;
	private Integer documentId;
	private UsersBO user;
	
	
	public Integer getReferedBy() {
		return referedBy;
	}
	public void setReferedBy(Integer referedBy) {
		this.referedBy = referedBy;
	}
	public Integer getDocumentId() {
		return documentId;
	}
	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}
	public UsersBO getUser() {
		return user;
	}
	public void setUser(UsersBO user) {
		this.user = user;
	}

}
