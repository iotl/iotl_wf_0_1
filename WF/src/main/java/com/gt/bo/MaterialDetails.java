package com.gt.bo;

public class MaterialDetails {
	int matDet;
	String materialDetail;
	
	
	public int getMatDet() {
		return matDet;
	}
	public void setMatDet(int matDet) {
		this.matDet = matDet;
	}
	public String getMaterialDetail() {
		return materialDetail;
	}
	public void setMaterialDetail(String materialDetail) {
		this.materialDetail = materialDetail;
	}
	

}
