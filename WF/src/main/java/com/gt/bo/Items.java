package com.gt.bo;

public class Items {
	
	
	private Boolean isExpress;
	private Integer userId;
	private int srNo;
	private String rpNumber;
	private String personName;
	private String delegateName;
	private String recvdDate;
	private String pendingSince;
	private Integer documentId;
	private Boolean pending_Express;
	private Boolean isRelease;
	private Boolean isInitiatorDraft;
	private String initiatedBy;
	private Integer isDept;
	private String documentType;
	private Boolean isRefferedToInitiator;
	private Boolean isDelegated=false;
	private Boolean isTransportationDraft;
	private Boolean isFinalApproval;

	public Items(){
	}

	
	public Boolean getIsExpress() {
		return isExpress;
	}

	public void setIsExpress(Boolean isExpress) {
		this.isExpress = isExpress;
	}

	public Integer getUserId() {
		return userId;
	}


	public void setUserId(Integer userId) {
		this.userId = userId;
	}


	public int getSrNo() {
		return srNo;
	}

	public void setSrNo(int srNo) {
		this.srNo = srNo;
	}
	public String getRpNumber() {
		return rpNumber;
	}

	public void setRpNumber(String rpNumber) {
		this.rpNumber = rpNumber;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getDelegateName() {
		return delegateName;
	}


	public void setDelegateName(String delegateName) {
		this.delegateName = delegateName;
	}


	public String getRecvdDate() {
		return recvdDate;
	}

	public void setRecvdDate(String recvdDate) {
		this.recvdDate = recvdDate;
	}

	public String getPendingSince() {
		return pendingSince;
	}

	public void setPendingSince(String pendingSince) {
		this.pendingSince = pendingSince;
	}


	/**
	 * @return the documentId
	 */
	public Integer getDocumentId() {
		return documentId;
	}


	/**
	 * @param documentId the documentId to set
	 */
	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}


	public Boolean getPending_Express() {
		return pending_Express;
	}


	public void setPending_Express(Boolean pending_Express) {
		this.pending_Express = pending_Express;
	}


	public Boolean getIsRelease() {
		return isRelease;
	}


	public void setIsRelease(Boolean isRelease) {
		this.isRelease = isRelease;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Items))
			return false;	
		if (obj == this)
			return true;
		return this.rpNumber.equals(((Items) obj).rpNumber);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode(){
		return this.documentId;
	}


	public Boolean getIsInitiatorDraft() {
		return isInitiatorDraft;
	}


	public void setIsInitiatorDraft(Boolean isInitiatorDraft) {
		this.isInitiatorDraft = isInitiatorDraft;
	}


	public String getInitiatedBy() {
		return initiatedBy;
	}


	public void setInitiatedBy(String initiatedBy) {
		this.initiatedBy = initiatedBy;
	}


/*	public Boolean getIsDept() {
		return isDept;
	}


	public void setIsDept(Boolean isDept) {
		this.isDept = isDept;
	}
*/

	public String getDocumentType() {
		return documentType;
	}


	public Integer getIsDept() {
		return isDept;
	}


	public void setIsDept(Integer isDept) {
		this.isDept = isDept;
	}


	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}


	public Boolean getIsRefferedToInitiator() {
		return isRefferedToInitiator;
	}


	public void setIsRefferedToInitiator(Boolean isRefferedToInitiator) {
		this.isRefferedToInitiator = isRefferedToInitiator;
	}


	public Boolean getIsDelegated() {
		return isDelegated;
	}


	public void setIsDelegated(Boolean isDelegated) {
		this.isDelegated = isDelegated;
	}


	public Boolean getIsTransportationDraft() {
		return isTransportationDraft;
	}


	public void setIsTransportationDraft(Boolean isTransportationDraft) {
		this.isTransportationDraft = isTransportationDraft;
	}


	public Boolean getIsFinalApproval() {
		return isFinalApproval;
	}


	public void setIsFinalApproval(Boolean isFinalApproval) {
		this.isFinalApproval = isFinalApproval;
	}


}
