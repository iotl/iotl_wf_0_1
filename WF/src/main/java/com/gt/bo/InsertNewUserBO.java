package com.gt.bo;

import com.gt.entity.Users;


/**
 * added for user management on 7-12-15
 * @author Kant
 *
 */
public class InsertNewUserBO {

	String firstName;
	String lastName;
	String emailId;
	
	String designation;
	String dashboardType;
	UsersBO user;
	Users users;
	boolean isSupportingEndorser;
	Integer roleId;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getDashboardType() {
		return dashboardType;
	}
	public void setDashboardType(String dashboardType) {
		this.dashboardType = dashboardType;
	}
	public boolean getIsSupportingEndorser() {
		return isSupportingEndorser;
	}
	public void setSupportingEndorser(boolean isSupportingEndorser) {
		this.isSupportingEndorser = isSupportingEndorser;
	}
	public UsersBO getUser() {
		return user;
	}
	public void setUser(UsersBO user) {
		this.user = user;
	}
	public Users getUsers() {
		return users;
	}
	public void setUsers(Users users) {
		this.users = users;
	}
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	
	
	
}
