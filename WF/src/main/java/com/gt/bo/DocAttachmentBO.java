/**
 * 
 */
package com.gt.bo;

import java.io.Serializable;

/**
 * @author Abilash
 *
 */
public class DocAttachmentBO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3503967414385485107L;

	/**
	 * 
	 */
	private int attachmentId;
	
	/**
	 * 
	 */
	private String fileName;

	/**
	 * @return the attachmentId
	 */
	public int getAttachmentId() {
		return attachmentId;
	}

	/**
	 * @param attachmentId the attachmentId to set
	 */
	public void setAttachmentId(int attachmentId) {
		this.attachmentId = attachmentId;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	
	

}
