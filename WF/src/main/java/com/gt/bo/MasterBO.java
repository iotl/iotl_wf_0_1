package com.gt.bo;

import com.gt.entity.Businesses;
import com.gt.entity.Cluster;
import com.gt.entity.Companies;
import com.gt.entity.Sbus;

public class MasterBO {
	
	Companies company;
	Businesses business;
	Cluster cluster;
	Sbus sbu;
	
	
	
	public Companies getCompany() {
		return company;
	}
	public void setCompany(Companies company) {
		this.company = company;
	}
	public Businesses getBusiness() {
		return business;
	}
	public void setBusiness(Businesses business) {
		this.business = business;
	}
	public Cluster getCluster() {
		return cluster;
	}
	public void setCluster(Cluster cluster) {
		this.cluster = cluster;
	}
	public Sbus getSbu() {
		return sbu;
	}
	public void setSbu(Sbus sbu) {
		this.sbu = sbu;
	}
	

	
	
	

}
