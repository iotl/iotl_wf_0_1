package com.gt.bo;

import java.util.ArrayList;
import java.util.List;

import com.gt.entity.Businesses;
import com.gt.entity.Category;
import com.gt.entity.Companies;
import com.gt.entity.Material;
import com.gt.entity.Plant;
import com.gt.entity.RevenueProposal;
import com.gt.entity.RpTransportation;
import com.gt.entity.Sbus;

public class DocumentMetaDataCreate {
	private Integer createdBy;
	private String title;
	private Companies company;
	private Businesses business;
	private Sbus sbu;
	private List<Plant> plantList;
	private Material material;
	private Boolean isExp;
	private Boolean isRelPar;
//	private Boolean isMultiplePlant;
	private ArrayList<RevenueProposal> rpData;
	private ArrayList<RevenueProposal> rpUnitData;
	private ArrayList<DocumentData> rpTotalData;
	private ArrayList<DocumentData> rpList;
	private Integer documentId;
	private Boolean createRPNo;
	private String rpNumber;
	private Integer selectedImmediatedEndorser;
	private String plantName;
	private Integer plantId;
	private String otherMaterialName;
	private Boolean isReferredToInitiator;
	private String vendorName;
	private Integer companyId;
	private Integer businessId;
	private Integer sbuId;
	private Integer materialId;
	private Integer userId;
	private String isBudgeted;
	private String isRelParty;
	private String fromDate;
	private String toDate;
	private Double upperLimit;
	private Double lowerLimit;
	private String currentDateTime;
	private MaterialDetails materialDetails;
	private Boolean isPlantReport;
	private Boolean inPrincipleApproval;
	private Category category;
	private String sourceName;
	
	ArrayList<RpTransportation> transportList;
	
	private String justification;
	
	private String xportation;
	
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	
	public Companies getCompany() {
		return company;
	}
	public void setCompany(Companies company) {
		this.company = company;
	}
	
	public Businesses getBusiness() {
		return business;
	}
	public void setBusiness(Businesses business) {
		this.business = business;
	}
	
	public Sbus getSbu() {
		return sbu;
	}
	public void setSbu(Sbus sbu) {
		this.sbu = sbu;
	}
	
	public List<Plant> getPlantList() {
		return plantList;
	}
	public void setPlantList(List<Plant> plantList) {
		this.plantList = plantList;
	}
	
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	
		
	public ArrayList<RevenueProposal> getRpData() {
		return rpData;
	}
	public void setRpData(ArrayList<RevenueProposal> rpData) {
		this.rpData = rpData;
	}
	

	public Boolean getIsExp() {
		return isExp;
	}
	public void setIsExp(Boolean isExp) {
		this.isExp = isExp;
	}
	public Boolean getIsRelPar() {
		return isRelPar;
	}
	public void setIsRelPar(Boolean isRelPar) {
		this.isRelPar = isRelPar;
	}
	
	
/*	public Boolean getIsMultiplePlant() {
		return isMultiplePlant;
	}
	public void setIsMultiplePlant(Boolean isMultiplePlant) {
		this.isMultiplePlant = isMultiplePlant;
	}*/
	/**
	 * @return the rpUnitData
	 */
	public ArrayList<RevenueProposal> getRpUnitData() {
		return rpUnitData;
	}
	/**
	 * @param rpUnitData the rpUnitData to set
	 */
	public void setRpUnitData(ArrayList<RevenueProposal> rpUnitData) {
		this.rpUnitData = rpUnitData;
	}
	/**
	 * @return the documentId
	 */
	public Integer getDocumentId() {
		return documentId;
	}
	/**
	 * @param documentId the documentId to set
	 */
	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}
	/**
	 * @return the createRPNo
	 */
	public Boolean getCreateRPNo() {
		return createRPNo;
	}
	/**
	 * @param createRPNo the createRPNo to set
	 */
	public void setCreateRPNo(Boolean createRPNo) {
		this.createRPNo = createRPNo;
	}
	/**
	 * @return the rpNumber
	 */
	public String getRpNumber() {
		return rpNumber;
	}
	/**
	 * @param rpNumber the rpNumber to set
	 */
	public void setRpNumber(String rpNumber) {
		this.rpNumber = rpNumber;
	}
	public ArrayList<DocumentData> getRpList() {
		return rpList;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public void setRpList(ArrayList<DocumentData> rpList) {
		this.rpList = rpList;
	}
	
	public Integer getSelectedImmediatedEndorser() {
		return selectedImmediatedEndorser;
	}
	public void setSelectedImmediatedEndorser(Integer selectedImmediatedEndorser) {
		this.selectedImmediatedEndorser = selectedImmediatedEndorser;
	}
	public String getPlantName() {
		return plantName;
	}
	public void setPlantName(String plantName) {
		this.plantName = plantName;
	}
	public Integer getPlantId() {
		return plantId;
	}
	public void setPlantId(Integer plantId) {
		this.plantId = plantId;
	}
	public String getOtherMaterialName() {
		return otherMaterialName;
	}
	public void setOtherMaterialName(String otherMaterialName) {
		this.otherMaterialName = otherMaterialName;
	}
	public Boolean getIsReferredToInitiator() {
		return isReferredToInitiator;
	}
	public void setIsReferredToInitiator(Boolean isReferredToInitiator) {
		this.isReferredToInitiator = isReferredToInitiator;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public Integer getBusinessId() {
		return businessId;
	}
	public void setBusinessId(Integer businessId) {
		this.businessId = businessId;
	}
	public Integer getSbuId() {
		return sbuId;
	}
	public void setSbuId(Integer sbuId) {
		this.sbuId = sbuId;
	}
	public Integer getMaterialId() {
		return materialId;
	}
	public void setMaterialId(Integer materialId) {
		this.materialId = materialId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getIsBudgeted() {
		return isBudgeted;
	}
	public void setIsBudgeted(String isBudgeted) {
		this.isBudgeted = isBudgeted;
	}
	public String getIsRelParty() {
		return isRelParty;
	}
	public void setIsRelParty(String isRelParty) {
		this.isRelParty = isRelParty;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public Double getUpperLimit() {
		return upperLimit;
	}
	public void setUpperLimit(Double upperLimit) {
		this.upperLimit = upperLimit;
	}
	public Double getLowerLimit() {
		return lowerLimit;
	}
	public void setLowerLimit(Double lowerLimit) {
		this.lowerLimit = lowerLimit;
	}
	public String getCurrentDateTime() {
		return currentDateTime;
	}
	public void setCurrentDateTime(String currentDateTime) {
		this.currentDateTime = currentDateTime;
	}
	/*public String getMaterialDetails() {
		return materialDetails;
	}
	public void setMaterialDetails(String materialDetails) {
		this.materialDetails = materialDetails;
	}*/
	public MaterialDetails getMaterialDetails() {
		return materialDetails;
	}
	public void setMaterialDetails(MaterialDetails materialDetails) {
		this.materialDetails = materialDetails;
	}
	public Boolean getIsPlantReport() {
		return isPlantReport;
	}
	public void setIsPlantReport(Boolean isPlantReport) {
		this.isPlantReport = isPlantReport;
	}
	public Boolean getInPrincipleApproval() {
		return inPrincipleApproval;
	}
	public void setInPrincipleApproval(Boolean inPrincipleApproval) {
		this.inPrincipleApproval = inPrincipleApproval;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public String getSourceName() {
		return sourceName;
	}
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}
	public ArrayList<RpTransportation> getTransportList() {
		return transportList;
	}
	public void setTransportList(ArrayList<RpTransportation> transportList) {
		this.transportList = transportList;
	}
	public String getJustification() {
		return justification;
	}
	public void setJustification(String justification) {
		this.justification = justification;
	}
	public String getXportation() {
		return xportation;
	}
	public void setXportation(String xportation) {
		this.xportation = xportation;
	}
/*	public RevenueProposal getRpTotalData() {
		return rpTotalData;
	}
	public void setRpTotalData(RevenueProposal rpTotalData) {
		this.rpTotalData = rpTotalData;
	}*/
	public ArrayList<DocumentData> getRpTotalData() {
		return rpTotalData;
	}
	public void setRpTotalData(ArrayList<DocumentData> rpTotalData) {
		this.rpTotalData = rpTotalData;
	}


	

	
	
	
}
