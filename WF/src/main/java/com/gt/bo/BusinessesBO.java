package com.gt.bo;

import java.util.ArrayList;
import java.util.List;

import com.gt.entity.Businesses;

public class BusinessesBO {
    private String businessId;
	private String businessName ;
	private String abbreviation;
	private Integer companyId;
	List<Businesses> previousBusinessList= new ArrayList<Businesses>();
	List<Businesses> newBusinessList= new ArrayList<Businesses>();
	
	
	public String getBusinessId() {
		return businessId;
	}
	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	
	
	public String getAbbreviation() {
		return abbreviation;
	}
	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public List<Businesses> getPreviousBusinessList() {
		return previousBusinessList;
	}
	public void setPreviousBusinessList(List<Businesses> previousBusinessList) {
		this.previousBusinessList = previousBusinessList;
	}
	public List<Businesses> getNewBusinessList() {
		return newBusinessList;
	}
	public void setNewBusinessList(List<Businesses> newBusinessList) {
		this.newBusinessList = newBusinessList;
	}


	

	
	
	

}
