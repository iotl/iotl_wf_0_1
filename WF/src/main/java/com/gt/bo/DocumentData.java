package com.gt.bo;

public class DocumentData {
	private String plantName;
	private Integer inBudget;
	private String annualQuantity;
	private String orderQuantity;
	private String pNBRate;
	private String poleStarRate;
	private String negotiatedRate;
	private String negotiatedAmt;
	private String savingsBudgeted;
	private String savingsPolestar;
	private String savingsPolestarInLakhs;
	private String savingsBudgetedInLakhs;
	private String negotiatedAmtInLakhs;
	private Double approvedQuantity;
	private Double approvedRate;
	private Double amountApproved;
	private String amountApprovedInLakhs;
	private int plantId;
	private int documentId;
	private Float amountPretax;
	private Float taxPercent;
	private String negotiatedRateTotal;
	private String amountPretaxStr;
	private String taxPercentStr;
//	private String expectedSavingLossCmpTo;
	private String orderPeriod;
	private String annualPeriod;
	private Double approvedPeriod;
	private Double taxAmount;
	private String taxAmountStr;
	private Double approvedTaxAmount;
	private String negotiatedRate_mkcal;
	private String budgetedRate_mkcal;
	private String proposedPremium;
	
	private Double approvedNegotiatedRate_mkcal;
	private Double approvedPremium;
	
	private Double finalQuantity;
	private Double finalRate;
	private Double finalApprovedAmount;
	private Double finalNegotiatedRate_mkcal;
	private Double finalPremium;
	
	public DocumentData(){}
	
	public DocumentData(String docNumber) {
		// TODO Auto-generated constructor stub
	}
	
	public String getPlantName() {
		return plantName;
	}



	public void setPlantName(String plantName) {
		this.plantName = plantName;
	}

	

	public String getAnnualQuantity() {
		return annualQuantity;
	}

	public void setAnnualQuantity(String annualQuantity) {
		this.annualQuantity = annualQuantity;
	}


	/**
	 * @return the inBudget
	 */
	public Integer getInBudget() {
		return inBudget;
	}

	/**
	 * @param inBudget the inBudget to set
	 */
	public void setInBudget(Integer inBudget) {
		this.inBudget = inBudget;
	}

	public String getOrderQuantity() {
		return orderQuantity;
	}
	public void setOrderQuantity(String orderQuantity) {
		this.orderQuantity = orderQuantity;
	}
	
	public String getpNBRate() {
		return pNBRate;
	}
	public void setpNBRate(String pNBRate) {
		this.pNBRate = pNBRate;
	}
	
	public String getPoleStarRate() {
		return poleStarRate;
	}
	public void setPoleStarRate(String poleStarRate) {
		this.poleStarRate = poleStarRate;
	}
	
	public String getNegotiatedRate() {
		return negotiatedRate;
	}
	public void setNegotiatedRate(String negogiatedRate) {
		this.negotiatedRate = negogiatedRate;
	}
	
	public String getNegotiatedAmt() {
		return negotiatedAmt;
	}
	public void setNegotiatedAmt(String negogiatedAmt) {
		this.negotiatedAmt = negogiatedAmt;
	}
	
	/*public String getExpectedSavingLossCmpTo() {
		return expectedSavingLossCmpTo;
	}
	public void setExpectedSavingLossCmpTo(String expectedSavingLossCmpTo) {
		this.expectedSavingLossCmpTo = expectedSavingLossCmpTo;
	}*/

	public String getSavingsBudgeted() {
		return savingsBudgeted;
	}

	public void setSavingsBudgeted(String savingsBudgeted) {
		this.savingsBudgeted = savingsBudgeted;
	}

	public String getSavingsPolestar() {
		return savingsPolestar;
	}

	public void setSavingsPolestar(String savingsPolestar) {
		this.savingsPolestar = savingsPolestar;
	}

	public String getSavingsPolestarInLakhs() {
		return savingsPolestarInLakhs;
	}

	public void setSavingsPolestarInLakhs(String savingsPolestarInLakhs) {
		this.savingsPolestarInLakhs = savingsPolestarInLakhs;
	}

	public String getSavingsBudgetedInLakhs() {
		return savingsBudgetedInLakhs;
	}

	public void setSavingsBudgetedInLakhs(String savingsBudgetedInLakhs) {
		this.savingsBudgetedInLakhs = savingsBudgetedInLakhs;
	}

	public String getNegotiatedAmtInLakhs() {
		return negotiatedAmtInLakhs;
	}

	public void setNegotiatedAmtInLakhs(String negotiatedAmtInLakhs) {
		this.negotiatedAmtInLakhs = negotiatedAmtInLakhs;
	}

	public Double getApprovedQuantity() {
		return approvedQuantity;
	}

	public void setApprovedQuantity(Double approvedQuantity) {
		this.approvedQuantity = approvedQuantity;
	}

	public Double getApprovedRate() {
		return approvedRate;
	}

	public void setApprovedRate(Double approvedRate) {
		this.approvedRate = approvedRate;
	}

	public Double getAmountApproved() {
		return amountApproved;
	}

	public void setAmountApproved(Double amountApproved) {
		this.amountApproved = amountApproved;
	}

	public int getPlantId() {
		return plantId;
	}

	public void setPlantId(int plantId) {
		this.plantId = plantId;
	}

	public int getDocumentId() {
		return documentId;
	}

	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}

	public String getAmountApprovedInLakhs() {
		return amountApprovedInLakhs;
	}

	public void setAmountApprovedInLakhs(String amountApprovedInLakhs) {
		this.amountApprovedInLakhs = amountApprovedInLakhs;
	}

	public Float getAmountPretax() {
		return amountPretax;
	}

	public void setAmountPretax(Float amountPretax) {
		this.amountPretax = amountPretax;
	}

	public Float getTaxPercent() {
		return taxPercent;
	}

	public void setTaxPercent(Float taxPercent) {
		this.taxPercent = taxPercent;
	}


	public String getNegotiatedRateTotal() {
		return negotiatedRateTotal;
	}

	public void setNegotiatedRateTotal(String negotiatedRateTotal) {
		this.negotiatedRateTotal = negotiatedRateTotal;
	}

	public String getAmountPretaxStr() {
		return amountPretaxStr;
	}

	public void setAmountPretaxStr(String amountPretaxStr) {
		this.amountPretaxStr = amountPretaxStr;
	}

	public String getTaxPercentStr() {
		return taxPercentStr;
	}

	public void setTaxPercentStr(String taxPercentStr) {
		this.taxPercentStr = taxPercentStr;
	}

	public String getOrderPeriod() {
		return orderPeriod;
	}

	public void setOrderPeriod(String orderPeriod) {
		this.orderPeriod = orderPeriod;
	}

	public String getAnnualPeriod() {
		return annualPeriod;
	}

	public void setAnnualPeriod(String annualPeriod) {
		this.annualPeriod = annualPeriod;
	}

	public Double getApprovedPeriod() {
		return approvedPeriod;
	}

	public void setApprovedPeriod(Double approvedPeriod) {
		this.approvedPeriod = approvedPeriod;
	}

	

	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public String getTaxAmountStr() {
		return taxAmountStr;
	}

	public void setTaxAmountStr(String taxAmountStr) {
		this.taxAmountStr = taxAmountStr;
	}

	public Double getApprovedTaxAmount() {
		return approvedTaxAmount;
	}

	public void setApprovedTaxAmount(Double approvedTaxAmount) {
		this.approvedTaxAmount = approvedTaxAmount;
	}

	public String getNegotiatedRate_mkcal() {
		return negotiatedRate_mkcal;
	}

	public void setNegotiatedRate_mkcal(String negotiatedRate_mkcal) {
		this.negotiatedRate_mkcal = negotiatedRate_mkcal;
	}

	public String getBudgetedRate_mkcal() {
		return budgetedRate_mkcal;
	}

	public void setBudgetedRate_mkcal(String budgetedRate_mkcal) {
		this.budgetedRate_mkcal = budgetedRate_mkcal;
	}

	public String getProposedPremium() {
		return proposedPremium;
	}

	public void setProposedPremium(String proposedPremium) {
		this.proposedPremium = proposedPremium;
	}

	public Double getApprovedNegotiatedRate_mkcal() {
		return approvedNegotiatedRate_mkcal;
	}

	public void setApprovedNegotiatedRate_mkcal(Double approvedNegotiatedRate_mkcal) {
		this.approvedNegotiatedRate_mkcal = approvedNegotiatedRate_mkcal;
	}

	public Double getApprovedPremium() {
		return approvedPremium;
	}

	public void setApprovedPremium(Double approvedPremium) {
		this.approvedPremium = approvedPremium;
	}

	public Double getFinalQuantity() {
		return finalQuantity;
	}

	public void setFinalQuantity(Double finalQuantity) {
		this.finalQuantity = finalQuantity;
	}

	public Double getFinalRate() {
		return finalRate;
	}

	public void setFinalRate(Double finalRate) {
		this.finalRate = finalRate;
	}

	public Double getFinalApprovedAmount() {
		return finalApprovedAmount;
	}

	public void setFinalApprovedAmount(Double finalApprovedAmount) {
		this.finalApprovedAmount = finalApprovedAmount;
	}

	public Double getFinalNegotiatedRate_mkcal() {
		return finalNegotiatedRate_mkcal;
	}

	public void setFinalNegotiatedRate_mkcal(Double finalNegotiatedRate_mkcal) {
		this.finalNegotiatedRate_mkcal = finalNegotiatedRate_mkcal;
	}

	public Double getFinalPremium() {
		return finalPremium;
	}

	public void setFinalPremium(Double finalPremium) {
		this.finalPremium = finalPremium;
	}

	


}
