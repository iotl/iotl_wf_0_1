package com.gt.bo;

import java.util.ArrayList;

public class AdvisorCreate {
	
	ArrayList<Integer> userId;
	ArrayList<String> role;
	Integer docId;
	String currentRole;
	Boolean serialSelection;
	Integer currentUserId;

	public AdvisorCreate() {
		// TODO Auto-generated constructor stub
	}


	public Integer getDocId() {
		return docId;
	}


	public void setDocId(Integer docId) {
		this.docId = docId;
	}


	public ArrayList<Integer> getUserId() {
		return userId;
	}


	public void setUserId(ArrayList<Integer> userId) {
		this.userId = userId;
	}


	public ArrayList<String> getRole() {
		return role;
	}

	public void setRole(ArrayList<String> role) {
		this.role = role;
	}


	public String getCurrentRole() {
		return currentRole;
	}


	public void setCurrentRole(String currentRole) {
		this.currentRole = currentRole;
	}


	public Boolean getSerialSelection() {
		return serialSelection;
	}


	public void setSerialSelection(Boolean serialSelection) {
		this.serialSelection = serialSelection;
	}


	public Integer getCurrentUserId() {
		return currentUserId;
	}


	public void setCurrentUserId(Integer currentUserId) {
		this.currentUserId = currentUserId;
	}




}
