package com.gt.bo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class DocumentUserActionBO  implements Serializable{
	/**
	 * 
	 */
	
	private static final long serialVersionUID = -7902650839860570878L;
	Integer docSequence;
	Integer userId;
	Integer actionTakenBy;
	String personName;
	String actionTakenByName;
	String docRole;
	Boolean action;
	String comment;
	String attachment;
	Integer attachmentId;
	String dateStr;
	String rpStatus;
//	String userDocRole;
	String docStatus;
	String isParallel;
	List<DocAttachmentBO> docAttachmentBOList;
	Date date;
	Integer actionId;
	Boolean isRefferedToInitiator;
	String referredTo;
	Boolean enableLink;
	
	public Integer getDocSequence() {
		return docSequence;
	}
	public void setDocSequence(Integer docSequence) {
		this.docSequence = docSequence;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getDocRole() {
		return docRole;
	}
	public void setDocRole(String docRole) {
		this.docRole = docRole;
	}
	public Boolean getAction() {
		return action;
	}
	public void setAction(Boolean action) {
		this.action = action;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getAttachment() {
		return attachment;
	}
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	/**
	 * @return the attachmentId
	 */
	public Integer getAttachmentId() {
		return attachmentId;
	}
	/**
	 * @param attachmentId the attachmentId to set
	 */
	public void setAttachmentId(Integer attachmentId) {
		this.attachmentId = attachmentId;
	}
	/**
	 * @return the docAttachmentBOList
	 */
	public List<DocAttachmentBO> getDocAttachmentBOList() {
		return docAttachmentBOList;
	}
	/**
	 * @param docAttachmentBOList the docAttachmentBOList to set
	 */
	public void setDocAttachmentBOList(List<DocAttachmentBO> docAttachmentBOList) {
		this.docAttachmentBOList = docAttachmentBOList;
	}
	/**
	 * @return the dateStr
	 */
	public String getDateStr() {
		return dateStr;
	}
	/**
	 * @param dateStr the dateStr to set
	 */
	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
	}
	/**
	 * @return the rpStatus
	 */
	public String getRpStatus() {
		return rpStatus;
	}
	/**
	 * @param rpStatus the rpStatus to set
	 */
	public void setRpStatus(String rpStatus) {
		this.rpStatus = rpStatus;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
/*	public String getUserDocRole() {
		return userDocRole;
	}
	public void setUserDocRole(String userDocRole) {
		this.userDocRole = userDocRole;
	}*/
	public String getDocStatus() {
		return docStatus;
	}
	public void setDocStatus(String docStatus) {
		this.docStatus = docStatus;
	}
	public String getIsParallel() {
		return isParallel;
	}
	public void setIsParallel(String isParallel) {
		this.isParallel = isParallel;
	}
	public Integer getActionTakenBy() {
		return actionTakenBy;
	}
	public void setActionTakenBy(Integer actionTakenBy) {
		this.actionTakenBy = actionTakenBy;
	}
	public String getActionTakenByName() {
		return actionTakenByName;
	}
	public void setActionTakenByName(String actionTakenByName) {
		this.actionTakenByName = actionTakenByName;
	}
	public Integer getActionId() {
		return actionId;
	}
	public void setActionId(Integer actionId) {
		this.actionId = actionId;
	}
	public Boolean getIsRefferedToInitiator() {
		return isRefferedToInitiator;
	}
	public void setIsRefferedToInitiator(Boolean isRefferedToInitiator) {
		this.isRefferedToInitiator = isRefferedToInitiator;
	}
	public String getReferredTo() {
		return referredTo;
	}
	public void setReferredTo(String referredTo) {
		this.referredTo = referredTo;
	}
	public Boolean getEnableLink() {
		return enableLink;
	}
	public void setEnableLink(Boolean enableLink) {
		this.enableLink = enableLink;
	}
	
	

}
