package com.gt.bo;

public class DocumentUserAction {
	String docSequence;
	String personName;
	String docRole;
	Boolean action;
	String comment;
	String attachment;
	String date;
	
	
	public String getDocSequence() {
		return docSequence;
	}
	public void setDocSequence(String docSequence) {
		this.docSequence = docSequence;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getDocRole() {
		return docRole;
	}
	public void setDocRole(String docRole) {
		this.docRole = docRole;
	}
	public Boolean getAction() {
		return action;
	}
	public void setAction(Boolean action) {
		this.action = action;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getAttachment() {
		return attachment;
	}
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	

}
