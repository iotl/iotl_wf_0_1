package com.gt.bo;

import java.util.ArrayList;
import java.util.List;

import com.gt.entity.Material;

public class MaterialBO {
	String materialId;
	String name;
	String abbreviation;
	String business;
	List<Material> materialList= new ArrayList<Material>();
	List<Material> materialListToDelete= new ArrayList<Material>();
	public String getMaterialId() {
		return materialId;
	}
	public void setMaterialId(String materialId) {
		this.materialId = materialId;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getAbbreviation() {
		return abbreviation;
	}
	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}
	
	public String getBusiness() {
		return business;
	}
	public void setBusiness(String business) {
		this.business = business;
	}
	public List<Material> getMaterialList() {
		return materialList;
	}
	public void setMaterialList(List<Material> materialList) {
		this.materialList = materialList;
	}
	public List<Material> getMaterialListToDelete() {
		return materialListToDelete;
	}
	public void setMaterialListToDelete(List<Material> materialListToDelete) {
		this.materialListToDelete = materialListToDelete;
	}

}
