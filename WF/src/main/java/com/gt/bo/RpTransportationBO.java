package com.gt.bo;
import java.util.ArrayList;
import java.util.List;

public class RpTransportationBO {
	
	
	/**
	 * 
	 */
	private Integer documentId;
	
	/**
	 * 
	 */
	private Integer plantId;
	
	/**
	 * 
	 */
	private Integer transporterSeqId;
	
	/**
	 * 
	 */
	private Integer transporterId;
	
	/**
	 * 
	 */
	private String transporterName;
	
	/**
	 * 
	 */
	private String modesOfTransport;
	
	/**
	 * 
	 */
	private Double proposedQuantity;
	
	/**
	 * 
	 */
	private String proposedQuantityUnit;
	
	/**
	 * 
	 */
	private Double proposedRate;
	
	/**
	 * 
	 */
	private String proposedRateUnit;
	
	/**
	 * 
	 */
	private Double proposedValue;
	
	/**
	 * 
	 */
	private String proposedValueUnit;
	
	private ArrayList<RpTransportationBO> transporterList;
	
	private Double averagePnb;
	
	private Double averageFreightRate;
	
	private Double averageProposedRate;
	
	private List<String> docAttachmentList= new ArrayList<String>();
	private String comments;
	private int actionId;
	private int commentId;
	
	private String plantName;
	
	private String remark;

	public  RpTransportationBO(){
		
	}






	/**
	 * @return the transporterName
	 */
	public String getTransporterName() {
		return transporterName;
	}


	public Integer getDocumentId() {
		return documentId;
	}



	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}


	public Integer getPlantId() {
		return plantId;
	}



	public void setPlantId(Integer plantId) {
		this.plantId = plantId;
	}


	public Integer getTransporterSeqId() {
		return transporterSeqId;
	}



	public void setTransporterSeqId(Integer transporterSeqId) {
		this.transporterSeqId = transporterSeqId;
	}


	public Integer getTransporterId() {
		return transporterId;
	}



	public void setTransporterId(Integer transporterId) {
		this.transporterId = transporterId;
	}



	/**
	 * @param transporterName the transporterName to set
	 */
	public void setTransporterName(String transporterName) {
		this.transporterName = transporterName;
	}



	/**
	 * @return the modesOfTransport
	 */
	public String getModesOfTransport() {
		return modesOfTransport;
	}



	/**
	 * @param modesOfTransport the modesOfTransport to set
	 */
	public void setModesOfTransport(String modesOfTransport) {
		this.modesOfTransport = modesOfTransport;
	}



	/**
	 * @return the proposedQuantity
	 */
	public Double getProposedQuantity() {
		return proposedQuantity;
	}



	/**
	 * @param proposedQuantity the proposedQuantity to set
	 */
	public void setProposedQuantity(Double proposedQuantity) {
		this.proposedQuantity = proposedQuantity;
	}



	/**
	 * @return the proposedQuantityUnit
	 */
	public String getProposedQuantityUnit() {
		return proposedQuantityUnit;
	}



	/**
	 * @param proposedQuantityUnit the proposedQuantityUnit to set
	 */
	public void setProposedQuantityUnit(String proposedQuantityUnit) {
		this.proposedQuantityUnit = proposedQuantityUnit;
	}



	/**
	 * @return the proposedRate
	 */
	public Double getProposedRate() {
		return proposedRate;
	}



	/**
	 * @param proposedRate the proposedRate to set
	 */
	public void setProposedRate(Double proposedRate) {
		this.proposedRate = proposedRate;
	}



	/**
	 * @return the proposedRateUnit
	 */
	public String getProposedRateUnit() {
		return proposedRateUnit;
	}



	/**
	 * @param proposedRateUnit the proposedRateUnit to set
	 */
	public void setProposedRateUnit(String proposedRateUnit) {
		this.proposedRateUnit = proposedRateUnit;
	}



	/**
	 * @return the proposedValue
	 */
	public Double getProposedValue() {
		return proposedValue;
	}



	/**
	 * @param proposedValue the proposedValue to set
	 */
	public void setProposedValue(Double proposedValue) {
		this.proposedValue = proposedValue;
	}



	/**
	 * @return the proposedValueUnit
	 */
	public String getProposedValueUnit() {
		return proposedValueUnit;
	}



	/**
	 * @param proposedValueUnit the proposedValueUnit to set
	 */
	public void setProposedValueUnit(String proposedValueUnit) {
		this.proposedValueUnit = proposedValueUnit;
	}



	/**
	 * @return the transporterList
	 */
	public ArrayList<RpTransportationBO> getTransporterList() {
		return transporterList;
	}



	/**
	 * @param transporterList the transporterList to set
	 */
	public void setTransporterList(ArrayList<RpTransportationBO> transporterList) {
		this.transporterList = transporterList;
	}






	public Double getAveragePnb() {
		return averagePnb;
	}






	public void setAveragePnb(Double averagePnb) {
		this.averagePnb = averagePnb;
	}






	public Double getAverageFreightRate() {
		return averageFreightRate;
	}






	public void setAverageFreightRate(Double averageFreightRate) {
		this.averageFreightRate = averageFreightRate;
	}






	public Double getAverageProposedRate() {
		return averageProposedRate;
	}






	public void setAverageProposedRate(Double averageProposedRate) {
		this.averageProposedRate = averageProposedRate;
	}





	public List<String> getDocAttachmentList() {
		return docAttachmentList;
	}






	public void setDocAttachmentList(List<String> docAttachmentList) {
		this.docAttachmentList = docAttachmentList;
	}





	public String getComments() {
		return comments;
	}






	public void setComments(String comments) {
		this.comments = comments;
	}





	public int getActionId() {
		return actionId;
	}






	public void setActionId(int actionId) {
		this.actionId = actionId;
	}





	public int getCommentId() {
		return commentId;
	}






	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}





	public String getPlantName() {
		return plantName;
	}





	
	public void setPlantName(String plantName) {
		this.plantName = plantName;
	}






	public String getRemark() {
		return remark;
	}






	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	

}
