package com.gt.bo;

import java.util.ArrayList;
import java.util.List;

import com.gt.entity.Companies;

public class CompaniesBO {
	private String companyId ;
	private String companyName ;
	private String abbreviation;
	List<Companies> companyList= new ArrayList<Companies>();
	List<Companies> companyListToDelete= new ArrayList<Companies>();
	
	
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	
	
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getAbbreviation() {
		return abbreviation;
	}
	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}
	public List<Companies> getCompanyList() {
		return companyList;
	}
	public void setCompanyList(List<Companies> companyList) {
		this.companyList = companyList;
	}
	public List<Companies> getCompanyListToDelete() {
		return companyListToDelete;
	}
	public void setCompanyListToDelete(List<Companies> companyListToDelete) {
		this.companyListToDelete = companyListToDelete;
	}

	
	
}
