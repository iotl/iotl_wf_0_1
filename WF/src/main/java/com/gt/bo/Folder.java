package com.gt.bo;

public class Folder  {
	
	private String uid;
//	private String url;
//	private String role;
	private String displayName;
//	public Folder(String role){
//		this.role = role;
//	}
	private boolean active;
	
	public Folder(String uid){
		this.uid = uid;
	}
	
	
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

//	public String getUrl() {
//		return url;
//	}
//	public void setUrl(String url) {
//		this.url = url;
//	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}


	public boolean isActive() {
		return active;
	}


	public void setActive(boolean active) {
		this.active = active;
	}

}
