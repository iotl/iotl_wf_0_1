package com.gt.bo;

import java.util.ArrayList;
import java.util.List;

import com.gt.entity.Plant;

public class PlantsBO {
	
	private String plantId;
	private String name;
	private String abbreviation;
	
	private int companyId;
	private int businessId;
	private int sbuId;
	
	List<Plant> previousPlantList= new ArrayList<Plant>();
	List<Plant> newPlantList= new ArrayList<Plant>();
	
	private Plant plant;
	
	public String getPlantId() {
		return plantId;
	}
	public void setPlantId(String plantId) {
		this.plantId = plantId;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getAbbreviation() {
		return abbreviation;
	}
	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}
	public Plant getPlant() {
		return plant;
	}
	public void setPlant(Plant plant) {
		this.plant = plant;
	}
	
	public List<Plant> getPreviousPlantList() {
		return previousPlantList;
	}
	public void setPreviousPlantList(List<Plant> previousPlantList) {
		this.previousPlantList = previousPlantList;
	}
	public List<Plant> getNewPlantList() {
		return newPlantList;
	}
	public void setNewPlantList(List<Plant> newPlantList) {
		this.newPlantList = newPlantList;
	}
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public int getBusinessId() {
		return businessId;
	}
	public void setBusinessId(int businessId) {
		this.businessId = businessId;
	}
	public int getSbuId() {
		return sbuId;
	}
	public void setSbuId(int sbuId) {
		this.sbuId = sbuId;
	}
	

}
