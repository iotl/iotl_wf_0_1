package com.gt.bo;

public class Navigation {
	
	private String uid;
	private String url;
	private String role;
	
	public Navigation(String role){
		this.role = role;
	}

	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	

}
