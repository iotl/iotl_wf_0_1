package com.gt.bo;

public class CommentBlock {
	private String personName;
	private String comment;
	private String date;
	private String attachment;
	private String personFunction;
	

	
	
	public CommentBlock(String docNumber) {
		// TODO Auto-generated constructor stub
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}


	
	public String getPersonFunction() {
		return personFunction;
	}

	public void setPersonFunction(String personfunction) {
		this.personFunction = personfunction;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	

}
