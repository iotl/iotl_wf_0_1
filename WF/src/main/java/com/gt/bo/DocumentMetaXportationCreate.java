package com.gt.bo;

import java.util.ArrayList;
import java.util.List;

import com.gt.entity.Businesses;
import com.gt.entity.Companies;
import com.gt.entity.Material;
import com.gt.entity.Plant;
import com.gt.entity.RpTransportation;
import com.gt.entity.Sbus;

public class DocumentMetaXportationCreate {
	
	private Integer createdBy;
	private String title;
	private Companies company;
	private Businesses business;
	private Sbus sbu;
	private List<Plant> plantList;
	private Material material;
	private ArrayList<RpTransportation> rpData;
	private Integer documentId;
	private String rpNumber;
	private String justification;
//	private String rpNumber;
	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the company
	 */
	public Companies getCompany() {
		return company;
	}
	/**
	 * @param company the company to set
	 */
	public void setCompany(Companies company) {
		this.company = company;
	}
	/**
	 * @return the business
	 */
	public Businesses getBusiness() {
		return business;
	}
	/**
	 * @param business the business to set
	 */
	public void setBusiness(Businesses business) {
		this.business = business;
	}
	/**
	 * @return the sbu
	 */
	public Sbus getSbu() {
		return sbu;
	}
	/**
	 * @param sbu the sbu to set
	 */
	public void setSbu(Sbus sbu) {
		this.sbu = sbu;
	}
	/**
	 * @return the plantList
	 */
	public List<Plant> getPlantList() {
		return plantList;
	}
	/**
	 * @param plantList the plantList to set
	 */
	public void setPlantList(List<Plant> plantList) {
		this.plantList = plantList;
	}
	/**
	 * @return the material
	 */
	public Material getMaterial() {
		return material;
	}
	/**
	 * @param material the material to set
	 */
	public void setMaterial(Material material) {
		this.material = material;
	}
	/**
	 * @return the rpData
	 */
	public ArrayList<RpTransportation> getRpData() {
		return rpData;
	}
	/**
	 * @param rpData the rpData to set
	 */
	public void setRpData(ArrayList<RpTransportation> rpData) {
		this.rpData = rpData;
	}
	/**
	 * @return the documentId
	 */
	public Integer getDocumentId() {
		return documentId;
	}
	/**
	 * @param documentId the documentId to set
	 */
	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}
	/**
	 * @return the rpNumber
	 */
	public String getRpNumber() {
		return rpNumber;
	}
	/**
	 * @param rpNumber the rpNumber to set
	 */
	public void setRpNumber(String rpNumber) {
		this.rpNumber = rpNumber;
	}
	/**
	 * @return the justification
	 */
	public String getJustification() {
		return justification;
	}
	/**
	 * @param justification the justification to set
	 */
	public void setJustification(String justification) {
		this.justification = justification;
	}
	
	

	
}