package com.gt.bo;

import java.io.Serializable;

public class AmendmentBO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3581706536362258351L;

	private String rpNumber;
	
	private Integer createdBy;

	public String getRpNumber() {
		return rpNumber;
	}

	public void setRpNumber(String rpNumber) {
		this.rpNumber = rpNumber;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	
	
	

}
