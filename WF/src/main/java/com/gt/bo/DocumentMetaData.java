package com.gt.bo;

import java.util.ArrayList;
import java.util.List;

import com.gt.entity.Cluster;
import com.gt.entity.Plant;

public class DocumentMetaData {

	
	private String rpNo;
	private String companyName;
	private String businessName;
	private String SBUName;
	private String plantName;
	private String materialName;
	private String CategoryName;
	private String title;
	private Boolean isExpress;
	private Boolean isRelParty;
	private String sbuName;
	
	
	private Integer companyId;
	private Integer businessId;
	private Integer sbuId;
	private String plantId;
	private Integer materialId;
	private Integer categoryId;
	
	private ArrayList<Plant> plantList= new ArrayList<Plant>();
	
	List<String> docAttachmentList= new ArrayList<String>();
	private Integer actionId;
	private Integer commentId;
	private String comments;
	private String commentDate;
	private String isAmendment;
	private Cluster cluster;
	private String otherMaterialName;
	private String isPlant;
	private Boolean isDept;
	private String vendorName;
	private String materialDetails;
	private String sourceName;
	private Boolean inPrincipalApprocal;;
//	private Boolean isMultiPlant;
	private String justification;
	
	private Integer orginalProposalId;
	
	private Boolean isFinalApproval;
	
	private String rpStatus;
	
	
	public DocumentMetaData(){}
	
	public DocumentMetaData(String docNumber) {
		//RPVIEW INFO
		// TODO Auto-generated constructor stub
		
	}
	public String getRpNo() {
		return rpNo;
	}
	public void setRpNo(String rpNo) {
		this.rpNo = rpNo;
	}
	
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public String getSBUName() {
		return SBUName;
	}
	public void setSBUName(String sBUName) {
		SBUName = sBUName;
	}
	public String getPlantName() {
		return plantName;
	}
	public void setPlantName(String plantName) {
		this.plantName = plantName;
	}
	public String getMaterialName() {
		return materialName;
	}
	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}
	public String getTitle() {
		return title;
	}
	public Boolean getIsExpress() {
		return isExpress;
	}

	public void setIsExpress(Boolean isExpress) {
		this.isExpress = isExpress;
	}

	public Boolean getIsRelParty() {
		return isRelParty;
	}

	public void setIsRelParty(Boolean isRelParty) {
		this.isRelParty = isRelParty;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the sbuName
	 */
	public String getSbuName() {
		return sbuName;
	}

	/**
	 * @param sbuName the sbuName to set
	 */
	public void setSbuName(String sbuName) {
		this.sbuName = sbuName;
	}

	/**
	 * @return the companyId
	 */
	public Integer getCompanyId() {
		return companyId;
	}

	/**
	 * @param companyId the companyId to set
	 */
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	/**
	 * @return the businessId
	 */
	public Integer getBusinessId() {
		return businessId;
	}

	/**
	 * @param businessId the businessId to set
	 */
	public void setBusinessId(Integer businessId) {
		this.businessId = businessId;
	}

	/**
	 * @return the sbuId
	 */
	public Integer getSbuId() {
		return sbuId;
	}

	/**
	 * @param sbuId the sbuId to set
	 */
	public void setSbuId(Integer sbuId) {
		this.sbuId = sbuId;
	}

	/**
	 * @return the plantId
	 */
	public String getPlantId() {
		return plantId;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	/**
	 * @param plantId the plantId to set
	 */
	public void setPlantId(String plantId) {
		this.plantId = plantId;
	}

	/**
	 * @return the materialId
	 */
	public Integer getMaterialId() {
		return materialId;
	}

	/**
	 * @param materialId the materialId to set
	 */
	public void setMaterialId(Integer materialId) {
		this.materialId = materialId;
	}

	/**
	 * @return the plantList
	 */
	public ArrayList<Plant> getPlantList() {
		return plantList;
	}

	/**
	 * @param plantList the plantList to set
	 */
	public void setPlantList(ArrayList<Plant> plantList) {
		this.plantList = plantList;
	}


	public Integer getActionId() {
		return actionId;
	}

	public void setActionId(Integer actionId) {
		this.actionId = actionId;
	}

	public Integer getCommentId() {
		return commentId;
	}

	public void setCommentId(Integer commentId) {
		this.commentId = commentId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	
	public String getCommentDate() {
		return commentDate;
	}

	public void setCommentDate(String commentDate) {
		this.commentDate = commentDate;
	}

	public List<String> getDocAttachmentList() {
		return docAttachmentList;
	}

	public void setDocAttachmentList(List<String> docAttachmentList) {
		this.docAttachmentList = docAttachmentList;
	}

	public String getIsAmendment() {
		return isAmendment;
	}

	public void setIsAmendment(String isAmendment) {
		this.isAmendment = isAmendment;
	}

	public Cluster getCluster() {
		return cluster;
	}

	public void setCluster(Cluster cluster) {
		this.cluster = cluster;
	}

	public String getOtherMaterialName() {
		return otherMaterialName;
	}

	public void setOtherMaterialName(String otherMaterialName) {
		this.otherMaterialName = otherMaterialName;
	}

	public String getIsPlant() {
		return isPlant;
	}

	public void setIsPlant(String isPlant) {
		this.isPlant = isPlant;
	}

	public Boolean getIsDept() {
		return isDept;
	}

	public void setIsDept(Boolean isDept) {
		this.isDept = isDept;
	}

	public String getMaterialDetails() {
		return materialDetails;
	}

	public void setMaterialDetails(String materialDetails) {
		this.materialDetails = materialDetails;
	}

	public String getCategoryName() {
		return CategoryName;
	}

	public void setCategoryName(String categoryName) {
		CategoryName = categoryName;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public Boolean getInPrincipalApprocal() {
		return inPrincipalApprocal;
	}

	public void setInPrincipalApprocal(Boolean inPrincipalApprocal) {
		this.inPrincipalApprocal = inPrincipalApprocal;
	}

	public String getJustification() {
		return justification;
	}

	public void setJustification(String justification) {
		this.justification = justification;
	}

	public Integer getOrginalProposalId() {
		return orginalProposalId;
	}

	public void setOrginalProposalId(Integer orginalProposalId) {
		this.orginalProposalId = orginalProposalId;
	}

	public Boolean getIsFinalApproval() {
		return isFinalApproval;
	}

	public void setIsFinalApproval(Boolean isFinalApproval) {
		this.isFinalApproval = isFinalApproval;
	}

	public String getRpStatus() {
		return rpStatus;
	}

	public void setRpStatus(String rpStatus) {
		this.rpStatus = rpStatus;
	}

	

/*	public Boolean getIsMultiPlant() {
		return isMultiPlant;
	}

	public void setIsMultiPlant(Boolean isMultiPlant) {
		this.isMultiPlant = isMultiPlant;
	}*/

	


}
