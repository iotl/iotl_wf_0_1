package com.gt.bo;

import java.util.ArrayList;
import java.util.List;

import com.gt.entity.Sbus;

public class SBUsBO {
	private int sbuId;
	private String name ;
	private String abbreviation;
	
	private int businessId;
	private int companyId;
	List<Sbus> previousSBUsList= new ArrayList<Sbus>();
	List<Sbus> newSBUsList= new ArrayList<Sbus>();

	public int getSbuId() {
		return sbuId;
	}
	public void setSbuId(int sbuId) {
		this.sbuId = sbuId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAbbreviation() {
		return abbreviation;
	}
	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}
	public int getBusinessId() {
		return businessId;
	}
	public void setBusinessId(int businessId) {
		this.businessId = businessId;
	}
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public List<Sbus> getPreviousSBUsList() {
		return previousSBUsList;
	}
	public void setPreviousSBUsList(List<Sbus> previousSBUsList) {
		this.previousSBUsList = previousSBUsList;
	}
	public List<Sbus> getNewSBUsList() {
		return newSBUsList;
	}
	public void setNewSBUsList(List<Sbus> newSBUsList) {
		this.newSBUsList = newSBUsList;
	}
	
	
	
}
