package com.gt.bo;



public class UsersBO {

	String name;
	String function;
	Boolean added;

	Integer userid;
	String firstname;
	String lastname;
	String address;
	String alternatecontactperson;
	Boolean is_parallel;
	String docRole;
	
	UsersBO usersBO;

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	/*public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}*/

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAlternatecontactperson() {
		return alternatecontactperson;
	}

	public void setAlternatecontactperson(String alternatecontactperson) {
		this.alternatecontactperson = alternatecontactperson;
	}

	public UsersBO() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFunction() {
		return function;
	}

	public Boolean getAdded() {
		return added;
	}

	public void setAdded(Boolean added) {
		this.added = added;
	}
	


	public Boolean getIs_parallel() {
		return is_parallel;
	}

	public void setIs_parallel(Boolean is_parallel) {
		this.is_parallel = is_parallel;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	/**
	 * @return the usersBO
	 */
	public UsersBO getUsersBO() {
		return usersBO;
	}

	/**
	 * @param usersBO the usersBO to set
	 */
	public void setUsersBO(UsersBO usersBO) {
		this.usersBO = usersBO;
	}

	public String getDocRole() {
		return docRole;
	}

	public void setDocRole(String docRole) {
		this.docRole = docRole;
	}

	
	

}
