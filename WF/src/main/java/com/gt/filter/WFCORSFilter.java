package com.gt.filter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gt.entity.Material;
import com.gt.services.AdminService;
import com.gt.utils.PropsUtil;

@Component
public class WFCORSFilter implements Filter {
	
	@Autowired
	private AdminService adminService;

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletResponse response = (HttpServletResponse) res;
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Headers", " Origin, X-Requested-With, Content-Type, Accept");
		chain.doFilter(req, res);
	}

	public void init(FilterConfig filterConfig) {
		
		  // Do required initialization
	     // message = "Hello World";
		/*List<Material> matListBybusinessId=adminService.findByBusinessIdAndDeptId(1,0);
		JSONObject obj = new JSONObject();
		
		JSONArray list = new JSONArray();
		Map<Object,Object> map=null;
		for(Material material:matListBybusinessId){
			map=new HashMap<Object,Object>();
			//obj.put("", material);
			map.put("materialId", material.getMaterialId());
			map.put("materialName", material.getMaterialName());
			map.put("abbreviation", material.getAbbreviation());
			map.put("businessId", material.getBusinessId());
			map.put("deptId", material.getDeptId());
			map.put("clusterId", material.getClusterId());
		    map.put("doa", material.getDoa());
			if((material.getIsActive()==null || material.getIsActive()) && (material.getClusterId() == null || material.getClusterId()==0)){
				list.add(map);
			}
		}
		
		obj.put("mat1", list);
		matListBybusinessId=adminService.findByBusinessIdAndDeptId(2,0);
		list = new JSONArray();
		for(Material material:matListBybusinessId){
			map=new HashMap<Object,Object>();
			//obj.put("", material);
			map.put("materialId", material.getMaterialId());
			map.put("materialName", material.getMaterialName());
			map.put("abbreviation", material.getAbbreviation());
			map.put("businessId", material.getBusinessId());
			map.put("deptId", material.getDeptId());
			map.put("clusterId", material.getClusterId());
			map.put("doa", material.getDoa());
			if((material.getIsActive()==null || material.getIsActive()) && (material.getClusterId() == null || material.getClusterId()==0)){
				list.add(map);
			}
		}
		
		obj.put("mat2", list);
		matListBybusinessId=adminService.findByBusinessIdAndDeptId(3,0);
		list = new JSONArray();
		for(Material material:matListBybusinessId){
			map=new HashMap<Object,Object>();
			//obj.put("", material);
			map.put("materialId", material.getMaterialId());
			map.put("materialName", material.getMaterialName());
			map.put("abbreviation", material.getAbbreviation());
			map.put("businessId", material.getBusinessId());
			map.put("deptId", material.getDeptId());
			map.put("clusterId", material.getClusterId());
			map.put("doa", material.getDoa());
			if((material.getIsActive()==null || material.getIsActive()) && (material.getClusterId() == null || material.getClusterId()==0)){
				list.add(map);
			}
		}
		
		obj.put("mat3", list);
		
JSONObject objTmp = new JSONObject();
		
		JSONArray listTmp = new JSONArray();
		
		matListBybusinessId=adminService.findByBusinessIdIsNull();
		//list = new JSONArray();
		for(Material material:matListBybusinessId){
			map=new HashMap<Object,Object>();
			//obj.put("", material);
			map.put("materialId", material.getMaterialId());
			map.put("materialName", material.getMaterialName());
			map.put("abbreviation", material.getAbbreviation());
			map.put("businessId", material.getBusinessId());
			map.put("deptId", material.getDeptId());
			map.put("clusterId", material.getClusterId());
			map.put("doa", material.getDoa());
			if((material.getIsActive()==null || material.getIsActive()) && (material.getClusterId() == null || material.getClusterId()==5)){
				listTmp.add(map);
			}
		}
		listTmp.addAll((JSONArray) obj.get("mat1"));
		//listTmp=;
		objTmp.put("mat1", listTmp);

//		JSONObject objtemp = new JSONObject();
//		objtemp.put("MaterialData", obj);
		try {
			 String path=System.getProperty("user.dir");
			 String appMode=PropsUtil.getValue("app.mode"); 
			 if(appMode.equals("debug")){
			 if(path!=null && path.lastIndexOf("WF")!=-1){
				 path=path.substring(0,path.lastIndexOf("WF"));
			 }
			 }
			 String test="MaterialData="+obj.toJSONString().replace("\\", "");
			FileWriter file = new FileWriter(path+"\\UI\\material.json");
			file.write(test);
			file.flush();
			file.close();

			test="MaterialData="+objTmp.toJSONString().replace("\\", "");
			 file = new FileWriter(path+"\\UI\\material_utkal.json");
				file.write(test);
				file.flush();
				file.close();

		} catch (IOException e) {
			e.printStackTrace();
		}*/
	}

	public void destroy() {}

}