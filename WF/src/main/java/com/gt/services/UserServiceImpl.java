package com.gt.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.controllers.Status;
import com.gt.bo.AdvisorCreate;
import com.gt.bo.DelegationBO;
import com.gt.bo.Items;
//import com.gt.bo.DocumentUserMasterCreat;
import com.gt.bo.UsersBO;
import com.gt.entity.Delegation;
import com.gt.entity.DocUserMaster;
import com.gt.entity.Role;
import com.gt.entity.UserRoles;
import com.gt.entity.Users;
import com.gt.repository.DelegationRepository;
import com.gt.repository.DocUserMasterRepository;
import com.gt.repository.RolesRepository;
import com.gt.repository.UserDelegatesRepository;
import com.gt.repository.UserRepository;
import com.gt.repository.UserRoleRepository;
import com.gt.utils.DocRole;
import com.gt.utils.WFUtil;

@Component("userservice")
@Transactional
public class UserServiceImpl implements UserService {
	
	
	private final UserRepository userrepo;
	private final DocUserMasterRepository docUserMasterRepo;
	private final UserRoleRepository userRoleRepo;
	private final RolesRepository roleRepo;
	private final DelegationRepository delegationRepo;
	private final UserDelegatesRepository userdelegationRepo;
//	private final DelegationRepository delegateRepo;

	
	@Autowired
	public UserServiceImpl(UserRepository userrepo,  DocUserMasterRepository docUserMasterRepo, UserRoleRepository userRoleRepo,RolesRepository roleRepo, DelegationRepository delegationRepo ,UserDelegatesRepository userdelegationRepo) {
		this.userrepo = userrepo;
		this.docUserMasterRepo = docUserMasterRepo;
		this.userRoleRepo = userRoleRepo;
		this.roleRepo = roleRepo;
		this.delegationRepo = delegationRepo;
		this.userdelegationRepo=userdelegationRepo;
	
	}
	
	@Override
	public boolean createUser(Users users) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Users delete(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Users> findAllUsers() {
		List<Users> users = this.userrepo.findAll();
		return users;
	}

	@Override
	public Users findByUserid(int id) {
		Assert.notNull(id, "Id must not be null");
		
		return this.userrepo.findByUserId(id);
	}

	@Override
	public Users update(Users todo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DocUserMaster> findAllDocUserMasters() {
		return docUserMasterRepo.findAll();
	}
	
	@Override
	public ArrayList<Users> getAllUsers() {
		return (ArrayList<Users>) userrepo.findAll();
	}

	@Override
	public Users getUsersById(int id) {
		// return MasterCompaniesRepo.findByCompanyId(id);
		return userrepo.findByUserId(id);
	}
	

	@Override
	public List<UsersBO> findAllSupportingEndorsers(int id) {
		     ArrayList<UsersBO> userBo = new ArrayList <UsersBO>();
		     UsersBO usBo;
		     
		//     String regex = "\\d+";
		
		    List<Users> supportingEndorsers = userrepo.findByIsSupportingEndorser(true);
		   
		    List<DocUserMaster> docUsers = docUserMasterRepo.findByDocumentIdAndDocRoleNotIn(id, WFUtil.getDocRoles());
		    
            for(Users supportingEndorser : supportingEndorsers){
				usBo=new UsersBO();
				
				if(supportingEndorser.getIsSupportingEndorser() == true){
					 ArrayList<UserRoles> urole = userRoleRepo.findByUserId(supportingEndorser.getUserId()); 
					Role role = roleRepo.findByRoleId(urole.get(0).getRoleId());
					//usBo.setFunction(role.getRoleName());
					usBo.setUserid(urole.get(0).getUserId());
					String[] arr= role.getRoleName().split("-", 2);
					if(arr.length == 1){
						usBo.setFunction(role.getRoleName());
					}else{
						usBo.setFunction(arr[1]);
					}
					Users user =  userrepo.findByUserId(supportingEndorser.getUserId());
					
					usBo.setName(user.getFirstName()+" "+user.getLastName());
	                
	                boolean isAdded = false;
	                boolean isPara = true;
	                boolean toAdd = true;
	                int count = 0;
					
					for(DocUserMaster dUser : docUsers ){
					   
				       if(supportingEndorser.getUserId() == dUser.getUserId() ){
				    	   if(dUser.getDocRole().equalsIgnoreCase(DocRole.CONTROLLER)||dUser.getDocRole().equalsIgnoreCase(DocRole.ENDORSER)||dUser.getDocRole().equalsIgnoreCase(DocRole.INITIATOR)||dUser.getDocRole().equalsIgnoreCase(DocRole.APPROVER))
		 				    	  
				    	   {
				    		    toAdd = false;
				    	   }
							isAdded = true;
							if(count == 0)
							{
								if(dUser.getSerialNumber() != null)
								{
									isPara = false;
								}
							}
							else
							{
								isPara = isPara;
							}
							count++;
						}
					}

					usBo.setIs_parallel(isPara);
					usBo.setAdded(isAdded);
					if(toAdd){
					userBo.add(usBo);
					}
					
				}
			 
			}

        return  userBo;
	
	}
	
	/*@Override
	public List<UsersBO> findAllCPSupportingEndorsers(int id) {
		     ArrayList<UsersBO> userBo = new ArrayList <UsersBO>();
		     UsersBO usBo;
		     
		//     String regex = "\\d+";
		
		    List<Users> supportingEndorsers = userrepo.findByCpIsSupportingEndorser(true);
		   
		    List<DocUserMaster> docUsers = docUserMasterRepo.findByDocumentIdAndDocRoleNotIn(id, WFUtil.getDocRoles());
		    
            for(Users supportingEndorser : supportingEndorsers){
				usBo=new UsersBO();
				
				if(supportingEndorser.getCpIsSupportingEndorser()){
					 ArrayList<UserRoles> urole = userRoleRepo.findByUserId(supportingEndorser.getUserId()); 
					Role role = roleRepo.findByRoleId(urole.get(0).getRoleId());
					//usBo.setFunction(role.getRoleName());
					usBo.setUserid(urole.get(0).getUserId());
					String[] arr= role.getRoleName().split("-", 2);
					usBo.setFunction(arr[1]);
					Users user =  userrepo.findByUserId(supportingEndorser.getUserId());
					
					usBo.setName(user.getFirstName()+" "+user.getLastName());
	                
	                boolean isAdded = false;
	                boolean isPara = true;
	                int count = 0;
					
					for(DocUserMaster dUser : docUsers ){
					   
				       if(supportingEndorser.getUserId() == dUser.getUserId() ){
							isAdded = true;
							if(count == 0)
							{
								if(dUser.getSerialNumber() != null)
								{
									isPara = false;
								}
							}
							else
							{
								isPara = isPara;
							}
							count++;
						}
					}

					usBo.setIs_parallel(isPara);
					usBo.setAdded(isAdded);
					userBo.add(usBo);
					
				}
			 
			}

        return  userBo;
	
	}

	*/
	
    @Override
	public List<Users> findByIsSupportingEndorser(Boolean isSupportingEndorser) {
		
		return userrepo.findByIsSupportingEndorser(isSupportingEndorser);
		
	}
    
   
    @Override
	public Status saveDelegationData(DelegationBO delegations) {
		Delegation del = new Delegation();
		
		Integer userId = delegations.getUserId();	
		List<Delegation> delegate = delegationRepo.findByUserIdAndIsActive(userId, true);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		System.out.println("delegations.getFromDate()"+delegations.getFromDate());
		String fromTime= "00:00:00";
		String fromDateFormat = df.format(delegations.getFromDate());
		
		String toTime ="23:59:59";
		String toDateFormat = df.format(delegations.getToDate());
		System.out.println("toDateFormat >>>>"+toDateFormat);
		
		Date delegatedByFromDate = null;
		Date delegatedByToDate= null;
			delegatedByFromDate = WFUtil.getStringToDate(fromDateFormat+" "+fromTime, "yyyy-MM-dd HH:mm:ss");
			
			delegatedByToDate = WFUtil.getStringToDate(toDateFormat+" "+toTime, "yyyy-MM-dd HH:mm:ss");
			System.out.println("delegatedByFromDate >>>>"+delegatedByFromDate);
			System.out.println("delegatedByToDate >>>>"+delegatedByToDate);
	

		Boolean delegationPresent = null;
		Boolean delegationPresent1 = null;
		
		
		String mailId= delegations.getMailId();
		System.out.println("mailId >>>>"+mailId);
		Integer delegatedTo = getEntityByEmailId(mailId).getUserId();
		System.out.println("delegatedTo >>>>"+delegatedTo);

		List<Delegation> delegationDelegatedToList = delegationRepo.findByUserIdAndIsActive(delegatedTo, true);
		List<Delegation> delegationDelegatedForList = delegationRepo.findByDelegateUserIdAndIsActive(userId, true);
		Date delegatedToFromDate =delegations.getFromDate();
		Date delegatedToToDate = delegations.getToDate();
		
		if(delegationDelegatedForList.size()>0){
			try {
			for(Delegation dele : delegationDelegatedForList){
				Date fromDate =dele.getFromDate();		
				Date toDate = dele.getToDate();	
				String fromDateStr = df.format(fromDate);
				String toDateStr = df.format(toDate);
				String delegatedByFromDateStr=df.format(delegatedByFromDate);
				String delegatedByToDateStr=df.format(delegatedByToDate);
//		
//				delegationPresent =((df.parse(delegatedByFromDateStr).after(df.parse(fromDateStr))||df.parse(delegatedByFromDateStr).equals(df.parse(fromDateStr)))&& (df.parse(delegatedByFromDateStr).before(df.parse(toDateStr)) ||df.parse(delegatedByFromDateStr).equals(df.parse(toDateStr))));
//				delegationPresent1=((df.parse(delegatedByToDateStr).after(df.parse(fromDateStr))||df.parse(delegatedByToDateStr).equals(df.parse(fromDateStr)))&& (df.parse(delegatedByToDateStr).before(df.parse(toDateStr)) ||df.parse(delegatedByToDateStr).equals(df.parse(toDateStr))));
				
				delegationPresent =((df.parse(fromDateStr)).before(df.parse(delegatedByToDateStr))||(df.parse(fromDateStr)).equals(df.parse(delegatedByToDateStr)))&&(df.parse(delegatedByFromDateStr).before(df.parse(toDateStr)) ||df.parse(delegatedByFromDateStr).equals(df.parse(toDateStr)));
				
				if(delegationPresent){
					Users usr=userrepo.findByUserId(dele.getUserId());
//					return new Status(-4,"You have currently assigned Temporary Delegation by "+usr.getFirstName() +" "+usr.getLastName()+".");
					return new Status(-4,"You have been assigned Temporary Delegation during this period.");
				}
			}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		if(delegationDelegatedToList.size()>0){
			try{
			for(Delegation dele : delegationDelegatedToList){
				Date fromDate =dele.getFromDate();		
				Date toDate = dele.getToDate();		
				String fromDateStr = df.format(fromDate);
				String toDateStr = df.format(toDate);
				String delegatedByFromDateStr=df.format(delegatedByFromDate);
				String delegatedByToDateStr=df.format(delegatedByToDate);
				
//				delegationPresent =((df.parse(delegatedByFromDateStr).after(df.parse(fromDateStr))||df.parse(delegatedByFromDateStr).equals(df.parse(fromDateStr)))&& (df.parse(delegatedByFromDateStr).before(df.parse(toDateStr)) ||df.parse(delegatedByFromDateStr).equals(df.parse(toDateStr))));
//				delegationPresent1=((df.parse(delegatedByToDateStr).after(df.parse(fromDateStr))||df.parse(delegatedByToDateStr).equals(df.parse(fromDateStr)))&& (df.parse(delegatedByToDateStr).before(df.parse(toDateStr)) ||df.parse(delegatedByToDateStr).equals(df.parse(toDateStr))));
				
				
				delegationPresent =((df.parse(fromDateStr)).before(df.parse(delegatedByToDateStr))||(df.parse(fromDateStr)).equals(df.parse(delegatedByToDateStr)))&&(df.parse(delegatedByFromDateStr).before(df.parse(toDateStr)) ||df.parse(delegatedByFromDateStr).equals(df.parse(toDateStr)));
				if(delegationPresent){
					Users usr=userrepo.findByUserId(userId);
					
					return new Status(-3,usr.getFirstName() +" "+usr.getLastName()+" is Unavailable for Delegation");
//					return new Status(-3,"Delegatee to Person Unavailable");
				}
			}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		if(delegate.size()>0){
			for(Delegation dele : delegate){
				Date fromDate =dele.getFromDate();		
				Date toDate = dele.getToDate();		
				
//				delegationPresent =((delegatedByFromDate.after(fromDate)||delegatedByFromDate.equals(fromDate))&& (delegatedByFromDate.before(toDate) ||delegatedByFromDate.equals(toDate)));
//				delegationPresent1=((delegatedByToDate.after(fromDate)||delegatedByToDate.equals(fromDate))&& (delegatedByToDate.before(toDate) ||delegatedByToDate.equals(toDate)));
				
				delegationPresent =((fromDate.before(delegatedByToDate)||fromDate.equals(delegatedByToDate))&&(delegatedByFromDate.before(toDate) ||delegatedByFromDate.equals(toDate)));
				if(delegationPresent){
					break;
				}
			}
		}
		if((delegate.size()==0)|| (delegationPresent!=null && !delegationPresent)){
			del.setFromDate(delegatedByFromDate);
			del.setToDate(delegatedByToDate);
			del.setDelegateUserId(delegatedTo);
			del.setUserId(userId);
			Date Date = new Date();
			del.setCreatedTimeStamp(Date);
			del.setIsActive(true);
			del = delegationRepo.save(del);
			return new Status(1,"Delegation Successful");
		}
		return new Status(-2,"Delegation Date OverLap");
	}
	
	@Override
	public DocUserMaster saveSupportingEndorser(AdvisorCreate ac) {

		List<DocUserMaster> dums = docUserMasterRepo.findByDocumentIdAndDocRoleNotIn(ac.getDocId(), WFUtil.getDocRoles());
		DocUserMaster dumCurrent = docUserMasterRepo.findByDocumentIdAndUserId(ac.getDocId(), ac.getCurrentUserId());
		ArrayList<DocUserMaster> advisoryEndorser = new ArrayList<DocUserMaster>();
		DocUserMaster dum= new DocUserMaster();
		ArrayList<Integer> userId = ac.getUserId();
		Integer srNo = null;
		String currentRole =ac.getCurrentRole();
		for(DocUserMaster dum1:dums){
			if(!dum1.getDocRole().equals(DocRole.INITIATOR) && !dum1.getDocRole().equals(DocRole.ENDORSER) &&!dum1.getDocRole().equals(DocRole.CONTROLLER) && !dum1.getDocRole().equals(DocRole.APPROVER) ){
				advisoryEndorser.add(dum1);
			}
		}
		if(advisoryEndorser!=null && advisoryEndorser.size()>0){
			if(!ac.getSerialSelection()){
				for(DocUserMaster dum1:advisoryEndorser){
					dum1.setSerialNumber(null);
					docUserMasterRepo.save(dum1);
				}
			}else{
System.out.println("currentRole >>>>"+currentRole);
System.out.println("dumCurrent. >>>>"+dumCurrent);
				if((currentRole!= null && dumCurrent!=null && dumCurrent.getSerialNumber()!=null) && (currentRole!=null && "Controller".equals(currentRole))|| (dumCurrent!=null && dumCurrent.getSerialNumber()==50)){
					 srNo= 51;
					 List<DocUserMaster> docUserMasterList=docUserMasterRepo.findByDocumentIdOrderBySerialNumberDesc(ac.getDocId());
					 if(docUserMasterList!=null && docUserMasterList.size()>0){
						 int tempSrNo= docUserMasterList.get(0).getSerialNumber();
						 if(tempSrNo > 51 && tempSrNo<100){
							 srNo= tempSrNo;
						 }
					 }
				}else{
					 srNo= 11;
					 List<DocUserMaster> docUserMasterList=docUserMasterRepo.findByDocumentIdOrderBySerialNumberDesc(ac.getDocId());
					 if(docUserMasterList!=null && docUserMasterList.size()>0){
						 int tempSrNo= docUserMasterList.get(0).getSerialNumber();
						 if(tempSrNo > 11 && tempSrNo<21){
							 srNo= tempSrNo;
						 }
					 }
				}
				for(DocUserMaster dum1:advisoryEndorser){
					dum1.setSerialNumber(srNo);
					docUserMasterRepo.save(dum1);
					srNo++;
				}
			}
			System.out.println("Advisory Endorser Already Added " +advisoryEndorser.get(0).getSerialNumber()+advisoryEndorser.size() +ac.getSerialSelection());
		}
		
		
		if(ac.getSerialSelection()){
			if(!(advisoryEndorser!=null && advisoryEndorser.size()>0)){
				if("Controller".equals(currentRole)){
					 srNo= 51;
					 List<DocUserMaster> docUserMasterList=docUserMasterRepo.findByDocumentIdOrderBySerialNumberDesc(ac.getDocId());
					 if(docUserMasterList!=null && docUserMasterList.size()>0){
						 int tempSrNo= docUserMasterList.get(0).getSerialNumber();
						 if(tempSrNo > 51 && tempSrNo<100){
							 srNo= tempSrNo;
						 }
					 }
				}else{
					 srNo= 11;
					 List<DocUserMaster> docUserMasterList=docUserMasterRepo.findByDocumentIdOrderBySerialNumberDesc(ac.getDocId());
					 if(docUserMasterList!=null && docUserMasterList.size()>0){
						 int tempSrNo= docUserMasterList.get(0).getSerialNumber();
						 if(tempSrNo > 11 && tempSrNo<21){
							 srNo= tempSrNo;
						 }
					 }
				}
			}
		}
			dum.setDocumentId(ac.getDocId());
			for(int i = 0; i<userId.size(); i++){
			
			dum.setUserId(ac.getUserId().get(i));
			dum.setDocRole(ac.getRole().get(i));
			if(ac.getSerialSelection()){
				dum.setSerialNumber(srNo);
				srNo++;
			}else{
				dum.setSerialNumber(null);
			}
			
			docUserMasterRepo.save(dum);
		}
//		docUserMasterRepo.save(dum);
		
		return null;
	}


	@Override
	public List<Users> findUserBasedOnRole(String Role) {
		ArrayList<Users> users= new ArrayList<Users>();
		List<com.gt.entity.Role> roleObj = roleRepo.findByRoleName(Role);
		if(roleObj!=null && roleObj.size()>0){
		List<UserRoles> uRoles = userRoleRepo.findByRoleId(roleObj.get(0).getRoleId());
		if(uRoles!=null && uRoles.size()>0){
		Users user = userrepo.findByUserId(uRoles.get(0).getUserId());
		users.add(user);
		}
		}
		return users;
	}

	@Override
	public Integer deleteDocUserMaster(Integer userId, Integer documentId) {
		return docUserMasterRepo.deleteByUserIdAndDocumentId(userId, documentId);
	}
	
	/*@Override
	public List<UserDelegates> findUserDelegatesBasedOnUserId(Integer userid) {
		
		return userdelegationRepo.findByUserId(userid);
	}
	
	@Override
	public List<Delegation> findUserDelegated(Integer userId) {
		
		return delegationRepo.findByUserId(userId);
	}*/
	
	@Override
	public  Users getEntityByEmailId(String emailId)  {
		// TODO Auto-generated method stub
		Users users =null;
		List<Users> list=userrepo.findByEmailId(emailId);
		if(list!=null && list.size()>0){
			users=(Users)list.get(0);
		}
		return users;
	}

	@Override
	public Boolean getIsActiveDelegation(Integer userId) {
		// TODO Auto-generated method stub
		List<Delegation> delegates = delegationRepo.findByDelegateUserId(userId);
		
		for (Delegation del : delegates) {
			Date fromDate = del.getFromDate();
			Date toDate = del.getToDate();
			Date currentDate = new Date();

			boolean getMin = (currentDate.compareTo(toDate) <= 0 && currentDate
					.compareTo(fromDate) >= 0);
			if(getMin && del.getIsActive()){
				return true;
			}
			
		}
		return false;
//		return null;
	}

/*	@Override
	public Boolean getIsActiveDelegation(Integer userId) {
		List<Delegation> delegates = delegationRepo.findByDelegateUserId(userId);
		
		for (Delegation del : delegates) {
			Date fromDate = del.getFromDate();
			Date toDate = del.getToDate();
			Date currentDate = new Date();

			boolean getMin = (currentDate.compareTo(toDate) <= 0 && currentDate
					.compareTo(fromDate) >= 0);
			if(getMin && del.getIsActive()){
				return true;
			}
			
		}
		return false;
	}*/

	

}
