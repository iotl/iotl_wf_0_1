package com.gt.services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.controllers.DocumentController;
import com.controllers.Status;
import com.gt.bo.InsertNewUserBO;
import com.gt.bo.UsersBO;
import com.gt.drools.WFRules.Business;
import com.gt.entity.Businesses;
import com.gt.entity.Companies;
import com.gt.entity.Material;
import com.gt.entity.Plant;
import com.gt.entity.Role;
import com.gt.entity.Sbus;
import com.gt.entity.UserEndorsers;
import com.gt.entity.UserRoles;
import com.gt.entity.Users;
import com.gt.repository.DocumentMasterRepository;
import com.gt.repository.MasterBusinessDataRepository;
import com.gt.repository.MasterCompaniesDataRepository;
import com.gt.repository.MasterMaterialDataRepository;
import com.gt.repository.MasterPlantDataRepository;
import com.gt.repository.MasterSBUDataRepository;
import com.gt.repository.RolesRepository;
import com.gt.repository.UserEndorserRepository;
import com.gt.repository.UserRepository;
import com.gt.repository.UserRoleRepository;
import com.gt.utils.DocRole;
import com.gt.utils.PasswordHashing;
import com.gt.utils.PropsUtil;


@Component("adminService")
@Transactional
public class AdminServiceImpl implements AdminService{
	
	public static final Logger logger = Logger
			.getLogger(AdminServiceImpl.class.getName());
	
	private final MasterMaterialDataRepository masterMaterialDataRepository;

	
	@Autowired
	private UserRepository userRepository;
	
	
	@Autowired
	private RolesRepository roleRepository;
	
	@Autowired
	private UserRoleRepository userRoleRepo;
	
	@Autowired
	private UserEndorserRepository userEndorserRepo;
	
    @Autowired
	private MasterCompaniesDataRepository masterCompaniesDataRepo;
			
	@Autowired
	private MasterBusinessDataRepository masterBusinessDataRepo;
			
	@Autowired
	private MasterSBUDataRepository masterSBUDataRepo;
			
	@Autowired
	private MasterPlantDataRepository masterPlantDataRepo;
	
	@Autowired
	private DocumentMasterRepository documentMasterRepo;
		
	
	
	@Autowired
	public AdminServiceImpl(MasterMaterialDataRepository masterMaterialDataRepository){
		this.masterMaterialDataRepository=masterMaterialDataRepository;
	}


	@Override
	public Material saveMaterial(Material material) {
		
		return masterMaterialDataRepository.save(material);
	}


	@Override
	public List<Material> findByBusinessId(Integer businessId) {
		
		if(businessId==-1){
			List<Integer> businessIds= new ArrayList<Integer>();
			List<Businesses> busi= masterBusinessDataRepo.findAll();
			for (Businesses b:busi){
				businessIds.add(b.getBusinessId());
			}
			return masterMaterialDataRepository.findByBusinessIdIn(businessIds);	
			
		}
		
		
		return masterMaterialDataRepository.findByBusinessId(businessId);
	}


	@Override
	public List<Material> findAllMaterials() {
		
		return masterMaterialDataRepository.findAll();
	}
	
	


//	@Override
//	public Integer deleteByMaterialId(Integer materialId) {
//	
//		return masterMaterialDataRepository.deleteByMaterialId(materialId);
//	}
	
	@Override
	public void deleteByMaterialId(Integer materialId) {
	
		Material material = masterMaterialDataRepository.findByMaterialId(materialId);
		material.setIsActive(false);
//		return masterMaterialDataRepository.deleteByMaterialId(materialId);
	}
	
	
	@Override
	public List<Material> findByBusinessIdIsNull() {
		
		return masterMaterialDataRepository.findByBusinessIdIsNull();
	}
	@Override
	public List<Material> findByBusinessIdAndDeptId(Integer businessId,Integer deptId) {
		Boolean isActive = true;
		System.out.println("deptId >>>>"+deptId);
		if(businessId!=null && businessId ==-1){
			List<Integer> businessIdList= new ArrayList<Integer>();
			businessIdList.add(1);
			businessIdList.add(2);
			businessIdList.add(3);
			Collection<Integer> businessIds = new ArrayList<Integer>(businessIdList);
			if(deptId!=null){
				//return MasterMaterialRepo.findByBusinessIdAndDeptId(businessId, deptId);
				return masterMaterialDataRepository.findByBusinessIdInAndDeptIdAndIsActive(businessIds,deptId,isActive);

			}else{
			    return masterMaterialDataRepository.findByBusinessIdInAndDeptIdIsNull(businessIds);
			}
			
		}
		if(deptId!=null){
			//return MasterMaterialRepo.findByBusinessIdAndDeptId(businessId, deptId);
			return masterMaterialDataRepository.findByBusinessIdAndDeptIdAndIsActive(businessId,deptId,isActive);

		}else{
		    return masterMaterialDataRepository.findByBusinessIdAndDeptIdIsNull(businessId);
		}
	
//		return masterMaterialDataRepository.findByBusinessIdAndDeptId(businessId,deptId);
	}
	
	
	@Override
	public Integer findByMaxId() {
		// TODO Auto-generated method stub
		return userRepository.findMaxUserId();
	}

	
	
	@Override
	public Status saveUser(InsertNewUserBO insertNewUserBO) {
		
		//Finding max userId
		int userId = userRepository.findMaxUserId() + 1; 
		 List<Users> user = userRepository.findByEmailId(insertNewUserBO.getEmailId());
		if(user.size()!= 0){
			return new Status(2, "MailId already exists");
		}
		Boolean rolePresent= false;
		if(!insertNewUserBO.getDesignation().equalsIgnoreCase(DocRole.INITIATOR) && !insertNewUserBO.getDesignation().equalsIgnoreCase(DocRole.ENDORSER) && !insertNewUserBO.getDesignation().equalsIgnoreCase(DocRole.TEMPORARY_DELEGATEE) &&  !insertNewUserBO.getDesignation().equalsIgnoreCase("Initiator & Endorser") ){
			List<Role> role = roleRepository.findByRoleName(insertNewUserBO.getDesignation());
			if(role.size()!= 0 || !(role.isEmpty())){
				rolePresent= true;
				List<UserRoles> usrRole = userRoleRepo.findByRoleId(role.get(0).getRoleId());
				if(usrRole.size()!= 0 || !(usrRole.isEmpty())){
					return new Status(3, "User already exists with this role");
				}	
			}
		}
		String hashedPassword=PasswordHashing.PasswordEncoderGenerator();
		
		//Create and save new user entity object
		Users newUser = new Users();
		newUser.setUserId(userId);
		newUser.setFirstName(insertNewUserBO.getFirstName());
		newUser.setLastName(insertNewUserBO.getLastName());
		newUser.setEmailId(insertNewUserBO.getEmailId());
		newUser.setIsSupportingEndorser(insertNewUserBO.getIsSupportingEndorser());
		newUser.setPassword(hashedPassword);
		if(insertNewUserBO.getDesignation().equalsIgnoreCase(DocRole.INITIATOR)||insertNewUserBO.getDesignation().equalsIgnoreCase(DocRole.ENDORSER)||insertNewUserBO.getDesignation().equalsIgnoreCase("Initiator & Endorser")){
			newUser.setDeptId(0);
		}
		userRepository.save(newUser);
		Role role = new Role();
		ArrayList<Role> roles = new ArrayList<Role>();
		UserRoles userRoles;
		if(!rolePresent && !insertNewUserBO.getDesignation().equalsIgnoreCase(DocRole.INITIATOR) && !insertNewUserBO.getDesignation().equalsIgnoreCase(DocRole.ENDORSER) && !insertNewUserBO.getDesignation().equalsIgnoreCase("Initiator & Endorser") && !insertNewUserBO.getDesignation().equalsIgnoreCase("Temporary-Delegatee") ){
		//Create and save role entity object
		//set roleId here after finding max
			int roleId=roleRepository.findMaxRoleId() + 1;
			role.setRoleId(roleId);
			role.setRoleName(insertNewUserBO.getDesignation());
			role.setDashboard_Type(insertNewUserBO.getDashboardType());
			roleRepository.save(role);
			roles.add(role);
			//Create and save userRole entity object
			
		}else if (insertNewUserBO.getDesignation().equalsIgnoreCase("Initiator & Endorser")){
			role =roleRepository.findByRoleName(DocRole.INITIATOR).get(0);
			roles.add(role);
			role =roleRepository.findByRoleName(DocRole.ENDORSER).get(0);
			roles.add(role);
			
		}else{
			role =roleRepository.findByRoleName(insertNewUserBO.getDesignation()).get(0);
			roles.add(role);
		}
		for(Role rl : roles){
			//Create and save userRole entity object
			userRoles = new UserRoles();
			userRoles.setUserId(newUser.getUserId());
			userRoles.setRoleId(rl.getRoleId());
			userRoleRepo.save(userRoles);
		}
		if(insertNewUserBO.getUser()!= null){
			Integer endorserId =insertNewUserBO.getUser().getUserid();
			if(endorserId != -1){
				UserEndorsers userEndorser = new UserEndorsers();
				userEndorser.setUserId(newUser.getUserId());
				userEndorser.setEndorserId(endorserId);
				userEndorserRepo.save(userEndorser);
			}
		}
		//Save Immediate Endorser
		
		
		return new Status(1, "User Saved Successfully");
	}

	@Override
	public ArrayList<UsersBO> getAllEndorsers() {
		ArrayList<UsersBO> users =new ArrayList<UsersBO>();
		List<Role> role = roleRepository.findByRoleName(DocRole.ENDORSER);
		ArrayList<UserRoles> userRoles = userRoleRepo.findByRoleId(role.get(0).getRoleId());
		UsersBO user;
		for(UserRoles userRole:userRoles){
			Users usr = userRepository.findByUserId(userRole.getUserId());
			if(usr.getPlantId() != null || (usr.getDeptId() != null && usr.getDeptId().intValue() != 0) ){
				continue;
			}
			user = new UsersBO();
			if((usr.getIsSupportingEndorser()==null || !usr.getIsSupportingEndorser()) && (usr.getDeptId()==null || usr.getDeptId()==0) && usr.getPlantId()==null){
				user.setName(usr.getFirstName() +" "+ usr.getLastName());
				user.setUserid(usr.getUserId());
				users.add(user);
			}
		}
		user = new UsersBO();	
		user.setName("None");
		user.setUserid(-1);
		user.setUsersBO(new UsersBO());
		users.add(user);
		
		return users;
	}


	@Override
	public String updateSampleDAO(String fileName) {
		String jarFile = PropsUtil.getValue("jar.file.location")+"\\wildfly-8.2.1.Final\\bin\\repositories\\kie\\com\\gt\\Admin\\1.0\\Admin-1.0.jar";
		String destDir = PropsUtil.getValue("jar.destDir.location");
		String output="success";
		
		try {
			java.util.jar.JarFile jar = new java.util.jar.JarFile(jarFile);
			java.util.Enumeration enumEntries = jar.entries();
			String filePath="";
			
			
			while (enumEntries.hasMoreElements()) {
			    java.util.jar.JarEntry file = (java.util.jar.JarEntry) enumEntries.nextElement();
			    
			    filePath = file.getName();
			  //  System.out.println("filename is"+filePath);
			    java.io.File f = new java.io.File(destDir + java.io.File.separator + filePath);
			    if (file.isDirectory()) { // if its a directory, create it
			        f.mkdir();
			        continue;
			    }
		        java.io.InputStream is = jar.getInputStream(file); // get the input stream
			    java.io.FileOutputStream fos = new java.io.FileOutputStream(f);
			    while (is.available() > 0) {  // write contents of 'is' to 'fos'
			        fos.write(is.read());
			    }
			   
			    fos.close();
			    is.close();
			   
			}
			jar.close();
			
			String extractedRuleFile = destDir+"\\com\\gt\\admin\\"+fileName+".drl";
			
			replaceContentOfRuleFile(extractedRuleFile);                                              //appends content to the extracted rule file
			
			File folder = new File(System.getProperty("user.dir"));

			File existingRuleFile = null;
			if (folder.exists()) {
				existingRuleFile = new File(folder.getAbsolutePath()+ "//DRules//"+fileName+".drl");
			}
			
			String dateString = new SimpleDateFormat("yyyy-MM-dd-HHmmss").format(new Date());
			
			copyFileUsingStream(existingRuleFile,new File(folder+"//DRules//"+fileName+dateString+".drl.bak"));      //create a copy of existing Rule file 
			
			copyFileUsingStream(new File(extractedRuleFile), existingRuleFile);  // Replace the original rule file with extracted rule file
			
			
			
		} catch (FileNotFoundException e) {
			logger.error("Error while updating file dao in RuleEnginehelper",e);
			e.printStackTrace();
			System.out.println(e);
			output = "error";
		} catch (IOException e) {
			logger.error("Error while updating file dao in RuleEnginehelper",e);
			e.printStackTrace();
			System.out.println(e);
			output = "error";
		}
		return output;
	}
	
	private  void copyFileUsingStream(File source, File dest) throws IOException {
	    InputStream is = null;
	    OutputStream os = null;
	    try {
	        is = new FileInputStream(source);
	        os = new FileOutputStream(dest);
	        byte[] buffer = new byte[1024];
	        int length;
	        while ((length = is.read(buffer)) > 0) {
	            os.write(buffer, 0, length);
	        }
	    } finally {
	        is.close();
	        os.close();
	    }
	}
	
	public void replaceContentOfRuleFile(String filePath) throws FileNotFoundException,IOException{
		
    	
			FileReader fileReader = new FileReader(filePath);
			BufferedReader br = new BufferedReader(fileReader);
			
			String line;
			StringBuffer input = new StringBuffer();
			int count = 0;
			
		    while (br.ready()) {	
				line=br.readLine();
				
			    if (count == 0) {
			        line = "package com.sample;"+"\n"+"import com.gt.drools.WFRules.Message;"+"\n"+
			      "import com.gt.drools.WFRules.User;"+"\n"+"import com.gt.drools.WFRules.Business;"+"\n"+
			      "import java.util.ArrayList;";
			   }
			    input.append(line).append('\n');
			    ++count;
			}
		
		    FileWriter fileWriter  = new FileWriter(filePath);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(input.toString());
			br.close();
			fileReader.close();
			bufferedWriter.close();
			fileWriter.close();
		
	}
	
	@Override
	public Boolean isMaterialDuplicated(Material material){
		Material matDB = masterMaterialDataRepository.findByMaterialId(material.getMaterialId());
		if(material.getAbbreviation().equalsIgnoreCase(matDB.getAbbreviation()) && material.getMaterialName().equalsIgnoreCase(matDB.getMaterialName())){
			return false;
		}else{
			List<Material> material1 = masterMaterialDataRepository.findByAbbreviationAndMaterialIdNotIn(material.getAbbreviation(), material.getMaterialId());
			List<Material> material2 = masterMaterialDataRepository.findByMaterialNameAndMaterialIdNotIn(material.getMaterialName(), material.getMaterialId());
			if(material1.size()>0 ||material2.size()>0){
				return true;
			}
			
		}
		return false;
	}
	
	public List<Companies> findAllCompany(){
	
		
		
		return masterCompaniesDataRepo.findAll();
		
		
	};
	
	public List<Businesses> findByCompanyId(int companyId){
		List<Businesses> busi;
		busi = masterBusinessDataRepo.findByCompanyId(companyId);
		if(companyId == -1){
			Collection<Integer> companyIds = masterCompaniesDataRepo.findAllCompanyId();
			busi = masterBusinessDataRepo.findAllBusinessByCompanyIdIn(companyIds);
		}
		return busi;
	}
	
	public List<Sbus> findByCompanyIdAndBusinessId(int companyId,int businessId){
		if(companyId==-1 || businessId ==-1 ){
			Collection<Integer> companyIds = new ArrayList<Integer>();
			if(companyId == -1){
				 companyIds = masterCompaniesDataRepo.findAllCompanyId();
			}else{
				companyIds.add(companyId);
			}
			Collection<Integer> businessIds = new ArrayList<Integer>();
			if(businessId == -1){
				businessIds = masterBusinessDataRepo.findAllBusinessIdByCompanyIdIn(companyIds);
			}else{
				businessIds.add(businessId);
			}
			return masterSBUDataRepo.findAllSbuByCompanyIdInAndBusinessIdIn(companyIds,businessIds);
		}
		return masterSBUDataRepo.findByCompanyIdAndBusinessId(companyId,businessId);
	}
	
	public List<Plant> findByCompanyIdAndBusinessIdAndSbuIdAndisCorp(int companyId,int businessId,int sbuId){
		if(companyId==-1 || businessId ==-1 || sbuId==-1){
			Collection<Integer> companyIds = new ArrayList<Integer>();
			if(companyId == -1){
				 companyIds = masterCompaniesDataRepo.findAllCompanyId();
			}else{
				companyIds.add(companyId);
			}
			Collection<Integer> businessIds = new ArrayList<Integer>();
			if(businessId == -1){
				businessIds = masterBusinessDataRepo.findAllBusinessIdByCompanyIdIn(companyIds);
			}else{
				businessIds.add(businessId);
			}
			Collection<Integer> sbuIds =new ArrayList<Integer>();
			if(sbuId == -1){
				sbuIds = masterSBUDataRepo.findAllSbuIdByCompanyIdInAndBusinessIdIn(companyIds, businessIds);
			}else{
				sbuIds.add(sbuId);
			}
			
			
			return masterPlantDataRepo.findAllPlantByCompanyIdInAndBusinessIdInAndSbuIdIn (companyIds, businessIds, sbuIds,true);
		}
		return masterPlantDataRepo.findByCompanyIdAndBusinessIdAndSbuIdAndCorpLogin(companyId, businessId, sbuId,true);
	}


	@Override
	public Boolean isCompanyDuplicated(Companies company) {
		Companies companyDB = masterCompaniesDataRepo.findByCompanyId(company.getCompanyId());
		if(company.getAbbreviation().equalsIgnoreCase(companyDB.getAbbreviation()) && company.getCompanyName().equalsIgnoreCase(companyDB.getCompanyName())){
			return false;
		}else{
			List<Companies> company1 = masterCompaniesDataRepo.findByAbbreviationAndCompanyIdNotIn(company.getAbbreviation(), company.getCompanyId());
			List<Companies> company2 = masterCompaniesDataRepo.findByCompanyNameAndCompanyIdNotIn(company.getCompanyName(), company.getCompanyId());
			if(company1.size()>0 ||company2.size()>0){
				return true;
			}
			
		}
		return false;
	}


	@Override
	public Companies saveCompany(Companies company) {
		return  masterCompaniesDataRepo.save(company);
	}
	
	@Override
	public Businesses saveBusiness(Businesses business){
		
		return masterBusinessDataRepo.save(business);
	}
	
	@Override
	public Sbus updateSbu(Sbus sbu){
		
	 return masterSBUDataRepo.save(sbu);	
	}
	
	
	@Override
	public Sbus saveSbu(Sbus sbu){
		int sbuId;
		Sbus existingSbu = masterSBUDataRepo.findBySbuNameAndAbbreviation(sbu.getSbuName(), sbu.getAbbreviation());
		if(existingSbu!=null){
			System.out.println("existing sbu is "+existingSbu.getSbuId()+"sbu name "+sbu.getSbuName());
			sbuId=existingSbu.getSbuId();
			
		}
		else{
			 sbuId = masterSBUDataRepo.findMaxSbuId()+1;
			 System.out.println("Sbu Id is "+(sbuId));
			 
		}
		sbu.setSbuId(sbuId);
		return masterSBUDataRepo.save(sbu);
	}
	


	@Override
	public Integer findMaxBusinessId() {
		return masterBusinessDataRepo.findMaxBusinessId();
	}


	@Override
	public Businesses findByBusinessNameAndAbbreviation(String businessName, String abbrevation) {
		
		return masterBusinessDataRepo.findByBusinessNameAndAbbreviation(businessName, abbrevation);
	}


	@Override
	public Integer findMaxCompanyId() {
		return masterCompaniesDataRepo.findMaxCompanyId();
	}
	
	@Override
    public Plant updatePlant(Plant plant){
    	return masterPlantDataRepo.save(plant);
    }
	
	@Override
	public Plant savePlant(Plant plant){
		int pkId;
		int plantId;
		Plant existingPlant = masterPlantDataRepo.findByPlantNameAndAbbreviation(plant.getPlantName(), plant.getAbbreviation());
		if(existingPlant!=null){
			plantId = existingPlant.getPlantId();
			
		}
		else{
			plantId = masterPlantDataRepo.findMaxPlantId()+1;
			
		}
		
		pkId = masterPlantDataRepo.findMaxPkId()+1;
		plant.setPlantId(plantId);
		plant.setPkId(pkId);
		plant.setCorpLogin(true);
		
		System.out.println("Plant Id "+plantId);
		System.out.println("PKId is "+pkId);
		
		return masterPlantDataRepo.save(plant);
	}
	

	@Override
	public Status updateUser(InsertNewUserBO insertNewUserBO) {
		Boolean rolePresent= false;
		List<Role> roleByName = roleRepository.findByRoleName(insertNewUserBO.getDesignation());
		Users newUser = insertNewUserBO.getUsers();
		if(!insertNewUserBO.getDesignation().equalsIgnoreCase(DocRole.INITIATOR) && !insertNewUserBO.getDesignation().equalsIgnoreCase(DocRole.ENDORSER) && !insertNewUserBO.getDesignation().equalsIgnoreCase(DocRole.TEMPORARY_DELEGATEE) &&  !insertNewUserBO.getDesignation().equalsIgnoreCase("Initiator & Endorser") ){
		/*	List<Role> role = roleRepository.findByRoleName(insertNewUserBO.getDesignation());
			if(role!=null && role.size()>0 && insertNewUserBO.getRoleId()!= role.get(0).getRoleId()){
				return new Status(3, "Role already exists");
			}*/
			
			
		
			if(roleByName.size()!= 0 || !(roleByName.isEmpty())){
				rolePresent= true;
				List<UserRoles> usrRole = userRoleRepo.findByRoleId(roleByName.get(0).getRoleId());
				if(usrRole.size()!= 0 || !(usrRole.isEmpty())){
					System.out.println("usrRole.get(0).getUserId()"+usrRole.get(0).getUserId());
					System.out.println("insertNewUserBO.getUser().getUserid()"+insertNewUserBO.getUsers().getUserId());
					if(usrRole.get(0).getUserId() != insertNewUserBO.getUsers().getUserId()){
						return new Status(3, "User already exists with this role");
					}
					
				}	
			}
		}
		else {
			if( !insertNewUserBO.getDesignation().equalsIgnoreCase(DocRole.TEMPORARY_DELEGATEE) ){
				newUser.setDeptId(0);
			}
		}
		
		
		//Create and save new user entity object
//		Users newUser = insertNewUserBO.getUsers();
		System.out.println("newUser >>>>"+newUser.getFirstName());
		
		userRepository.save(newUser);
		
		Role role = new Role();
		ArrayList<Role> roles = new ArrayList<Role>();
		UserRoles userRoles;
		if(!insertNewUserBO.getDesignation().equalsIgnoreCase(DocRole.INITIATOR) &&
				!insertNewUserBO.getDesignation().equalsIgnoreCase(DocRole.ENDORSER) &&
				!insertNewUserBO.getDesignation().equalsIgnoreCase("Initiator & Endorser") &&
				!insertNewUserBO.getDesignation().equalsIgnoreCase("Temporary-Delegatee") ){
		//Create and save role entity object
		//set roleId here after finding max
			
			
			List<UserRoles> roleList=userRoleRepo.findByRoleId(insertNewUserBO.getRoleId());
			
			if(roleList!=null && roleList.size()==1 && roleByName==null){
				role=roleRepository.findByRoleId(insertNewUserBO.getRoleId());
				role.setRoleName(insertNewUserBO.getDesignation());
				role.setDashboard_Type(insertNewUserBO.getDashboardType());
				roleRepository.save(role);
			}else if(roleByName!=null && roleByName.size()>0){
				role=roleRepository.findByRoleId(roleByName.get(0).getRoleId());
				role.setRoleName(insertNewUserBO.getDesignation());
				role.setDashboard_Type(insertNewUserBO.getDashboardType());
				roleRepository.save(role);
			}else{
				int roleId=roleRepository.findMaxRoleId() + 1;
				role.setRoleId(roleId);
				role.setRoleName(insertNewUserBO.getDesignation());
				role.setDashboard_Type(insertNewUserBO.getDashboardType());
				roleRepository.save(role);
			}
			
			roles.add(role);
			//Create and save userRole entity object
			
		}else if (insertNewUserBO.getDesignation().equalsIgnoreCase("Initiator & Endorser")){
			role =roleRepository.findByRoleName(DocRole.INITIATOR).get(0);
			roles.add(role);
			role =roleRepository.findByRoleName(DocRole.ENDORSER).get(0);
			roles.add(role);
			
		}else{
			role =roleRepository.findByRoleName(insertNewUserBO.getDesignation()).get(0);
			roles.add(role);
		}
		userRoleRepo.deleteByUserId(newUser.getUserId());
		for(Role rl : roles){
			//Create and save userRole entity object
			userRoles = new UserRoles();
			userRoles.setUserId(newUser.getUserId());
			userRoles.setRoleId(rl.getRoleId());
			userRoleRepo.save(userRoles);
		}

		List<UserEndorsers> usrEndorser = userEndorserRepo.findByUserId(newUser.getUserId());
		for(UserEndorsers usr : usrEndorser){
			userEndorserRepo.delete(usr);
		}
		List<UserEndorsers> usrEndorser1 = userEndorserRepo.findByUserId(newUser.getUserId());
		if(insertNewUserBO.getUser()!= null){
			Integer endorserId =insertNewUserBO.getUser().getUserid();
//			userEndorserRepo.deleteByUserId(newUser.getUserId());
			if(endorserId != -1){
				
				UserEndorsers userEndorser = new UserEndorsers();
				userEndorser.setUserId(newUser.getUserId());
				userEndorser.setEndorserId(endorserId);
				userEndorserRepo.save(userEndorser);
			}
		}
		//Save Immediate Endorser
		
		
		return new Status(1, "User Saved Successfully");
	}


	@Override
	public InsertNewUserBO getUserDetailsByEmailId(String emailId) {
		// TODO Auto-generated method stub
		Users users =null;
		InsertNewUserBO insertNewUserBO= new InsertNewUserBO();
		List<Users> userList=userRepository.findByEmailId(emailId);
		if(userList!=null && userList.size()>0){
			users=(Users)userList.get(0);
			if(users.getPlantId() != null || (users.getDeptId() != null && users.getDeptId().intValue() != 0) ){
				return insertNewUserBO;
			}
			List<UserEndorsers> usrEndorser=userEndorserRepo.findByUserId(users.getUserId());
			UsersBO user = new UsersBO();
			if(usrEndorser!=null && usrEndorser.size()>0){
				Users usr = userRepository.findByUserId(usrEndorser.get(0).getEndorserId());
				user.setName(usr.getFirstName() +" "+ usr.getLastName());
				user.setUserid(usr.getUserId());
			}else{
				user.setName("None");
				user.setUserid(-1);
			}
			insertNewUserBO.setUser(user);
			insertNewUserBO.setUsers(users);
		}
		if(users!=null){
		Boolean isSupportingEndorser =userList.get(0).getIsSupportingEndorser();
			if(isSupportingEndorser!=null){
			insertNewUserBO.setSupportingEndorser(isSupportingEndorser);
		}
		List<UserRoles> roleList=userRoleRepo.findByUserId(users.getUserId());
		if(roleList!=null && roleList.size()>1){
			insertNewUserBO.setDesignation("Initiator & Endorser");
			insertNewUserBO.setRoleId(roleList.get(0).getRoleId());
			insertNewUserBO.setDashboardType("InitEndorser");
		}else{
			Role role=roleRepository.findByRoleId(roleList.get(0).getRoleId());
			insertNewUserBO.setDesignation(role.getRoleName());
			insertNewUserBO.setRoleId(role.getRoleId());
			insertNewUserBO.setDashboardType(role.getDashboard_Type());
		}
		}
		return insertNewUserBO;
	}

	@Override
	public void deleteBusiness(Businesses business){
		int count = masterBusinessDataRepo.countByBusinessId(business.getBusinessId());
		if(count>1){
		masterBusinessDataRepo.delete(business);	
		}
	}
	
	public List<Sbus> findByNotCompanyIdAndNotBusinessId(int companyId,int businessId,List<Integer> sbuIdList){
		
		List<Sbus> sbs=masterSBUDataRepo.findbyNotInCompanyIdAndNotInBusinessId(companyId,businessId,sbuIdList);
			return sbs;
		}
	
	@Override
	public void deleteSbus(Sbus previousSbus){
		int count = masterSBUDataRepo.countBySbuId(previousSbus.getSbuId());
		if(count>1){
		masterSBUDataRepo.delete(previousSbus);	
		}
		
	}


	@Override
	public void saveSbus(Sbus newSbus) {
		masterSBUDataRepo.save(newSbus)	;	
	}
	
	@Override
	public void deletePlant(Plant plant) {
		int count = masterPlantDataRepo.countByPlantId(plant.getPlantId());
		if(count>1){
		masterPlantDataRepo.delete(plant);
		}
		
	}


	@Override
	public int findMaxPrimaryKeyofPlant() {
		
		return masterPlantDataRepo.findMaxPkId() ;
	}
	
	@Override
	public ArrayList<Sbus> findAllSbus(){
		
		return (ArrayList<Sbus>) masterSBUDataRepo.findAll();
	}
	
	@Override
	public int updateBusinessByBusinessId(String businessName,String abbreviation,int businessId){
		return masterBusinessDataRepo.updateBusinessNameByBusinessId(businessName, abbreviation, businessId);
	}
	
	@Override
	public int updateSbuBySbuId(String sbuName,String abbreviation,int sbuId){
		return masterSBUDataRepo.updateSbuNameBySbuId(sbuName, abbreviation, sbuId);
		
	}
	
	@Override
	public int updatePlantByPlantId(String plantName,String abbreviation,int plantId){
		return masterPlantDataRepo.updatePlantNameByPlantId(plantName, abbreviation, plantId);
	}


	@Override
	public int findDocumentMasterCountByCompanyId(int companyId) {
		return documentMasterRepo.findDocumentMasterCountByCompanyId(companyId);
	}


	@Override
	public int findDocumentMasterCountByBusinessId(int businessId) {
		return documentMasterRepo.findDocumentMasterCountByBusinessId(businessId);
	}


	@Override
	public int findDocumentMasterCountBySbuId(int sbuId) {
		return documentMasterRepo.findDocumentMasterCountBySbuId(sbuId);
	}


	@Override
	public int findDocumentMasterCountByPlantId(int plantId) {
		return documentMasterRepo.findDocumentMasterCountByPlantId(plantId);
	}


	@Override
	public List<Businesses> findAllBusiness() {
		// TODO Auto-generated method stub
		return masterBusinessDataRepo.findAll();
	}


	@Override
	public List<Plant> findByisCorp() {
		// TODO Auto-generated method stub
		return  masterPlantDataRepo.findByCorpLogin(true);
	}


	@Override
	public Sbus findBySbuId(Integer sbuId) {
		
		return masterSBUDataRepo.findBySbuId(sbuId);
	}


	@Override
	public Plant findByPlantId(Integer plantId) {
		
		return masterPlantDataRepo.findByPlantId(plantId);
	}


	@Override
	public List<Material> findMaterialByClusterId(Integer clusterId) {
		
		return masterMaterialDataRepository.findByClusterId(clusterId);
	}

	@Override
	public List<Plant> findByIsPlant() {
		
		return masterPlantDataRepo.findByPlantLogin(true);
	}

	
}
