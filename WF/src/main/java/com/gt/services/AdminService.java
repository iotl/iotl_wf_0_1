package com.gt.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.repository.query.Param;

import com.controllers.Status;
import com.gt.bo.InsertNewUserBO;
import com.gt.bo.UsersBO;
import com.gt.entity.Businesses;
import com.gt.entity.Companies;
import com.gt.entity.Material;
import com.gt.entity.Plant;
import com.gt.entity.Sbus;
import com.gt.entity.Users;

public interface AdminService {

	public abstract Material saveMaterial(Material material);

	public abstract List<Material> findByBusinessId(Integer businessId);
	

	public abstract List<Material> findAllMaterials();

	// public abstract Integer deleteByMaterialId(Integer materialId);

	public abstract void deleteByMaterialId(Integer materialId);

	public abstract List<Material> findByBusinessIdIsNull();
	
	public abstract List<Material> findMaterialByClusterId(Integer clusterId);

	public Status saveUser(InsertNewUserBO insertNewUserBO);

	public Integer findByMaxId();

	public ArrayList<UsersBO> getAllEndorsers();

	public String updateSampleDAO(String fileName);

	public Boolean isMaterialDuplicated(Material material);

	public List<Material> findByBusinessIdAndDeptId(Integer businessId,
			Integer deptId);
	
	public List<Companies> findAllCompany();

	public List<Businesses> findByCompanyId(int companyId);

	public List<Sbus> findByCompanyIdAndBusinessId(int companyId, int businessId);

	public List<Plant> findByCompanyIdAndBusinessIdAndSbuIdAndisCorp(
			int companyId, int businessId, int sbuId);

	public abstract Boolean isCompanyDuplicated(Companies company);

	public abstract Companies saveCompany(Companies company);

	public Businesses saveBusiness(Businesses business);

	public Sbus updateSbu(Sbus sbu);

	public Sbus saveSbu(Sbus sbu);
	
	public Sbus findBySbuId(Integer sbuId);
	
	public Plant findByPlantId(Integer plantId);

	public Integer findMaxBusinessId();

	public abstract Businesses findByBusinessNameAndAbbreviation(
			String businessName, String abbrevation);

	public Integer findMaxCompanyId();

	public Plant updatePlant(Plant plant);

	public Plant savePlant(Plant plant);

	public abstract Status updateUser(InsertNewUserBO insertNewUserBO);

	public abstract InsertNewUserBO getUserDetailsByEmailId(String emailId);

	public void deleteBusiness(Businesses business);

	public abstract List<Sbus> findByNotCompanyIdAndNotBusinessId(
			int companyId, int businessId, List<Integer> sbuIdList);

	public abstract void deleteSbus(Sbus previousSbus);

	public abstract void saveSbus(Sbus newSbus);

	public void deletePlant(Plant plant);

	public int findMaxPrimaryKeyofPlant();

	public List<Sbus> findAllSbus();

	public int updateBusinessByBusinessId(String businessName,
			String abbreviation, int businessId);

	public int updateSbuBySbuId(String sbuName, String abbreviation, int sbuId);

	public int updatePlantByPlantId(String plantName, String abbreviation,
			int plantId);

	public int findDocumentMasterCountByCompanyId(int companyId);

	public int findDocumentMasterCountByBusinessId(int businessId);

	public int findDocumentMasterCountBySbuId(int sbuId);

	public int findDocumentMasterCountByPlantId(int plantId);

	public List<Businesses> findAllBusiness();
	
	public List<Plant> findByIsPlant();
	
	public List<Plant> findByisCorp();

}
