package com.gt.services;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.controllers.Status;
import com.gt.bo.Items;
import com.gt.bo.SearchBO;
import com.gt.entity.Delegation;
import com.gt.entity.DocUserAction;
import com.gt.entity.DocumentMaster;
import com.gt.entity.Plant;
import com.gt.entity.RevenueProposal;
import com.gt.entity.RpTransportation;
import com.gt.entity.Users;
import com.gt.repository.DelegationRepository;
import com.gt.repository.DocUserActionRepository;
import com.gt.repository.DocumentMasterRepository;
import com.gt.repository.MasterPlantDataRepository;
import com.gt.repository.RPTransportationRepository;
import com.gt.repository.RevenueProposalRepository;
import com.gt.repository.UserRepository;
import com.gt.utils.ActionStatus;
import com.gt.utils.DocRole;
import com.gt.utils.DocStatus;
import com.gt.utils.Formats;
import com.gt.utils.PropsUtil;

@Component("itemsService")
@Transactional
public class ItemsServiceImpl implements ItemsService {

	private final DocUserActionRepository docUserActionRepo;
	private final DocumentMasterRepository documentMasterRepository;
	private final RevenueProposalRepository revenueProposalRepository;
	private final UserRepository userrepo;
	private final DelegationRepository delegateRepo;
	private final MasterPlantDataRepository masterPLantDataRepo;
	private final RPTransportationRepository rpTransportationRepository;
	
	@Autowired
	private UserService userService;

	@Autowired
	public ItemsServiceImpl(DocUserActionRepository docUserActionRepo,
			RevenueProposalRepository revenueProposalRepository,
			DocumentMasterRepository documentMasterRepository,
			UserRepository userrepo, DelegationRepository delegateRepo, MasterPlantDataRepository masterPLantDataRepo,RPTransportationRepository rpTransportationRepository) {
		this.docUserActionRepo = docUserActionRepo;
		this.revenueProposalRepository = revenueProposalRepository;
		this.documentMasterRepository = documentMasterRepository;
		this.userrepo = userrepo;
		this.delegateRepo = delegateRepo;
		this.masterPLantDataRepo =masterPLantDataRepo;
		this.rpTransportationRepository=rpTransportationRepository;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.gt.services.ItemsService1#getApprovedRPs(java.lang.Integer)
	 */
	@Override
	public ArrayList<Items> getApprovedRPs(Integer userId, String roleName,String plantName) {
		ArrayList<Items> items = new ArrayList<Items>();
		Items item;
		String rpVisiblity=PropsUtil.getValue("rp.visiblity.initiator");
		Users users=userService.findByUserid(userId);
		/*if(users!=null && users.getDeptId()!=null && users.getDeptId()!=0){
			rpVisiblity ="false";
		}*/	
		boolean isVisiblity= false;
if(rpVisiblity!=null && rpVisiblity.equalsIgnoreCase("true")){
	if (DocRole.INITIATOR.equalsIgnoreCase(roleName)
			|| DocRole.INITENDORSER.equalsIgnoreCase(roleName) || DocRole.VIEWER.equalsIgnoreCase(roleName)|| DocRole.PO_VIEWER.equalsIgnoreCase(roleName) ) {
		isVisiblity=true;
		List<DocumentMaster> docMasterList = null;
		System.out.println("users.getDeptId()"+users.getDeptId());
		Integer InitiatorPlantId=users.getPlantId();
		Plant plant= null;
		Integer clusterId= null;
		if(InitiatorPlantId!=null){
			plant = masterPLantDataRepo.findByPlantId(InitiatorPlantId);
		}
		if(plant!=null && InitiatorPlantId!=null){
			clusterId=plant.getClusterId();
		}
		if(plantName!=null && !plantName.equalsIgnoreCase("undefined")){
			docMasterList = documentMasterRepository
					.findByClusterId(clusterId);
		}else if(users.getDeptId()==null || users.getDeptId() ==0){
			docMasterList = documentMasterRepository.getDocListByDeptIdIsNullAndClusterId(users.getDeptId(), 0);
		}else{
			docMasterList = documentMasterRepository.getDocListByDeptIdAndClusterId(users.getDeptId(), 0);
		}
		
		List<String> approvedStatus = new ArrayList<String>();
		approvedStatus.add(DocStatus.APPROVED);
	
		
		if(DocRole.VIEWER.equalsIgnoreCase(roleName)){
			docMasterList = documentMasterRepository.findByStatusInAndDeptIdNot(approvedStatus, 1);
		}
		List<Integer> depts = new ArrayList<Integer>();
		depts.add(1);
		
		if(DocRole.PO_VIEWER.equalsIgnoreCase(roleName)){
			docMasterList = documentMasterRepository.findByStatusInAndDeptIdInAndInitiatorPlantIdIsNUll(approvedStatus,depts);
		}
		System.out.println("docMasterList"+docMasterList.size());
		for (DocumentMaster documentMaster : docMasterList) {
			Integer docId = documentMaster.getDocumentId();
			if (DocStatus.APPROVED.equals(documentMaster.getStatus())) {
				item = new Items();
				item.setIsExpress(documentMaster.getIsExpress());

				if (documentMaster.getStatus().equals(ActionStatus.RELEASED)) {
					item.setIsRelease(true);
				} else {
					item.setIsRelease(false);
				}
			/*	if(documentMaster.getDeptId()!=null && documentMaster.getDeptId()!=0){
					item.setIsDept(1);
				}else{
					item.setIsDept(0);
				}*/
				
				if(documentMaster.getDeptId()!=null){
					item.setIsDept(documentMaster.getDeptId());
				}else{
					item.setIsDept(0);
				}
				List<RpTransportation> transportList=rpTransportationRepository.findByDocumentId(documentMaster.getDocumentId());

				if (transportList != null && transportList.size() > 0) {
									item.setIsTransportationDraft(true);
								} else {
									item.setIsTransportationDraft(false);
								}
				Users usr = userrepo.findByUserId(documentMaster.getCreatedBy());
				item.setInitiatedBy(usr.getFirstName() + " "
						+ usr.getLastName());
				// item.setSrNo(i);
				String rpNumber=null;
				if(documentMaster.getDocumentType().equalsIgnoreCase("RP")){
					List<RevenueProposal> rps = revenueProposalRepository
							.findByDocumentId(docId);
					if(rps!=null && rps.size()>0){
						rpNumber=rps.get(0).getRpNumber();
					}
				}
				item.setRpNumber(rpNumber);
				item.setDocumentType(documentMaster.getDocumentType());

				List<DocUserAction> duasApproved = docUserActionRepo
						.findByDocumentIdAndActionStatus(docId,
								ActionStatus.APPROVED);
				if (duasApproved == null || duasApproved.size() == 0) {
					continue;
				}
				//System.out.println("duasApproved" + duasApproved.size());
				DocUserAction duaApproved = duasApproved.get(0);
				Users user = userrepo.findByUserId(duaApproved.getUserId());

				item.setPersonName(user.getFirstName() + " "
						+ user.getLastName());

				DateFormat dateFormat = new SimpleDateFormat(
						Formats.DATE_WITH_HOUR_MINUTE_SECOND);
				if (duaApproved.getActionTakenTimeStamp() != null) {
					String dateStr = dateFormat.format(duaApproved
							.getActionTakenTimeStamp());
					item.setRecvdDate(dateStr);
				}

				// item.setRecvdDate(duaApproved.getActionTakenTimeStamp().toString());
				item.setDocumentId(duaApproved.getDocumentId());
				items.add(item);
				// i++;
			}

		}
	} 
	
}
		if(!isVisiblity){
			List<DocUserAction> docUserActions = docUserActionRepo
					.findByUserIdAndActionStatusOrderByActionTakenTimeStampDesc(
							userId, ActionStatus.INITIATED);
			List<DocUserAction> docUserActionsEndorsed = docUserActionRepo
					.findByUserIdAndActionStatusAndDocRoleOrderByActionTakenTimeStampDesc(
							userId, ActionStatus.ENDORSED, DocRole.ENDORSER);
			ArrayList<DocUserAction> duas = new ArrayList<DocUserAction>();
			if (docUserActions != null && docUserActions.size() != 0) {
				for (DocUserAction dua : docUserActions) {
					duas.add(dua);
				}
			}
			if (docUserActionsEndorsed != null
					&& docUserActionsEndorsed.size() != 0) {
				for (DocUserAction dua : docUserActionsEndorsed) {
					duas.add(dua);
				}
			}
			// int i = 1;
			for (DocUserAction dua : duas) {
				Integer docId = dua.getDocumentId();
				DocumentMaster dm = (DocumentMaster) documentMasterRepository
						.findByDocumentId(docId);
				if (DocStatus.APPROVED.equals(dm.getStatus())) {
					item = new Items();
					item.setIsExpress(dm.getIsExpress());

					if (dm.getStatus().equals(ActionStatus.RELEASED)) {
						item.setIsRelease(true);
					} else {
						item.setIsRelease(false);
					}
					
					/*if(dm.getDeptId()!=null && dm.getDeptId()!=0){
						item.setIsDept(true);
					}else{
						item.setIsDept(false);
					}*/
					if(dm.getDeptId()!=null){
						item.setIsDept(dm.getDeptId());
					}else{
						item.setIsDept(0);
					}
					List<RpTransportation> transportList=rpTransportationRepository.findByDocumentId(dua.getDocumentId());

					if (transportList != null && transportList.size() > 0) {
										item.setIsTransportationDraft(true);
									} else {
										item.setIsTransportationDraft(false);
									}
					// item.setSrNo(i);
					String rpNumber=null;
					if(dm.getDocumentType().equalsIgnoreCase("RP")){
						List<RevenueProposal> rps = revenueProposalRepository
								.findByDocumentId(docId);
						if(rps!=null && rps.size()>0){
							rpNumber=rps.get(0).getRpNumber();
						}
					}
					item.setRpNumber(rpNumber);
					item.setDocumentType(dm.getDocumentType());

					List<DocUserAction> duasApproved = docUserActionRepo
							.findByDocumentIdAndActionStatus(docId,
									ActionStatus.APPROVED);

					DocUserAction duaApproved = duasApproved.get(0);
					Users user = userrepo.findByUserId(duaApproved.getUserId());

					item.setPersonName(user.getFirstName() + " "
							+ user.getLastName());

					DateFormat dateFormat = new SimpleDateFormat(
							Formats.DATE_WITH_HOUR_MINUTE_SECOND);
					if (duaApproved.getActionTakenTimeStamp() != null) {
						String dateStr = dateFormat.format(duaApproved
								.getActionTakenTimeStamp());
						item.setRecvdDate(dateStr);
					}

					// item.setRecvdDate(duaApproved.getActionTakenTimeStamp().toString());
					item.setDocumentId(duaApproved.getDocumentId());
					items.add(item);
					// i++;
				}

			}
		}
		Set<Items> itemsSet = new HashSet<Items>(items);
		items = new ArrayList<Items>();
		items.addAll(itemsSet);
		Collections.sort(items, new Comparator<Items>() {

			@Override
			public int compare(Items dua1, Items dua2) {
				String stringDate1 = dua1.getRecvdDate();
				String stringDate2 = dua2.getRecvdDate();
				SimpleDateFormat sdf = new SimpleDateFormat(
						Formats.DATE_WITH_HOUR_MINUTE_SECOND);
				Date date1 = new Date();
				Date date2 = new Date();
				try {
					date1 = sdf.parse(stringDate1);
					date2 = sdf.parse(stringDate2);

					if (date1.compareTo(date2) > 0)
						return -1;
					else if (date1.compareTo(date2) < 0)
						return 1;

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return 0;
			}
		});
		for (int i = 0; i < items.size(); i++) {
			items.get(i).setSrNo(i + 1);
		}
		return items;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.gt.services.ItemsService1#getReleasedRPs(java.lang.Integer)
	 */
	@Override
	public ArrayList<Items> getReleasedRPs(Integer userId,String roleName,String plantName) {
		ArrayList<Items> items = new ArrayList<Items>();
		Items item;
		String rpVisiblity=PropsUtil.getValue("rp.visiblity.initiator");
		Users users=userService.findByUserid(userId);
		/*if(users!=null && users.getDeptId()!=null && users.getDeptId()!=0){
			rpVisiblity ="false";
		}	*/
		boolean isVisiblity= false;
if(rpVisiblity!=null && rpVisiblity.equalsIgnoreCase("true")){
	if (DocRole.INITIATOR.equalsIgnoreCase(roleName)
			|| DocRole.INITENDORSER.equalsIgnoreCase(roleName) || DocRole.VIEWER.equalsIgnoreCase(roleName)|| DocRole.PO_VIEWER.equalsIgnoreCase(roleName)) {
		isVisiblity=true;
		Integer InitiatorPlantId=users.getPlantId();
		Plant plant= null;
		Integer clusterId= null;
		if(InitiatorPlantId!=null){
			plant = masterPLantDataRepo.findByPlantId(InitiatorPlantId);
		}
		if(plant!=null && InitiatorPlantId!=null){
			clusterId=plant.getClusterId();
		}
		List<DocumentMaster> docMasterList = null;
		if(plantName!=null && !plantName.equalsIgnoreCase("undefined")){
			docMasterList = documentMasterRepository
					.findByClusterId(clusterId);
		}else if(users.getDeptId()==null){
			docMasterList = documentMasterRepository.getDocListByDeptIdIsNullAndClusterId(users.getDeptId(), 0);
		}else{
			docMasterList = documentMasterRepository.getDocListByDeptIdAndClusterId(users.getDeptId(), 0);
		}
		List<String> releasedStatus = new ArrayList<String>();
		releasedStatus.add(DocStatus.RELEASED);
		if( DocRole.VIEWER.equalsIgnoreCase(roleName)){
			docMasterList = documentMasterRepository.findByStatusInAndDeptIdNot(releasedStatus,1);
		}
		List<Integer> depts = new ArrayList<Integer>();
		depts.add(1);
		
		if(DocRole.PO_VIEWER.equalsIgnoreCase(roleName)){
			docMasterList = documentMasterRepository.findByStatusInAndDeptIdInAndInitiatorPlantIdIsNUll(releasedStatus,depts);
		}
		
		for (DocumentMaster documentMaster : docMasterList) {
			Integer docId = documentMaster.getDocumentId();
			if (DocStatus.RELEASED.equals(documentMaster.getStatus())) {
				item = new Items();
				item.setIsExpress(documentMaster.getIsExpress());
		/*		if (documentMaster.getStatus().equals(ActionStatus.RELEASED)) {
					item.setIsRelease(true);
				} else {
					item.setIsRelease(false);
				}*/
				
				List<DocUserAction> isApprovedRPList = docUserActionRepo
						.findByDocumentIdAndActionStatus(documentMaster.getDocumentId(), "Approved");
				List<DocUserAction> isReleasedRPList = docUserActionRepo
						.findByDocumentIdAndActionStatus(documentMaster.getDocumentId(), "Released");
				if (isApprovedRPList != null && isApprovedRPList.size() > 0 && documentMaster.getStatus().equals(ActionStatus.RELEASED)) {
					
					item.setIsRelease(true);
				}  else  {
					item.setIsRelease(false);
				}
				
				if(isReleasedRPList!=null && isReleasedRPList.size() > 0 && documentMaster.getProposalType()!=null && documentMaster.getProposalType().equalsIgnoreCase("Final")){
					item.setIsFinalApproval(true);
				}else{
					item.setIsFinalApproval(false);
				}
					
				
				
				
				
				/*if(documentMaster.getDeptId()!=null && documentMaster.getDeptId()!=0){
					item.setIsDept(true);
				}else{
					item.setIsDept(false);
				}*/
				if(documentMaster.getDeptId()!=null){
					item.setIsDept(documentMaster.getDeptId());
				}else{
					item.setIsDept(0);
				}
				List<RpTransportation> transportList=rpTransportationRepository.findByDocumentId(documentMaster.getDocumentId());

				if (transportList != null && transportList.size() > 0) {
									item.setIsTransportationDraft(true);
								} else {
									item.setIsTransportationDraft(false);
								}
				Users usr = userrepo.findByUserId(documentMaster.getCreatedBy());
				item.setInitiatedBy(usr.getFirstName() + " "
						+ usr.getLastName());
				// item.setSrNo(i);
				String rpNumber=null;
				if(documentMaster.getDocumentType().equalsIgnoreCase("RP")){
					List<RevenueProposal> rps = revenueProposalRepository
							.findByDocumentId(docId);
					if(rps!=null && rps.size()>0){
						rpNumber=rps.get(0).getRpNumber();
					}
				}
				item.setRpNumber(rpNumber);
				item.setDocumentType(documentMaster.getDocumentType());

				List<DocUserAction> duasApproved = docUserActionRepo
						.findByDocumentIdAndActionStatus(docId,
								ActionStatus.RELEASED);
				if (documentMaster.getStatus().equalsIgnoreCase(DocStatus.RELEASED)
						&& duasApproved.size() > 0) {

					DocUserAction duaApproved = duasApproved.get(0);
					
					Users user = null;
					if(documentMaster.getCategoryId()!=null && documentMaster.getCategoryId() ==1){
						List<DocUserAction> duact=docUserActionRepo.findByDocumentIdAndActionStatusOrderByDocActionSerialNumberDesc(documentMaster.getDocumentId(),ActionStatus.APPROVED);
						if(duact!=null && duact.size() >1){
							user = userrepo
										.findByUserId(duact.get(0).getUserId());
						}else{
							 user = userrepo.findByUserId(duaApproved.getUserId());
								}
						
					}else{
						 user = userrepo.findByUserId(duaApproved.getUserId());
					}

					item.setPersonName(user.getFirstName() + " "
							+ user.getLastName());

					DateFormat dateFormat = new SimpleDateFormat(
							Formats.DATE_WITH_HOUR_MINUTE_SECOND);
					if (duaApproved.getActionTakenTimeStamp() != null) {
						String dateStr = dateFormat.format(duaApproved
								.getActionTakenTimeStamp());
						item.setRecvdDate(dateStr);
					}

					// item.setRecvdDate(duaApproved.getActionTakenTimeStamp().toString());
					item.setDocumentId(duaApproved.getDocumentId());
					items.add(item);
					// i++;
				}

			}

		}
	}
}
		if(!isVisiblity){
		List<DocUserAction> docUserActions = docUserActionRepo
				.findByUserIdAndActionStatusOrderByActionTakenTimeStampDesc(
						userId, ActionStatus.INITIATED);
		List<DocUserAction> docUserActionsEndorsed = docUserActionRepo
				.findByUserIdAndActionStatusAndDocRoleOrderByActionTakenTimeStampDesc(
						userId, ActionStatus.ENDORSED, DocRole.ENDORSER);
		ArrayList<DocUserAction> duas = new ArrayList<DocUserAction>();
		if (docUserActions != null && docUserActions.size() != 0) {
			for (DocUserAction dua : docUserActions) {
				duas.add(dua);
			}
		}
		if (docUserActionsEndorsed != null
				&& docUserActionsEndorsed.size() != 0) {
			for (DocUserAction dua : docUserActionsEndorsed) {
				duas.add(dua);
			}
		}
		// int i = 1;
		for (DocUserAction dua : duas) {
			Integer docId = dua.getDocumentId();
			DocumentMaster dm = (DocumentMaster) documentMasterRepository
					.findByDocumentId(docId);
			if (DocStatus.RELEASED.equals(dm.getStatus())) {
				item = new Items();
				item.setIsExpress(dm.getIsExpress());
			/*	if (dm.getStatus().equals(ActionStatus.RELEASED)) {
					item.setIsRelease(true);
				} else {
					item.setIsRelease(false);
				}*/

				List<DocUserAction> isApprovedRPList = docUserActionRepo
						.findByDocumentIdAndActionStatus(dm.getDocumentId(), "Approved");
				List<DocUserAction> isReleasedRPList = docUserActionRepo
						.findByDocumentIdAndActionStatus(dm.getDocumentId(), "Released");
				if (isApprovedRPList != null && isApprovedRPList.size() > 0 && dm.getStatus().equals(ActionStatus.RELEASED)) {
					
					item.setIsRelease(true);
				}  else  {
					item.setIsRelease(false);
				}
				
				if(isReleasedRPList!=null && isReleasedRPList.size() > 0 && dm.getProposalType()!=null && dm.getProposalType().equalsIgnoreCase("Final")){
					item.setIsFinalApproval(true);
				}else{
					item.setIsFinalApproval(false);
				}
				
				
				
				/*if(dm.getDeptId()!=null && dm.getDeptId()!=0){
					item.setIsDept(true);
				}else{
					item.setIsDept(false);
				}*/
				if(dm.getDeptId()!=null){
					item.setIsDept(dm.getDeptId());
				}else{
					item.setIsDept(0);
				}
				List<RpTransportation> transportList=rpTransportationRepository.findByDocumentId(dua.getDocumentId());

				if (transportList != null && transportList.size() > 0) {
									item.setIsTransportationDraft(true);
								} else {
									item.setIsTransportationDraft(false);
								}
				// item.setSrNo(i);
				String rpNumber=null;
				if(dm.getDocumentType().equalsIgnoreCase("RP")){
					List<RevenueProposal> rps = revenueProposalRepository
							.findByDocumentId(docId);
					if(rps!=null && rps.size()>0){
						rpNumber=rps.get(0).getRpNumber();
					}
				}
				item.setRpNumber(rpNumber);
				item.setDocumentType(dm.getDocumentType());

				List<DocUserAction> duasApproved = docUserActionRepo
						.findByDocumentIdAndActionStatus(docId,
								ActionStatus.RELEASED);
				if (dm.getStatus().equalsIgnoreCase(DocStatus.RELEASED)
						&& duasApproved.size() > 0) {

					DocUserAction duaApproved = duasApproved.get(0);
					Users user = null;
					if(dm.getCategoryId()!=null && dm.getCategoryId() ==1){
						List<DocUserAction> duact=docUserActionRepo.findByDocumentIdAndActionStatusOrderByDocActionSerialNumberDesc(dm.getDocumentId(),ActionStatus.APPROVED);
						if(duact!=null && duact.size() >1){
							user = userrepo
										.findByUserId(duact.get(0).getUserId());
						}else{
							 user = userrepo.findByUserId(duaApproved.getUserId());
								}
						
					}else{
						 user = userrepo.findByUserId(duaApproved.getUserId());
					}


					item.setPersonName(user.getFirstName() + " "
							+ user.getLastName());

					DateFormat dateFormat = new SimpleDateFormat(
							Formats.DATE_WITH_HOUR_MINUTE_SECOND);
					if (duaApproved.getActionTakenTimeStamp() != null) {
						String dateStr = dateFormat.format(duaApproved
								.getActionTakenTimeStamp());
						item.setRecvdDate(dateStr);
					}

					// item.setRecvdDate(duaApproved.getActionTakenTimeStamp().toString());
					item.setDocumentId(duaApproved.getDocumentId());
					items.add(item);
					// i++;
				}

			}

		}
		}
		Set<Items> itemsSet = new HashSet<Items>(items);
		items = new ArrayList<Items>();
		items.addAll(itemsSet);
		Collections.sort(items, new Comparator<Items>() {

			@Override
			public int compare(Items dua1, Items dua2) {
				String stringDate1 = dua1.getRecvdDate();
				String stringDate2 = dua2.getRecvdDate();
				SimpleDateFormat sdf = new SimpleDateFormat(
						Formats.DATE_WITH_HOUR_MINUTE_SECOND);
				Date date1 = new Date();
				Date date2 = new Date();
				try {
					date1 = sdf.parse(stringDate1);
					date2 = sdf.parse(stringDate2);

					if (date1.compareTo(date2) > 0)
						return -1;
					else if (date1.compareTo(date2) < 0)
						return 1;

				} catch (ParseException e) {

					e.printStackTrace();
				}

				return 0;
			}
		});
		for (int i = 0; i < items.size(); i++) {
			items.get(i).setSrNo(i + 1);
		}
		System.out.println("items size "+items.size());
		return items;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.gt.services.ItemsService1#getRPsApprovedByMe(java.lang.Integer)
	 */
	@Override
	public ArrayList<Items> getRPsApprovedByMe(Integer userId) {
		ArrayList<Items> items = new ArrayList<Items>();
		Items item;
		List<DocUserAction> docUserActions = docUserActionRepo
				.findByUserIdAndActionStatusOrderByActionTakenTimeStampDesc(
						userId, ActionStatus.APPROVED);
		List<DocUserAction> docUserActionsEndorsed = docUserActionRepo
				.findByUserIdAndActionStatusAndDocRoleOrderByActionTakenTimeStampDesc(
						userId, ActionStatus.ENDORSED, DocRole.ENDORSER);
		List<DocUserAction> docUserActionsController = docUserActionRepo
				.findByUserIdAndActionStatusAndDocRoleOrderByActionTakenTimeStampDesc(
						userId, ActionStatus.ENDORSED, DocRole.CONTROLLER);

		ArrayList<DocUserAction> duas = new ArrayList<DocUserAction>();
		if (docUserActions != null && docUserActions.size() != 0) {
			for (DocUserAction dua : docUserActions) {
				duas.add(dua);
			}
		}
		if (docUserActionsEndorsed != null
				&& docUserActionsEndorsed.size() != 0) {
			for (DocUserAction dua : docUserActionsEndorsed) {
				duas.add(dua);
			}
		}
		if (docUserActionsController != null
				&& docUserActionsController.size() != 0) {
			for (DocUserAction dua : docUserActionsController) {
				duas.add(dua);
			}
		}

		for (DocUserAction dua : duas) {
			Integer docId = dua.getDocumentId();
			DocumentMaster dm = documentMasterRepository
					.findByDocumentId(docId);

			item = new Items();
			/*if(dm.getDeptId()!=null && dm.getDeptId()!=0){
				item.setIsDept(true);
			}else{
				item.setIsDept(false);
			}*/
			if(dm.getDeptId()!=null){
				item.setIsDept(dm.getDeptId());
			}else{
				item.setIsDept(0);
			}
			List<RpTransportation> transportList=rpTransportationRepository.findByDocumentId(dua.getDocumentId());

			if (transportList != null && transportList.size() > 0) {
								item.setIsTransportationDraft(true);
							} else {
								item.setIsTransportationDraft(false);
							}
			item.setIsExpress(dm.getIsExpress());
			String rpNumber=null;
			if(dm.getDocumentType().equalsIgnoreCase("RP")){
				List<RevenueProposal> rps = revenueProposalRepository
						.findByDocumentId(docId);
				if(rps!=null && rps.size()>0){
					rpNumber=rps.get(0).getRpNumber();
				}
			}
			item.setRpNumber(rpNumber);
			item.setDocumentType(dm.getDocumentType());
			List<DocUserAction> duasApproved = docUserActionRepo
					.findByDocumentIdAndActionStatus(docId,
							ActionStatus.APPROVED);
			if (dm.getStatus().equalsIgnoreCase(DocStatus.APPROVED)
					&& duasApproved.size() > 0) {
				DocUserAction duaApproved = duasApproved.get(0);
				Users user = userrepo.findByUserId(duaApproved.getUserId());

				item.setPersonName(user.getFirstName() + " "
						+ user.getLastName());

				DateFormat dateFormat = new SimpleDateFormat(
						Formats.DATE_WITH_HOUR_MINUTE_SECOND);
				if (duaApproved.getActionTakenTimeStamp() != null) {
					String dateStr = dateFormat.format(duaApproved
							.getActionTakenTimeStamp());
					item.setRecvdDate(dateStr);
				}

				item.setDocumentId(duaApproved.getDocumentId());
				items.add(item);
			}

		}
		Set<Items> itemsSet = new HashSet<Items>(items);
		items = new ArrayList<Items>();
		items.addAll(itemsSet);
		Collections.sort(items, new Comparator<Items>() {

			@Override
			public int compare(Items dua1, Items dua2) {
				String stringDate1 = dua1.getRecvdDate();
				String stringDate2 = dua2.getRecvdDate();
				SimpleDateFormat sdf = new SimpleDateFormat(
						Formats.DATE_WITH_HOUR_MINUTE_SECOND);
				Date date1 = new Date();
				Date date2 = new Date();
				try {
					date1 = sdf.parse(stringDate1);
					date2 = sdf.parse(stringDate2);

					if (date1.compareTo(date2) > 0)
						return -1;
					else if (date1.compareTo(date2) < 0)
						return 1;

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return 0;
			}
		});
		for (int i = 0; i < items.size(); i++) {
			items.get(i).setSrNo(i + 1);
		}

		return items;
	}

	/*
	 * @Override public ArrayList<Items> getRPsApprovedByMe(Integer userId) {
	 * ArrayList<Items> items = new ArrayList<Items>(); Items item;
	 * List<DocUserAction> docUserActions =
	 * docUserActionRepo.findByUserIdAndActionStatusOrderByActionTakenTimeStampDesc
	 * (userId, ActionStatus.APPROVED); int i = 1; for(DocUserAction dua :
	 * docUserActions){ Integer docId = dua.getDocumentId(); DocumentMaster dm =
	 * (DocumentMaster) documentMasterRepository.findByDocumentId(docId);
	 * 
	 * item = new Items(); item.setIsExpress(dm.getIsExpress());
	 * item.setSrNo(i); List<RevenueProposal> rps =
	 * revenueProposalRepository.findByDocumentId(docId);
	 * item.setRpNumber(rps.get(0).getRpNumber());
	 * 
	 * DateFormat dateFormat = new
	 * SimpleDateFormat(Formats.DATE_WITH_HOUR_MINUTE_SECOND);
	 * if(dua.getActionPendingTimeStamp()!=null){ String
	 * dateStr=dateFormat.format(dua.getActionTakenTimeStamp());
	 * item.setRecvdDate(dateStr); }
	 * 
	 * // item.setRecvdDate(dua.getActionTakenTimeStamp().toString());
	 * item.setDocumentId(docId); items.add(item); i++; // } }
	 * 
	 * return items; }
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.gt.services.ItemsService1#getRPsReleasedByMe(java.lang.Integer)
	 */
	@Override
	public ArrayList<Items> getRPsReleasedByMe(Integer userId) {
		ArrayList<Items> items = new ArrayList<Items>();
		Items item;
		List<DocUserAction> docUserActions = docUserActionRepo
				.findByUserIdAndActionStatusOrderByActionTakenTimeStampDesc(
						userId, ActionStatus.RELEASED);
		List<DocUserAction> docUserActionsEndorsed = docUserActionRepo
				.findByUserIdAndActionStatusAndDocRoleOrderByActionTakenTimeStampDesc(
						userId, ActionStatus.ENDORSED, DocRole.ENDORSER);
		List<DocUserAction> docUserActionsController = docUserActionRepo
				.findByUserIdAndActionStatusAndDocRoleOrderByActionTakenTimeStampDesc(
						userId, ActionStatus.ENDORSED, DocRole.CONTROLLER);
		List<DocUserAction> docUserActionsApproved = docUserActionRepo
				.findByUserIdAndActionStatusAndDocRoleOrderByActionTakenTimeStampDesc(
						userId, ActionStatus.APPROVED, DocRole.APPROVER);
		List<DocUserAction> docUserActionsControllerApproved = docUserActionRepo
				.findByUserIdAndActionStatusAndDocRoleOrderByActionTakenTimeStampDesc(
						userId, ActionStatus.APPROVED, DocRole.CONTROLLER);

		ArrayList<DocUserAction> duas = new ArrayList<DocUserAction>();
		if (docUserActions != null && docUserActions.size() != 0) {
			for (DocUserAction dua : docUserActions) {
				duas.add(dua);
			}
		}
		if (docUserActionsEndorsed != null
				&& docUserActionsEndorsed.size() != 0) {
			for (DocUserAction dua : docUserActionsEndorsed) {
				duas.add(dua);
			}
		}
		if (docUserActionsController != null
				&& docUserActionsController.size() != 0) {
			for (DocUserAction dua : docUserActionsController) {
				duas.add(dua);
			}
		}

		if (docUserActionsApproved != null
				&& docUserActionsApproved.size() != 0) {
			for (DocUserAction dua : docUserActionsApproved) {
				duas.add(dua);
			}
		}
		if (docUserActionsControllerApproved != null
				&& docUserActionsControllerApproved.size() != 0) {
			for (DocUserAction dua : docUserActionsControllerApproved) {
				duas.add(dua);
			}
		}
		for (DocUserAction dua : duas) {
			// int i = 1;
			// for(DocUserAction dua : docUserActions){

			Integer docId = dua.getDocumentId();
			DocumentMaster dm = documentMasterRepository
					.findByDocumentId(docId);

			List<DocUserAction> duaRelease = docUserActionRepo
					.findByDocumentIdAndActionStatus(docId,
							ActionStatus.RELEASED);

			if (dm.getStatus().equalsIgnoreCase(DocStatus.RELEASED)
					&& duaRelease.size() > 0) {
				item = new Items();
				
//				List<DocUserAction> isApprovedRPList = docUserActionRepo
//						.findByDocumentIdAndActionStatus(dm.getDocumentId(), "Approved");
				List<DocUserAction> isReleasedRPList = docUserActionRepo
						.findByDocumentIdAndActionStatus(dm.getDocumentId(), "Released");
		/*		if (isApprovedRPList != null && isApprovedRPList.size() > 0 && documentMaster.getStatus().equals(ActionStatus.RELEASED)) {
					
					item.setIsRelease(true);
				}  else  {
					item.setIsRelease(false);
				}*/
				
				if(isReleasedRPList!=null && isReleasedRPList.size() > 0 && dm.getProposalType()!=null && dm.getProposalType().equalsIgnoreCase("Final")){
					item.setIsFinalApproval(true);
				}else{
					item.setIsFinalApproval(false);
				}
					
				Users usr = null;
				if(dm.getCategoryId()!=null && dm.getCategoryId() ==1){
					List<DocUserAction> duact=docUserActionRepo.findByDocumentIdAndActionStatusOrderByDocActionSerialNumberDesc(dm.getDocumentId(),ActionStatus.APPROVED);
					if(duact!=null && duact.size() >1){
						 usr = userrepo
									.findByUserId(duact.get(0).getUserId());
					}else{
						 usr = userrepo
									.findByUserId(duaRelease.get(0).getUserId());
							}
					
				}else{
				 usr = userrepo
						.findByUserId(duaRelease.get(0).getUserId());
				}
				
				item.setIsExpress(dm.getIsExpress());
				// item.setSrNo(i);
				String rpNumber=null;
				if(dm.getDocumentType().equalsIgnoreCase("RP")){
					List<RevenueProposal> rps = revenueProposalRepository
							.findByDocumentId(docId);
					if(rps!=null && rps.size()>0){
						rpNumber=rps.get(0).getRpNumber();
					}
				}
				item.setRpNumber(rpNumber);
				item.setDocumentType(dm.getDocumentType());
				item.setPersonName(usr.getFirstName() + " " + usr.getLastName());
				DateFormat dateFormat = new SimpleDateFormat(
						Formats.DATE_WITH_HOUR_MINUTE_SECOND);
				if (duaRelease.get(0).getActionPendingTimeStamp() != null) {
					String dateStr = dateFormat.format(duaRelease.get(0)
							.getActionTakenTimeStamp());
					item.setRecvdDate(dateStr);
				}
			/*	if(dm.getDeptId()!=null && dm.getDeptId()!=0){
					item.setIsDept(true);
				}else{
					item.setIsDept(false);
				}*/
				if(dm.getDeptId()!=null){
					item.setIsDept(dm.getDeptId());
				}else{
					item.setIsDept(0);
				}
				List<RpTransportation> transportList=rpTransportationRepository.findByDocumentId(dua.getDocumentId());

				if (transportList != null && transportList.size() > 0) {
									item.setIsTransportationDraft(true);
								} else {
									item.setIsTransportationDraft(false);
								}
				// item.setRecvdDate(dua.getActionTakenTimeStamp().toString());
				item.setDocumentId(docId);
				items.add(item);
				// i++;
				// }
			}
		}

		Set<Items> itemsSet = new HashSet<Items>(items);
		items = new ArrayList<Items>();
		items.addAll(itemsSet);
		Collections.sort(items, new Comparator<Items>() {

			@Override
			public int compare(Items dua1, Items dua2) {
				String stringDate1 = dua1.getRecvdDate();
				String stringDate2 = dua2.getRecvdDate();
				SimpleDateFormat sdf = new SimpleDateFormat(
						Formats.DATE_WITH_HOUR_MINUTE_SECOND);
				Date date1 = new Date();
				Date date2 = new Date();
				try {
					date1 = sdf.parse(stringDate1);
					date2 = sdf.parse(stringDate2);

					if (date1.compareTo(date2) > 0)
						return -1;
					else if (date1.compareTo(date2) < 0)
						return 1;

				} catch (ParseException e) {

					e.printStackTrace();
				}

				return 0;
			}
		});
		for (int i = 0; i < items.size(); i++) {
			items.get(i).setSrNo(i + 1);
		}

		return items;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.gt.services.ItemsService1#getRejectedRPs(java.lang.Integer)
	 */
	@Override
	public ArrayList<Items> getRejectedRPs(Integer userId) {
		ArrayList<Items> items = new ArrayList<Items>();
		Items item;
		List<DocUserAction> docUserActions = docUserActionRepo
				.findByUserIdAndActionStatusOrderByActionTakenTimeStampDesc(
						userId, ActionStatus.REJECTED);
		List<DocUserAction> docUserActionsEndorsed = docUserActionRepo
				.findByUserIdAndActionStatusAndDocRoleOrderByActionTakenTimeStampDesc(
						userId, ActionStatus.ENDORSED, DocRole.ENDORSER);
		List<DocUserAction> docUserActionsController = docUserActionRepo
				.findByUserIdAndActionStatusAndDocRoleOrderByActionTakenTimeStampDesc(
						userId, ActionStatus.ENDORSED, DocRole.CONTROLLER);

		ArrayList<DocUserAction> duas = new ArrayList<DocUserAction>();
		if (docUserActions != null && docUserActions.size() != 0) {
			for (DocUserAction dua : docUserActions) {
				duas.add(dua);
			}
		}
		if (docUserActionsEndorsed != null
				&& docUserActionsEndorsed.size() != 0) {
			for (DocUserAction dua : docUserActionsEndorsed) {
				duas.add(dua);
			}
		}
		if (docUserActionsController != null
				&& docUserActionsController.size() != 0) {
			for (DocUserAction dua : docUserActionsController) {
				duas.add(dua);
			}
		}

		for (DocUserAction dua : duas) {
			Integer docId = dua.getDocumentId();
			DocumentMaster dm = (DocumentMaster) documentMasterRepository
					.findByDocumentId(docId);

			item = new Items();
			item.setIsExpress(dm.getIsExpress());
			String rpNumber=null;
			if(dm.getDocumentType().equalsIgnoreCase("RP")){
				List<RevenueProposal> rps = revenueProposalRepository
						.findByDocumentId(docId);
				if(rps!=null && rps.size()>0){
					rpNumber=rps.get(0).getRpNumber();
				}
			}
			item.setRpNumber(rpNumber);
			item.setDocumentType(dm.getDocumentType());
		/*	if (dm.getStatus().equals(ActionStatus.RELEASED)) {
				item.setIsRelease(true);
			} else {
				item.setIsRelease(false);
			}*/
			List<DocUserAction> isApprovedRPList = docUserActionRepo
					.findByDocumentIdAndActionStatus(dm.getDocumentId(), "Approved");
			List<DocUserAction> isReleasedRPList = docUserActionRepo
					.findByDocumentIdAndActionStatus(dm.getDocumentId(), "Released");
			if (isApprovedRPList != null && isApprovedRPList.size() > 0 && dm.getStatus().equals(ActionStatus.RELEASED)) {
				
				item.setIsRelease(true);
			}  else  {
				item.setIsRelease(false);
			}
			
			if(isReleasedRPList!=null && isReleasedRPList.size() > 0 && dm.getProposalType()!=null && dm.getProposalType().equalsIgnoreCase("Final")){
				item.setIsFinalApproval(true);
			}else{
				item.setIsFinalApproval(false);
			}
				
			
			
			
			
			/*if(dm.getDeptId()!=null && dm.getDeptId()!=0){
				item.setIsDept(true);
			}else{
				item.setIsDept(false);
			}*/
			if(dm.getDeptId()!=null){
				item.setIsDept(dm.getDeptId());
			}else{
				item.setIsDept(0);
			}
			List<RpTransportation> transportList=rpTransportationRepository.findByDocumentId(dua.getDocumentId());

			if (transportList != null && transportList.size() > 0) {
								item.setIsTransportationDraft(true);
							} else {
								item.setIsTransportationDraft(false);
							}
			List<DocUserAction> duasApproved = docUserActionRepo
					.findByDocumentIdAndActionStatus(docId,
							ActionStatus.REJECTED);
			if (duasApproved.size() > 0) {
				DocUserAction duaApproved = duasApproved.get(0);
				Users user = userrepo.findByUserId(duaApproved.getUserId());

				item.setPersonName(user.getFirstName() + " "
						+ user.getLastName());

				DateFormat dateFormat = new SimpleDateFormat(
						Formats.DATE_WITH_HOUR_MINUTE_SECOND);
				if (duaApproved.getActionTakenTimeStamp() != null) {
					String dateStr = dateFormat.format(duaApproved
							.getActionTakenTimeStamp());
					item.setRecvdDate(dateStr);
				}

				item.setDocumentId(duaApproved.getDocumentId());
				items.add(item);
			}

		}
		Set<Items> itemsSet = new HashSet<Items>(items);
		items = new ArrayList<Items>();
		items.addAll(itemsSet);
		Collections.sort(items, new Comparator<Items>() {

			@Override
			public int compare(Items dua1, Items dua2) {
				String stringDate1 = dua1.getRecvdDate();
				String stringDate2 = dua2.getRecvdDate();
				SimpleDateFormat sdf = new SimpleDateFormat(
						Formats.DATE_WITH_HOUR_MINUTE_SECOND);
				Date date1 = new Date();
				Date date2 = new Date();
				try {
					date1 = sdf.parse(stringDate1);
					date2 = sdf.parse(stringDate2);

					if (date1.compareTo(date2) > 0)
						return -1;
					else if (date1.compareTo(date2) < 0)
						return 1;

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return 0;
			}
		});
		for (int i = 0; i < items.size(); i++) {
			items.get(i).setSrNo(i + 1);
		}

		return items;

		/*
		 * int i = 1; for(DocUserAction dua : docUserActions){ Integer docId =
		 * dua.getDocumentId(); DocumentMaster dm = (DocumentMaster)
		 * documentMasterRepository.findByDocumentId(docId);
		 * 
		 * item = new Items(); item.setIsExpress(dm.getIsExpress());
		 * item.setSrNo(i); List<RevenueProposal> rps =
		 * revenueProposalRepository.findByDocumentId(docId);
		 * item.setRpNumber(rps.get(0).getRpNumber());
		 * 
		 * DateFormat dateFormat = new
		 * SimpleDateFormat(Formats.DATE_WITH_HOUR_MINUTE_SECOND);
		 * if(dua.getActionTakenTimeStamp()!=null){ String
		 * dateStr=dateFormat.format(dua.getActionTakenTimeStamp());
		 * item.setRecvdDate(dateStr); } item.setDocumentId(docId);
		 * items.add(item); i++;
		 * 
		 * }
		 * 
		 * return items;
		 */

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.gt.services.ItemsService1#getReferedRPs(java.lang.Integer)
	 */
	@Override
	public ArrayList<Items> getReferedRPs(Integer userId) {
		ArrayList<Items> items = new ArrayList<Items>();
		// HashMap<String, Items> rpNumberVsItem = new HashMap<String, Items>();
		HashSet<String> rpNumbers = new HashSet<String>();
		Items item;
		List<DocUserAction> docUserActions = docUserActionRepo
				.findByUserIdAndActionStatusOrderByActionTakenTimeStampDesc(
						userId, ActionStatus.REFERRED);
		int i = 1;
		for (DocUserAction dua : docUserActions) {
			item = new Items();

			Integer docId = dua.getDocumentId();
			DocumentMaster dm = (DocumentMaster) documentMasterRepository
					.findByDocumentId(docId);
		/*	if (dm.getStatus().equals(ActionStatus.RELEASED)) {
				item.setIsRelease(true);
			} else {
				item.setIsRelease(false);
			}*/
			
			List<DocUserAction> isApprovedRPList = docUserActionRepo
					.findByDocumentIdAndActionStatus(dm.getDocumentId(), "Approved");
			List<DocUserAction> isReleasedRPList = docUserActionRepo
					.findByDocumentIdAndActionStatus(dm.getDocumentId(), "Released");
			if (isApprovedRPList != null && isApprovedRPList.size() > 0 && dm.getStatus().equals(ActionStatus.RELEASED)) {
				
				item.setIsRelease(true);
			}  else  {
				item.setIsRelease(false);
			}
			
			if(isReleasedRPList!=null && isReleasedRPList.size() > 0 && dm.getProposalType()!=null && dm.getProposalType().equalsIgnoreCase("Final")){
				item.setIsFinalApproval(true);
			}else{
				item.setIsFinalApproval(false);
			}
				
			
			
			
		/*	if(dm.getDeptId()!=null && dm.getDeptId()!=0){
				item.setIsDept(true);
			}else{
				item.setIsDept(false);
			}*/
			if(dm.getDeptId()!=null){
				item.setIsDept(dm.getDeptId());
			}else{
				item.setIsDept(0);
			}
			List<RpTransportation> transportList=rpTransportationRepository.findByDocumentId(dua.getDocumentId());

			if (transportList != null && transportList.size() > 0) {
								item.setIsTransportationDraft(true);
							} else {
								item.setIsTransportationDraft(false);
							}
			Integer referedSrNO = (dua.getDocActionSerialNumber() + 1);
			// DocUserAction duaRefered =
			// docUserActionRepo.findByDocumentIdAndDocActionSerialNumber(docId,
			// referedSrNO);
			// Integer currentlyWith =duaRefered.getUserId();
			Integer currentlyWith = dm.getCurrentlyWith();
			if (currentlyWith != null) {
				Users user = userrepo.findByUserId(currentlyWith);
				item.setPersonName(user.getFirstName() + " "
						+ user.getLastName());
			} else {
				item.setPersonName("-");
			}
			item.setIsExpress(dm.getIsExpress());
			item.setSrNo(i);
			String rpNumber=null;
			if(dm.getDocumentType().equalsIgnoreCase("RP")){
				List<RevenueProposal> rps = revenueProposalRepository
						.findByDocumentId(docId);
				if(rps!=null && rps.size()>0){
					rpNumber=rps.get(0).getRpNumber();
				}
			}
			//item.setRpNumber(rpNumber);
			item.setDocumentType(dm.getDocumentType());
			if (rpNumbers.contains(rpNumber))
				continue;
			item.setRpNumber(rpNumber);
			item.setDocumentId(docId);

			DateFormat dateFormat = new SimpleDateFormat(
					Formats.DATE_WITH_HOUR_MINUTE_SECOND);
			if (dua.getActionTakenTimeStamp() != null) {
				String dateStr = dateFormat.format(dua
						.getActionTakenTimeStamp());
				item.setRecvdDate(dateStr);
			}

			// if(!rpNumberVsItem.containsKey(rpNumber)){
			rpNumbers.add(rpNumber);
			items.add(item);
			// }
			i++;
		}

		Set<Items> itemsSet = new HashSet<Items>(items);
		items = new ArrayList<Items>();
		items.addAll(itemsSet);
		Collections.sort(items, new Comparator<Items>() {

			@Override
			public int compare(Items dua1, Items dua2) {
				String stringDate1 = dua1.getRecvdDate();
				String stringDate2 = dua2.getRecvdDate();
				SimpleDateFormat sdf = new SimpleDateFormat(
						Formats.DATE_WITH_HOUR_MINUTE_SECOND);
				Date date1 = new Date();
				Date date2 = new Date();
				try {
					date1 = sdf.parse(stringDate1);
					date2 = sdf.parse(stringDate2);

					if (date1.compareTo(date2) > 0)
						return -1;
					else if (date1.compareTo(date2) < 0)
						return 1;

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return 0;
			}
		});
		return items;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.gt.services.ItemsService1#getPendingActions(java.lang.Integer)
	 */
	@Override
	public ArrayList<Items> getPendingActions(Integer userId) {
		Boolean delegated= false;
		List<Delegation> delegates = delegateRepo.findByUserId(userId);
		Users usr ;
		ArrayList<Items> items = new ArrayList<Items>();
		Items item;
		String delegationEnabled=PropsUtil.getValue("rp.delegation");	
		if(delegates.size()>0 && delegationEnabled.equalsIgnoreCase("enable") && !delegated){
			for(Delegation del :delegates){
				Date fromDate =del.getFromDate();
				Date toDate = del.getToDate();
				Date currentDate =new Date();
				
				boolean getMin = (currentDate.compareTo(toDate)<=0 &&currentDate.compareTo(fromDate)>=0);
				if(getMin){
					if(del.getIsActive()){
					delegated =true;
					/*item = new Items();
					item.setIsDelegated(delegated);
					items.add(item);*/
					return null;
					}
				}
			}
		}
	
		

		long dayTime = new Date().getTime();
		List<DocUserAction> docUserActions = docUserActionRepo
				.findByUserIdAndActionStatusOrderByActionPendingTimeStampDesc(
						userId, ActionStatus.PENDING);
		List<DocUserAction> docUserActionParallelPending = docUserActionRepo
				.findByUserIdAndActionStatusOrderByActionPendingTimeStampDesc(
						userId, ActionStatus.PENDING_PARALLEL);
		List<DocUserAction> docUserActionExpressPending = docUserActionRepo
				.findByUserIdAndActionStatusOrderByActionPendingTimeStampDesc(
						userId, ActionStatus.PENDING_EXPRESS);
		/*
		 * ArrayList<DocUserAction> duas = new ArrayList<DocUserAction>();
		 * if(docUserActions!=null && docUserActions.size()!=0){
		 * for(DocUserAction dua:docUserActions){ duas.add(dua); } }
		 * if(docUserActionParallelPending!=null &&
		 * docUserActionParallelPending.size()!=0){ for(DocUserAction
		 * dua:docUserActionParallelPending){ duas.add(dua); } }
		 * if(docUserActionExpressPending!=null &&
		 * docUserActionExpressPending.size()!=0){ for(DocUserAction
		 * dua:docUserActionExpressPending){ duas.add(dua); } }
		 */
		// int i = 1;
		// if(duas.size()!=0){
		for (DocUserAction dua : docUserActionExpressPending) {
			if ("Yes".equalsIgnoreCase(dua.getIsDraft())) {
				continue;
			}
			item = new Items();
			Users user = userrepo.findByUserId(dua.getFromUserAction());
			Integer docId = dua.getDocumentId();
			DocumentMaster dm = (DocumentMaster) documentMasterRepository
					.findByDocumentId(docId);
		/*	if(dm.getDeptId()!=null && dm.getDeptId()!=0){
				item.setIsDept(true);
			}else{
				item.setIsDept(false);
			}*/
			if(dm.getDeptId()!=null){
				item.setIsDept(dm.getDeptId());
			}else{
				item.setIsDept(0);
			}
			List<RpTransportation> transportList=rpTransportationRepository.findByDocumentId(dua.getDocumentId());

			if (transportList != null && transportList.size() > 0) {
								item.setIsTransportationDraft(true);
							} else {
								item.setIsTransportationDraft(false);
							}

			if(dua.getUserId().intValue()== userId.intValue() && DocRole.INITIATOR.equalsIgnoreCase(dua.getDocRole())){
				List<DocUserAction> docuaList=docUserActionRepo.findByDocumentIdAndActionStatus(docId, ActionStatus.INITIATED);
				if(docuaList!=null && docuaList.size()>0){
					item.setIsRefferedToInitiator(true);	
				}else{
					item.setIsRefferedToInitiator(false);	
				}
					
				}else{
					item.setIsRefferedToInitiator(false);
				}
			List<DocUserAction> isApprovedRPList = docUserActionRepo
					.findByDocumentIdAndActionStatus(docId, "Approved");
			List<DocUserAction> isReleasedRPList = docUserActionRepo
					.findByDocumentIdAndActionStatus(docId, "Released");
			if (isApprovedRPList != null && isApprovedRPList.size() > 0) {
				
				item.setIsRelease(true);
			}  else  {
				item.setIsRelease(false);
			}
			
			if(isReleasedRPList!=null && isReleasedRPList.size() > 0){
				item.setIsFinalApproval(true);
			}else{
				item.setIsFinalApproval(false);
			}
				
				
				
				
			item.setIsExpress(dm.getIsExpress());
			// item.setSrNo(i);
			String rpNumber=null;
			if(dm.getDocumentType().equalsIgnoreCase("RP")){
				List<RevenueProposal> rps = revenueProposalRepository
						.findByDocumentId(docId);
				if(rps!=null && rps.size()>0){
					rpNumber=rps.get(0).getRpNumber();
				}
			}
			item.setRpNumber(rpNumber);
			item.setDocumentType(dm.getDocumentType());
			long pendingTime = dua.getActionPendingTimeStamp().getTime();
			item.setPendingSince(String.valueOf(daysBetween(pendingTime,
					dayTime)) + " day(s)");
			item.setPersonName(user.getFirstName() + " " + user.getLastName());

			DateFormat dateFormat = new SimpleDateFormat(
					Formats.DATE_WITH_HOUR_MINUTE_SECOND);
			if (dua.getActionPendingTimeStamp() != null) {
				String dateStr = dateFormat.format(dua
						.getActionPendingTimeStamp());
				item.setRecvdDate(dateStr);
			}

			// item.setRecvdDate(dua.getActionPendingTimeStamp().toString());
			item.setDocumentId(dua.getDocumentId());
			item.setPending_Express(true);
			items.add(item);
			// i++;
		}
		// }

		if (docUserActionParallelPending.size() != 0) {
			for (DocUserAction dua : docUserActionParallelPending) {
				if ("Yes".equalsIgnoreCase(dua.getIsDraft())) {
					continue;
				}
				item = new Items();
				Users user = userrepo.findByUserId(dua.getFromUserAction());
				Integer docId = dua.getDocumentId();
				DocumentMaster dm = (DocumentMaster) documentMasterRepository
						.findByDocumentId(docId);
				/*if(dm.getDeptId()!=null && dm.getDeptId()!=0){
					item.setIsDept(true);
				}else{
					item.setIsDept(false);
				}*/
				if(dm.getDeptId()!=null){
					item.setIsDept(dm.getDeptId());
				}else{
					item.setIsDept(0);
				}
				List<RpTransportation> transportList=rpTransportationRepository.findByDocumentId(dua.getDocumentId());

				if (transportList != null && transportList.size() > 0) {
									item.setIsTransportationDraft(true);
								} else {
									item.setIsTransportationDraft(false);
								}
				
				if(dua.getUserId().intValue() == userId.intValue() && DocRole.INITIATOR.equalsIgnoreCase(dua.getDocRole())){
					List<DocUserAction> docuaList=docUserActionRepo.findByDocumentIdAndActionStatus(docId, ActionStatus.INITIATED);
					if(docuaList!=null && docuaList.size()>0){
						item.setIsRefferedToInitiator(true);	
					}else{
						item.setIsRefferedToInitiator(false);	
					}
						
					}else{
						item.setIsRefferedToInitiator(false);
					}
				List<DocUserAction> isReleaseRPList = docUserActionRepo
						.findByDocumentIdAndActionStatus(docId, "Approved");
				if (isReleaseRPList != null && isReleaseRPList.size() > 0) {
					item.setIsRelease(true);
				} else {
					item.setIsRelease(false);
				}
				item.setIsExpress(dm.getIsExpress());
				// item.setSrNo(i);
				String rpNumber=null;
				if(dm.getDocumentType().equalsIgnoreCase("RP")){
					List<RevenueProposal> rps = revenueProposalRepository
							.findByDocumentId(docId);
					if(rps!=null && rps.size()>0){
						rpNumber=rps.get(0).getRpNumber();
					}
				}
				item.setRpNumber(rpNumber);
				item.setDocumentType(dm.getDocumentType());
				long pendingTime = dua.getActionPendingTimeStamp().getTime();
				item.setPendingSince(String.valueOf(daysBetween(pendingTime,
						dayTime)) + " day(s)");
				item.setPersonName(user.getFirstName() + " "
						+ user.getLastName());

				DateFormat dateFormat = new SimpleDateFormat(
						Formats.DATE_WITH_HOUR_MINUTE_SECOND);
				if (dua.getActionPendingTimeStamp() != null) {
					String dateStr = dateFormat.format(dua
							.getActionPendingTimeStamp());
					item.setRecvdDate(dateStr);
				}

				// item.setRecvdDate(dua.getActionPendingTimeStamp().toString());
				item.setDocumentId(dua.getDocumentId());
				item.setPending_Express(false);
				items.add(item);
				// i++;
			}
		}
		if (docUserActions.size() != 0) {
			for (DocUserAction dua : docUserActions) {
				if ("Yes".equalsIgnoreCase(dua.getIsDraft())) {
					continue;
				}
				item = new Items();
				Users user = userrepo.findByUserId(dua.getFromUserAction());
				Integer docId = dua.getDocumentId();
				DocumentMaster dm = (DocumentMaster) documentMasterRepository
						.findByDocumentId(docId);
				/*if(dm.getDeptId()!=null && dm.getDeptId()!=0){
					item.setIsDept(true);
				}else{
					item.setIsDept(false);
				}*/
				if(dm.getDeptId()!=null){
					item.setIsDept(dm.getDeptId());
				}else{
					item.setIsDept(0);
				}
				List<RpTransportation> transportList=rpTransportationRepository.findByDocumentId(dua.getDocumentId());

				if (transportList != null && transportList.size() > 0) {
									item.setIsTransportationDraft(true);
								} else {
									item.setIsTransportationDraft(false);
								}
				System.out.println(dua.getUserId().intValue()+"dua.getUserId().intValue()");
				System.out.println(userId.intValue()+"userId.intValue()");
				if(dua.getUserId().intValue() == userId.intValue() && DocRole.INITIATOR.equalsIgnoreCase(dua.getDocRole())){
					List<DocUserAction> docuaList=docUserActionRepo.findByDocumentIdAndActionStatus(docId, ActionStatus.INITIATED);
					if(docuaList!=null && docuaList.size()>0){
						item.setIsRefferedToInitiator(true);	
					}else{
						item.setIsRefferedToInitiator(false);	
					}
						
					}else{
						item.setIsRefferedToInitiator(false);
					}
				
				
				
				List<DocUserAction> isApprovedRPList = docUserActionRepo
						.findByDocumentIdAndActionStatus(docId, "Approved");
				List<DocUserAction> isReleasedRPList = docUserActionRepo
						.findByDocumentIdAndActionStatus(docId, "Released");
				if (isApprovedRPList != null && isApprovedRPList.size() > 0) {
					
					item.setIsRelease(true);
				}  else  {
					item.setIsRelease(false);
				}
				
				if(isReleasedRPList!=null && isReleasedRPList.size() > 0 ){
					item.setIsFinalApproval(true);
				}else{
					item.setIsFinalApproval(false);
				}
				
				
				item.setIsExpress(dm.getIsExpress());
				// item.setSrNo(i);
				String rpNumber=null;
				if(dm.getDocumentType().equalsIgnoreCase("RP")){
					List<RevenueProposal> rps = revenueProposalRepository
							.findByDocumentId(docId);
					if(rps!=null && rps.size()>0){
						rpNumber=rps.get(0).getRpNumber();
					}
				}
				item.setRpNumber(rpNumber);
				item.setDocumentType(dm.getDocumentType());
				long pendingTime = dua.getActionPendingTimeStamp().getTime();
				item.setPendingSince(String.valueOf(daysBetween(pendingTime,
						dayTime)) + " day(s)");
				item.setPersonName(user.getFirstName() + " "
						+ user.getLastName());

				DateFormat dateFormat = new SimpleDateFormat(
						Formats.DATE_WITH_HOUR_MINUTE_SECOND);
				if (dua.getActionPendingTimeStamp() != null) {
					String dateStr = dateFormat.format(dua
							.getActionPendingTimeStamp());
					item.setRecvdDate(dateStr);
				}

				// item.setRecvdDate(dua.getActionPendingTimeStamp().toString());
				item.setDocumentId(dua.getDocumentId());
				item.setPending_Express(false);
				items.add(item);
				// i++;
			}

		}
		Set<Items> itemsSet = new HashSet<Items>(items);
		items = new ArrayList<Items>();
		items.addAll(itemsSet);
		Collections.sort(items, new Comparator<Items>() {

			@Override
			public int compare(Items dua1, Items dua2) {
				String stringDate1 = dua1.getRecvdDate();
				String stringDate2 = dua2.getRecvdDate();
				SimpleDateFormat sdf = new SimpleDateFormat(
						Formats.DATE_WITH_HOUR_MINUTE_SECOND);
				Date date1 = new Date();
				Date date2 = new Date();
				try {
					date1 = sdf.parse(stringDate1);
					date2 = sdf.parse(stringDate2);

					if (date1.compareTo(date2) > 0)
						return -1;
					else if (date1.compareTo(date2) < 0)
						return 1;

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return 0;
			}
		});

		for (int i = 0; i < items.size(); i++) {
			items.get(i).setSrNo(i + 1);
		}
		return items;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.gt.services.ItemsService1#getInProcessRPs(java.lang.Integer)
	 */
	@Override
	public ArrayList<Items> getInProcessRPs(Integer userId, String roleName) {
		ArrayList<Items> items = new ArrayList<Items>();
		Items item;
		long dayTime = new Date().getTime();
		List<DocUserAction> docUserActions = docUserActionRepo
				.findByUserIdAndActionStatusOrderByActionPendingTimeStampDesc(
						userId, ActionStatus.INITIATED);
		List<DocUserAction> docUserActionsEndorsed = docUserActionRepo
				.findByUserIdAndActionStatusAndDocRoleOrderByActionTakenTimeStampDesc(
						userId, ActionStatus.ENDORSED, DocRole.ENDORSER);
		List<DocUserAction> docUserActionsEndorsedByDelegatee = docUserActionRepo
				.findByActionTakenBy(userId);
		Users usr = userrepo.findByUserId(userId);
		Integer InitiatorPlantId=usr.getPlantId();
		Plant plant= null;
		Integer clusterId= null;
		if(InitiatorPlantId!=null){
			plant = masterPLantDataRepo.findByPlantId(InitiatorPlantId);
		}
		if(plant!=null && InitiatorPlantId!=null){
			clusterId=plant.getClusterId();
		}
	
		List<DocumentMaster> docMasterList;
		String rpVisiblity=PropsUtil.getValue("rp.visiblity.initiator");
		Users users=userService.findByUserid(userId);
		/*if(users!=null && users.getDeptId()!=null && users.getDeptId()!=0){
			rpVisiblity ="false";
		}	*/
		if(rpVisiblity.equalsIgnoreCase("true")){
			if(InitiatorPlantId!=null){
				docMasterList = documentMasterRepository.findByClusterId(clusterId);
			}else if(users.getDeptId()==null || users.getDeptId() ==0){
				docMasterList = documentMasterRepository.getDocListByDeptIdIsNullAndClusterId(users.getDeptId(), 0);
			}else{
				docMasterList = documentMasterRepository.getDocListByDeptIdAndClusterId(users.getDeptId(), 0);
			}
			List<String> inProcessStatus = new ArrayList<String>();
			inProcessStatus.add(DocStatus.IN_PROCESS);
			inProcessStatus.add(DocStatus.IN_PROCESS_PARALLEL);
			
			if(DocRole.VIEWER.equalsIgnoreCase(roleName)){
				docMasterList = documentMasterRepository.findByStatusInAndDeptIdNot(inProcessStatus,1);
			}
			
			List<Integer> depts = new ArrayList<Integer>();
			depts.add(1);
			
			if(DocRole.PO_VIEWER.equalsIgnoreCase(roleName)){
				docMasterList = documentMasterRepository.findByStatusInAndDeptIdInAndInitiatorPlantIdIsNUll(inProcessStatus,depts);
			}
		
			for (DocumentMaster dm : docMasterList) {
				if (DocStatus.IN_PROCESS.equals(dm.getStatus())
						|| DocStatus.IN_PROCESS_PARALLEL.equals(dm.getStatus())) {
					item = new Items();
					item.setIsExpress(dm.getIsExpress());
					/*if (dm.getStatus().equals(ActionStatus.RELEASED)) {
						item.setIsRelease(true);
					} else {
						item.setIsRelease(false);
					}*/
					
					List<DocUserAction> isApprovedRPList = docUserActionRepo
							.findByDocumentIdAndActionStatus(dm.getDocumentId(), "Approved");
					List<DocUserAction> isReleasedRPList = docUserActionRepo
							.findByDocumentIdAndActionStatus(dm.getDocumentId(), "Released");
					if (isApprovedRPList != null && isApprovedRPList.size() > 0 && dm.getStatus().equals(ActionStatus.RELEASED)) {
						
						item.setIsRelease(true);
					}  else  {
						item.setIsRelease(false);
					}
					
					if(isReleasedRPList!=null && isReleasedRPList.size() > 0 && dm.getProposalType()!=null && dm.getProposalType().equalsIgnoreCase("Final")){
						item.setIsFinalApproval(true);
					}else{
						item.setIsFinalApproval(false);
					}
						
					
					
					
					
					/*if(dm.getDeptId()!=null && dm.getDeptId()!=0){
						item.setIsDept(true);
					}else{
						item.setIsDept(false);
					}*/
					if(dm.getDeptId()!=null){
						item.setIsDept(dm.getDeptId());
					}else{
						item.setIsDept(0);
					}
					
					List<RpTransportation> transportList=rpTransportationRepository.findByDocumentId(dm.getDocumentId());
System.out.println("transportList.size() >>>>>>>"+transportList.size());
					if (transportList != null && transportList.size() > 0) {
										item.setIsTransportationDraft(true);
									} else {
										item.setIsTransportationDraft(false);
									}
					String rpNumber=null;
					if(dm.getDocumentType().equalsIgnoreCase("RP")){
						List<RevenueProposal> rps = revenueProposalRepository
								.findByDocumentId(dm.getDocumentId());
						if(rps!=null && rps.size()>0){
							rpNumber=rps.get(0).getRpNumber();
						}
					}
					item.setRpNumber(rpNumber);
					item.setDocumentType(dm.getDocumentType());
					
					Users user = userrepo.findByUserId(dm.getCurrentlyWith());
					item.setPersonName(user.getFirstName() + " "
							+ user.getLastName());
					
					 user = userrepo.findByUserId(dm.getCreatedBy());
					item.setInitiatedBy(user.getFirstName() + " "
							+ user.getLastName());
					DateFormat dateFormat = new SimpleDateFormat(
							Formats.DATE_WITH_HOUR_MINUTE_SECOND);
					if (dm.getCreatedTimeStamp() != null) {
						String dateStr = dateFormat.format(dm.getCreatedTimeStamp());
						item.setRecvdDate(dateStr);
					}
					item.setDocumentId(dm.getDocumentId());
					long pendingTime = dm.getCreatedTimeStamp().getTime();
					item.setPendingSince(String.valueOf(daysBetween(pendingTime,
							dayTime)) + " day(s)");
					// }
					items.add(item);
				
			}
		}
	}
	else{
		
		ArrayList<DocUserAction> duas = new ArrayList<DocUserAction>();
		if (docUserActions != null && docUserActions.size() != 0) {
			for (DocUserAction dua : docUserActions) {
				duas.add(dua);
			}
		}
		if (docUserActionsEndorsed != null
				&& docUserActionsEndorsed.size() != 0) {
			for (DocUserAction dua : docUserActionsEndorsed) {
				duas.add(dua);
			}
		}

		if (docUserActionsEndorsedByDelegatee != null
				&& docUserActionsEndorsedByDelegatee.size() != 0) {
			for (DocUserAction dua : docUserActionsEndorsedByDelegatee) {
				duas.add(dua);
			}
		}

		for (DocUserAction dua : duas) {
			Integer docId = dua.getDocumentId();
			DocumentMaster dm = (DocumentMaster) documentMasterRepository
					.findByDocumentId(docId);
			
			if (DocStatus.IN_PROCESS.equals(dm.getStatus())
					|| DocStatus.IN_PROCESS_PARALLEL.equals(dm.getStatus())) {
				item = new Items();
				/*if(dm.getDeptId()!=null && dm.getDeptId()!=0){
					item.setIsDept(true);
				}else{
					item.setIsDept(false);
				}*/
				
				if(dm.getDeptId()!=null){
					item.setIsDept(dm.getDeptId());
				}else{
					item.setIsDept(0);
				}
				
				List<RpTransportation> transportList=rpTransportationRepository.findByDocumentId(dua.getDocumentId());
				System.out.println("transportList.size() qwerty>>>>>>>"+transportList.size());
				if (transportList != null && transportList.size() > 0) {
									item.setIsTransportationDraft(true);
								} else {
									item.setIsTransportationDraft(false);
								}
				item.setIsExpress(dm.getIsExpress());
				if (dm.getStatus().equals(ActionStatus.RELEASED)) {
					item.setIsRelease(true);
				} else {
					item.setIsRelease(false);
				}
				String rpNumber=null;
				if(dm.getDocumentType().equalsIgnoreCase("RP")){
					List<RevenueProposal> rps = revenueProposalRepository
							.findByDocumentId(docId);
					if(rps!=null && rps.size()>0){
						rpNumber=rps.get(0).getRpNumber();
					}
				}
				item.setRpNumber(rpNumber);
				item.setDocumentType(dm.getDocumentType());
				/*
				 * List<DocUserAction> duasInitiated = docUserActionRepo.
				 * findByDocumentIdAndActionStatusAndUserIdOrderByDocActionSerialNumberDesc
				 * (docId, ActionStatus.INITIATED, userId); List<DocUserAction>
				 * duasEndorsed = docUserActionRepo.
				 * findByDocumentIdAndActionStatusAndUserIdOrderByDocActionSerialNumberDesc
				 * (docId, ActionStatus.ENDORSED, userId); List<DocUserAction>
				 * duasEndorsedDelegated =docUserActionRepo.
				 * findByDocumentIdAndActionTakenByOrderByDocActionSerialNumberDesc
				 * (docId,userId); DocUserAction docUserAction = null;
				 * if(duasInitiated.size()!=0){ docUserAction =
				 * duasInitiated.get(0); }else if(duasEndorsed.size()!=0){
				 * docUserAction = duasEndorsed.get(0); }else
				 * if(duasEndorsedDelegated.size()!=0){ docUserAction =
				 * duasEndorsedDelegated.get(0); } if(docUserAction!=null){
				 */
				Users user = userrepo.findByUserId(dm.getCurrentlyWith());
				item.setPersonName(user.getFirstName() + " "
						+ user.getLastName());
				DateFormat dateFormat = new SimpleDateFormat(
						Formats.DATE_WITH_HOUR_MINUTE_SECOND);
				if (dua.getActionTakenTimeStamp() != null) {
					String dateStr = dateFormat.format(dua
							.getActionTakenTimeStamp());
					item.setRecvdDate(dateStr);
				}
				item.setDocumentId(dua.getDocumentId());
				long pendingTime = dua.getActionPendingTimeStamp().getTime();
				item.setPendingSince(String.valueOf(daysBetween(pendingTime,
						dayTime)) + " day(s)");
				// }
				items.add(item);

			}
		}
	}
		Set<Items> itemsSet = new HashSet<Items>(items);
		items = new ArrayList<Items>();
		items.addAll(itemsSet);
		Collections.sort(items, new Comparator<Items>() {

			@Override
			public int compare(Items dua1, Items dua2) {
				String stringDate1 = dua1.getRecvdDate();
				String stringDate2 = dua2.getRecvdDate();
				SimpleDateFormat sdf = new SimpleDateFormat(
						"dd/MM/yyyy HH:mm:ss");
				Date date1 = new Date();
				Date date2 = new Date();
				try {
					date1 = sdf.parse(stringDate1);
					date2 = sdf.parse(stringDate2);

					if (date1.compareTo(date2) > 0)
						return -1;
					else if (date1.compareTo(date2) < 0)
						return 1;

				} catch (ParseException e) {

					e.printStackTrace();
				}

				return 0;
			}
		});
		for (int i = 0; i < items.size(); i++) {
			items.get(i).setSrNo(i + 1);
		}

		/*
		 * int i = 1; for(DocUserAction dua : docUserActions){ Integer docId =
		 * dua.getDocumentId(); DocumentMaster dm = (DocumentMaster)
		 * documentMasterRepository.findByDocumentId(docId);
		 * if(DocStatus.IN_PROCESS.equals(dm.getStatus()) ||
		 * DocStatus.IN_PROCESS_PARALLEL.equals(dm.getStatus())){ item = new
		 * Items(); item.setIsExpress(dm.getIsExpress()); item.setSrNo(i);
		 * List<RevenueProposal> rps =
		 * revenueProposalRepository.findByDocumentId(docId);
		 * item.setRpNumber(rps.get(0).getRpNumber());
		 * 
		 * List<DocUserAction> duasInitiated =
		 * docUserActionRepo.findByDocumentIdAndActionStatus(docId,
		 * ActionStatus.INITIATED);
		 * 
		 * DocUserAction duaInitiated = duasInitiated.get(0);
		 * 
		 * Users user = userrepo.findByUserId(dm.getCurrentlyWith());
		 * 
		 * item.setPersonName(user.getFirstName()+" "+user.getLastName());
		 * 
		 * DateFormat dateFormat = new
		 * SimpleDateFormat(Formats.DATE_WITH_HOUR_MINUTE_SECOND);
		 * if(duaInitiated.getActionTakenTimeStamp()!=null){ String
		 * dateStr=dateFormat.format(duaInitiated.getActionTakenTimeStamp());
		 * item.setRecvdDate(dateStr); }
		 * 
		 * //
		 * item.setRecvdDate(duaInitiated.getActionTakenTimeStamp().toString());
		 * item.setDocumentId(duaInitiated.getDocumentId());
		 * 
		 * 
		 * long pendingTime=duaInitiated.getActionPendingTimeStamp().getTime();
		 * item.setPendingSince(String.valueOf(daysBetween(pendingTime,
		 * dayTime))+"day(s)");
		 * 
		 * 
		 * items.add(item); i++; }
		 * 
		 * 
		 * }
		 */

		return items;
	}

	private int daysBetween(long t1, long t2) {
		return (int) ((t2 - t1) / (1000 * 60 * 60 * 24));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.gt.services.ItemsService1#getEndorsedRPs(java.lang.Integer)
	 */
	@Override
	public ArrayList<Items> getEndorsedRPs(Integer userId) {
		ArrayList<Items> items = new ArrayList<Items>();
		HashSet<String> rpNumbers = new HashSet<String>();
		Items item;
		// List<DocUserAction> docUserActions =
		// docUserActionRepo.findByUserIdAndActionStatusOrderByActionPendingTimeStampDesc(userId,
		// "Endorsed");
		List<DocUserAction> docUserActions = docUserActionRepo
				.findByUserIdAndActionStatusOrderByActionTakenTimeStampDesc(
						userId, ActionStatus.ENDORSED);
		// int i = 1;
		for (DocUserAction dua : docUserActions) {
			item = new Items();

			Integer docId = dua.getDocumentId();
			DocumentMaster dm = (DocumentMaster) documentMasterRepository
					.findByDocumentId(docId);
			Integer currentlyId = dm.getCurrentlyWith();
			if (currentlyId != null) {
				Users user = userrepo.findByUserId(currentlyId);
				item.setPersonName(user.getFirstName() + " "
						+ user.getLastName());
			} else {
				item.setPersonName("-");
			}
			/*if(dm.getDeptId()!=null && dm.getDeptId()!=0){
				item.setIsDept(true);
			}else{
				item.setIsDept(false);
			}*/
			if(dm.getDeptId()!=null){
				item.setIsDept(dm.getDeptId());
			}else{
				item.setIsDept(0);
			}
			List<RpTransportation> transportList=rpTransportationRepository.findByDocumentId(dua.getDocumentId());

			if (transportList != null && transportList.size() > 0) {
								item.setIsTransportationDraft(true);
							} else {
								item.setIsTransportationDraft(false);
							}
			item.setIsExpress(dm.getIsExpress());
			/*if (dm.getStatus().equals(ActionStatus.RELEASED)) {
				item.setIsRelease(true);
			} else {
				item.setIsRelease(false);
			}*/
			
			List<DocUserAction> isApprovedRPList = docUserActionRepo
					.findByDocumentIdAndActionStatus(dm.getDocumentId(), "Approved");
			List<DocUserAction> isReleasedRPList = docUserActionRepo
					.findByDocumentIdAndActionStatus(dm.getDocumentId(), "Released");
			if (isApprovedRPList != null && isApprovedRPList.size() > 0 && dm.getStatus().equals(ActionStatus.RELEASED)) {
				
				item.setIsRelease(true);
			}  else  {
				item.setIsRelease(false);
			}
			
			if(isReleasedRPList!=null && isReleasedRPList.size() > 0 && dm.getProposalType()!=null && dm.getProposalType().equalsIgnoreCase("Final")){
				item.setIsFinalApproval(true);
			}else{
				item.setIsFinalApproval(false);
			}
				
			
			
			
			// item.setSrNo(i);
			String rpNumber=null;
			if(dm.getDocumentType().equalsIgnoreCase("RP")){
				List<RevenueProposal> rps = revenueProposalRepository
						.findByDocumentId(docId);
				if(rps!=null && rps.size()>0){
					rpNumber=rps.get(0).getRpNumber();
				}
			}
			//item.setRpNumber(rpNumber);
			item.setDocumentType(dm.getDocumentType());
			if (rpNumbers.contains(rpNumber))
				continue;
			item.setRpNumber(rpNumber);
			item.setDocumentId(docId);

			DateFormat dateFormat = new SimpleDateFormat(
					Formats.DATE_WITH_HOUR_MINUTE_SECOND);
			if (dua.getActionTakenTimeStamp() != null) {
				String dateStr = dateFormat.format(dua
						.getActionTakenTimeStamp());
				item.setRecvdDate(dateStr);
			}

			rpNumbers.add(rpNumber);
			items.add(item);
			// i++;
		}
		Set<Items> itemsSet = new HashSet<Items>(items);
		items = new ArrayList<Items>();
		items.addAll(itemsSet);
		Collections.sort(items, new Comparator<Items>() {

			@Override
			public int compare(Items dua1, Items dua2) {
				String stringDate1 = dua1.getRecvdDate();
				String stringDate2 = dua2.getRecvdDate();
				SimpleDateFormat sdf = new SimpleDateFormat(
						Formats.DATE_WITH_HOUR_MINUTE_SECOND);
				Date date1 = new Date();
				Date date2 = new Date();
				try {
					date1 = sdf.parse(stringDate1);
					date2 = sdf.parse(stringDate2);

					if (date1.compareTo(date2) > 0)
						return -1;
					else if (date1.compareTo(date2) < 0)
						return 1;

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return 0;
			}
		});
		for (int i = 0; i < items.size(); i++) {
			items.get(i).setSrNo(i + 1);
		}

		return items;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.gt.services.ItemsService1#getEndorsedRPs(java.lang.Integer)
	 */
	@Override
	public ArrayList<Items> getEndorsedByApprover(Integer userId) {
		ArrayList<Items> items = new ArrayList<Items>();
		HashSet<String> rpNumbers = new HashSet<String>();
		Items item;
		List<DocUserAction> docUserActions = docUserActionRepo
				.findByUserIdAndActionStatusOrderByActionTakenTimeStampDesc(
						userId, ActionStatus.ENDORSED);
		// int i = 1;
		for (DocUserAction dua : docUserActions) {
			item = new Items();

			Integer docId = dua.getDocumentId();
			DocumentMaster dm = (DocumentMaster) documentMasterRepository
					.findByDocumentId(docId);
		/*	if(dm.getDeptId()!=null && dm.getDeptId()!=0){
				item.setIsDept(true);
			}else{
				item.setIsDept(false);
			}*/
			if(dm.getDeptId()!=null){
				item.setIsDept(dm.getDeptId());
			}else{
				item.setIsDept(0);
			}
			List<RpTransportation> transportList=rpTransportationRepository.findByDocumentId(dua.getDocumentId());

			if (transportList != null && transportList.size() > 0) {
								item.setIsTransportationDraft(true);
							} else {
								item.setIsTransportationDraft(false);
							}
			Integer currentlyId = dm.getCurrentlyWith();
			String docStatus = dm.getStatus();
			if (!docStatus.equalsIgnoreCase(DocStatus.APPROVED)
					&& !docStatus.equalsIgnoreCase(DocStatus.REJECTED)
					&& !docStatus.equalsIgnoreCase(DocStatus.RELEASED)) {
				if (currentlyId != null) {
					Users user = userrepo.findByUserId(currentlyId);
					item.setPersonName(user.getFirstName() + " "
							+ user.getLastName());
				}

				item.setIsExpress(dm.getIsExpress());
			/*	if (dm.getStatus().equals(ActionStatus.RELEASED)) {
					item.setIsRelease(true);
				} else {
					item.setIsRelease(false);
				}*/
				
				List<DocUserAction> isApprovedRPList = docUserActionRepo
						.findByDocumentIdAndActionStatus(dm.getDocumentId(), "Approved");
				List<DocUserAction> isReleasedRPList = docUserActionRepo
						.findByDocumentIdAndActionStatus(dm.getDocumentId(), "Released");
				if (isApprovedRPList != null && isApprovedRPList.size() > 0 && dm.getStatus().equals(ActionStatus.RELEASED)) {
					
					item.setIsRelease(true);
				}  else  {
					item.setIsRelease(false);
				}
				
				if(isReleasedRPList!=null && isReleasedRPList.size() > 0 && dm.getProposalType()!=null && dm.getProposalType().equalsIgnoreCase("Final")){
					item.setIsFinalApproval(true);
				}else{
					item.setIsFinalApproval(false);
				}
					
				
				
				
				
				// item.setSrNo(i);
				String rpNumber=null;
				if(dm.getDocumentType().equalsIgnoreCase("RP")){
					List<RevenueProposal> rps = revenueProposalRepository
							.findByDocumentId(docId);
					if(rps!=null && rps.size()>0){
						rpNumber=rps.get(0).getRpNumber();
					}
				}
				//item.setRpNumber(rpNumber);
				item.setDocumentType(dm.getDocumentType());
				if (rpNumbers.contains(rpNumber))
					continue;
				item.setRpNumber(rpNumber);
				item.setDocumentId(docId);

				DateFormat dateFormat = new SimpleDateFormat(
						Formats.DATE_WITH_HOUR_MINUTE_SECOND);
				if (dua.getActionTakenTimeStamp() != null) {
					String dateStr = dateFormat.format(dua
							.getActionTakenTimeStamp());
					item.setRecvdDate(dateStr);
				}

				rpNumbers.add(rpNumber);
				items.add(item);
				// i++;
			}
		}
		Set<Items> itemsSet = new HashSet<Items>(items);
		items = new ArrayList<Items>();
		items.addAll(itemsSet);
		Collections.sort(items, new Comparator<Items>() {

			@Override
			public int compare(Items dua1, Items dua2) {
				String stringDate1 = dua1.getRecvdDate();
				String stringDate2 = dua2.getRecvdDate();
				SimpleDateFormat sdf = new SimpleDateFormat(
						Formats.DATE_WITH_HOUR_MINUTE_SECOND);
				Date date1 = new Date();
				Date date2 = new Date();
				try {
					date1 = sdf.parse(stringDate1);
					date2 = sdf.parse(stringDate2);

					if (date1.compareTo(date2) > 0)
						return -1;
					else if (date1.compareTo(date2) < 0)
						return 1;

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return 0;
			}
		});
		for (int i = 0; i < items.size(); i++) {
			items.get(i).setSrNo(i + 1);
		}

		return items;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.gt.services.ItemsService1#getDrafts(java.lang.Integer)
	 */
	@Override
	public ArrayList<Items> getDrafts(Integer userId, String roleName) {
		// ArrayList<Items> items = new ArrayList<Items>();
		// Items item;
		//
		// long dayTime= new Date().getTime();
		// List<DocumentMaster>
		// documentMasterList=documentMasterRepository.findByCreatedByAndStatusOrderByCreatedTimeStampDesc(userId,DocStatus.INITIATED);
		// if(!DocRole.INITIATOR.equalsIgnoreCase(roleName)){
		return getDraftForEndorsers(userId, roleName);
		// }
		/*
		 * int i = 1; for(DocumentMaster dua : documentMasterList){ item = new
		 * Items(); Users user = userrepo.findByUserId(dua.getCreatedBy());
		 * Integer docId = dua.getDocumentId();
		 * item.setIsExpress(dua.getIsExpress()); item.setSrNo(i);
		 * //System.out.println(docId); List<RevenueProposal> rps =
		 * revenueProposalRepository.findByDocumentId(docId);
		 * //System.out.println(rps.size());
		 * 
		 * if(rps == null || rps.size()==0) continue;
		 * 
		 * item.setRpNumber(rps.get(0).getRpNumber()); long
		 * pendingTime=dua.getCreatedTimeStamp().getTime();
		 * item.setPendingSince(String.valueOf(daysBetween(pendingTime,
		 * dayTime))+"day(s)");
		 * item.setPersonName(user.getFirstName()+" "+user.getLastName());
		 * 
		 * DateFormat dateFormat = new
		 * SimpleDateFormat(Formats.DATE_WITH_HOUR_MINUTE_SECOND);
		 * if(dua.getCreatedTimeStamp()!=null){ String
		 * dateStr=dateFormat.format(dua.getCreatedTimeStamp());
		 * item.setRecvdDate(dateStr); }
		 * 
		 * // item.setRecvdDate(dua.getCreatedTimeStamp().toString());
		 * item.setDocumentId(dua.getDocumentId()); items.add(item); i++; }
		 */

		// return items;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.gt.services.ItemsService#getDrafts(java.lang.Integer)
	 */
	@Override
	public ArrayList<Items> searchRPNumber(SearchBO searchBO) {
		Integer userId=searchBO.getUserId();
		String rpNum =searchBO.getRpNum();
		String roleName =searchBO.getRoleName();
		String plantName =searchBO.getPlantName();
		ArrayList<Items> items = new ArrayList<Items>();
		Items item;

		long dayTime = new Date().getTime();
		// List<DocumentMaster>
		// documentMasterList=documentMasterRepository.findByCreatedBy(userId);
		List<DocUserAction> docuserActionList = docUserActionRepo
				.findByUserId(userId);

		List<Integer> documentIdList = new ArrayList<Integer>();
		/*
		 * for(DocumentMaster docMaster: documentMasterList){
		 * documentIdList.add(docMaster.getDocumentId()); }
		 */
		String rpVisiblity=PropsUtil.getValue("rp.visiblity.initiator");
		Users users=userService.findByUserid(userId);
		/*if(users!=null && users.getDeptId()!=null && users.getDeptId()!=0){
			rpVisiblity ="false";
		}*/	
		boolean isVisiblity= false;
		if(rpVisiblity!=null && rpVisiblity.equalsIgnoreCase("true")){
			if (DocRole.INITIATOR.equalsIgnoreCase(roleName)
					|| DocRole.INITENDORSER.equalsIgnoreCase(roleName) || DocRole.VIEWER.equalsIgnoreCase(roleName)|| DocRole.PO_VIEWER.equalsIgnoreCase(roleName)) {
				isVisiblity= true;
				Integer InitiatorPlantId=users.getPlantId();
				Plant plant= null;
				Integer clusterId= null;
				if(InitiatorPlantId!=null){
					plant = masterPLantDataRepo.findByPlantId(InitiatorPlantId);
				}
				if(plant!=null && InitiatorPlantId!=null){
					clusterId=plant.getClusterId();
				}
				List<DocumentMaster> documentMasterList = null;
				if(plantName!=null && !plantName.equalsIgnoreCase("undefined")){
					documentMasterList = documentMasterRepository
							.findByClusterId(clusterId);
				}else if(users.getDeptId()==null){
					documentMasterList = documentMasterRepository.getDocListByDeptIdIsNullAndClusterId(users.getDeptId(), 0);
				}else{
					documentMasterList = documentMasterRepository.getDocListByDeptIdAndClusterId(users.getDeptId(), 0);
				}
				List<String> inStatus = new ArrayList<String>();
				inStatus.add(DocStatus.IN_PROCESS);
				inStatus.add(DocStatus.IN_PROCESS_PARALLEL);
				inStatus.add(DocStatus.APPROVED);
				inStatus.add(DocStatus.RELEASED);
				
				if(DocRole.VIEWER.equalsIgnoreCase(roleName)){
					documentMasterList = documentMasterRepository.findByStatusInAndDeptIdNot(inStatus,1);
				}
				List<Integer> depts = new ArrayList<Integer>();
				depts.add(1);
				
				if(DocRole.PO_VIEWER.equalsIgnoreCase(roleName)){
					documentMasterList = documentMasterRepository.findByStatusInAndDeptIdInAndInitiatorPlantIdIsNUll(inStatus,depts);
				}
								
				for(DocumentMaster docMaster: documentMasterList){
					  documentIdList.add(docMaster.getDocumentId()); }
			}
		}
		if(!isVisiblity){
			for (DocUserAction docUserAction : docuserActionList) {
				documentIdList.add(docUserAction.getDocumentId());
			}	
		}
		
		// System.out.println("documentIdList >>>>"+documentIdList.size());
		Collection<Integer> documentCollectionList = new ArrayList<Integer>(
				documentIdList);
		List<RevenueProposal> rpList = revenueProposalRepository
				.findByDocumentIdInAndRpNumberContaining(
						documentCollectionList, rpNum);
		
		// System.out.println("rpList >>>>"+rpList.size());
		// int i = 1;
		Users user = userrepo.findByUserId(userId);
		for (RevenueProposal rps : rpList) {
			item = new Items();
			DocumentMaster dua = documentMasterRepository.findByDocumentId(rps
					.getDocumentId());
			// Integer docId = rps.getDocumentId();
			item.setIsExpress(dua.getIsExpress());
			if(dua.getDeptId()!=null){
				item.setIsDept(dua.getDeptId());
			}else{
				item.setIsDept(0);
			}
			List<RpTransportation> transportList=rpTransportationRepository.findByDocumentId(dua.getDocumentId());

			if (transportList != null && transportList.size() > 0) {
								item.setIsTransportationDraft(true);
							} else {
								item.setIsTransportationDraft(false);
							}
			
			
			List<DocUserAction> isApprovedRPList = docUserActionRepo
					.findByDocumentIdAndActionStatus(dua.getDocumentId(), "Approved");
			List<DocUserAction> isReleasedRPList = docUserActionRepo
					.findByDocumentIdAndActionStatus(dua.getDocumentId(), "Released");
			if (isApprovedRPList != null && isApprovedRPList.size() > 0 && dua.getStatus().equals(ActionStatus.RELEASED)) {
				
				item.setIsRelease(true);
			}  else  {
				item.setIsRelease(false);
			}
			
			if(isReleasedRPList!=null && isReleasedRPList.size() > 0 && dua.getProposalType()!=null && dua.getProposalType().equalsIgnoreCase("Final")){
				item.setIsFinalApproval(true);
			}else{
				item.setIsFinalApproval(false);
			}
				
			System.out.println("row.entity.isDept=="+item.getIsDept());
			System.out.println("row.entity.isFinalApproval"+item.getIsFinalApproval());
			System.out.println("row.entity.isTransportationDraft"+item.getIsTransportationDraft());
			System.out.println("row.entity.isRelease"+item.getIsRelease());
			Users usr = userrepo.findByUserId(dua.getCreatedBy());
			item.setInitiatedBy(usr.getFirstName() + " "
					+ usr.getLastName());

			// item.setSrNo(i);
			// System.out.println(docId);
			item.setRpNumber(rps.getRpNumber());
			item.setDocumentType(dua.getDocumentType());
			//System.out.println("HIL/All/NA/COR/FMS/000005/16"+rps.getRpNumber());
			long pendingTime = dua.getCreatedTimeStamp().getTime();
			item.setPendingSince(String.valueOf(daysBetween(pendingTime,
					dayTime)) + " day(s)");
			item.setPersonName(user.getFirstName() + " " + user.getLastName());

			DateFormat dateFormat = new SimpleDateFormat(
					Formats.DATE_WITH_HOUR_MINUTE_SECOND);
			if (dua.getCreatedTimeStamp() != null) {
				String dateStr = dateFormat.format(dua.getCreatedTimeStamp());
				item.setRecvdDate(dateStr);
			}

			// item.setRecvdDate(dua.getCreatedTimeStamp().toString());
			item.setDocumentId(dua.getDocumentId());
			items.add(item);
			// i++;
		}
		
		Set<Items> itemsSet = new HashSet<Items>(items);
		items = new ArrayList<Items>();
		items.addAll(itemsSet);
		Collections.sort(items, new Comparator<Items>() {

			@Override
			public int compare(Items dua1, Items dua2) {
				String stringDate1 = dua1.getRecvdDate();
				String stringDate2 = dua2.getRecvdDate();
				SimpleDateFormat sdf = new SimpleDateFormat(
						"dd/MM/yyyy HH:mm:ss");
				Date date1 = new Date();
				Date date2 = new Date();
				try {
					date1 = sdf.parse(stringDate1);
					date2 = sdf.parse(stringDate2);

					if (date1.compareTo(date2) > 0)
						return -1;
					else if (date1.compareTo(date2) < 0)
						return 1;

				} catch (ParseException e) {

					e.printStackTrace();
				}

				return 0;
			}
		});

		for (int i = 0; i < items.size(); i++) {
			items.get(i).setSrNo(i + 1);
		}

		return items;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.gt.services.ItemsService1#getCancelledRPs(java.lang.Integer)
	 */
	@Override
	public ArrayList<Items> getCancelledRPs(Integer userId) {
		ArrayList<Items> items = new ArrayList<Items>();
		Items item;

		System.out.println("<<<< getCancelledRPs >>>>");

		List<DocumentMaster> documentMasterList = documentMasterRepository
				.findByCreatedByAndStatusOrderByModifiedTimeStampDesc(userId,
						DocStatus.CANCELLED);

		System.out.println("<<<< documentMasterList >>>>"
				+ documentMasterList.size());

		int i = 1;
		for (DocumentMaster dua : documentMasterList) {
			item = new Items();

			Integer docId = dua.getDocumentId();
			item.setIsExpress(dua.getIsExpress());
			item.setSrNo(i);

			List<RevenueProposal> rps = revenueProposalRepository
					.findByDocumentId(docId);
			
		

			if (rps == null || rps.size() == 0)
				continue;
			
			String rpNumber=null;
			if(rps!=null && rps.size()>0){
				rpNumber=rps.get(0).getRpNumber();
			}

			item.setRpNumber(rpNumber);

			DateFormat dateFormat = new SimpleDateFormat(
					Formats.DATE_WITH_HOUR_MINUTE_SECOND);
			if (dua.getModifiedTimeStamp() != null) {
				String dateStr = dateFormat.format(dua.getModifiedTimeStamp());
				item.setRecvdDate(dateStr);

			}
		/*	if(dua.getDeptId()!=null && dua.getDeptId()!=0){
				item.setIsDept(true);
			}else{
				item.setIsDept(false);
			}*/
			if(dua.getDeptId()!=null){
				item.setIsDept(dua.getDeptId());
			}else{
				item.setIsDept(0);
			}
			List<RpTransportation> transportList=rpTransportationRepository.findByDocumentId(dua.getDocumentId());

			if (transportList != null && transportList.size() > 0) {
								item.setIsTransportationDraft(true);
							} else {
								item.setIsTransportationDraft(false);
							}
			item.setDocumentId(dua.getDocumentId());
			items.add(item);
			i++;
		}

		return items;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.gt.services.ItemsService1#getDelegateRPs(java.lang.Integer)
	 */
	@Override
	public ArrayList<Items> getDelegateRPs(Integer userId) {
		List<Delegation> delegates = delegateRepo.findByDelegateUserId(userId);
		ArrayList<Items> items = new ArrayList<Items>();
		for (Delegation del : delegates) {
			Date fromDate = del.getFromDate();
			Date toDate = del.getToDate();
			Date currentDate = new Date();

			boolean getMin = (currentDate.compareTo(toDate) <= 0 && currentDate
					.compareTo(fromDate) >= 0);
			if (getMin) {
				if(del.getIsActive()){
				List<DocUserAction> duaPending = docUserActionRepo
						.findByUserIdAndActionStatusOrderByActionPendingTimeStampDesc(
								del.getUserId(), "Pending");
				List<DocUserAction> duaParallel = docUserActionRepo
						.findByUserIdAndActionStatusOrderByActionPendingTimeStampDesc(
								del.getUserId(), "Pending_Parallel");
				List<DocUserAction> duaExpress = docUserActionRepo
						.findByUserIdAndActionStatusOrderByActionPendingTimeStampDesc(
								del.getUserId(), "Pending_Express");
				ArrayList<DocUserAction> duas = new ArrayList<DocUserAction>();
				if (duaPending != null && duaPending.size() != 0) {
					for (DocUserAction dua : duaPending) {
						duas.add(dua);
					}
				}
				if (duaParallel != null && duaParallel.size() != 0) {
					for (DocUserAction dua : duaParallel) {
						duas.add(dua);
					}
				}
				if (duaExpress != null && duaExpress.size() != 0) {
					for (DocUserAction dua : duaExpress) {
						duas.add(dua);
					}
				}

				Items item;
				// int srNo= 1;
				for (DocUserAction dua : duas) {
					DocumentMaster dm = documentMasterRepository
							.findByDocumentId(dua.getDocumentId());
					List<RevenueProposal> rps = revenueProposalRepository
							.findByDocumentId(dua.getDocumentId());
					RevenueProposal rp = rps.get(0);

					item = new Items();
					if (dm.getStatus().equals(ActionStatus.APPROVED)) {
						item.setIsRelease(true);
					} else {
						item.setIsRelease(false);
					}
					// item.setSrNo(srNo);
				/*	if(dm.getDeptId()!=null && dm.getDeptId()!=0){
						item.setIsDept(true);
					}else{
						item.setIsDept(false);
					}*/
					if(dm.getDeptId()!=null){
						item.setIsDept(dm.getDeptId());
					}else{
						item.setIsDept(0);
					}
					List<RpTransportation> transportList=rpTransportationRepository.findByDocumentId(dua.getDocumentId());

					if (transportList != null && transportList.size() > 0) {
										item.setIsTransportationDraft(true);
									} else {
										item.setIsTransportationDraft(false);
									}
					
					List<DocUserAction> isApprovedRPList = docUserActionRepo
							.findByDocumentIdAndActionStatus(dua.getDocumentId(), "Approved");
					List<DocUserAction> isReleasedRPList = docUserActionRepo
							.findByDocumentIdAndActionStatus(dua.getDocumentId(), "Released");
					if (isApprovedRPList != null && isApprovedRPList.size() > 0) {
						
						item.setIsRelease(true);
					}  else  {
						item.setIsRelease(false);
					}
					
					if(isReleasedRPList!=null && isReleasedRPList.size() > 0 ){
						item.setIsFinalApproval(true);
					}else{
						item.setIsFinalApproval(false);
					}
					item.setIsExpress(dm.getIsExpress());
					item.setRpNumber(rp.getRpNumber());
					item.setDocumentType(dm.getDocumentType());
					Integer referById = dua.getFromUserAction();
					Users delegatedByUser = userrepo.findByUserId(del
							.getUserId());
					item.setDelegateName(delegatedByUser.getFirstName() + " "
							+ delegatedByUser.getLastName());
					Users referedByUser = userrepo.findByUserId(referById);
					item.setPersonName(referedByUser.getFirstName() + " "
							+ referedByUser.getLastName());
					item.setDocumentId(dua.getDocumentId());
					item.setUserId(del.getUserId());

					DateFormat dateFormat = new SimpleDateFormat(
							"dd/MM/yyyy HH:mm:ss");
					if (dua.getActionPendingTimeStamp() != null) {
						String dateStr = dateFormat.format(dua
								.getActionPendingTimeStamp());
						item.setRecvdDate(dateStr);
					}
					long pendingTime = dua.getActionPendingTimeStamp()
							.getTime();
					item.setPendingSince(String.valueOf(daysBetween(
							pendingTime, currentDate.getTime())) + " day(s)");
					items.add(item);
					// srNo++;
				}
			}
			}
		}
		Collections.sort(items, new Comparator<Items>() {

			@Override
			public int compare(Items dua1, Items dua2) {
				String stringDate1 = dua1.getRecvdDate();
				String stringDate2 = dua2.getRecvdDate();
				SimpleDateFormat sdf = new SimpleDateFormat(
						"dd/MM/yyyy HH:mm:ss");
				Date date1 = new Date();
				Date date2 = new Date();
				try {
					date1 = sdf.parse(stringDate1);
					date2 = sdf.parse(stringDate2);

					if (date1.compareTo(date2) > 0)
						return -1;
					else if (date1.compareTo(date2) < 0)
						return 1;

				} catch (ParseException e) {

					e.printStackTrace();
				}

				return 0;
			}
		});
		for (int i = 0; i < items.size(); i++) {
			items.get(i).setSrNo(i + 1);
		}

		return items;
	}

	/**
	 * @param userId
	 * @return
	 */
	private ArrayList<Items> getDraftForEndorsers(Integer userId,
			String roleName) {
		Boolean delegated= false;
		List<Delegation> delegates = delegateRepo.findByUserId(userId);
		Users usr ;
		ArrayList<Items> items = new ArrayList<Items>();
		Items item;
		String delegationEnabled=PropsUtil.getValue("rp.delegation");	
		if(delegates.size()>0 && delegationEnabled.equalsIgnoreCase("enable") && !delegated){
			for(Delegation del :delegates){
				Date fromDate =del.getFromDate();
				Date toDate = del.getToDate();
				Date currentDate =new Date();
				
				boolean getMin = (currentDate.compareTo(toDate)<=0 &&currentDate.compareTo(fromDate)>=0);
				if(getMin){
					if(del.getIsActive()){
					delegated =true;
					/*item = new Items();
					item.setIsDelegated(delegated);
					items.add(item);*/
					return null;
					}
				}
			}
		}
		long dayTime = new Date().getTime();
		List<DocUserAction> duas = docUserActionRepo.findByUserIdAndIsDraft(
				userId, "Yes");

		// int srNo= 1;
		for (DocUserAction dua : duas) {

			item = new Items();
			if (DocRole.INITIATOR.equals(roleName)
					|| DocRole.INITENDORSER.equals(roleName)) {
				List<DocUserAction> docUserList = docUserActionRepo
						.findByDocumentIdAndActionStatus(dua.getDocumentId(),
								ActionStatus.INITIATED);
				List<RpTransportation> transportList=rpTransportationRepository.findByDocumentId(dua.getDocumentId());
				if (docUserList != null && docUserList.size() > 0) {
					item.setIsInitiatorDraft(true);
				} else {
					item.setIsInitiatorDraft(false);
				}
				if (transportList != null && transportList.size() > 0) {
					item.setIsTransportationDraft(true);
				} else {
					item.setIsTransportationDraft(false);
				}
			} else {
				item.setIsInitiatorDraft(false);
			}
			if(dua.getUserId().intValue() == userId.intValue() && DocRole.INITIATOR.equalsIgnoreCase(dua.getDocRole())){
				List<DocUserAction> docuaList=docUserActionRepo.findByDocumentIdAndActionStatus(dua.getDocumentId(), ActionStatus.INITIATED);
				if(docuaList!=null && docuaList.size()>0){
					item.setIsRefferedToInitiator(true);	
				}else{
					item.setIsRefferedToInitiator(false);	
				}
					
				}else{
					item.setIsRefferedToInitiator(false);
				}
			Users user = userrepo.findByUserId(dua.getFromUserAction());
			Integer docId = dua.getDocumentId();
			DocumentMaster dm = (DocumentMaster) documentMasterRepository
					.findByDocumentId(docId);
		/*	if(dm.getDeptId()!=null && dm.getDeptId()!=0){
				item.setIsDept(true);
			}else{
				item.setIsDept(false);
			}*/
			if(dm.getDeptId()!=null){
				item.setIsDept(dm.getDeptId());
			}else{
				item.setIsDept(0);
			}
			List<RpTransportation> transportList=rpTransportationRepository.findByDocumentId(dua.getDocumentId());

			if (transportList != null && transportList.size() > 0) {
								item.setIsTransportationDraft(true);
							} else {
								item.setIsTransportationDraft(false);
							}
			/*
			List<DocUserAction> isReleaseRPList = docUserActionRepo
					.findByDocumentIdAndActionStatus(docId, "Approved");
			if (isReleaseRPList != null && isReleaseRPList.size() > 0) {
				item.setIsRelease(true);
			} else {
				item.setIsRelease(false);
			}*/
			
			List<DocUserAction> isApprovedRPList = docUserActionRepo
					.findByDocumentIdAndActionStatus(docId, "Approved");
			List<DocUserAction> isReleasedRPList = docUserActionRepo
					.findByDocumentIdAndActionStatus(docId, "Released");
			if (isApprovedRPList != null && isApprovedRPList.size() > 0) {
				
				item.setIsRelease(true);
			}  else  {
				item.setIsRelease(false);
			}
			
			if(isReleasedRPList!=null && isReleasedRPList.size() > 0 ){
				item.setIsFinalApproval(true);
			}else{
				item.setIsFinalApproval(false);
			}
				
			
			item.setIsExpress(dm.getIsExpress());
			// item.setSrNo(i);
			String rpNumber=null;
			if(dm.getDocumentType().equalsIgnoreCase("RP")){
				List<RevenueProposal> rps = revenueProposalRepository
						.findByDocumentId(docId);
				if(rps!=null && rps.size()>0){
					rpNumber=rps.get(0).getRpNumber();
				}
			}
			item.setRpNumber(rpNumber);
			item.setDocumentType(dm.getDocumentType());
			long pendingTime = dua.getActionPendingTimeStamp().getTime();
			item.setPendingSince(String.valueOf(daysBetween(pendingTime,
					dayTime)) + " day(s)");
			item.setPersonName(user.getFirstName() + " " + user.getLastName());

			DateFormat dateFormat = new SimpleDateFormat(
					Formats.DATE_WITH_HOUR_MINUTE_SECOND);
			if (dua.getActionPendingTimeStamp() != null) {
				String dateStr = dateFormat.format(dua
						.getActionPendingTimeStamp());
				item.setRecvdDate(dateStr);
			}

			// item.setRecvdDate(dua.getActionPendingTimeStamp().toString());
			item.setDocumentId(dua.getDocumentId());
			item.setPending_Express(false);
			items.add(item);
		}

		Collections.sort(items, new Comparator<Items>() {

			@Override
			public int compare(Items dua1, Items dua2) {
				String stringDate1 = dua1.getRecvdDate();
				String stringDate2 = dua2.getRecvdDate();
				SimpleDateFormat sdf = new SimpleDateFormat(
						"dd/MM/yyyy HH:mm:ss");
				Date date1 = new Date();
				Date date2 = new Date();
				try {
					date1 = sdf.parse(stringDate1);
					date2 = sdf.parse(stringDate2);

					if (date1.compareTo(date2) > 0)
						return -1;
					else if (date1.compareTo(date2) < 0)
						return 1;

				} catch (ParseException e) {

					e.printStackTrace();
				}

				return 0;
			}
		});
		for (int i = 0; i < items.size(); i++) {
			items.get(i).setSrNo(i + 1);
		}

		return items;
	}

	@Override
	public List<Delegation> findByToDateBefore(Date currentDate) {
		// TODO Auto-generated method stub
		return delegateRepo.findByToDateBeforeAndIsActive(currentDate,true);
	}

	@Override
	public void saveDelegation(Delegation delegation) {
		// TODO Auto-generated method stub
		delegateRepo.save(delegation);
	}

	@Override
	public Status isUserDelegated(Integer userId) {
		// TODO Auto-generated method stub
		List<Delegation> delList=delegateRepo.findByUserIdAndIsActive(userId,true);
		if(delList!=null && delList.size()>0){
			for(Delegation del :delList){
				Date fromDate =del.getFromDate();
				Date toDate = del.getToDate();
				Date currentDate =new Date();
				
				boolean getMin = (currentDate.compareTo(toDate)<=0 &&currentDate.compareTo(fromDate)>=0);
				if(getMin){
					Users usr= userrepo.findByUserId(del.getDelegateUserId());
					if(del.getIsActive()){
						//isUserDelegated =true;
					/*item = new Items();
					item.setIsDelegated(delegated);
					items.add(item);*/
					return new Status(0, usr.getFirstName()+" "+usr.getLastName());
					}
				}
			}
		}
		
		delList=delegateRepo.findByDelegateUserIdAndIsActive(userId, true);
		if(delList!=null && delList.size()>0){
			for(Delegation del :delList){
				Date fromDate =del.getFromDate();
				Date toDate = del.getToDate();
				Date currentDate =new Date();
				
				boolean getMin = (currentDate.compareTo(toDate)<=0 &&currentDate.compareTo(fromDate)>=0);
				if(getMin){
					Users usr= userrepo.findByUserId(del.getDelegateUserId());
					if(del.getIsActive()){
						//isUserDelegated =true;
					/*item = new Items();
					item.setIsDelegated(delegated);
					items.add(item);*/
					return new Status(2, usr.getFirstName()+" "+usr.getLastName());
					}
				}
			}
		}
		return new Status(1, "");
	}

	@Override
	public List<Delegation> getIsActiveUserId(Integer userId) {
		// TODO Auto-generated method stub
		return delegateRepo.findByUserIdAndIsActive(userId, true);
	}

	@Override
	public List<Delegation> getIsActiveUserIdOrderByToDate(Integer userId) {
		// TODO Auto-generated method stub
		return delegateRepo.findByUserIdAndIsActiveOrderByToDateAsc(userId, true);
	}

}
