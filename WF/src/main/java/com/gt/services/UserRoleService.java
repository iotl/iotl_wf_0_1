package com.gt.services;

import java.util.ArrayList;

import com.gt.entity.UserRoles;

/**
 * @author Abilash
 *
 */
public interface UserRoleService {

	ArrayList<UserRoles> findAll();

	ArrayList<UserRoles> findByUserId(int userId); // int id

	ArrayList<UserRoles> findByRoleId(int roleId);
}
