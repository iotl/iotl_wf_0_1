package com.gt.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.gt.bo.MasterBO;
import com.gt.entity.Businesses;
import com.gt.entity.Category;
import com.gt.entity.Cluster;
import com.gt.entity.Companies;
import com.gt.entity.DocUserAction;
import com.gt.entity.Material;
import com.gt.entity.Plant;
import com.gt.entity.Sbus;
import com.gt.repository.ClustersRepository;
import com.gt.repository.MasterBusinessDataRepository;
import com.gt.repository.MasterCategoryDataRepository;
import com.gt.repository.MasterCompaniesDataRepository;
import com.gt.repository.MasterMaterialDataRepository;
import com.gt.repository.MasterPlantDataRepository;
import com.gt.repository.MasterSBUDataRepository;
import com.gt.utils.DocStatus;

@Component("masterdata")
@Transactional
public class MasterDataImpl implements MasterData {

	private final MasterBusinessDataRepository MasterBusinessRepo;
	private final MasterCompaniesDataRepository MasterCompaniesRepo;
	private final MasterSBUDataRepository MasterSbuRepo;
	private final MasterMaterialDataRepository MasterMaterialRepo;
	private final MasterPlantDataRepository MasterPlantRepo;
	private final ClustersRepository ClusterRepo;
	private final MasterCategoryDataRepository MasterCategoryRepo;

	
	@Autowired
	public MasterDataImpl(MasterBusinessDataRepository MasterBusinessRepo,MasterCompaniesDataRepository MasterCompaniesRepo,MasterSBUDataRepository MasterSbuRepo,MasterMaterialDataRepository MasterMaterialRepo,MasterPlantDataRepository MasterPlantRepo,ClustersRepository ClusterRepo,MasterCategoryDataRepository MasterCategoryRepo){
		this.MasterBusinessRepo = MasterBusinessRepo;
		this.MasterCompaniesRepo = MasterCompaniesRepo;
		this.MasterSbuRepo = MasterSbuRepo;
		this.MasterMaterialRepo = MasterMaterialRepo;
		this.MasterPlantRepo = MasterPlantRepo;
		this.ClusterRepo = ClusterRepo;
		this.MasterCategoryRepo = MasterCategoryRepo;
	}
	
	@Override
	public ArrayList<Plant> getPlantData() {
		return (ArrayList<Plant>) MasterPlantRepo.findAll();
	}

	@Override
	public ArrayList<Businesses> getBusinesses() {
		return (ArrayList<Businesses>) MasterBusinessRepo.findAll();
	}

	@Override
	public ArrayList<Companies> getCompanies() {
		return (ArrayList<Companies>) MasterCompaniesRepo.findAll();
	}

	@Override
	public ArrayList<Sbus> getSbuData() {
		return (ArrayList<Sbus>) MasterSbuRepo.findAll();
	}

	@Override
	public ArrayList<Material> getMaterialData() {
		return (ArrayList<Material>) MasterMaterialRepo.findAll();
	}

	@Override
	public Companies getCompaniesById(int id) {
	
		return MasterCompaniesRepo.findByCompanyId(id);
	}
	
	@Override
	public Businesses getBusinessesById(int id) {
	
		return MasterBusinessRepo.findByBusinessId(id);
	}

	@Override
	public Sbus getSbusById(int id) {
	
		return MasterSbuRepo.findBySbuId(id);
	}

	@Override
	public Plant getPlantById(int id) {
	
		return MasterPlantRepo.findByPlantId(id);
	}
	
	@Override
	public Material getMaterialById(int id) {
	
		return MasterMaterialRepo.findByMaterialId(id);
	}

	@Override
	public MasterBO getMasterDataForPlant(Integer plantId) {
		MasterBO masterBO = new MasterBO();
		Plant plant= MasterPlantRepo.findByPlantId(plantId);
		Companies comp= null;
		if(plant.getCompanyId()!=null){
			comp=MasterCompaniesRepo.findByCompanyId(plant.getCompanyId());
		}else{
			comp=MasterCompaniesRepo.findByCompanyId(1);
		}
		Businesses busi= null;
		if(plant.getBusinessId()!=null){
			busi =MasterBusinessRepo.findByBusinessId(plant.getBusinessId());
		}else{
			 busi =MasterBusinessRepo.findByBusinessId(1);
		}
		Integer clusterId=plant.getClusterId();
	    List<Cluster> cluster = ClusterRepo.findByClusterId(clusterId);
	    Integer sbuId=MasterPlantRepo.findByClusterIdAndPlantLogin(cluster.get(0).getClusterId(), true).get(0).getSbuId();
	    Sbus sbu = MasterSbuRepo.findBySbuId(sbuId);
		masterBO.setCluster(cluster.get(0));
		masterBO.setCompany(comp);
		masterBO.setBusiness(busi);
		masterBO.setSbu(sbu);
	
		return masterBO;
	}

	@Override
	public ArrayList<Plant> getPlantByClusterId(Integer plantId) {
		if(plantId != null){
		Plant plant = MasterPlantRepo.findByPlantId(plantId);
		List<Plant> plantList =  MasterPlantRepo.findByClusterIdAndPlantLogin(plant.getClusterId(),true);
		return (ArrayList<Plant>) plantList;
		}
		return null ;
	}

	@Override
	public ArrayList<Material> getMaterialsForCluster(Integer plantId) {
		Plant plant = MasterPlantRepo.findByPlantId(plantId);
		Integer clusterId = plant.getClusterId();
		ArrayList<Material> materials = new ArrayList<Material>();
		/* List<Material> materialsForAluminium = MasterMaterialRepo.findByBusinessId(1);
		for(Material mat:materialsForAluminium){
			if(mat.getIsActive()== null|| mat.getIsActive()){
				materials.add(mat);
			}
		}*/
		if(clusterId!=null){
			List<Material> materialsForCluster = MasterMaterialRepo.findByClusterId(clusterId);
			for(Material mat:materialsForCluster){
				if(mat.getIsActive()==null || mat.getIsActive()){
					materials.add(mat);
				}
			}
		}
		return materials;
	}



	@Override
	public List<Material> getMaterialsByBusinessIdAndDeptId(Integer businessId,
			Integer deptId) {
		// TODO Auto-generated method stub
		Boolean isActive = true;
		System.out.println("deptId >>>>"+deptId);
		if(businessId!=null && businessId ==-1){
			List<Integer> businessIdList= new ArrayList<Integer>();
			businessIdList.add(1);
			businessIdList.add(2);
			businessIdList.add(3);
			Collection<Integer> businessIds = new ArrayList<Integer>(businessIdList);
			if(deptId!=null){
				//return MasterMaterialRepo.findByBusinessIdAndDeptId(businessId, deptId);
				return MasterMaterialRepo.findByBusinessIdInAndDeptIdAndIsActive(businessIds,deptId,isActive);

			}else{
			    return MasterMaterialRepo.findByBusinessIdInAndDeptIdIsNull(businessIds);
			}
			
		}
		if(deptId!=null){
			//return MasterMaterialRepo.findByBusinessIdAndDeptId(businessId, deptId);
			return MasterMaterialRepo.findByBusinessIdAndDeptIdAndIsActive(businessId,deptId,isActive);

		}else{
		    return MasterMaterialRepo.findByBusinessIdAndDeptIdIsNull(businessId);
		}
		
	}
	
	@Override
	public ArrayList<Cluster> getClusterSbuData(Integer plantId) {
		
		if(plantId != null){
			Plant plant = MasterPlantRepo.findByPlantId(plantId);
			Integer clusterId=plant.getClusterId();
			List<Cluster> cluster = ClusterRepo.findByClusterId(clusterId);
			return (ArrayList<Cluster>) cluster;
		}
		return null;	
		
	}

	@Override
	public ArrayList<Plant> getPlantNameByClusterId(Integer clusterId) {
	
			if(clusterId != null){
			List<Plant> plantList =  MasterPlantRepo.findByClusterId(clusterId);
			return (ArrayList<Plant>) plantList;
			}
			return null ;
		}

	@Override
	public List<Material> getMaterialsByDeptId(Integer deptId) {
		
			// TODO Auto-generated method stub
			Boolean isActive = true;
			System.out.println("deptId >>>>"+deptId);
			if(deptId!=null){
				//return MasterMaterialRepo.findByBusinessIdAndDeptId(businessId, deptId);
				return MasterMaterialRepo.findByDeptIdAndIsActive(deptId,isActive);
							}
			return null;
	}
	
	@Override
	public ArrayList<Businesses> findBusinessesByCompanyId(int companyId){
		return (ArrayList<Businesses>) MasterBusinessRepo.findByCompanyId(companyId);

	}
	
	@Override
	public ArrayList<Sbus> getSBUsbyCompanyIdandBusinessId(int companyId,int businessId){
		return (ArrayList<Sbus>)MasterSbuRepo.findByCompanyIdAndBusinessId(companyId, businessId);
		
	}	
	
	@Override
	public ArrayList<Plant> getPlantsbyCompanyIdandBusinessIdandSbuId(int companyId,int businessId,int sbusId){
		
		return (ArrayList<Plant>)MasterPlantRepo.findByCompanyIdAndBusinessIdAndSbuId(companyId, businessId, sbusId);
	}
	
	@Override
	public ArrayList<Businesses> findbyNotInCompanyIdAndBusinessIdNotInBusinessCollection(int companyId,List<Integer> businessIdList){
		
		return (ArrayList<Businesses>) MasterBusinessRepo.findbyCompanyIdNotInAndBusinessIdNotInBusinessCollection(companyId, businessIdList);
		
	}

	@Override
	public ArrayList<Plant> findAllRpPlants() {
		// TODO Auto-generated method stub
		return (ArrayList<Plant>) MasterPlantRepo.findAllRpPlants();
	}
	
	public ArrayList<Plant> findNotMatchingRecordsInPlantsId(int companyId, int businessId,int sbuId,List<Integer> plantListId){
		
		return (ArrayList<Plant>) MasterPlantRepo.findNotMatchingRecordsInMatchedPlants(companyId, businessId, sbuId, plantListId) ;
	}
	
	@Override
	public ArrayList<Businesses> findAllBusiness(){
		return (ArrayList<Businesses>) MasterBusinessRepo.findAll();
		
	}

	@Override
	public ArrayList<Category> getCategories() {
		
		return (ArrayList<Category>) MasterCategoryRepo.findAll();
	}
	
		

	

}
