package com.gt.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.gt.dao.Wf_UserDao;
import com.gt.entity.Users;
import com.gt.repository.UserRepository;
@Component("wf_UserService")
@Transactional
public class Wf_UserServiceImpl implements Wf_UserService {
	@Autowired
	Wf_UserDao wf_UserDao;
	@Autowired
     UserRepository userrepo;
	@Override
	public boolean addEntity(Users wf_User) throws Exception {
	 
		Users users=  userrepo.save(wf_User);
		  if(users!=null)
				return true;
				return false;
	}

	@Override
	public Users getEntityById(Integer userid) throws Exception {
		// TODO Auto-generated method stub
		return userrepo.findByUserId(userid);
	}

	@Override
	public boolean editEntityById(Users wf_User) throws Exception {
		// TODO Auto-generated method stub
		Users users=userrepo.save(wf_User);
		if(users!=null)
		return true;
		return false;
				
	}

	@Override
	public List<Users> getEntityList() throws Exception {
		 
		  return userrepo.findAll();
	}

	@Override
	public boolean deleteEntity(Integer userid) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public  Users getEntityByEmailId(String emailId) throws Exception {
		// TODO Auto-generated method stub
		Users users =null;
		List<Users> list=userrepo.findByEmailId(emailId);
		if(list!=null && list.size()>0){
			users=(Users)list.get(0);
		}
		return users;
	}
	
	@Override
	public  Users getEntityByAuthorizationKey(String authorizationKey) throws Exception {
		// TODO Auto-generated method stub
		return userrepo.findByAuthorizationKey(authorizationKey);
	}

}
