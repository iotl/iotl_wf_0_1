package com.gt.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.gt.entity.Role;
import com.gt.repository.RolesRepository;

/**
 * @author Abilash
 *
 */
@Component("roleService")
@Transactional
public class RoleServiceImpl implements RoleService {
	
	@Autowired
	RolesRepository roleRepo;

	@Override
	public ArrayList<Role> findAll() {
		// TODO Auto-generated method stub
		return roleRepo.findAll();
	}

	@Override
	public Role findByRoleId(int roleId) {
		// TODO Auto-generated method stub
		return roleRepo.findByRoleId(roleId);
	}

}
