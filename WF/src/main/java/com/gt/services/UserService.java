package com.gt.services;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.id.SequenceIdentityGenerator.Delegate;

import com.controllers.Status;
import com.gt.bo.AdvisorCreate;
import com.gt.bo.DelegationBO;
//import com.gt.bo.DocumentUserMasterCreat;
import com.gt.bo.UsersBO;
import com.gt.entity.Delegation;
import com.gt.entity.DocUserMaster;
import com.gt.entity.UserDelegates;
import com.gt.entity.Users;

public interface UserService {
 
	public boolean createUser(Users users);
	Users delete(String id);
	
	public ArrayList<Users> getAllUsers();
	public Users getUsersById(int id);
	
	List<Users> findAllUsers();

	Users findByUserid(int id);

	Users update(Users todo);
	
	List<DocUserMaster> findAllDocUserMasters();
	
	List<Users> findByIsSupportingEndorser(Boolean isSupportingEndorser);
	List<UsersBO> findAllSupportingEndorsers(int id);
	
//	List<UsersBO> findAllCPSupportingEndorsers(int id);
	
	public  Status saveDelegationData(DelegationBO delegations);
	public DocUserMaster saveSupportingEndorser(AdvisorCreate ac);
	List<Users> findUserBasedOnRole(String Role);
	
	Integer deleteDocUserMaster(Integer userId,Integer documentId);
//	List<UserDelegates> findUserDelegatesBasedOnUserId(Integer UserId);
//	List<Delegation> findUserDelegated(Integer UserId);
	
	public  Users getEntityByEmailId(String emailId) ;
	
	public Boolean getIsActiveDelegation(Integer userId);
}
