package com.gt.services;

import java.util.List;

import com.gt.entity.Users;

public interface Wf_UserService {
	public boolean addEntity(Users wf_User) throws Exception;
	public Users getEntityById(Integer userid) throws Exception;
	public boolean editEntityById(Users wf_User) throws Exception;
	public List<Users> getEntityList() throws Exception;
	public boolean deleteEntity(Integer userid) throws Exception;
	public  Users getEntityByEmailId(String emailId) throws Exception;
	public  Users getEntityByAuthorizationKey(String authorizationKey) throws Exception;
}
