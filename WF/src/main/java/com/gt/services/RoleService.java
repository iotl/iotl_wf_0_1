package com.gt.services;

import java.util.ArrayList;

import com.gt.entity.Role;

/**
 * @author Abilash
 *
 */
public interface RoleService {
	
	ArrayList<Role> findAll();

	Role findByRoleId(int roleId);
}
