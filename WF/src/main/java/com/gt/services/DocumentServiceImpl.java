package com.gt.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.controllers.Status;
import com.gt.bo.DocumentData;
import com.gt.bo.DocumentMetaData;
import com.gt.bo.DocumentMetaDataCreate;
import com.gt.bo.RpTransportationBO;
import com.gt.dao.DocAttachDao;
import com.gt.entity.Businesses;
import com.gt.entity.Category;
import com.gt.entity.Cluster;
import com.gt.entity.Comments;
import com.gt.entity.Companies;
import com.gt.entity.DocAttachment;
import com.gt.entity.DocUserAction;
import com.gt.entity.DocUserMaster;
import com.gt.entity.DocumentMaster;
import com.gt.entity.FinalApproval;
import com.gt.entity.Material;
import com.gt.entity.Plant;
import com.gt.entity.RPNumber;
import com.gt.entity.RevenueProposal;
import com.gt.entity.RpTransportation;
import com.gt.entity.Sbus;
import com.gt.entity.Users;
import com.gt.repository.ClustersRepository;
import com.gt.repository.CommentRepository;
import com.gt.repository.DocAttachmentRepository;
import com.gt.repository.DocUserActionRepository;
import com.gt.repository.DocUserMasterRepository;
import com.gt.repository.DocumentMasterRepository;
import com.gt.repository.FinalApprovalRepository;
import com.gt.repository.MasterBusinessDataRepository;
import com.gt.repository.MasterCategoryDataRepository;
import com.gt.repository.MasterCompaniesDataRepository;
import com.gt.repository.MasterMaterialDataRepository;
import com.gt.repository.MasterPlantDataRepository;
import com.gt.repository.MasterSBUDataRepository;
import com.gt.repository.RPNumberRepository;
import com.gt.repository.RPTransportationRepository;
import com.gt.repository.RevenueProposalRepository;
import com.gt.repository.RolesRepository;
import com.gt.repository.UserRepository;
import com.gt.repository.UserRoleRepository;
import com.gt.utils.ActionStatus;
import com.gt.utils.DocRole;
import com.gt.utils.DocStatus;
import com.gt.utils.Formats;
import com.gt.utils.PropsUtil;
import com.gt.utils.WFUtil;


@Component("documentService")
@Transactional
public class DocumentServiceImpl implements DocumentService{
	
	private final DocUserMasterRepository docUserMasterRepo;
	private final RevenueProposalRepository revenueProposalRepository;
	private final FinalApprovalRepository rp_FinalApprovalRepository;
	private final DocumentMasterRepository documentMasterRepository;
	private final MasterBusinessDataRepository MasterBusinessRepo;
	private final MasterCompaniesDataRepository MasterCompaniesRepo;
	private final MasterSBUDataRepository MasterSbuRepo;
	private final MasterMaterialDataRepository MasterMaterialRepo;
	private final MasterPlantDataRepository MasterPlantRepo;
	private final MasterCategoryDataRepository MasterCategoryRepo;
	private final CommentRepository CommentRepo;
	private final DocAttachmentRepository docAttachmentRepository;
	private final RPNumberRepository rpNumRepo;
	private final DocUserActionRepository docUserActionRepo;
	private final ClustersRepository clusterRepo;
	private final RPTransportationRepository rpTransportationRep;
	
	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	private DocAttachDao docAttachDao;
	
	@Autowired
	private RuleEngineHelper ruleEngineHelper;	

	@Autowired
	private ItemsService documentsListingHelper;
	
	@Autowired
	private MailNotificationHelper mailNotificationHelper;
	
	@Autowired
	private DocUserActionService docUserActionHelper;
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private UserRoleRepository userRoleRepo;
	
	@Autowired
	private RolesRepository roleRepo;

	@Autowired
	private UserService userService;
	
	
	
    private final static Logger logger = Logger.getLogger(DocumentServiceImpl.class.getName());
	
	@Autowired
	public DocumentServiceImpl(DocUserActionRepository docUserActionRepo,
			RevenueProposalRepository revenueProposalRepository, DocumentMasterRepository documentMasterRepository,
			MasterBusinessDataRepository MasterBusinessRepo, MasterCompaniesDataRepository MasterCompaniesRepo,
			MasterSBUDataRepository MasterSbuRepo, MasterMaterialDataRepository MasterMaterialRepo,
			MasterPlantDataRepository MasterPlantRepo, CommentRepository CommentRepo,
			DocUserMasterRepository docUserMasterRepo, DocAttachmentRepository docAttachmentRepository,
			RPNumberRepository rpNumRepo, ClustersRepository clusterRepo,
			RPTransportationRepository rpTransportationRep, MasterCategoryDataRepository MasterCategoryRepo,
			FinalApprovalRepository rp_FinalApprovalRepository) {

		this.rp_FinalApprovalRepository = rp_FinalApprovalRepository;
		this.docUserMasterRepo = docUserMasterRepo;
		this.revenueProposalRepository = revenueProposalRepository;
		this.documentMasterRepository = documentMasterRepository;
		this.MasterBusinessRepo = MasterBusinessRepo;
		this.MasterCompaniesRepo = MasterCompaniesRepo;
		this.MasterSbuRepo = MasterSbuRepo;
		this.MasterMaterialRepo = MasterMaterialRepo;
		this.MasterPlantRepo = MasterPlantRepo;
		this.CommentRepo = CommentRepo;
		this.docAttachmentRepository = docAttachmentRepository;
		this.rpNumRepo = rpNumRepo;
		this.docUserActionRepo = docUserActionRepo;
		this.clusterRepo = clusterRepo;
		this.MasterCategoryRepo = MasterCategoryRepo;

		this.rpTransportationRep = rpTransportationRep;
	}

	@Override
	public ArrayList<DocUserMaster> getDocUserMasterByDocumentIdAscending(int documentId) {
	
		return (ArrayList<DocUserMaster>)docUserMasterRepo.findByDocumentIdOrderBySerialNumberAsc(documentId);
	}
	
	@Override
	public ArrayList<RevenueProposal>  getRevenueProposalByDocumentId(
			int documentId) {
	
		return  (ArrayList<RevenueProposal>) revenueProposalRepository.findByDocumentId(documentId);
	}

	@Override
	public DocumentMaster getDocumentMasterByDocumentId(
			Integer documentId) {
		
		return (DocumentMaster) documentMasterRepository.findByDocumentId(documentId);
	}

	@Override
	public ArrayList<Comments> getCommentByDocumentId(Integer documentId) {
	
		ArrayList<Comments> commentsList =null;
		try {
			commentsList= (ArrayList<Comments>) CommentRepo.findByDocumentId(documentId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return commentsList;
	}

	@Override
	public Comments getCommentByAction_id(Integer actionId) {
		Comments comments = null;
		try {
			comments = CommentRepo.findByActionId(actionId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return comments;
	}

	@Override
	public ArrayList<DocumentMetaData> getDocumentMetada(
			Integer documentId,Integer userId) throws Exception{
		ArrayList<DocumentMetaData> dmds = new ArrayList<DocumentMetaData>();
		DocumentMetaData dmd = new DocumentMetaData();
		
		
		List<RevenueProposal> rps = revenueProposalRepository.findByDocumentId(documentId);
		
		dmd.setRpNo(rps.get(0).getRpNumber());
		DocumentMaster dm=documentMasterRepository.findByDocumentId(documentId);
		dmd.setIsAmendment(dm.getIsAmendment());
		dmd.setJustification(dm.getTransportationJustification());
		System.out.println("dm.getOriginalProposalDocId() >>>>>"+dm.getOriginalProposalDocId());
		dmd.setOrginalProposalId(dm.getOriginalProposalDocId());
		if(dm.getOriginalProposalDocId()!=null && dm.getOriginalProposalDocId() >0){
			DocumentMaster dom=documentMasterRepository.findByDocumentId(dm.getOriginalProposalDocId());
			if(dom!=null){
				dmd.setRpStatus(dom.getStatus());
			}
		}else{
			dmd.setRpStatus(dm.getStatus());
		}
		
		System.out.println("dm.setOrginalProposalId() >>>>>" + dmd.getOrginalProposalId());
		List<DocUserAction> dua = docUserActionHelper.findByDocumentIdAndActionStatus(dm.getDocumentId(),
				ActionStatus.PENDING);
		Users usr = null;
		if (userId != null) {
			usr = userRepo.findByUserId(userId);
		}
		 List<Users> miningController = userService.findUserBasedOnRole(DocRole.MINING_CONTROLLER);
		 List<Users> miningHead = userService.findUserBasedOnRole(DocRole.MINING_HEAD);
		 List<Users> miningCoalProcurement = userService.findUserBasedOnRole(DocRole.MINING_COAL);
		 Boolean isMining=false;
		 if(usr!=null&&(miningController.get(0).getUserId()== usr.getUserId()||miningHead.get(0).getUserId()== usr.getUserId()||miningCoalProcurement.get(0).getUserId()== usr.getUserId())){
			 isMining=true;
		 }
		Comments cmts = null;
		if ((usr != null && usr.getIsSupportingEndorser() == null)||(dm.getDeptId()!=null && dm.getDeptId()==2 && usr!=null && usr.getIsSupportingEndorser()!=null && usr.getIsSupportingEndorser() && isMining )){ 
			if (dua != null && dua.size() > 0) {
				cmts = CommentRepo.findByActionId(dua.get(0).getActionId());
			}
		} else {
			List<DocUserAction> duaTemp = docUserActionHelper.findByDocumentIdAndActionStatus(dm.getDocumentId(),
					ActionStatus.PENDING_PARALLEL);

			if (duaTemp != null && duaTemp.size() > 0) {
				for (DocUserAction duas : duaTemp) {
					if (duas.getUserId().intValue() == userId.intValue()) {
						System.out.println("userId" + userId);
						cmts = CommentRepo.findByActionId(duas.getActionId());
					}
				}
			} else {
				if (dua != null && dua.size() > 0) {
					cmts = CommentRepo.findByActionId(dua.get(0).getActionId());
				}
			}

		}
		List<DocUserAction> dua1 = docUserActionRepo.findByDocumentId(documentId);

		if (dua1.get(0).getActionTakenTimeStamp() != null) {
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			String createdDate = df.format(dua1.get(0).getActionTakenTimeStamp());
			System.out.println("Created Date: " + createdDate);
			dmd.setCommentDate(createdDate);
		} else {
			dmd.setCommentDate("");
		}
		if (cmts != null) {
			dmd.setActionId(cmts.getActionId());
			dmd.setCommentId(cmts.getComment_id());
			if (cmts.getComment() != null) {
				Blob blob = cmts.getComment();
				byte[] blobByte = blob.getBytes(1, (int) blob.length());
				String comments = new String(blobByte);
				comments.replaceAll("â‚¹", " Rs ");
				dmd.setComments(comments);

				List<DocAttachment> docAttachmentList = findByCommentId(cmts.getComment_id());

				List<String> attachmentList = new ArrayList<String>();
				for (DocAttachment docAttachment : docAttachmentList) {
					attachmentList.add(docAttachment.getFileName());
				}
				dmd.setDocAttachmentList(attachmentList);
			}
		}
		Companies company = MasterCompaniesRepo.findByCompanyId(dm.getCompanyId());
		dmd.setCompanyName(company.getCompanyName());
		dmd.setCompanyId(company.getCompanyId());
		Businesses business = MasterBusinessRepo.findByBusinessId(dm.getBusinessId());
		dmd.setBusinessName(business.getBusinessName());
		dmd.setBusinessId(business.getBusinessId());
		Sbus sbu = MasterSbuRepo.findBySbuId(dm.getSbuId());
		dmd.setSbuName(sbu.getSbuName());
		dmd.setSbuId(sbu.getSbuId());
		Plant plant = null;
		ArrayList<Plant> plantList = new ArrayList<Plant>();
		String plantName = null;
		String plantId = null;
		int i = 0;
		for (RevenueProposal rp : rps) {
			plant = MasterPlantRepo.findByPlantId(rp.getPlantId());
			plantList.add(plant);
			if (i == 0) {
				plantName = plant.getPlantName();
				plantId = String.valueOf(plant.getPlantId());
			} else {
				plantName = plantName + "," + plant.getPlantName();
				plantId = plantId + "," + plant.getPlantId();
			}
			i++;
		}
		if (plantList != null && plantList.size() == 1) {
			Integer clusterId = plant.getClusterId();
			if (clusterId != null) {
				List<Cluster> cluster = clusterRepo.findByClusterId(clusterId);
				if (cluster != null && cluster.size() > 0) {
					dmd.setCluster(cluster.get(0));
				} else {
					dmd.setCluster(new Cluster());
				}
			} else {
				dmd.setCluster(new Cluster());
			}

		} else {
			dmd.setCluster(new Cluster());
		}
		Users dmUser = userRepo.findByUserId(dm.getCreatedBy());
		if (dmUser != null && dmUser.getPlantId() != null) {
			dmd.setIsPlant("Yes");
		} else {
			dmd.setIsPlant("No");
		}
		dmd.setPlantList(plantList);
		dmd.setPlantId(plantId);
		dmd.setPlantName(plantName);
		Material material = MasterMaterialRepo.findByMaterialId(dm.getMaterialId());
		dmd.setMaterialName(material.getMaterialName());
		dmd.setMaterialId(material.getMaterialId());

		Category category = MasterCategoryRepo.findByCategoryId(dm.getCategoryId());
		if (category != null) {
			dmd.setCategoryName(category.getCategoryName());
			dmd.setCategoryId(category.getCategoryId());
		}

		dmd.setTitle(dm.getTitle());
		dmd.setVendorName(dm.getVendorName());
		dmd.setSourceName(dm.getSourceName());
		if (dm.getProposalType() != null && dm.getProposalType().equalsIgnoreCase("In-Principle")) {
			dmd.setInPrincipalApprocal(true);
		} else {
			dmd.setInPrincipalApprocal(false);
		}
		dmd.setIsExpress(dm.getIsExpress());
		dmd.setIsRelParty(dm.getIsRelatedParty());
		dmd.setMaterialDetails(dm.getMaterialDetails());

		if (dm.getOtherMaterialName() != null && !dm.getOtherMaterialName().isEmpty()) {
			dmd.setOtherMaterialName(dm.getOtherMaterialName());
		} else {
			dmd.setOtherMaterialName("");
		}
		if (dm.getDeptId() != null && dm.getDeptId() != 0) {
			dmd.setIsDept(true);
		} else {
			dmd.setIsDept(false);
		}
		dmds.add(dmd);

		return dmds;
	}

	@Override
	public ArrayList<DocumentData> getDocumentUnits(Integer documentId) {
		ArrayList<DocumentData> documentData = new ArrayList<DocumentData>();

		DocumentData documentData1;
		List<RevenueProposal> rps = revenueProposalRepository.findByDocumentId(documentId);
		if (rps != null && rps.size() > 0) {
			documentData1 = new DocumentData();
			documentData1.setPlantName("");
			documentData1.setInBudget(null);
			documentData1.setAnnualQuantity(rps.get(0).getAnnualQuantityUnit());
			documentData1
					.setOrderPeriod(rps.get(0).getOrderPeriodUnit() == null ? "" : rps.get(0).getOrderPeriodUnit());
			System.out.println("rps.get(0).getOrderPeriodUnit()" + rps.get(0).getOrderPeriodUnit());
			documentData1.setOrderQuantity(rps.get(0).getOrderQuantityUnit());
			documentData1.setpNBRate(rps.get(0).getBudgetRateUnit());
			documentData1.setPoleStarRate(rps.get(0).getPolestarRateUnit());
			documentData1.setNegotiatedRate(rps.get(0).getNegotiatedRateUnit());

			documentData.add(documentData1);
		}
		return documentData;
	}

	@Override
	public ArrayList<DocumentData> getDocumentData(Integer documentId) {

		DocumentMaster dm = documentMasterRepository.findByDocumentId(documentId);
		ArrayList<DocumentData> documentData = new ArrayList<DocumentData>();

		DocumentData documentData1;
		List<RevenueProposal> rps = revenueProposalRepository.findByDocumentId(documentId);

		for (RevenueProposal rp : rps) {

			documentData1 = new DocumentData();
			Double annualQuantity = rp.getAnnualQuantity();
			annualQuantity = annualQuantity == null ? 0f : annualQuantity;

			Double orderPeriod = rp.getOrderPeriod();
			orderPeriod = orderPeriod == null ? 0f : orderPeriod;
			
			System.out.println("orderPeriod >>>>>"+orderPeriod);

			Double annualPeriod = rp.getAnnualPeriod();
			annualPeriod = annualPeriod == null ? 0f : annualPeriod;

			Double orderQuantity = rp.getOrderQuantity();
			orderQuantity = orderQuantity == null ? 0f : orderQuantity;

			if (dm.getDeptId() != null && dm.getDeptId() == 2) {
				annualQuantity = annualQuantity == null ? 0f : annualQuantity/1000 ;
				orderQuantity = orderQuantity == null ? 0f : orderQuantity/1000;
			}

			Double budgetedRate = rp.getBudgetedRate();
			budgetedRate = budgetedRate == null ? 0f : budgetedRate;

			Double polestarRate = rp.getPolestarRate();
			polestarRate = polestarRate == null ? 0f : polestarRate;

			Double negotiatedRate = rp.getNegotiatedRate();
			negotiatedRate = negotiatedRate == null ? 0f : negotiatedRate;

			Double negotiatedRate_mkcal = rp.getNegotiatedRate_mkcal();
			negotiatedRate_mkcal = negotiatedRate_mkcal == null ? 0f : negotiatedRate_mkcal;

			Double bugdetedRate_mkcal = rp.getBudgetedRate_mkcal();
			bugdetedRate_mkcal = bugdetedRate_mkcal == null ? 0f : bugdetedRate_mkcal;

			Double proposedPremium = rp.getProposedPremium();
			proposedPremium = proposedPremium == null ? 0f : proposedPremium;

			Double amountNegotiated = rp.getAmountNegotiated();
			amountNegotiated = amountNegotiated == null ? 0f : amountNegotiated;

			Double savingsBudgeted = rp.getSavingsBudgeted();
			savingsBudgeted = savingsBudgeted == null ? 0f : savingsBudgeted;

			Double savingsPolestar = rp.getSavingsPolestar();
			savingsPolestar = savingsPolestar == null ? 0f : savingsPolestar;

			Double approvedRate = rp.getApprovedRate();
			approvedRate = approvedRate == null ? negotiatedRate : approvedRate;

			Double approvedQuantity = rp.getApprovedQuantity();

			if (dm.getDeptId() != null && dm.getDeptId() == 2) {
				approvedQuantity = approvedQuantity == null ? orderQuantity : approvedQuantity/1000;
			} else {
				approvedQuantity = approvedQuantity == null ? orderQuantity : approvedQuantity;
			}

			Double approvedPeriod = rp.getApprovedPeriod();
			approvedPeriod = approvedPeriod == null ? orderPeriod : approvedPeriod;

			Double amountApproved = rp.getAmountApproved();
			amountApproved = amountApproved == null ? amountNegotiated : amountApproved;

			Double amountApproved_mkcal = rp.getApprovedNegotiatedRate_mkcal();
			amountApproved_mkcal = amountApproved_mkcal == null ? negotiatedRate_mkcal : amountApproved_mkcal;

			Double approvedPremium = rp.getApprovedPremium();
			approvedPremium = approvedPremium == null ? proposedPremium : approvedPremium;
			
			FinalApproval rp_FinalApproval = rp_FinalApprovalRepository.findByDocumentIdAndPlantIdAndProposalSequenceNumber(rp.getDocumentId(),rp.getPlantId(),1);
			Double finalRate=null;
			Double finalQuantity=null;
			Double amountfinal=null;
			Double amountfinal_mkcal=null ;
			Double finalPremium =null;
			if(rp_FinalApproval!=null){
			 finalRate = rp_FinalApproval.getFinalRate();
			
			 finalQuantity = rp_FinalApproval.getFinalQuantity()/1000;
			
			
			 amountfinal = rp_FinalApproval.getFinalApprovedAmount();
			
			
			 amountfinal_mkcal = rp_FinalApproval.getFinalRate_mkcal();
			
			 finalPremium = rp_FinalApproval.getFinalPremium();
			
			}
			finalRate = finalRate == null ? approvedRate : finalRate;
			finalQuantity = finalQuantity == null ? approvedQuantity : finalQuantity;
			amountfinal = amountfinal == null ? amountApproved : amountfinal;
			amountfinal_mkcal = amountfinal_mkcal == null ? amountApproved_mkcal : amountfinal_mkcal;
			finalPremium = finalPremium == null ? approvedPremium : finalPremium;

			Float amountPretax = rp.getAmountPretax();
			System.out.println("<<<< amountPretax >>>>" + amountPretax);
			amountPretax = amountPretax == null ? 0f : amountPretax;

			Float taxPercent = rp.getTaxPercent();
			System.out.println("<<<< taxPercent >>>>" + taxPercent);
			taxPercent = taxPercent == null ? 0f : taxPercent;

			Double taxAmount = rp.getTaxAmount();
			System.out.println("<<<< taxPercent >>>>" + taxAmount);
			taxAmount = taxAmount == null ? 0f : taxAmount;

			Double approvedTaxAmount = rp.getApprovedTaxAmount();
			approvedTaxAmount = approvedTaxAmount == null ? taxAmount : approvedTaxAmount;

			DecimalFormat f = new DecimalFormat(Formats.INDIAN_NUMBER_FORMAT_WITH_TWO_DECIMALS);
			DecimalFormat fWithoutDec = new DecimalFormat(Formats.INDIAN_NUMBER_FORMAT_WITH_ZERO_DECIMALS);
			DecimalFormat fWithoutCommas = new DecimalFormat(Formats.INDIAN_NUMBER_FORMAT_WITH_TWO_DECIMALS_WITHOUT_SEPERATOR);

			String annualPeriodStr = null;
			if (annualPeriod % 1 == 0) {
				annualPeriodStr = fWithoutDec.format(annualPeriod);
			} else {
				annualPeriodStr = f.format(annualPeriod);
			}

			String annualQuantityStr = null;
			if (annualQuantity % 1 == 0) {
				annualQuantityStr = fWithoutDec.format(annualQuantity);
			} else {
				annualQuantityStr = f.format(annualQuantity);
			}

			String orderPeriodStr = null;
			if (orderPeriod % 1 == 0) {
				orderPeriodStr = fWithoutDec.format(orderPeriod);
			} else {
				orderPeriodStr = f.format(orderPeriod);
			}

			String orderQuantityStr = null;
			if (orderQuantity % 1 == 0) {
				orderQuantityStr = fWithoutDec.format(orderQuantity);
			} else {
				orderQuantityStr = f.format(orderQuantity);
			}

			String budgetedRateStr = null;
			if (budgetedRate % 1 == 0) {
				budgetedRateStr = fWithoutDec.format(budgetedRate);
			} else {
				budgetedRateStr = f.format(budgetedRate);
			}

			String polestarRateStr = null;
			if (polestarRate % 1 == 0) {
				polestarRateStr = fWithoutDec.format(polestarRate);
			} else {
				polestarRateStr = f.format(polestarRate);
			}

			String negotiatedRateStr = null;
			if (negotiatedRate % 1 == 0) {
				negotiatedRateStr = fWithoutDec.format(negotiatedRate);
			} else {
				negotiatedRateStr = f.format(negotiatedRate);
			}
			int i = 100000;
			String negotiatedAmtInLakhs = null;
			if (amountNegotiated != null) {
				double test = amountNegotiated / i;
				documentData1.setAmountApproved(test);
				negotiatedAmtInLakhs = f.format(test);
			}
			String savingsBudgetedInLakhs = null;
			if (savingsBudgeted != null) {
				double test = savingsBudgeted / i;
				savingsBudgetedInLakhs = f.format(test);
			}
			String savingsPolestarInLakhs = null;
			if (savingsPolestar != null) {
				double test = savingsPolestar / i;
				savingsPolestarInLakhs = f.format(test);
			}

			String amountApprovedInLakhs = null;
			if (amountApproved != null && amountApproved != 0) {
				double test = amountApproved / i;
				amountApprovedInLakhs = f.format(test);
			}

			Double amountFinal_inlakh = null;
			if (amountfinal != null && amountfinal != 0) {
				amountFinal_inlakh = amountfinal / i;

			}

			String amountPretaxStr = null;
			if (amountPretax != null && amountPretax != 0) {
				float test = amountPretax / i;
				amountPretaxStr = f.format(test);
			} else {
				amountPretaxStr = f.format(0);
			}

			String taxPercentStr = null;
			if (taxPercent % 1 == 0) {
				taxPercentStr = fWithoutDec.format(taxPercent);
			} else {
				taxPercentStr = f.format(taxPercent);
			}

			String taxAmountStr = null;
			if (taxAmount % 1 == 0) {
				taxAmountStr = fWithoutDec.format(taxAmount);
			} else {
				taxAmountStr = f.format(taxAmount);
			}

			String negotiatedRate_mkcal_Str = null;
			if (negotiatedRate_mkcal % 1 == 0) {
				negotiatedRate_mkcal_Str = fWithoutDec.format(negotiatedRate_mkcal);
			} else {
				negotiatedRate_mkcal_Str = f.format(negotiatedRate_mkcal);
			}

			String proposedPremium_Str = null;
			if (proposedPremium % 1 == 0) {
				proposedPremium_Str = fWithoutDec.format(proposedPremium);
			} else {
				proposedPremium_Str = f.format(proposedPremium);
			}

			String bugdetedRate_mkcal_Str = null;
			if (bugdetedRate_mkcal % 1 == 0) {
				bugdetedRate_mkcal_Str = fWithoutDec.format(bugdetedRate_mkcal);
			} else {
				bugdetedRate_mkcal_Str = f.format(bugdetedRate_mkcal);
			}
			
			System.out.println("orderPeriodStr >>>>"+orderPeriodStr);

			documentData1.setPlantName(MasterPlantRepo.findByPlantId(rp.getPlantId()).getPlantName());
			documentData1.setInBudget(Integer.parseInt(rp.getIsBudgeted()));
			documentData1.setAnnualPeriod(annualPeriodStr);
			documentData1.setAnnualQuantity(annualQuantityStr);
			documentData1.setOrderPeriod(orderPeriodStr);
			documentData1.setOrderQuantity(orderQuantityStr);
			documentData1.setpNBRate(budgetedRateStr);
			documentData1.setPoleStarRate(polestarRateStr);
			documentData1.setNegotiatedRate(negotiatedRateStr);
			documentData1.setNegotiatedAmt(amountNegotiated.toString());
			documentData1.setSavingsBudgeted(savingsBudgeted.toString());
			documentData1.setSavingsPolestar(savingsPolestar.toString());
			documentData1.setSavingsBudgetedInLakhs(savingsBudgetedInLakhs);
			documentData1.setNegotiatedAmtInLakhs(negotiatedAmtInLakhs);
			documentData1.setSavingsPolestarInLakhs(savingsPolestarInLakhs);
			documentData1.setAmountPretaxStr(amountPretaxStr);
			documentData1.setTaxPercentStr(taxPercentStr);
			documentData1.setTaxAmountStr(taxAmountStr);
			documentData1.setAmountPretax(amountPretax);
			documentData1.setTaxPercent(taxPercent);
			documentData1.setTaxAmount(taxAmount);
			documentData1.setApprovedTaxAmount(approvedTaxAmount);

			documentData1.setNegotiatedRate_mkcal(negotiatedRate_mkcal_Str);
			documentData1.setBudgetedRate_mkcal(bugdetedRate_mkcal_Str);
			documentData1.setProposedPremium(proposedPremium_Str);
			documentData1.setApprovedNegotiatedRate_mkcal(amountApproved_mkcal);
			documentData1.setApprovedPremium(approvedPremium);
			documentData1.setFinalRate(finalRate);
			documentData1.setFinalQuantity(finalQuantity);
			documentData1.setFinalApprovedAmount(amountFinal_inlakh);
			documentData1.setFinalNegotiatedRate_mkcal(amountfinal_mkcal);
			documentData1.setFinalPremium(finalPremium);

			if (approvedRate != null) {
				if (approvedRate == 0) {
					documentData1.setApprovedRate(approvedRate);
				} else {
					documentData1.setApprovedRate(approvedRate);
				}
			}

			System.out.println("<<<< approvedQuantity >>>>" + approvedQuantity);

			if (approvedPeriod != null) {
				if (approvedPeriod == 0) {
					documentData1.setApprovedPeriod(approvedPeriod);
				} else {
					documentData1.setApprovedPeriod(approvedPeriod);
				}

			}
			if (approvedTaxAmount != null) {
				if (approvedTaxAmount == 0) {
					documentData1.setApprovedTaxAmount(approvedTaxAmount);
				} else {
					documentData1.setApprovedTaxAmount(approvedTaxAmount);
				}

			}
			if (approvedQuantity != null) {
				if (approvedQuantity == 0) {
					documentData1.setApprovedQuantity(approvedQuantity);
				} else {
					documentData1.setApprovedQuantity(approvedQuantity);
				}

			}

			if (amountApproved != null) {
				if (amountApproved == 0) {
					String amtApp = fWithoutCommas.format(amountApproved);
					documentData1.setAmountApproved(Double.valueOf(amtApp));
					if (amountApprovedInLakhs == null) {
						amountApprovedInLakhs = "0";
					}
					documentData1.setAmountApprovedInLakhs(amountApprovedInLakhs);
				} else {
					String amtApp = fWithoutCommas.format(amountApproved / i);
					documentData1.setAmountApproved(Double.valueOf(amtApp));
					documentData1.setAmountApprovedInLakhs(amountApprovedInLakhs);
				}
			}

			documentData1.setPlantId(rp.getPlantId());
			documentData1.setDocumentId(documentId);
			documentData.add(documentData1);
		}
		return documentData;
	}

	@Override
	public DocAttachment getEntityObjByDocAccId(int p_Attachment_Id) {

		return docAttachmentRepository.findByPAttachmentId(p_Attachment_Id);
	}

	@Override
	public List<DocAttachment> findByCommentId(int commentId) {

		return docAttachmentRepository.findByCommentId(commentId);
	}

	@Override
	public byte[] getEntityByDocAccId(int p_Attachment_Id) {
		try {
			Session session = sessionFactory.openSession();
			// tx = session.beginTransaction();
			DocAttachment docAttachment = new DocAttachment();

			docAttachment = (DocAttachment) session.get(DocAttachment.class, p_Attachment_Id);
			Blob blob = docAttachment.getContent();

			byte[] blobByte = blob.getBytes(1, (int) blob.length());

			byte[] buff = blob.getBytes(1, (int) blob.length());
			session.close();
			return buff;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public RPNumber getRpNumberId(int rpNumberId) {

		return rpNumRepo.findByRpNumberId(rpNumberId);
	}

	@Override
	public ArrayList<RPNumber> getRPNumbers() {

		return (ArrayList<RPNumber>) rpNumRepo.findAll();
	}

	@Override
	public DocUserMaster getDocUserMasterByDocumentIdAndUserId(Integer documentId, Integer userId) {

		return docUserMasterRepo.findByDocumentIdAndUserId(documentId, userId);
	}

	@Override
	public Boolean addBusinessRuleUsers(DocumentMetaDataCreate dmdc, DocumentMaster dm, ArrayList<RevenueProposal> rps,
			Boolean isReferredToInitiator) {

		return ruleEngineHelper.addBusinessRuleUsers(dmdc, dm, rps, isReferredToInitiator);

	}

	@Override
	public ArrayList<DocumentData> getDocumentTotal(
			Integer documentId) {
		DocumentData documentData = new DocumentData();
		ArrayList<DocumentData> documentDataList = new ArrayList<DocumentData>();
		List<RevenueProposal> rps = revenueProposalRepository.findByDocumentId(documentId);
		DocumentMaster dm= documentMasterRepository.findByDocumentId(documentId);
		Integer deptId = null;
		if(dm.getDeptId()!=null){
			deptId=dm.getDeptId();
		}
		
		Double annualPeriod=0d;
		Double annualQuantity=0d;
		Double orderPeriod=0d;
		Double orderQuantity=0d;
		Double taxAmount=0d;
		Double pNBRate=0d;
		Double poleStarRate=0d;
		Double negotiatedRate=0d;
		Double negotiatedAmt=0d;
		Double savingsBudgeted=0d;
		Double savingsPolestar=0d;
		Double totPNBRate=0d;
		Double totPolestarRate=0d;
		Double totNegotiatedRate=0d;
		Double totOrderPeriod=0d;
		Double totOrderQuantity=0d;
		Double totApprovedRate=0d;
		Double totAmountPretax=0d;
		Double totTaxPercent=0d;
		Double totTaxAmount=0d;
		Double negotiatedRateTotal=0d;
		Double totApprovedTaxAmount=0d;
		Double totPNBRate_mkcal =0d;
		Double totProposedPremium =0d;
		Double totNegotiatedRate_mkcal=0d;
		Double totApprovedQuantity=0d;
		Double totApprovedPeriod=0d;
		Double totApprovedAmt=0d;
		Double totApprovedPremium =0d;
		Double totApprovedNegotiatedRate_mkcal=0d;
		Double totFinalAmt=0d;
		Double totFinalPremium =0d;
		Double totFinalNegotiatedRate_mkcal=0d;
		Double totFinalQuantity=0d;
		Double totFinalRate=0d;
		
		for(RevenueProposal rp:rps){
			Double tempAnnualPeriod=rp.getAnnualPeriod();
			tempAnnualPeriod = tempAnnualPeriod == null ? 0f :tempAnnualPeriod;
			

			
			Double tempAnnualQuantity=rp.getAnnualQuantity();
			tempAnnualQuantity = tempAnnualQuantity == null ? 0f :tempAnnualQuantity;
			
			orderPeriod = rp.getOrderPeriod();
			orderPeriod = orderPeriod == null ? 0f : orderPeriod;
			
			orderQuantity = rp.getOrderQuantity();
			orderQuantity = orderQuantity == null ? 0f : orderQuantity;
			
			if(dm.getDeptId()!=null && dm.getDeptId()==2){
				tempAnnualQuantity = tempAnnualQuantity == null ? 0f :tempAnnualQuantity/1000;
				orderQuantity = orderQuantity == null ? 0f : orderQuantity/1000;
			}
			
			
			taxAmount = rp.getTaxAmount();
			taxAmount = taxAmount == null ? 0f : taxAmount;
			
			pNBRate = rp.getBudgetedRate();
			pNBRate = pNBRate == null ? 0f : pNBRate;
			
			poleStarRate = rp.getPolestarRate();
			poleStarRate = poleStarRate == null ? 0f : poleStarRate;
			
			negotiatedRate = rp.getNegotiatedRate();
			negotiatedRate = negotiatedRate == null ? 0f : negotiatedRate;
			
			Double amountNegotiated = rp.getAmountNegotiated();
			amountNegotiated = amountNegotiated == null ? 0f : amountNegotiated;
			
			Double tmpSavingsBudgeted = rp.getSavingsBudgeted();
			tmpSavingsBudgeted = tmpSavingsBudgeted == null ? 0f : tmpSavingsBudgeted;
			
			Double tmpSavingsPolestar = rp.getSavingsPolestar();
			tmpSavingsPolestar = tmpSavingsPolestar == null ? 0f : tmpSavingsPolestar;
			
			Double approvedPeriod = rp.getApprovedPeriod();
			approvedPeriod = approvedPeriod == null ? orderPeriod : approvedPeriod;
			
			Double approvedQuantity = rp.getApprovedQuantity();
			
			
			if(dm.getDeptId()!=null && dm.getDeptId()==2){
				approvedQuantity = approvedQuantity == null ? orderQuantity*1000: approvedQuantity;
			}else{
				approvedQuantity = approvedQuantity == null ? orderQuantity : approvedQuantity;
			}
			

			Double approvedTaxAmount = rp.getApprovedTaxAmount();
			approvedTaxAmount = approvedTaxAmount == null ? taxAmount : approvedTaxAmount;
			
			Double approvedAmount = rp.getAmountApproved();
			approvedAmount = approvedAmount == null ? amountNegotiated : approvedAmount;
			
			Double approvedRate = rp.getApprovedRate();
			approvedRate = approvedRate == null ? negotiatedRate : approvedRate;
			
			Float amountPretax = rp.getAmountPretax();
			amountPretax = amountPretax == null ? 0 : amountPretax;
			
			Float taxPercent = rp.getTaxPercent();
			taxPercent = taxPercent == null ? 0 : taxPercent;
			taxPercent =taxPercent*amountPretax;
			totTaxPercent=totTaxPercent+taxPercent;
			
			totTaxAmount=totTaxAmount+taxAmount;
			
			
			Double negotiatedRate_mkcal = rp.getNegotiatedRate_mkcal();
			negotiatedRate_mkcal = negotiatedRate_mkcal == null ? 0f : negotiatedRate_mkcal;
			
			Double pNBRate_mkcal = rp.getBudgetedRate_mkcal();
			pNBRate_mkcal = pNBRate_mkcal == null ? 0f : pNBRate_mkcal;
			
			Double proposedPremium = rp.getProposedPremium();
			proposedPremium = proposedPremium == null ? 0f : proposedPremium;
			
			Double approvedNegotiatedRate_mkcal = rp.getApprovedNegotiatedRate_mkcal();
			approvedNegotiatedRate_mkcal = approvedNegotiatedRate_mkcal == null ? negotiatedRate_mkcal : approvedNegotiatedRate_mkcal;
			
			Double approvedPremium = rp.getApprovedPremium();
			approvedPremium = approvedPremium == null ? proposedPremium : approvedPremium;
			
			FinalApproval rp_final_Approval = new FinalApproval();
			
			FinalApproval rp_final_Approval_Values = rp_FinalApprovalRepository.findByDocumentIdAndPlantIdAndProposalSequenceNumber(rp.getDocumentId(), rp.getPlantId(), 1);
			if(rp_final_Approval_Values!=null){
				rp_final_Approval=rp_final_Approval_Values;
			}
			
			Double finalNegotiatedRate_mkcal=null;
			Double finalPremium = null;
			Double finalApprovedAmount = null;
			Double finalQuantity = null;
			Double finalRate = null;
			
			if(rp_final_Approval!=null){
				finalNegotiatedRate_mkcal = rp_final_Approval.getFinalRate_mkcal();
				finalNegotiatedRate_mkcal = finalNegotiatedRate_mkcal == null ? approvedNegotiatedRate_mkcal : finalNegotiatedRate_mkcal;
	
				finalPremium = rp_final_Approval.getFinalPremium();
				finalPremium = finalPremium == null ? approvedPremium : finalPremium;
				
				finalApprovedAmount = rp_final_Approval.getFinalApprovedAmount();
				finalApprovedAmount = finalApprovedAmount== null? approvedAmount :finalApprovedAmount;
				
				finalQuantity = rp_final_Approval.getFinalQuantity();
				finalQuantity = finalQuantity== null ? approvedQuantity : finalQuantity;
				
				finalRate = rp_final_Approval.getFinalRate();
				finalRate = finalRate == null? approvedRate : finalRate;
			}
			
				
			negotiatedRateTotal=negotiatedRateTotal+negotiatedRate;
			totAmountPretax=totAmountPretax+amountPretax;
			annualPeriod=annualPeriod+tempAnnualPeriod;
			annualQuantity=annualQuantity+tempAnnualQuantity;
			totApprovedQuantity=totApprovedQuantity+approvedQuantity;
			totApprovedPeriod=totApprovedPeriod+approvedPeriod;
			totApprovedTaxAmount=totApprovedTaxAmount+approvedTaxAmount;
			totApprovedAmt=totApprovedAmt+approvedAmount;
			negotiatedAmt=negotiatedAmt+amountNegotiated;
			savingsBudgeted=savingsBudgeted+tmpSavingsBudgeted;
			savingsPolestar=savingsPolestar+tmpSavingsPolestar;
			totFinalQuantity=totFinalQuantity+finalQuantity;
			totFinalRate=totFinalRate+finalRate*finalQuantity;
			totFinalAmt=totFinalAmt+finalApprovedAmount;
			totFinalPremium=totFinalPremium+finalPremium*finalQuantity;
			totFinalNegotiatedRate_mkcal=totFinalNegotiatedRate_mkcal+finalNegotiatedRate_mkcal*finalApprovedAmount;
			
			if(deptId!=null && deptId==1){
			totPNBRate=totPNBRate+pNBRate;//*orderQuantity;
			}else{
				totPNBRate=totPNBRate+pNBRate*orderQuantity;
			}
			totPolestarRate=totPolestarRate+poleStarRate*orderQuantity;
			totNegotiatedRate=totNegotiatedRate+negotiatedRate*orderQuantity;
			if(deptId!=null&& deptId==1){
				totApprovedRate=totApprovedRate+approvedRate;
			}else{
				totApprovedRate=totApprovedRate+approvedRate*approvedQuantity;
			}
			totOrderQuantity=totOrderQuantity+orderQuantity;
			totOrderPeriod=totOrderPeriod+orderPeriod;
			
			totNegotiatedRate_mkcal=totNegotiatedRate_mkcal+negotiatedRate_mkcal*amountNegotiated;
//			totPNBRate_mkcal=totPNBRate_mkcal+pNBRate_mkcal*amountNegotiated;
			totProposedPremium=totProposedPremium+proposedPremium*orderQuantity ;
			
			totApprovedNegotiatedRate_mkcal=totApprovedNegotiatedRate_mkcal+approvedNegotiatedRate_mkcal*approvedAmount;
			totApprovedPremium=totApprovedPremium+approvedPremium*approvedQuantity;
			
		}
		if( totAmountPretax != null && !totAmountPretax.isNaN()){
				totTaxPercent= totTaxPercent/totAmountPretax;
		}
		Double finNegotiatedRate_mkcal=dm.getTotal_Proposed_Rate_mkcal();
		Double finPNBRate_mkcal=dm.getTotal_PnB_Rate_mkcal();
		Double finProposedPremium=totProposedPremium/totOrderQuantity;
		
		finNegotiatedRate_mkcal = finNegotiatedRate_mkcal == null ? 0f : finNegotiatedRate_mkcal; 
		finPNBRate_mkcal = finPNBRate_mkcal == null ? 0f : finPNBRate_mkcal; 
		totProposedPremium = totProposedPremium == null ? 0f : totProposedPremium;
		
		Double finTotalApprovedNegotiatedRate_mkcal = null;
				if(dm.getTotal_Approved_Rate_mkcal()!=null){
					finTotalApprovedNegotiatedRate_mkcal=dm.getTotal_Approved_Rate_mkcal();
				}
//				totApprovedNegotiatedRate_mkcal/totApprovedAmt;
		finTotalApprovedNegotiatedRate_mkcal=(finTotalApprovedNegotiatedRate_mkcal==null || finTotalApprovedNegotiatedRate_mkcal.isNaN())?finNegotiatedRate_mkcal:finTotalApprovedNegotiatedRate_mkcal;
		Double finTotalApprovedPremium = totApprovedPremium/totApprovedQuantity;
		
		Double totalFinalApprovedNegotiatedRate_mkcal = null;
		if(dm.getTotal_Final_Rate_mkcal()!=null){
			totalFinalApprovedNegotiatedRate_mkcal=dm.getTotal_Final_Rate_mkcal();
		}
//				;
//				totFinalNegotiatedRate_mkcal/totFinalAmt;
		totalFinalApprovedNegotiatedRate_mkcal=(totalFinalApprovedNegotiatedRate_mkcal==null || totalFinalApprovedNegotiatedRate_mkcal.isNaN())?finTotalApprovedNegotiatedRate_mkcal:totalFinalApprovedNegotiatedRate_mkcal;
		Double totalFinalPremium = totFinalPremium/totFinalQuantity;
		
		
		Double finNegotiatedRate=totNegotiatedRate/totOrderQuantity;
		Double finPolestarRate=totPolestarRate/totOrderQuantity;
		Double finPNBRate= 0d;
		Double finApprovedRate= 0d;
		
		
		
		
		if(deptId!=null&& deptId==1){
			finPNBRate=totPNBRate;
			finApprovedRate=totApprovedRate;
		}else{
			finPNBRate=totPNBRate/totOrderQuantity;
			finApprovedRate=totApprovedRate/totApprovedQuantity;
			totFinalRate=totFinalRate/totFinalQuantity;
		}
		annualPeriod = (annualPeriod == null || annualPeriod.isNaN()) ? 0f : annualPeriod; 
		
		annualQuantity = (annualQuantity == null || annualQuantity.isNaN()) ? 0f : annualQuantity; 
		
		totOrderPeriod = (totOrderPeriod == null || totOrderPeriod.isNaN()) ? 0f : totOrderPeriod; 
		
		totOrderQuantity = (totOrderQuantity == null || totOrderQuantity.isNaN()) ? 0f : totOrderQuantity; 
		
		totTaxPercent = (totTaxPercent == null || totTaxPercent.isNaN()) ? 0f : totTaxPercent;
		
		totTaxAmount = (totTaxAmount == null || totTaxAmount.isNaN()) ? 0f : totTaxAmount;
		
		finPNBRate = (finPNBRate == null || finPNBRate.isNaN()) ? 0f : finPNBRate; 
		
		finPolestarRate = (finPolestarRate == null || finPolestarRate.isNaN()) ? 0f : finPolestarRate; 
		
		finNegotiatedRate = (finNegotiatedRate == null || finNegotiatedRate.isNaN()) ? 0f : finNegotiatedRate; 
		
		negotiatedAmt = (negotiatedAmt == null || negotiatedAmt.isNaN()) ? 0f : negotiatedAmt; 
		
		savingsBudgeted = (savingsBudgeted == null || savingsBudgeted.isNaN()) ? 0f : savingsBudgeted; 
		
		savingsPolestar = (savingsPolestar == null || savingsPolestar.isNaN()) ? 0f : savingsPolestar; 
		
		finApprovedRate = (finApprovedRate == null || finApprovedRate.isNaN())  ? 0f : finApprovedRate; 
		
		totalFinalPremium = (totalFinalPremium == null || totalFinalPremium.isNaN())  ? 0f : totalFinalPremium;
		
//		totFinalRate = (totFinalRate == null || totFinalRate.isNaN())  ? 0f : totFinalRate;
		

		
		DecimalFormat f = new DecimalFormat(Formats.INDIAN_NUMBER_FORMAT_WITH_TWO_DECIMALS);
		DecimalFormat fWithoutDec = new DecimalFormat(Formats.INDIAN_NUMBER_FORMAT_WITH_ZERO_DECIMALS);
		DecimalFormat fWithoutCommas = new DecimalFormat(Formats.INDIAN_NUMBER_FORMAT_WITH_TWO_DECIMALS_WITHOUT_SEPERATOR);
			
		String annualPeriodStr=null;
		if(annualPeriod % 1 == 0){
			annualPeriodStr= fWithoutDec.format(annualPeriod);
		}else{
			annualPeriodStr= f.format(annualPeriod);
		}
		
		String orderPeriodStr=null;
		if(orderPeriod % 1 == 0){
			orderPeriodStr= fWithoutDec.format(totOrderPeriod);
		}else{
			orderPeriodStr= f.format(totOrderPeriod);
		}
		
		String annualQuantityStr=null;
		if(annualQuantity % 1 == 0){
			annualQuantityStr= fWithoutDec.format(annualQuantity);
		}else{
			annualQuantityStr= f.format(annualQuantity);
		}
		
		String orderQuantityStr=null;
		if(orderQuantity % 1 == 0){
			orderQuantityStr= fWithoutDec.format(totOrderQuantity);
		}else{
			orderQuantityStr= f.format(totOrderQuantity);
		}
		
		String finPNBRateStr=null;
		if(finPNBRate % 1 == 0){
			finPNBRateStr= fWithoutDec.format(finPNBRate);
		}else{
			finPNBRateStr= f.format(finPNBRate);
		}
		
		String finPolestarRateStr=null;
		if(finPolestarRate % 1 == 0){
			finPolestarRateStr= fWithoutDec.format(finPolestarRate);
		}else{
			finPolestarRateStr= f.format(finPolestarRate);
		}
		
		String finNegotiatedRateStr=null;
		if(finNegotiatedRate % 1 == 0){
			finNegotiatedRateStr= fWithoutDec.format(finNegotiatedRate);
		}else{
			finNegotiatedRateStr= f.format(finNegotiatedRate);
		}
		int i=100000;
		String negotiatedAmtInLakhs=null;
		if(negotiatedAmt!=null){
			double test=negotiatedAmt/i;
			negotiatedAmtInLakhs= f.format(test);
		}
		String savingsBudgetedInLakhs=null;
		if(savingsBudgeted!=null){
			double test=savingsBudgeted/i;
			savingsBudgetedInLakhs= f.format(test);
		}
		String savingsPolestarInLakhs=null;
		if(savingsPolestar!=null){
			double test=savingsPolestar/i;
			savingsPolestarInLakhs= f.format(test);
		}
		
		String amountApprovedInLakhs=null;
		if(finApprovedRate!=null){
			double test=totApprovedAmt/i;
			amountApprovedInLakhs= f.format(test);
		}
		
		Double finalApprovedAmountInLakhs=null;
		if(totFinalAmt!=null){
			finalApprovedAmountInLakhs = totFinalAmt/i;
//			finalApprovedAmountInLakhs= f.format(test);
		}
		String amountPretaxInLakhs=null;
		if(totAmountPretax!=null){
			double test=totAmountPretax/i;
			amountPretaxInLakhs= f.format(test);
		}
		
		String negotiatedRateTotalStr=null;
		if(negotiatedRateTotal % 1 == 0){
			negotiatedRateTotalStr= fWithoutDec.format(negotiatedRateTotal);
		}else{
			negotiatedRateTotalStr= f.format(negotiatedRateTotal);
		}
		
		String finNegotiatedRate_mkcalStr=null;
		if(finNegotiatedRate_mkcal % 1 == 0){
			finNegotiatedRate_mkcalStr= fWithoutDec.format(finNegotiatedRate_mkcal);
		}else{
			finNegotiatedRate_mkcalStr= f.format(finNegotiatedRate_mkcal);
		}
		
		String finPNBRate_mkcalStr=null;
		if(finPNBRate_mkcal% 1 == 0){
			finPNBRate_mkcalStr= fWithoutDec.format(finPNBRate_mkcal);
		}else{
			finPNBRate_mkcalStr= f.format(finPNBRate_mkcal);
		}
		
		String finProposedPremiumStr=null;
		if(finProposedPremium % 1 == 0){
			finProposedPremiumStr= fWithoutDec.format(finProposedPremium);
		}else{
			finProposedPremiumStr= f.format(finProposedPremium);
		}
		finTotalApprovedPremium = (finTotalApprovedPremium == null || finTotalApprovedPremium.isNaN())  ? 0f : finTotalApprovedPremium;
		
		documentData.setPlantName("Total");
		documentData.setAnnualPeriod(annualPeriodStr);
		documentData.setOrderPeriod(orderPeriodStr);
		documentData.setAnnualQuantity(annualQuantityStr);
		documentData.setOrderQuantity(orderQuantityStr);
		documentData.setpNBRate(finPNBRateStr);
		documentData.setPoleStarRate(finPolestarRateStr);
		documentData.setNegotiatedRate(finNegotiatedRateStr);
		documentData.setNegotiatedAmt(negotiatedAmt.toString());
		documentData.setSavingsBudgeted(savingsBudgeted.toString());
		documentData.setSavingsPolestar(savingsPolestar.toString());
		documentData.setSavingsBudgetedInLakhs(savingsBudgetedInLakhs);
		documentData.setNegotiatedAmtInLakhs(negotiatedAmtInLakhs);
		documentData.setSavingsPolestarInLakhs(savingsPolestarInLakhs);
		documentData.setAmountPretax(new BigDecimal(totAmountPretax).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue());
		documentData.setTaxPercent(new BigDecimal(totTaxPercent).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue());
		documentData.setTaxAmount(new BigDecimal(totTaxAmount).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
		System.out.println("negotiatedRateTotal"+negotiatedRateTotal);
		documentData.setNegotiatedRateTotal(negotiatedRateTotalStr);
		documentData.setAmountPretaxStr(amountPretaxInLakhs);
		documentData.setNegotiatedRate_mkcal(finNegotiatedRate_mkcalStr);
		documentData.setBudgetedRate_mkcal(finPNBRate_mkcalStr);
		System.out.println("budgetedRate_mkcal"+finPNBRate_mkcal.doubleValue());
		documentData.setProposedPremium(finProposedPremiumStr);
		documentData.setApprovedNegotiatedRate_mkcal(new BigDecimal(finTotalApprovedNegotiatedRate_mkcal).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
		documentData.setApprovedPremium(new BigDecimal(finTotalApprovedPremium).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
		documentData.setFinalApprovedAmount(new BigDecimal(finalApprovedAmountInLakhs).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
		documentData.setFinalNegotiatedRate_mkcal(new BigDecimal(totalFinalApprovedNegotiatedRate_mkcal).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
		documentData.setFinalPremium(new BigDecimal(totalFinalPremium).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
		documentData.setFinalQuantity(new BigDecimal(totFinalQuantity).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
		totFinalRate = (totFinalRate == null || totFinalRate.isNaN())  ? 0f : totFinalRate;
		documentData.setFinalRate(new BigDecimal(totFinalRate).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
		
		

		if(totApprovedPeriod!=null){
			if(totApprovedPeriod ==0 )
			{
				String appPeriod =fWithoutCommas.format(totApprovedPeriod);
				documentData.setApprovedPeriod(Double.valueOf(appPeriod));
			}
			else
			{
				String appPeriod =fWithoutCommas.format(totApprovedPeriod);
				documentData.setApprovedPeriod(Double.valueOf(appPeriod));
			}			
		}
		
		if(totApprovedQuantity!=null){
			if(totApprovedQuantity ==0 )
			{
				String appRate =fWithoutCommas.format(totApprovedQuantity);
				documentData.setApprovedQuantity(Double.valueOf(appRate));
			}
			else
			{
				String appRate =fWithoutCommas.format(totApprovedQuantity);
				documentData.setApprovedQuantity(Double.valueOf(appRate));
			}			
		}
		if(totApprovedTaxAmount!=null){
			if(totApprovedTaxAmount ==0 )
			{
				String appTax =fWithoutCommas.format(totApprovedTaxAmount);
				documentData.setApprovedTaxAmount(Double.valueOf(appTax));
			}
			else
			{
				String appTax =fWithoutCommas.format(totApprovedTaxAmount);
				documentData.setApprovedTaxAmount(Double.valueOf(appTax));
			}
		}
		
		System.out.println("<<<< finApprovedRate >>>>>"+finApprovedRate);
		if(finApprovedRate!=null){
			if(finApprovedRate == 0)
			{
				String appRate =fWithoutCommas.format(finApprovedRate);
				documentData.setApprovedRate(Double.valueOf(appRate));
			}
			else
			{
				String appRate =fWithoutCommas.format(finApprovedRate);
				documentData.setApprovedRate(Double.valueOf(appRate));
			}
		}
		
		if(totApprovedAmt!=null){
			if(totApprovedAmt == 0)
			{
				String amtApp =fWithoutCommas.format(totApprovedAmt/i);
				documentData.setAmountApproved(Double.valueOf(amtApp));
				documentData.setAmountApprovedInLakhs(amountApprovedInLakhs);
			}
			else
			{
				String amtApp =fWithoutCommas.format(totApprovedAmt/i);
				documentData.setAmountApproved(Double.valueOf(amtApp));
				documentData.setAmountApprovedInLakhs(amountApprovedInLakhs);
			}
		}
		
		documentDataList.add(documentData);
		return documentDataList;
	}

	@Override
	public DocUserMaster getUserBydocumentIdAndDocRole(Integer documentId) {
		return docUserMasterRepo.findByDocumentIdAndDocRole(documentId, DocRole.APPROVER);

	}

	@Override
	public Map<String, List<String>> getMailIdsToSend(DocumentMaster dm, String isCreate,String isCreateFinalApproved, String isApprove,
			String isRejected, String isRelease, String isEndorse, Integer userIdInt, Integer delegatedTo,
			Integer delegatedByInt, String rpNumber, List<RevenueProposal> revenueProposalList, String isReferedBack,
			String referedTo, String isReferedRp, String isCancelled) {

		// return mailNotificationHelper.getMailIdsToSend(dm, isCreate,
		// isApprove, isRejected, isRelease, isEndorse, userIdInt,
		// delegatedByInt, rpNumber, revenueProposalList, isReferedBack,
		// referedTo, isReferedRp);
		return mailNotificationHelper.getMailIds(dm, isCreate,isCreateFinalApproved, isApprove, isRejected, isRelease, isEndorse, userIdInt,
				delegatedTo, delegatedByInt, rpNumber, revenueProposalList, isReferedBack, referedTo, isReferedRp,
				isCancelled);

	}

	@Override
	public Integer deleteByDocumentIdAndDocRoleIn(DocumentMaster dm) {
		// TODO Auto-generated method stub
		int documentId = dm.getDocumentId();
		List<String> docRoleList = new ArrayList<String>();
		docRoleList.add(DocRole.APPROVER);
		docRoleList.add(DocRole.ENDORSER);
		docRoleList.add(DocRole.INITIATOR);
		docRoleList.add(DocRole.CONTROLLER);
		return docUserMasterRepo.deleteByDocumentId(documentId);
	}

	@Override
	public ArrayList<RevenueProposal> getDraftDocumentData(Integer documentId) throws SQLException {
		DocumentMaster dm = documentMasterRepository.findByDocumentId(documentId);
		List<RevenueProposal> rps = revenueProposalRepository.findByDocumentId(documentId);
		List<Comments> cmts = CommentRepo.findByDocumentId(documentId);
		ArrayList<RevenueProposal> tempRps = new ArrayList<RevenueProposal>();
		for (RevenueProposal rp : rps) {

			Double annualQuantity = rp.getAnnualQuantity();
			annualQuantity = annualQuantity == null ? 0f : annualQuantity;

			Double orderQuantity = rp.getOrderQuantity();
			orderQuantity = orderQuantity == null ? 0f : orderQuantity;

			if (dm.getDeptId() != null && dm.getDeptId() == 2) {
				annualQuantity = annualQuantity == null ? 0f : annualQuantity ;
				orderQuantity = orderQuantity == null ? 0f : orderQuantity ;
			}

			Double budgetedRate = rp.getBudgetedRate();
			budgetedRate = budgetedRate == null ? 0f : budgetedRate;

			Double polestarRate = rp.getPolestarRate();
			polestarRate = polestarRate == null ? 0f : polestarRate;

			Double negotiatedRate = rp.getNegotiatedRate();
			negotiatedRate = negotiatedRate == null ? 0f : negotiatedRate;

			Double negotiatedRate_mkcal = rp.getNegotiatedRate_mkcal();
			negotiatedRate_mkcal = negotiatedRate_mkcal == null ? 0f : negotiatedRate_mkcal;

			Double bugdetedRate_mkcal = rp.getBudgetedRate_mkcal();
			bugdetedRate_mkcal = bugdetedRate_mkcal == null ? 0f : bugdetedRate_mkcal;

			Double proposedPremium = rp.getProposedPremium();
			proposedPremium = proposedPremium == null ? 0f : proposedPremium;

			Double amountNegotiated = rp.getAmountNegotiated();
			amountNegotiated = amountNegotiated == null ? 0f : amountNegotiated;

			Double savingsBudgeted = rp.getSavingsBudgeted();
			savingsBudgeted = savingsBudgeted == null ? 0f : savingsBudgeted;

			Double savingsPolestar = rp.getSavingsPolestar();
			savingsPolestar = savingsPolestar == null ? 0f : savingsPolestar;

			Float taxPercent = rp.getTaxPercent();
			taxPercent = taxPercent == null ? 0f : taxPercent;

			Double taxAmount = rp.getTaxAmount();
			taxAmount = taxAmount == null ? 0f : taxAmount;

			Float amountPretax = rp.getAmountPretax();
			amountPretax = amountPretax == null ? 0f : amountPretax;

			BigDecimal bdAnnualQuantity = new BigDecimal(annualQuantity).setScale(2, BigDecimal.ROUND_HALF_UP);
			BigDecimal bdOrderQuantity = new BigDecimal(orderQuantity).setScale(2, BigDecimal.ROUND_HALF_UP);
			BigDecimal bdBudgetedRate = new BigDecimal(budgetedRate).setScale(2, BigDecimal.ROUND_HALF_UP);

			BigDecimal bdBudgetedRate_mkcal = new BigDecimal(bugdetedRate_mkcal).setScale(2, BigDecimal.ROUND_HALF_UP);
			BigDecimal bdNegotiatedRate_mkcal = new BigDecimal(negotiatedRate_mkcal).setScale(2,
					BigDecimal.ROUND_HALF_UP);
			BigDecimal bdProposedPremium = new BigDecimal(proposedPremium).setScale(2, BigDecimal.ROUND_HALF_UP);

			BigDecimal bdPolestarRate = new BigDecimal(polestarRate).setScale(2, BigDecimal.ROUND_HALF_UP);
			BigDecimal bdNegotiatedRate = new BigDecimal(negotiatedRate).setScale(2, BigDecimal.ROUND_HALF_UP);
			BigDecimal bdTaxPercent = new BigDecimal(taxPercent).setScale(2, BigDecimal.ROUND_HALF_UP);
			BigDecimal bdTaxAmount = new BigDecimal(taxAmount).setScale(5, BigDecimal.ROUND_HALF_UP);
			BigDecimal bdAmountPretax = new BigDecimal(amountPretax).setScale(2, BigDecimal.ROUND_HALF_UP);
			rp.setPlantName(MasterPlantRepo.findByPlantId(rp.getPlantId()).getPlantName());
			rp.setAnnualQuantity(bdAnnualQuantity.doubleValue());
			rp.setOrderQuantity(bdOrderQuantity.doubleValue());
			rp.setBudgetedRate(bdBudgetedRate.doubleValue());
			rp.setPolestarRate(bdPolestarRate.doubleValue());
			rp.setNegotiatedRate(bdNegotiatedRate.doubleValue());
			rp.setSavingsBudgetedStr(savingsBudgeted.toString());
			rp.setSavingsPolestarStr(savingsPolestar.toString());
			rp.setAmountNegotiatedStr(amountNegotiated.toString());
			rp.setTaxPercent(bdTaxPercent.floatValue());
			rp.setTaxAmount(bdTaxAmount.doubleValue());
			rp.setAmountPretax(bdAmountPretax.floatValue());
			rp.setNegotiatedRate_mkcal(bdNegotiatedRate_mkcal.doubleValue());
			rp.setBudgetedRate_mkcal(bdBudgetedRate_mkcal.doubleValue());
			rp.setProposedPremium(bdProposedPremium.doubleValue());

			if (cmts != null && cmts.size() > 0) {
				rp.setActionId(cmts.get(cmts.size() - 1).getActionId());
				rp.setCommentId(cmts.get(cmts.size() - 1).getComment_id());
				if (cmts.get(cmts.size() - 1) != null && cmts.get(cmts.size() - 1).getComment() != null) {
					Blob blob = cmts.get(cmts.size() - 1).getComment();
					byte[] blobByte = blob.getBytes(1, (int) blob.length());
					String comments = new String(blobByte);
					comments.replaceAll("â‚¹", " Rs ");
					rp.setComments(comments);
					List<DocAttachment> docAttachmentList = findByCommentId(cmts.get(cmts.size() - 1).getComment_id());
					List<String> attachmentList = new ArrayList<String>();
					for (DocAttachment docAttachment : docAttachmentList) {
						attachmentList.add(docAttachment.getFileName());
					}
					rp.setDocAttachmentList(attachmentList);
				}

			}
			tempRps.add(rp);
		}
		System.out.println(tempRps);
		return tempRps;
		
	}

	@Override
	public ArrayList<RevenueProposal> getDraftDocumentTotal(Integer documentId) {
		DocumentMaster dm = documentMasterRepository.findByDocumentId(documentId);
		ArrayList<RevenueProposal> documentDataList = new ArrayList<RevenueProposal>();
				
		List<RevenueProposal> rps = revenueProposalRepository.findByDocumentId(documentId);
		Double annualQuantity = 0d;
		Double orderQuantity = 0d;
		Double orderPeriod = 0d;
		Double pNBRate = 0d;
		Double poleStarRate = 0d;
		Double negotiatedRate = 0d;
		Double negotiatedAmt = 0d;
		Double savingsBudgeted = 0d;
		Double savingsPolestar = 0d;
		Double totPNBRate = 0d;
		Double totPolestarRate = 0d;
		Double totNegotiatedRate = 0d;
		Double totOrderQuantity = 0d;
		Double totOrderPeriod = 0d;
		Double totAmountPreTax = 0d;
		Double totTaxPercent = 0d;
		Double totTaxAmount = 0d;
		Double negotiatedRateTotal = 0d;
		// Double negotiatedRateTotal_mkcal =0d;
		Double totPNBRate_mkcal = 0d;
		Double totProposedPremium = 0d;
		Double totNegotiatedRate_mkcal = 0d;

		for (RevenueProposal rp : rps) {

			Double tempAnnualQuantity = rp.getAnnualQuantity();
			tempAnnualQuantity = tempAnnualQuantity == null ? 0f : tempAnnualQuantity;

			orderQuantity = rp.getOrderQuantity();
			orderQuantity = orderQuantity == null ? 0f : orderQuantity;

			if (dm.getDeptId() != null && dm.getDeptId() == 2) {
				tempAnnualQuantity = tempAnnualQuantity == null ? 0f : tempAnnualQuantity ;
				orderQuantity = orderQuantity == null ? 0f : orderQuantity ;
			}

			orderPeriod = rp.getOrderPeriod();
			orderPeriod = orderPeriod == null ? 1f : orderPeriod;

			pNBRate = rp.getBudgetedRate();
			pNBRate = pNBRate == null ? 0f : pNBRate;

			poleStarRate = rp.getPolestarRate();
			poleStarRate = poleStarRate == null ? 0f : poleStarRate;

			negotiatedRate = rp.getNegotiatedRate();
			negotiatedRate = negotiatedRate == null ? 0f : negotiatedRate;

			Double amountNegotiated = rp.getAmountNegotiated();
			amountNegotiated = amountNegotiated == null ? 0f : amountNegotiated;

			Double tmpSavingsBudgeted = rp.getSavingsBudgeted();
			tmpSavingsBudgeted = tmpSavingsBudgeted == null ? 0f : tmpSavingsBudgeted;

			Double tmpSavingsPolestar = rp.getSavingsPolestar();
			tmpSavingsPolestar = tmpSavingsPolestar == null ? 0f : tmpSavingsPolestar;

			Float taxPercent = rp.getTaxPercent();
			taxPercent = taxPercent == null ? 0f : taxPercent;

			Double taxAmount = rp.getTaxAmount();
			taxAmount = taxAmount == null ? 0f : taxAmount;

			Float amountPretax = rp.getAmountPretax();
			amountPretax = amountPretax == null ? 0f : amountPretax;

			Double negotiatedRate_mkcal = dm.getTotal_Proposed_Rate_mkcal();
			negotiatedRate_mkcal = negotiatedRate_mkcal == null ? 0f : negotiatedRate_mkcal;

			Double pNBRate_mkcal = dm.getTotal_PnB_Rate_mkcal();
			pNBRate_mkcal = pNBRate_mkcal == null ? 0f : pNBRate_mkcal;

			Double proposedPremium = rp.getProposedPremium();
			proposedPremium = proposedPremium == null ? 0f : proposedPremium;

			negotiatedRateTotal = negotiatedRateTotal + negotiatedRate;
			annualQuantity = annualQuantity + tempAnnualQuantity;
			negotiatedAmt = negotiatedAmt + amountNegotiated;
			savingsBudgeted = savingsBudgeted + tmpSavingsBudgeted;
			savingsPolestar = savingsPolestar + tmpSavingsPolestar;
			totPNBRate = totPNBRate + pNBRate * orderQuantity * orderPeriod;
			totPolestarRate = totPolestarRate + poleStarRate * orderQuantity * orderPeriod;
			totNegotiatedRate = totNegotiatedRate + negotiatedRate * orderQuantity * orderPeriod;
			totOrderQuantity = totOrderQuantity + orderQuantity;
			totOrderPeriod = totOrderPeriod + orderPeriod;
			totAmountPreTax = totAmountPreTax + amountPretax;
			totTaxAmount = totTaxAmount + taxAmount;

			// negotiatedRateTotal_mkcal=negotiatedRateTotal_mkcal+negotiatedRate_mkcal;
			totNegotiatedRate_mkcal = totNegotiatedRate_mkcal + negotiatedRate_mkcal * amountNegotiated;
			totPNBRate_mkcal = totPNBRate_mkcal + pNBRate_mkcal * amountNegotiated;
			totProposedPremium = totProposedPremium + proposedPremium * orderQuantity;

		}
		if (totAmountPreTax != null && !totAmountPreTax.isNaN()) {
			totTaxPercent = totTaxPercent / totAmountPreTax;
		}
		Double finNegotiatedRate = totNegotiatedRate / totOrderQuantity;
		Double finPolestarRate = totPolestarRate / totOrderQuantity;
		Double finPNBRate = totPNBRate / totOrderQuantity;

		Double finProposedPremium = totProposedPremium / totOrderQuantity;

		Double finNegotiatedRate_mkcal = totNegotiatedRate_mkcal / negotiatedAmt;
		Double finPNBRate_mkcal = totPNBRate_mkcal / negotiatedAmt;

		annualQuantity = annualQuantity == null ? 0f : annualQuantity;

		totOrderQuantity = totOrderQuantity == null ? 0f : totOrderQuantity;

		totOrderPeriod = totOrderPeriod == null ? 0f : totOrderPeriod;

		totTaxPercent = (totTaxPercent == null || totTaxPercent.isNaN()) ? 0f : totTaxPercent;

		finPNBRate = finPNBRate == null ? 0f : finPNBRate;

		finPolestarRate = finPolestarRate == null ? 0f : finPolestarRate;

		finNegotiatedRate = finNegotiatedRate == null ? 0f : finNegotiatedRate;

		negotiatedAmt = negotiatedAmt == null ? 0f : negotiatedAmt;

		savingsBudgeted = savingsBudgeted == null ? 0f : savingsBudgeted;

		savingsPolestar = savingsPolestar == null ? 0f : savingsPolestar;

		finNegotiatedRate_mkcal = finNegotiatedRate_mkcal == null ? 0f : finNegotiatedRate_mkcal;
		finPNBRate_mkcal = finPNBRate_mkcal == null ? 0f : finPNBRate_mkcal;
		totProposedPremium = totProposedPremium == null ? 0f : totProposedPremium;

		if (annualQuantity.isNaN()) {
			annualQuantity = 0d;
		}

		if (totOrderQuantity.isNaN()) {
			totOrderQuantity = 0d;
		}

		if (totOrderPeriod.isNaN()) {
			totOrderPeriod = 0d;
		}

		if (finPNBRate.isNaN()) {
			finPNBRate = 0d;
		}

		if (finPolestarRate.isNaN()) {
			finPolestarRate = 0d;
		}

		if (finNegotiatedRate.isNaN()) {
			finNegotiatedRate = 0d;
		}

		if (totTaxPercent.isNaN()) {
			totTaxPercent = 0d;
		}

		if (totTaxAmount.isNaN()) {
			totTaxAmount = 0d;
		}

		if (finNegotiatedRate_mkcal.isNaN()) {
			finNegotiatedRate_mkcal = 0d;
		}

		if (finPNBRate_mkcal.isNaN()) {
			finPNBRate_mkcal = 0d;
		}

		if (totProposedPremium.isNaN()) {
			totProposedPremium = 0d;
		}

		if (finProposedPremium.isNaN()) {
			finProposedPremium = 0d;
		}

		System.out.println("<<< finPolestarRate >>>>" + finPolestarRate);
		RevenueProposal rp = new RevenueProposal();
		BigDecimal bdAnnualQuantity = new BigDecimal(annualQuantity).setScale(2, BigDecimal.ROUND_HALF_UP);
		BigDecimal bdOrderQuantity = new BigDecimal(totOrderQuantity).setScale(2, BigDecimal.ROUND_HALF_UP);
		BigDecimal bdOrderPeriod = new BigDecimal(totOrderPeriod).setScale(2, BigDecimal.ROUND_HALF_UP);
		BigDecimal bdBudgetedRate = new BigDecimal(finPNBRate).setScale(2, BigDecimal.ROUND_HALF_UP);
		BigDecimal bdPolestarRate = new BigDecimal(finPolestarRate).setScale(2, BigDecimal.ROUND_HALF_UP);
		BigDecimal bdNegotiatedRate = new BigDecimal(finNegotiatedRate).setScale(2, BigDecimal.ROUND_HALF_UP);
		BigDecimal bdTotTaxPercent = new BigDecimal(totTaxPercent).setScale(2, BigDecimal.ROUND_HALF_UP);
		BigDecimal bdTotTaxAmount = new BigDecimal(totTaxAmount).setScale(2, BigDecimal.ROUND_HALF_UP);
		BigDecimal bdNegotiatedRate_mkcal = new BigDecimal(finNegotiatedRate_mkcal).setScale(2,
				BigDecimal.ROUND_HALF_UP);
		BigDecimal bdPNBRate_mkcal = new BigDecimal(finPNBRate_mkcal).setScale(2, BigDecimal.ROUND_HALF_UP);
		BigDecimal bdTotProposedPremium = new BigDecimal(finProposedPremium).setScale(2, BigDecimal.ROUND_HALF_UP);

		rp.setPlantName("Total");
		rp.setAnnualQuantity(bdAnnualQuantity.doubleValue());
		rp.setOrderQuantity(bdOrderQuantity.doubleValue());
		rp.setOrderPeriod(bdOrderPeriod.doubleValue());
		rp.setBudgetedRate(bdBudgetedRate.doubleValue());
		rp.setPolestarRate(bdPolestarRate.doubleValue());
		rp.setNegotiatedRate(bdNegotiatedRate.doubleValue());
		rp.setSavingsBudgetedStr(savingsBudgeted.toString());
		rp.setSavingsPolestarStr(savingsPolestar.toString());
		rp.setAmountNegotiatedStr(negotiatedAmt.toString());
		rp.setAmountPretaxStr(totAmountPreTax.toString());
		rp.setTaxPercent(bdTotTaxPercent.floatValue());
		rp.setTaxAmount(bdTotTaxAmount.doubleValue());
		rp.setNegotiatedRateTotal(
				new BigDecimal(negotiatedRateTotal).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue());
		rp.setNegotiatedRate_mkcal(bdNegotiatedRate_mkcal.doubleValue());
		rp.setBudgetedRate_mkcal(bdPNBRate_mkcal.doubleValue());
		rp.setProposedPremium(bdTotProposedPremium.doubleValue());

		documentDataList.add(rp);
		return documentDataList;
	}

	@Override
	public void updateReleaseRP(DocumentMetaDataCreate dmdc) {

		// TODO Auto-generated method stub

		ArrayList<DocumentData> rpList = dmdc.getRpList();
		ArrayList<DocumentData> rpTotalData = dmdc.getRpTotalData();
		Integer documentId=rpList.get(0).getDocumentId();
		DocumentMaster dm = documentMasterRepository.findByDocumentId(documentId);
		for (DocumentData docData : rpList) {
			RevenueProposal rp = revenueProposalRepository.findByDocumentIdAndPlantId(docData.getDocumentId(),
					docData.getPlantId());

			System.out.println("IN Rel RP getApprovedQuantity = " + docData.getApprovedQuantity());
			System.out.println("IN Rel RP getApprovedRate = " + docData.getApprovedRate());
			DecimalFormat f = new DecimalFormat("##.00");
			DecimalFormat ff = new DecimalFormat("##.00000");
			if (docData.getApprovedQuantity() != null) {
				rp.setApprovedQuantity(Double.parseDouble(f.format(docData.getApprovedQuantity())));
			}

			if (dm.getDeptId() != null && dm.getDeptId() == 2) {
				rp.setApprovedQuantity(Double.parseDouble(f.format(docData.getApprovedQuantity()==null? 0f: docData.getApprovedQuantity() * 1000)));
			}

			if (docData.getApprovedPeriod() != null) {
				rp.setApprovedPeriod(Double.parseDouble(f.format(docData.getApprovedPeriod())));
			}
			if (docData.getApprovedTaxAmount() != null) {
				rp.setApprovedTaxAmount(Double.parseDouble(ff.format(docData.getApprovedTaxAmount())));
			}

			if (docData.getApprovedRate() != null) {
				rp.setApprovedRate(Double.parseDouble(f.format(docData.getApprovedRate())));
			}

			if (docData.getApprovedNegotiatedRate_mkcal() != null) {
				rp.setApprovedNegotiatedRate_mkcal(
						Double.parseDouble(f.format(docData.getApprovedNegotiatedRate_mkcal())));
			}

			if (docData.getApprovedPremium() != null) {
				rp.setApprovedPremium(Double.parseDouble(f.format(docData.getApprovedPremium())));
			}

			Double amountApproved = 0d;
			if (docData.getAmountApprovedInLakhs() != null && !docData.getAmountApprovedInLakhs().isEmpty()) {
				String amountApprovedVal = docData.getAmountApprovedInLakhs().replaceAll(",", "");
				amountApproved = Double.valueOf(amountApprovedVal) * 100000;
			}

			System.out.println("IN Rel RP getAmountApproved = " + docData.getAmountApproved());

			if (docData.getAmountApproved() != null) {
				amountApproved = docData.getAmountApproved() * 100000;
			}
			rp.setAmountApproved(Double.parseDouble(f.format(amountApproved)));
			revenueProposalRepository.save(rp);
			
		}
		if(rpTotalData!=null && rpTotalData.get(0).getApprovedRate()!=null){
//			documentMasterRepository.updateApprovedRate_mkcal(documentId,rpTotalData.get(0).getApprovedRate());
			dm.setTotal_Approved_Rate_mkcal(rpTotalData.get(0).getApprovedNegotiatedRate_mkcal());
			documentMasterRepository.save(dm);
			logger.info("DocumentMaster Status "+dm.getStatus());
			logger.info("rpTotalData.get(0).getApprovedRate() "+rpTotalData.get(0).getApprovedRate());
			logger.info("document Mater mkcal dm.getTotal_Approved_Rate_mkcal() " +dm.getTotal_Approved_Rate_mkcal());
//			dm.setTotal_Approved_Rate_mkcal(rpTotalData.get(0).getApprovedNegotiatedRate_mkcal());
		}
		

	}

	@Override
	public void updateFinalRP(DocumentMetaDataCreate dmdc) {

		// TODO Auto-generated method stub

		ArrayList<DocumentData> rpList = dmdc.getRpList();
		ArrayList<DocumentData> rpTotalValue = dmdc.getRpTotalData();
		Integer documentId=rpList.get(0).getDocumentId();
		DocumentMaster dm = documentMasterRepository.findByDocumentId(documentId);
		for (DocumentData docData : rpList) {
			RevenueProposal rp = revenueProposalRepository.findByDocumentIdAndPlantId(docData.getDocumentId(),
					docData.getPlantId());
			FinalApproval rp_finalApproval = rp_FinalApprovalRepository
					.findByDocumentIdAndPlantIdAndProposalSequenceNumber(docData.getDocumentId(), docData.getPlantId(),
							1);
			if (rp_finalApproval != null) {
				continue;
			} else {
				rp_finalApproval = new FinalApproval();
			}
			rp_finalApproval.setDocumentId(docData.getDocumentId());
			rp_finalApproval.setPlantId(docData.getPlantId());
			rp_finalApproval.setProposalSequenceNumber(1);
			System.out.println("IN Rel RP getApprovedQuantity = " + docData.getApprovedQuantity());
			System.out.println("IN Rel RP getApprovedRate = " + docData.getApprovedRate());
			DecimalFormat f = new DecimalFormat("##.00");
			DecimalFormat ff = new DecimalFormat("##.00000");
			if (docData.getFinalQuantity() != null) {
				rp_finalApproval.setFinalQuantity(Double.parseDouble(f.format(docData.getFinalQuantity())));
			}
			if (dm.getDeptId() != null && dm.getDeptId() == 2) {
				rp_finalApproval.setFinalQuantity(Double.parseDouble(f.format(docData.getFinalQuantity()==null? 0f : docData.getFinalQuantity() * 1000)));
			}

			if (docData.getFinalRate() != null) {
				rp_finalApproval.setFinalRate(Double.parseDouble(f.format(docData.getFinalRate())));
			}

			if (docData.getFinalNegotiatedRate_mkcal() != null) {
				rp_finalApproval
						.setFinalRate_mkcal(Double.parseDouble(f.format(docData.getFinalNegotiatedRate_mkcal())));
			}

			if (docData.getFinalPremium() != null) {
				rp_finalApproval.setFinalPremium(Double.parseDouble(f.format(docData.getFinalPremium())));
			}

			Double finalApprovedAmount = 0d;
			/*
			 * if(docData.getFinalApprovedAmount()!=null ){ // String
			 * finalApprovedAmountVal=docData.getFinalApprovedAmount().
			 * replaceAll(",", "");
			 * finalApprovedAmount=docData.getFinalApprovedAmount()*100000; }
			 */

			System.out.println("IN Rel RP docData.getFinalApprovedAmount = " + docData.getFinalApprovedAmount());

			if (docData.getFinalApprovedAmount() != null) {
				finalApprovedAmount = docData.getFinalApprovedAmount() * 100000;
			}
			rp_finalApproval.setFinalApprovedAmount(Double.parseDouble(f.format(finalApprovedAmount)));
			rp_FinalApprovalRepository.save(rp_finalApproval);
		}
		if(rpTotalValue!=null){
//			documentMasterRepository.updateFinalRate_mkcal(documentId, rpTotalValue.get(0).getFinalNegotiatedRate_mkcal());
			dm.setTotal_Final_Rate_mkcal(rpTotalValue.get(0).getFinalNegotiatedRate_mkcal());
			documentMasterRepository.save(dm);
			logger.info("DocumentMaster Status "+dm.getStatus());
			logger.info("rpTotalValue.get(0).getFinalNegotiatedRate_mkcal() "+rpTotalValue.get(0).getFinalNegotiatedRate_mkcal());
			logger.info("document Mater mkcal dm.getTotal_Final_Rate_mkcal() " +dm.getTotal_Final_Rate_mkcal());
//			dm.setTotal_Final_Rate_mkcal(rpTotalValue.get(0).getFinalNegotiatedRate_mkcal());
//			documentMasterRepository.save(dm);
		}

	}

	@Override
	public Integer deleteByFileNameAndCommentId(String fileName, Integer commentId) {

		return docAttachmentRepository.deleteByFileNameAndCommentId(fileName, commentId);
	}

	@Override
	public DocAttachment getDocAttachmentByFileNameAndCommentId(String fileName, Integer commentId) {
		return docAttachmentRepository.findByFileNameAndCommentId(fileName, commentId);
	}

	@Override
	public ArrayList<RevenueProposal> saveDocumentData(DocumentMetaDataCreate dmdc, DocumentMaster dm) {

		List<Plant> plants = dmdc.getPlantList();
		System.out.println("plantsplantsplantsplantsplantsplants"+plants.size());
		ArrayList<RevenueProposal> rps1 = dmdc.getRpData();
		ArrayList<RevenueProposal> rps2 = dmdc.getRpUnitData();
		ArrayList<RevenueProposal> rps = new ArrayList<RevenueProposal>();
		RevenueProposal rp1 = null;
		if (rps2 != null && rps2.size() > 0) {
			rp1 = rps2.get(0);
		}
		int i = 0;
		String rpNumber = null;
		if (dmdc.getCreateRPNo()) {
			if (dmdc.getPlantId() == null) {
				rpNumber = creatingRPNumber(dmdc,dm);
			} else {
				rpNumber = creatingPlantRPNumber(dmdc);
			}
		}

		/*
		 * if(dmdc.getXportation()!=null && !dmdc.getRpdNumber().isEmpty() &&
		 * dmdc.getXportation().equalsIgnoreCase("true")){ rpNumber =
		 * dmdc.getRpNumber() + " TRPT"; }
		 */

		if (dmdc.getXportation() != null && dmdc.getXportation().equalsIgnoreCase("true")) {
			if (dm.getOriginalProposalDocId() != null && dm.getOriginalProposalDocId() > 0) {
			Integer maxTransporterSeqId=rpTransportationRep.findByTransporterSeqIdWithOriginalProposalDocId(dm.getOriginalProposalDocId());
			if(maxTransporterSeqId!= null && maxTransporterSeqId <=9  && maxTransporterSeqId>0){
				rpNumber = dmdc.getRpNumber() + " TRPT 0"+maxTransporterSeqId;
			}else{
				rpNumber = dmdc.getRpNumber() + " TRPT "+maxTransporterSeqId;	
			}
				
			} else {
				rpNumber = rpNumber + " TRPT";
			}
		}
		for (Plant plant : plants) {
			RevenueProposal rp = rps1.get(i);
			if (rp1 != null) {
				rp.setAnnualQuantityUnit(rp1.getAnnualQuantityUnit());
				rp.setOrderPeriodUnit(rp1.getOrderPeriodUnit());
				rp.setOrderQuantityUnit(rp1.getOrderQuantityUnit());
				rp.setBudgetRateUnit(rp1.getBudgetRateUnit());
				rp.setPolestarRateUnit(rp1.getPolestarRateUnit());
				rp.setNegotiatedRateUnit(rp1.getNegotiatedRateUnit());

			}
			
			Boolean isXportation = false;
			
			if(dmdc.getXportation() != null &&(dmdc.getXportation().equalsIgnoreCase("true") || dmdc.getRpNumber().indexOf("TRPT")!=-1 || dmdc.getRpNumber().indexOf("Transportation")!=-1 )){
				isXportation = true;
			}
		
			if (dm.getDeptId() != null && dm.getDeptId() == 2 && !isXportation ) {
				if (rp.getSavingsBudgetedStr() != null) {					
					rp.setSavingsBudgeted(Double.parseDouble(rp.getSavingsBudgetedStr().replace(",", ""))*100000);
				}
				rp.setAnnualQuantity(rp.getAnnualQuantity()==null? 0f : rp.getAnnualQuantity() * 1000);
				rp.setOrderQuantity(rp.getOrderQuantity() * 1000);
			}

			rp.setDocumentId(dm.getDocumentId());
			if (rpNumber != null) {
				rp.setRpNumber(rpNumber);
			} else {
				rp.setRpNumber(dmdc.getRpNumber());
			}
			rp.setPlantId(plant.getPlantId());
			revenueProposalRepository.save(rp);
			rps.add(rp);
			i++;
		}
		return rps;
	}

	String creatingRPNumber(DocumentMetaDataCreate docuMetaDataCreate,DocumentMaster dm) {
		int plantId = docuMetaDataCreate.getPlantList().get(0).getPlantId();
		Integer userId = docuMetaDataCreate.getCreatedBy();
		Users usr = userRepo.findByUserId(userId);

		List<Plant> plants = docuMetaDataCreate.getPlantList();

		StringBuffer sb = new StringBuffer();
		sb.append(docuMetaDataCreate.getCompany().getAbbreviation());
		sb.append('/');
		sb.append(docuMetaDataCreate.getBusiness().getAbbreviation());
		sb.append('/');
		sb.append(docuMetaDataCreate.getSbu().getAbbreviation());
		sb.append('/');
		if (usr.getDeptId() == 2) {
			sb.append("MnM");
		} else if (plants.size() > 1) {
			sb.append("MULTI");
		} else {
			sb.append(docuMetaDataCreate.getPlantList().get(0).getAbbreviation());
		}
		sb.append('/');
		sb.append(docuMetaDataCreate.getMaterial().getAbbreviation());
		sb.append('/');
		if (plants.size() > 1 && !docuMetaDataCreate.getPlantName().equalsIgnoreCase("Corporate-IT")
				&& usr.getDeptId() != 2) {
			plantId = 99;
			sb.append(99);
		} else if (docuMetaDataCreate.getPlantName().equalsIgnoreCase("Corporate-IT")) {
			plantId = 98;
			sb.append(98);
		} else if (usr.getDeptId() == 2) {
			plantId = 97;
			sb.append(97);
		} else {
			sb.append(plantId < 10 ? "0" + plantId : plantId);
		}

		Calendar calendar = new GregorianCalendar();

		int intYear = calendar.get(Calendar.YEAR) % 100;
		int intmonth = calendar.get(Calendar.MONTH) + 1;
		int financialYear = (intmonth > 3) ? ++intYear : intYear;

		List<RPNumber> rpNumberList = rpNumRepo.findByPlantIdAndFinancialYearOrderByRpNumberIdDesc(plantId,
				financialYear);
		RPNumber tempRPNumber = null;
		if (rpNumberList != null && rpNumberList.size() > 0) {
			tempRPNumber = rpNumberList.get(0);
		}
		Integer rpNumberId = 0;
		if (tempRPNumber != null) {
			rpNumberId = tempRPNumber.getRpNumberId();
		}
		System.out.println("A" + rpNumberId);
		rpNumberId = rpNumberId == null ? 0 : rpNumberId;
		System.out.println("B" + rpNumberId);
		rpNumberId = rpNumberId + 1;
		System.out.println("C" + rpNumberId);
		if (!(docuMetaDataCreate.getXportation() != null && docuMetaDataCreate.getXportation().equalsIgnoreCase("true") && dm.getOriginalProposalDocId() != null && dm.getOriginalProposalDocId() > 0)){
				RPNumber rpnumberEntity = new RPNumber();
				rpnumberEntity.setPlantId(plantId);
				rpnumberEntity.setRpNumberId(rpNumberId);
				rpnumberEntity.setFinancialYear(financialYear);
				rpnumberEntity = rpNumRepo.save(rpnumberEntity);
		}
		sb.append(rpNumberId < 10 ? "000" + rpNumberId
				: (rpNumberId < 100 ? "00" + rpNumberId : (rpNumberId < 1000 ? "0" + rpNumberId : rpNumberId)));

		System.out.println("RPNumber Is:" + rpNumberId);
		sb.append('/');

		sb.append(financialYear);

		return sb.toString();

	}

	String creatingPlantRPNumber(DocumentMetaDataCreate docuMetaDataCreate) {
		int plantId = docuMetaDataCreate.getPlantList().get(0).getPlantId();
		Integer InitiatorPlantId = docuMetaDataCreate.getPlantId();
		Plant rpInitiatedAtPlant = MasterPlantRepo.findByPlantId(InitiatorPlantId);
		Integer seqNo = rpInitiatedAtPlant.getSeqNo();
		List<Plant> plants = docuMetaDataCreate.getPlantList();
		String sbuAbbreviation = MasterSbuRepo.findBySbuId(plants.get(0).getSbuId()).getAbbreviation();

		StringBuffer sb = new StringBuffer();
		sb.append(docuMetaDataCreate.getCompany().getAbbreviation());
		sb.append('/');
		sb.append(docuMetaDataCreate.getBusiness().getAbbreviation());
		sb.append('/');
		if (plants.get(0).getClusterId().equals(1)) {
			sb.append(sbuAbbreviation);
			System.out.println("sbuAbbreviation " + sbuAbbreviation);
		} else {
			sb.append(docuMetaDataCreate.getSbu().getAbbreviation());
		}
		sb.append('/');
		if (plants.size() > 1) {
			sb.append("MULTI");
		} else {
			sb.append(docuMetaDataCreate.getPlantList().get(0).getAbbreviation());
		}
		sb.append('/');
		sb.append(docuMetaDataCreate.getMaterial().getAbbreviation());
		sb.append('/');

		sb.append(plants.get(0).getSeqNo());

		Calendar calendar = new GregorianCalendar();

		int intYear = calendar.get(Calendar.YEAR) % 100;
		int intmonth = calendar.get(Calendar.MONTH) + 1;
		int financialYear = (intmonth > 3) ? ++intYear : intYear;

		List<RPNumber> rpNumberList = rpNumRepo.findByPlantIdAndFinancialYearOrderByRpNumberIdDesc(seqNo,
				financialYear);
		RPNumber tempRPNumber = null;
		if (rpNumberList != null && rpNumberList.size() > 0) {
			tempRPNumber = rpNumberList.get(0);
		}
		Integer rpNumberId = 0;
		if (tempRPNumber != null) {
			rpNumberId = tempRPNumber.getRpNumberId();
		}
		System.out.println("A" + rpNumberId);
		rpNumberId = rpNumberId == null ? 0 : rpNumberId;
		System.out.println("B" + rpNumberId);
		rpNumberId = rpNumberId + 1;
		System.out.println("C" + rpNumberId);
		RPNumber rpnumberEntity = new RPNumber();
		rpnumberEntity.setPlantId(seqNo);
		rpnumberEntity.setRpNumberId(rpNumberId);
		rpnumberEntity.setFinancialYear(financialYear);
		rpnumberEntity = rpNumRepo.save(rpnumberEntity);

		sb.append(rpNumberId < 10 ? "000" + rpNumberId
				: (rpNumberId < 100 ? "00" + rpNumberId : (rpNumberId < 1000 ? "0" + rpNumberId : rpNumberId)));

		System.out.println("RPNumber Is:" + rpNumberId);
		sb.append('/');
		sb.append(financialYear);

		return sb.toString();

	}

	@Override
	public DocumentMaster saveDocumentMetaData(DocumentMetaDataCreate dmdc) {
		
		
		 ArrayList<DocumentData> rpTotalData = dmdc.getRpTotalData();
		 DocumentData rpTotalValue = null;
		 if(rpTotalData!=null){
			 rpTotalValue =rpTotalData.get(0);
		 }
		 
		
		DocumentMaster dm = new DocumentMaster();
		Integer userId = dmdc.getCreatedBy();
		Users usr = userRepo.findByUserId(userId);
		Integer usrPlantId = usr.getPlantId();
		Plant plant = null;
		Integer clusterId = null;
		String materialDetails = null;
		if (dmdc.getMaterialDetails() != null) {
			materialDetails = dmdc.getMaterialDetails().getMaterialDetail();
		}
		if (usrPlantId != null) {
			plant = MasterPlantRepo.findByPlantId(usrPlantId);
		}
		if (plant != null && plant.getClusterId() != null) {
			clusterId = plant.getClusterId();
		} else {
			clusterId = 0;
		}
		int documentId = 0;
		/*
		 * if(dmdc.getXportation()!=null && !dmdc.getRpNumber().isEmpty() &&
		 * dmdc.getXportation().equalsIgnoreCase("true")){
		 * dm.setOriginalProposalDocId(dmdc.getDocumentId()); }
		 */
System.out.println("dm.getOriginalProposalDocId a("+dm.getOriginalProposalDocId());
		if (dmdc.getXportation() != null && dmdc.getXportation().equalsIgnoreCase("true")) {
			dm.setOriginalProposalDocId(dmdc.getDocumentId());
		} else {
			documentId = dmdc.getDocumentId();
		}
		System.out.println("dm.getOriginalProposalDocId( b"+dm.getOriginalProposalDocId());
		System.out.println("dm.getOriginalProposalDocId( b"+dmdc.getXportation());
		System.out.println("dm.getOriginalProposalDocId( b"+dmdc.getDocumentId());

		if (documentId != 0) {
			DocumentMaster dmTempDocumentMaster = documentMasterRepository.findByDocumentId(documentId);
			if (dmTempDocumentMaster != null && "Yes".equalsIgnoreCase(dmTempDocumentMaster.getIsAmendment())) {
				BeanUtils.copyProperties(dmTempDocumentMaster, dm);
			}
			if(dm.getOriginalProposalDocId()==null || dm.getOriginalProposalDocId().intValue() == 0){
			dm.setOriginalProposalDocId(dmTempDocumentMaster.getOriginalProposalDocId());
			}
			dm.setDocumentId(documentId);
			dmdc.setCreateRPNo(false);
		} else {
			dmdc.setCreateRPNo(true);
		}
		System.out.println("sbuid" + dmdc.getSbu().getSbuId());
		dm.setTitle(dmdc.getTitle());
		dm.setDocumentType("RP");
		dm.setCompanyId(dmdc.getCompany().getCompanyId());
		dm.setBusinessId(dmdc.getBusiness().getBusinessId());
		dm.setSbuId(dmdc.getSbu().getSbuId());
		if (clusterId.equals(1)) {
			Integer sbuIdPlantSelected = MasterPlantRepo.findByPlantId(dmdc.getPlantList().get(0).getPlantId())
					.getSbuId();
			dm.setSbuId(sbuIdPlantSelected);
		}
		if (dmdc.getMaterialDetails() != null) {
			dm.setMaterialDetails(materialDetails);
		}
		dm.setMaterialId(dmdc.getMaterial().getMaterialId());
		dm.setPlantId(dmdc.getPlantList().get(0).getPlantId());
		if (dmdc.getCategory() != null) {
			dm.setCategoryId(dmdc.getCategory().getCategoryId());
		}
		dm.setIsExpress(dmdc.getIsExp());
		dm.setIsRelatedParty(dmdc.getIsRelPar());

		// dm.setIsMultiPlant(dmdc.getIsMultiplePlant());
		Date date = new Date();
		dm.setCreatedTimeStamp(date);
		dm.setCreatedBy(userId);
		dm.setClusterId(clusterId);
		dm.setStatus(DocStatus.INITIATED);
		if (usr.getDeptId() != null) {
			dm.setDeptId(usr.getDeptId());
		}
		Boolean isXporter = false;
		if(dmdc.getXportation() != null &&(dmdc.getXportation().equalsIgnoreCase("true") || dmdc.getRpNumber().indexOf("TRPT")!=-1 || dmdc.getRpNumber().indexOf("Transportation")!=-1)){
			isXporter = true;
		}
		
		if (usr.getDeptId() != null && usr.getDeptId().equals(2) && dmdc.getCategory() != null
				&& dmdc.getCategory().getCategoryName().equalsIgnoreCase("e-Auction") && !isXporter) {
			if (dmdc.getInPrincipleApproval() != null && dmdc.getInPrincipleApproval()) {
				logger.info("dm.setStatus saveDocumentMetaData InPrinciple");
				dm.setProposalType("In-Principle");
			} else {
				logger.info("dm.setStatus saveDocumentMetaData Final");
				dm.setProposalType("Final");
			}
		}
		System.out.println("usr.getPlantId()" + usr.getPlantId());
		System.out.println("usr.getUserId()" + usr.getUserId());
		dm.setInitiatorPlantId(usr.getPlantId());
		dm.setOtherMaterialName(dmdc.getOtherMaterialName());
		dm.setVendorName(dmdc.getVendorName());
		dm.setSourceName(dmdc.getSourceName());
		dm.setCurrentlyWith(userId);
		dm.setTransportationJustification(dmdc.getJustification());
		if(rpTotalValue!=null){
			if(rpTotalValue.getNegotiatedRate_mkcal()!=""&&rpTotalValue.getNegotiatedRate_mkcal()!=null&&rpTotalValue.getNegotiatedRate_mkcal()!="undefined"){
				System.out.println("NegotiatedRate_mkcal"+rpTotalValue.getNegotiatedRate_mkcal());
				System.out.println("Double NegotiatedRate_mkcal"+Double.parseDouble(rpTotalValue.getNegotiatedRate_mkcal()));
			
				dm.setTotal_Proposed_Rate_mkcal(Double.parseDouble(rpTotalValue.getNegotiatedRate_mkcal()));
			}
			if(rpTotalValue.getBudgetedRate_mkcal()!=""&&rpTotalValue.getBudgetedRate_mkcal()!=null&&rpTotalValue.getBudgetedRate_mkcal()!="undefined"){
				System.out.println("BudgetedRate_mkcal"+rpTotalValue.getBudgetedRate_mkcal());
				System.out.println("Double BudgetedRate_mkcal"+Double.parseDouble(rpTotalValue.getBudgetedRate_mkcal()));
			
				dm.setTotal_PnB_Rate_mkcal(Double.parseDouble(rpTotalValue.getBudgetedRate_mkcal()));	
			}
		}
		logger.info("saveDocumentMetaData DocumentMaster Status "+dm.getStatus()+" Proposal Type "+dm.getProposalType()+" DocId "+dm.getDocumentId());		
		dm = documentMasterRepository.save(dm);
		return dm;
	}

	@Override
	public boolean isRPReleased(Integer documentId) {
		List<DocumentMaster> docMasterList = documentMasterRepository.findByDocumentIdAndStatus(documentId,
				ActionStatus.RELEASED);
		if (docMasterList != null && docMasterList.size() > 0) {
			return true;
		}
		return false;
	}

	@Override
	public List<String> writeFilesToTempFolder(Integer documentId) {
		List<String> writeFilesToTempFolder = new ArrayList<String>();
		try {
			List<DocAttachment> docAttacmentList = docAttachmentRepository
					.findByDocumentIdOrderByCommentIdAsc(documentId);
			int commentId = 0;
			int tempCommentId = 0;
			for (DocAttachment docAttachment : docAttacmentList) {
				if (commentId == 0) {
					commentId = docAttachment.getCommentId();
				}
				tempCommentId = docAttachment.getCommentId();

				if (commentId != tempCommentId) {
					break;
				}
				String filePath = PropsUtil.getValue("temp.path");
				byte[] byteArray = getEntityByDocAccId(docAttachment.getpAttachmentId());
				File file = new File(filePath + "\\" + docAttachment.getFileName());
				if (!file.exists()) {

					file.createNewFile();

				}
				String fileTempName = file.getAbsolutePath();
				FileOutputStream fop = new FileOutputStream(file);
				fop.write(byteArray);
				fop.flush();
				fop.close();
				writeFilesToTempFolder.add(fileTempName);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return writeFilesToTempFolder;
	}

	@Override
	public String getPlantName(Integer plantId) {
		String plantName = null;
		try {
			plantName = MasterPlantRepo.findByPlantId(plantId).getPlantName();

		} catch (Exception e) {

		}
		return plantName;

	}

	@Override
	public DocUserMaster findByDocumentIdAndDocRole(Integer documentId, String docRole) {
		return docUserMasterRepo.findByDocumentIdAndDocRole(documentId, docRole);

	}

	@Override
	public List<RevenueProposal> findByRpNumber(String rpNumber) {
		return revenueProposalRepository.findByRpNumber(rpNumber);

	}

	@Override
	public DocumentMaster saveDocumentMaster(DocumentMaster dm) {
		return documentMasterRepository.save(dm);
	}

	@Override
	public Integer getMaxSeqNo(Integer originalDocId) {
		System.out.println("originalDocId" + originalDocId);
		return documentMasterRepository.getMaxSeqNo(originalDocId);
	}

	@Override
	public RevenueProposal saveRP(RevenueProposal rp) {
		return revenueProposalRepository.save(rp);
	}

	@Override
	public List<DocumentMaster> getDocumentMasterByOriginalDocId(Integer documentId) {

		return documentMasterRepository.findByOriginalDocId(documentId);
	}

	@Override
	public List<DocUserMaster> findByDocumentIdAndSerialNumberIsNotNullOrderBySerialNumberAsc(int documentId) {
		return docUserMasterRepo.findByDocumentIdAndSerialNumberIsNotNullOrderBySerialNumberAsc(documentId);
	}

	@Override
	public DocUserMaster saveDocUserMaster(DocUserMaster dum) {
		return docUserMasterRepo.save(dum);
	}

	@Override
	public List<RevenueProposal> findRPByDocumentId(Integer docId) {
		return revenueProposalRepository.findByDocumentId(docId);
	}

	@Override
	public List<DocUserMaster> findByDocumentIdAndNoAdvisory(int documentId) {
		List<DocUserMaster> dums = docUserMasterRepo.findByDocumentIdAndDocRoleIn(documentId, WFUtil.docRoleListForMainLine());
		/*List<DocUserMaster> dums = docUserMasterRepo.findByDocumentIdAndDocRoleNotIn(documentId, WFUtil.getDocRoles());
		ArrayList<DocUserMaster> dumNoAdvisory = new ArrayList<DocUserMaster>();
		for (DocUserMaster dum : dums) {
			Integer userId = dum.getUserId();
			Users usr = userRepo.findByUserId(userId);
			if (usr.getIsSupportingEndorser() != null && usr.getIsSupportingEndorser()) {
				continue;
			} else {
				dumNoAdvisory.add(dum);
			}

		}*/
		return dums;
	}

	@Override
	public List<DocUserMaster> findByDocumentIdAndDocRoleIn(int documentId, Collection<String> docRoleList) {

		return docUserMasterRepo.findByDocumentIdAndDocRoleIn(documentId, docRoleList);
	}

	@Override
	public Integer getMaxAttachmentId() {
		return docAttachmentRepository.getMaxId();
	}

	@Override
	public Comments findCommentsByActionId(Integer actionId) {
		return CommentRepo.findByActionId(actionId);
	}

	@Override
	public List<DocumentMaster> findByStatusIn(Collection<String> status) {
		return documentMasterRepository.findByStatusIn(status);
	}

	@Override
	public List<DocAttachment> findByDocumentId(int documentId) {
		return docAttachmentRepository.findByDocumentIdOrderByCommentIdAsc(documentId);
	}

	@Override
	public HashMap<String, Object> getMisReport(Integer companyId, Integer businessId, Integer sbuId, Integer plantId,
			Integer materialId, String isRelParty, Integer userId, String isBudgeted, Date fromDate, Date toDate,
			Double upperLimit, Double lowerLimit, Boolean isPlantReport) {
		String corporate_MISReport = PropsUtil.getValue("coporate.plant.misreport");
		Users usr;
		Plant userPlant;
		List<DocumentMaster> dmListByCluster = new ArrayList<DocumentMaster>();

		HashMap<String, Object> misReport = new HashMap<String, Object>();
		List<String> statusList = new ArrayList<String>();
		statusList.add(DocStatus.INITIATED);
		statusList.add(DocStatus.APPROVED);
		statusList.add(DocStatus.REJECTED);
		statusList.add(DocStatus.RELEASED);
		statusList.add(DocStatus.CANCELLED);
		statusList.add(DocStatus.IN_PROCESS);
		statusList.add(DocStatus.IN_PROCESS_PARALLEL);
		Collection<String> status = new ArrayList<String>(statusList);
		List<DocumentMaster> documentList = null;
		List<Integer> documentIdList = new ArrayList<Integer>();
		List<RevenueProposal> rpList = new ArrayList<RevenueProposal>();

		String corporate = PropsUtil.getValue("corporate.user.rp");
		usr = userRepo.findByUserId(userId);

		if (usr.getPlantId() != null) {

			userPlant = MasterPlantRepo.findByPlantId(usr.getPlantId());
			dmListByCluster = documentMasterRepository.findByClusterId(userPlant.getClusterId());
			if (dmListByCluster != null) {
				for (DocumentMaster dm : dmListByCluster) {
					documentIdList.add(dm.getDocumentId());
				}
			}

		} else {
			if ("true".equalsIgnoreCase(corporate)) {
				List<DocUserMaster> docUserMasterList = docUserMasterRepo.findByUserId(userId);

				documentIdList = new ArrayList<Integer>();

				for (DocUserMaster docUserMaster : docUserMasterList) {
					documentIdList.add(docUserMaster.getDocumentId());
				}
			}

			Collection<Integer> companyIds = new ArrayList<Integer>();
			if (companyId == -1) {
				List<Integer> companies = MasterCompaniesRepo.findAllCompanyId();
				companyIds.addAll(companies);
			} else {
				companyIds.add(companyId);
			}
			Collection<Integer> businessIds = new ArrayList<Integer>();
			if (businessId == -1) {
				List<Integer> businesses = MasterBusinessRepo.findAllBusinessIdByCompanyIdIn(companyIds);
				businessIds.addAll(businesses);
			} else {
				businessIds.add(businessId);
			}
			Collection<Integer> sbuIds = new ArrayList<Integer>();
			if (sbuId == -1) {
				List<Integer> sbus = MasterSbuRepo.findAllSbuIdByCompanyIdInAndBusinessIdIn(companyIds, businessIds);
				sbuIds.addAll(sbus);
			} else {
				sbuIds.add(sbuId);
			}

			Collection<Integer> plantIds = new ArrayList<Integer>();
			if (plantId == -1) {
				boolean corpLogin = true;
				List<Integer> plants = new ArrayList<Integer>();
				if (isPlantReport != null && isPlantReport == false) {
					plants = MasterPlantRepo.findAllPlantIdByCompanyIdInAndBusinessIdInAndSbuIdIn(companyIds,
							businessIds, sbuIds, corpLogin);
				} else {
					List<Plant> plantsList = MasterPlantRepo.findByPlantLogin(true);
					for (Plant plnt : plantsList) {
						plants.add(plnt.getPlantId());
					}

				}
				plantIds.addAll(plants);
			} else {
				plantIds.add(plantId);
				if (corporate_MISReport.equals("enable")) {
					String plantNameClusterId = MasterPlantRepo.findByPlantId(plantId).getPlantName();
					List<Plant> plantListByClusterId = MasterPlantRepo
							.findByPlantNameIgnoreCaseContaining(plantNameClusterId);
					for (Plant pt : plantListByClusterId) {
						plantIds.add(pt.getPlantId());
					}
				}

			}
			Collection<Integer> materialIds = new ArrayList<Integer>();
			if (materialId == -1) {
				List<Integer> materials = MasterMaterialRepo.findAllMaterialIdByBusinessIdIn(businessIds);

				if (corporate_MISReport.equals("enable")) {
					if (plantId != null && plantId != -1) {

						Integer plantClusterId = MasterPlantRepo.findByPlantId(plantId).getClusterId();
						List<Material> materialsByCluster = MasterMaterialRepo.findByClusterId(plantClusterId);
						for (Material mat : materialsByCluster) {
							materialIds.add(mat.getMaterialId());
						}

					} else if (plantId == -1) {
						if (isPlantReport != null && isPlantReport == false) {
							List<Integer> material_Ids = MasterMaterialRepo
									.findAllMaterialIdByBusinessIdInAndDeptIdNotEqualtoOne(businessIds);
							materialIds.addAll(material_Ids);
						} else {
							List<Plant> plntsList = MasterPlantRepo.findByPlantLogin(true);
							List<Integer> clusterIds = new ArrayList<Integer>();
							for (Plant plnt : plntsList) {
								if (!clusterIds.contains(plnt.getClusterId())) {
									clusterIds.add(plnt.getClusterId());
								}
							}
							List<Material> allMaterial = MasterMaterialRepo.findByClusterIdIn(clusterIds);
							for (Material mat : allMaterial) {
								materialIds.add(mat.getMaterialId());
							}
						}
					}
				} else {
					materialIds.addAll(materials);
				}
			} else {
				materialIds.add(materialId);
			}

			Collection<Boolean> isRelatedPartyIn = new ArrayList<Boolean>();
			if (isRelParty.equalsIgnoreCase("Yes")) {
				isRelatedPartyIn.add(true);
			} else if (isRelParty.equalsIgnoreCase("No")) {
				isRelatedPartyIn.add(false);
			} else {
				isRelatedPartyIn.add(true);
				isRelatedPartyIn.add(false);
			}
			System.out.println("fromDate >>>>>>" + fromDate + "toDate >>>>>" + toDate);
			if (fromDate == null) {
				fromDate = new Date(0l);
			}

			if (toDate == null) {
				toDate = new Date();
			}

			System.out.println("isRelParty >>>>>" + companyIds + "businessId" + businessIds + "sbuId" + sbuIds
					+ "plantId" + plantIds + "materialId" + materialIds + "isRelatedPartyIn" + isRelatedPartyIn
					+ "status" + status);
			System.out.println("fromDate >>>>>>" + fromDate + "toDate >>>>>" + toDate);
			
			
			documentList = documentMasterRepository
					.findByCompanyIdInAndBusinessIdInAndSbuIdInAndPlantIdInAndMaterialIdInAndIsRelatedPartyInAndStatusInAndCreatedTimeStampBetween(
														companyIds, businessIds, sbuIds, plantIds, materialIds, isRelatedPartyIn, status, fromDate,
							toDate);
			System.out.println(documentList);

		}

		List<List<String>> rowList = new ArrayList<List<String>>();

		// objList.addAll(itemList);

		List<String> columnList = new ArrayList<String>();
		columnList.add("Sr.No");
		columnList.add("RP Number");
		columnList.add("UoM");
		columnList.add("P & B Rate");
		columnList.add("Proposed Rate");
		columnList.add("Proposed Quantity");
		columnList.add("Proposed Amount(In Rupees Lakhs)");
		columnList.add("Approved Rate");
		columnList.add("Approved Quantity");
		columnList.add("Approved Amount(In Rupees Lakhs)");
		columnList.add("Final Approver");
		columnList.add("Currently With");
		columnList.add("Status");
		columnList.add("In Budget");
		columnList.add("Company");
		columnList.add("Business");
		columnList.add("SBU");
		columnList.add("Plant");
		columnList.add("Material");
		if (!(isPlantReport != null && isPlantReport == false)) {
			columnList.add("Describe Other's");
			columnList.add("Material Details");
		}
		columnList.add("Initiated Date");
		columnList.add("Approval Date");
		columnList.add("Initiated By");
		columnList.add("Title");
		columnList.add("Express Approval");
		columnList.add("Related Party");

		String filePath = PropsUtil.getValue("temp.path");

		WFUtil.makeFilePath(filePath);

		filePath = filePath + "\\misreport";
		WFUtil.makeFilePath(filePath);
		filePath = filePath + "\\misreport.xls";

		List<Integer> documentsList = new ArrayList<Integer>();
		if (documentList != null) {
			for (DocumentMaster documentMaster : documentList) {

				documentsList.add(documentMaster.getDocumentId());
			}
		}

		Collection<Integer> documentIdsList = new ArrayList<Integer>(documentsList);
		System.out.println("upperLimit >>>>>>" + upperLimit + "lowerLimit >>>>>" + lowerLimit);
		if (upperLimit == null) {
			upperLimit = Double.MAX_VALUE;
		} else {
			upperLimit = upperLimit * 100000;
		}

		if (lowerLimit == null) {
			lowerLimit = 0d;
		} else {
			lowerLimit = lowerLimit * 100000;
		}
		System.out.println("upperLimit >>>>>>" + upperLimit + "lowerLimit >>>>>" + lowerLimit);
		if (usr.getPlantId() != null) {
			rpList = revenueProposalRepository.findByDocumentIdIn(documentIdList);
		} else {

			rpList = revenueProposalRepository.findByDocumentIdInAndAmountNegotiatedBetween(documentIdsList, lowerLimit,
					upperLimit);
		}
		// String pendingWith ="";
		int i = 1;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SS");
		SimpleDateFormat sdf1 = new SimpleDateFormat();
		Integer noColumnEndorserDetails = 0;
		for (RevenueProposal revenueProposal : rpList) {

			DocumentMaster documentMaster = documentMasterRepository.findByDocumentId(revenueProposal.getDocumentId());

			List<DocUserAction> dua;
			dua = docUserActionRepo.findByDocumentIdAndActionStatus(revenueProposal.getDocumentId(),
					ActionStatus.APPROVED);
			if (dua.isEmpty()) {
				dua = docUserActionRepo.findByDocumentIdAndActionStatus(revenueProposal.getDocumentId(),
						ActionStatus.REJECTED);

			}

			List<DocUserAction> duaDetails;
			duaDetails = docUserActionRepo.findByDocumentId(revenueProposal.getDocumentId());
			
			if(duaDetails == null || duaDetails.size() == 0){
				continue;
			}

			if (revenueProposal.getIsBudgeted().equals("0") && isBudgeted.equals("Yes")) {
				continue;
			} else if (revenueProposal.getIsBudgeted().equals("1") && isBudgeted.equals("No")) {
				continue;
			}
//System.out.println(" docUserActionRepo.findByDocumentId(revenueProposal.getDocumentId())"+ docUserActionRepo.findByDocumentId(revenueProposal.getDocumentId()).size());

List<DocUserAction> docUserList=docUserActionRepo.findByDocumentId(revenueProposal.getDocumentId());
String docStatus = null;
if(docUserList!= null && docUserList.size() > 0){
	docStatus = docUserActionRepo.findByDocumentId(revenueProposal.getDocumentId()).get(0)
			.getActionStatus();;
}

			if (docStatus != null && "Draft".equalsIgnoreCase(docStatus)) {
				docStatus = "In-Draft";
			} else {
				docStatus = null;

			}

			DocUserMaster dum = docUserMasterRepo.findByDocumentIdAndDocRole(documentMaster.getDocumentId(),
					DocRole.APPROVER);
			Users users = userRepo.findByUserId(documentMaster.getCreatedBy());
			Users createdBy = users;
			Double pNBRate = (revenueProposal.getBudgetedRate() == null ? 0d : revenueProposal.getBudgetedRate());
			Double negotiatedRate = (revenueProposal.getNegotiatedRate() == null ? 0d
					: revenueProposal.getNegotiatedRate());
			Double negotiatedQuantity = (revenueProposal.getOrderQuantity() == null ? 0d
					: revenueProposal.getOrderQuantity());
			Double negotiatedAmt = (revenueProposal.getAmountNegotiated() == null ? 0d
					: revenueProposal.getAmountNegotiated());
			Double approvedRate = (revenueProposal.getApprovedRate() == null ? 0d : revenueProposal.getApprovedRate());
			Double approvedQuantity = (revenueProposal.getApprovedQuantity() == null ? 0d
					: revenueProposal.getApprovedQuantity());
			Double approvedAmt = (revenueProposal.getAmountApproved() == null ? 0d
					: revenueProposal.getAmountApproved());

			if (negotiatedAmt > 0) {
				negotiatedAmt = negotiatedAmt / 100000;
			}

			if (approvedAmt > 0) {
				approvedAmt = approvedAmt / 100000;
			}
			String finalApproverName = "";
			if (dum != null) {
				users = userRepo.findByUserId(dum.getUserId());
				finalApproverName = users.getFirstName() + " " + users.getLastName();
			}
			String currentlyWith = "";
			if (documentMaster != null && documentMaster.getCurrentlyWith() != null) {
				Users user = userRepo.findByUserId(documentMaster.getCurrentlyWith());

				if (user != null) {
					currentlyWith = user.getFirstName() + " " + user.getLastName();
				}
			}

			List<String> tempList = new ArrayList<String>();
			tempList.add(String.valueOf(i));
			tempList.add(revenueProposal.getRpNumber());
			tempList.add(revenueProposal.getOrderQuantityUnit());

			tempList.add(new BigDecimal(pNBRate).setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString());
			tempList.add(new BigDecimal(negotiatedRate).setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString());
			tempList.add(new BigDecimal(negotiatedQuantity).setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString());
			tempList.add(new BigDecimal(negotiatedAmt).setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString());

			tempList.add(new BigDecimal(approvedRate).setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString());
			tempList.add(new BigDecimal(approvedQuantity).setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString());
			tempList.add(new BigDecimal(approvedAmt).setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString());

			tempList.add(finalApproverName);
			tempList.add(currentlyWith);
			String statusTmp = (documentMaster.getStatus().equalsIgnoreCase(DocStatus.IN_PROCESS)
					|| documentMaster.getStatus().equalsIgnoreCase(DocStatus.IN_PROCESS_PARALLEL))
							? DocStatus.IN_PROCESS : documentMaster.getStatus();

			if (statusTmp.equalsIgnoreCase(DocStatus.INITIATED) && docStatus != null && !statusTmp.isEmpty()) {
				statusTmp = docStatus;
			}
			tempList.add(statusTmp);
			tempList.add(revenueProposal.getIsBudgeted().equals("0") ? "No" : "Yes");
			tempList.add(MasterCompaniesRepo.findByCompanyId(documentMaster.getCompanyId()).getCompanyName());
			tempList.add(MasterBusinessRepo.findByBusinessId(documentMaster.getBusinessId()).getBusinessName());
			tempList.add(MasterSbuRepo.findBySbuId(documentMaster.getSbuId()).getSbuName());
			tempList.add(MasterPlantRepo.findByPlantId(revenueProposal.getPlantId()).getPlantName());
			tempList.add(MasterMaterialRepo.findByMaterialId(documentMaster.getMaterialId()).getMaterialName());
			if (!(isPlantReport != null && isPlantReport == false)) {
				tempList.add(documentMaster.getOtherMaterialName());
				tempList.add(documentMaster.getMaterialDetails());
			}
			String createDate =null;
			if(duaDetails!= null && duaDetails.size() >0){
			 createDate = sdf.format(duaDetails.get(0).getActionTakenTimeStamp());
			}
			sdf1.applyPattern("dd/MM/yyyy HH:mm");
			Date date = null;
			try {
				date = sdf1.parse(createDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			String createDateStr = sdf1.format(date);
			tempList.add(createDateStr);
			String approvedDateStr;
			String approvedDate;
			if (dua != null && !dua.isEmpty() && dua.size()!=0) {
				approvedDate = sdf.format(dua.get(0).getActionTakenTimeStamp());
				sdf1.applyPattern("dd/MM/yyyy HH:mm");

				try {
					date = sdf1.parse(approvedDate);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				approvedDateStr = sdf1.format(date);
			} else {
				approvedDateStr = "";
			}
			tempList.add(approvedDateStr);
			tempList.add(createdBy.getFirstName() + " " + createdBy.getLastName());
			tempList.add(documentMaster.getTitle());
			tempList.add((documentMaster.getIsExpress() != null && documentMaster.getIsExpress()) ? "Yes" : "No");
			
			tempList.add((documentMaster.getIsRelatedParty() != null && documentMaster.getIsRelatedParty()) ? "Yes" : "No");
			for (DocUserAction dua1 : duaDetails) {
				if (!dua1.getActionStatus().equalsIgnoreCase(ActionStatus.PENDING_INVALID)) {
					Users duaUser = userRepo.findByUserId(dua1.getUserId());
					tempList.add(dua1.getDocRole());
					tempList.add(duaUser.getFirstName() + " " + duaUser.getLastName());

					tempList.add(dua1.getActionStatus());
					String duaDateStr;
					String duaDate;
					if (dua1.getActionTakenTimeStamp() != null) {
						duaDate = sdf.format(dua1.getActionTakenTimeStamp());
						sdf1.applyPattern("dd/MM/yyyy HH:mm");

						try {
							date = sdf1.parse(duaDate);
						} catch (ParseException e) {
							e.printStackTrace();
						}
						duaDateStr = sdf1.format(date);
					} else {
						duaDateStr = "";
					}
					tempList.add(duaDateStr);
					if (noColumnEndorserDetails < duaDetails.size()) {
						noColumnEndorserDetails = duaDetails.size();
					}

				}
			}

			i++;
			rowList.add(tempList);

		}

		for (int coln = 0; coln < noColumnEndorserDetails - 1; coln++) {
			columnList.add("Role");
			columnList.add("Name");
			columnList.add("Action");
			columnList.add("Date");
		}
		misReport.put("columnList", columnList);
		misReport.put("rowList", rowList);
		return misReport;
	}
	
	@Override
	public ArrayList<RpTransportation> saveRpTransportation(DocumentMetaDataCreate dmdc,DocumentMaster dm) {
		
//		List<Plant> plants=	dmdc.getPlantList();
		ArrayList<RpTransportation> rps1=dmdc.getTransportList();
		ArrayList<RpTransportation> rtnvalue = new ArrayList<RpTransportation>();
//		ArrayList<RpTransportation> rps = new ArrayList<RpTransportation>();
		//RpTransportation rp1 = rps2.get(0);
//		int i = 0;
		/*String rpNumber =null;
		if(dmdc.getCreateRPNo()){
			if(dmdc.getPlantId() == null){	
				rpNumber=creatingRPNumber(dmdc);
			}else{
				rpNumber=creatingPlantRPNumber(dmdc);
			}
		}*/
//		System.out.println("dm.getOriginalProposalDocId() >>>>"+dm.getOriginalProposalDocId());
		if(dm.getOriginalProposalDocId()!= null && dm.getOriginalProposalDocId().intValue() == 0){
			dm.setOriginalProposalDocId(null);
		}
		Integer maxTransporterSeqId=rpTransportationRep.findByTransporterSeqIdWithOriginalProposalDocId(dm.getOriginalProposalDocId());
		System.out.println("maxTransporterSeqId >>>>"+maxTransporterSeqId);
		for(RpTransportation rpTransportation : rps1){
			ArrayList<RpTransportation> rp = rpTransportation.getTransporterList();
			int i=1;
			for(RpTransportation rpt : rp){
				System.out.println("rpt.getTransporterSeqId() >>>>"+rpt.getTransporterSeqId());
				if(maxTransporterSeqId==null || maxTransporterSeqId == 0 || rpt.getTransporterSeqId()>=1){
					maxTransporterSeqId=rpt.getTransporterSeqId();
				}
				System.out.println("rpt.maxTransporterSeqId() a>>>>"+maxTransporterSeqId);
				if((maxTransporterSeqId==null || maxTransporterSeqId == 0) && rpt.getTransporterSeqId()==1 ){
					maxTransporterSeqId=rpt.getTransporterSeqId();
				}
				System.out.println("rpt.maxTransporterSeqId b>>>>"+maxTransporterSeqId);
				if(rpt.getModesOfTransport()!=null && !rpt.getModesOfTransport().isEmpty() && rpt.getProposedQuantity()!=null  && rpt.getProposedQuantity().doubleValue() >0d
						&& rpt.getProposedRate()!=null && rpt.getProposedRate().doubleValue() >0d){
					if(rpt.getTransporterName()== null || rpt.getTransporterName().isEmpty()){
						rpt.setTransporterName("Transporter "+i);
					}
					rpt.setProposedQuantity(rpt.getProposedQuantity()*1000);
					rpt.setProposedValue(rpt.getProposedValue()*100000);
						rpt.setTransporterId(i);	
						rpt.setTransporterSeqId(maxTransporterSeqId);
				rpt.setDocumentId(dm.getDocumentId());
				rpt.setAverageFreightRate(rpTransportation.getAverageFreightRate());
				rpt.setAveragePnb(rpTransportation.getAveragePnb());
				rpt.setAverageProposedRate(rpTransportation.getAverageProposedRate());
				rpTransportationRep.save(rpt);
				i=i+1;
				}
				rtnvalue.add(rpt);
			}

			// rps.add(rp);
			// i++;
		}
		return rtnvalue;
	}

	@Override
	public ArrayList<RevenueProposal> getDraftXportationData(Integer documentId)
			throws SQLException {
		// TODO Auto-generated method stub

		List<RevenueProposal> rps = revenueProposalRepository.findByDocumentId(documentId);
		List<Comments> cmts=CommentRepo.findByDocumentId(documentId);
		ArrayList<RevenueProposal> tempRps = new ArrayList<RevenueProposal>();
		// String plantName="";
		for (RevenueProposal rp : rps) {
			Plant plant = MasterPlantRepo.findByPlantId(rp.getPlantId());
			rp.setPlantName(plant.getPlantName());
				ArrayList<RpTransportation> transporterList = rpTransportationRep.findByDocumentIdAndPlantId(documentId,rp.getPlantId());
				
				if(transporterList!=null && transporterList.size() > 0){
					ArrayList<RpTransportationBO> tempTansporterList = new ArrayList<RpTransportationBO>();
					for(RpTransportation rpTrans:transporterList){
						RpTransportationBO rpTransportationBO=new RpTransportationBO();
						BeanUtils.copyProperties(rpTrans, rpTransportationBO);
						if(rp.getApprovedRate()!=null && rp.getApprovedRate().doubleValue() == 0d){
							rp.setApprovedRate(rpTransportationBO.getProposedRate());
						}
						if(rp.getApprovedQuantity()!=null && rp.getApprovedQuantity().doubleValue() == 0d){
							rp.setApprovedQuantity(rpTransportationBO.getProposedQuantity());
						}
						if(rp.getAmountApproved()!=null && rp.getAmountApproved().doubleValue() == 0d){
							rp.setAmountApproved(rpTransportationBO.getProposedValue());
						}
						rpTransportationBO.setProposedQuantity(rpTransportationBO.getProposedQuantity()/1000);
						rpTransportationBO.setProposedValue(rpTransportationBO.getProposedValue()/100000);
						tempTansporterList.add(rpTransportationBO);
						
					}
					rp.setTransporterList(tempTansporterList);
					System.out.println("transporterList.get(0).getAverageFreightRate()"+transporterList.get(0).getAverageFreightRate());
					rp.setAverageFreightRate(transporterList.get(0).getAverageFreightRate());
					rp.setAveragePnb(transporterList.get(0).getAveragePnb());
					rp.setAverageProposedRate(transporterList.get(0).getAverageProposedRate());
				}else{
					ArrayList<RpTransportationBO> tempTransporterList = new ArrayList<RpTransportationBO>();
					RpTransportationBO rpTransportation = new RpTransportationBO();
					rpTransportation.setPlantId(rp.getPlantId());
					rpTransportation.setTransporterSeqId(0);
					tempTransporterList.add(rpTransportation);
					
					rpTransportation = new RpTransportationBO();
					rpTransportation.setPlantId(rp.getPlantId());
					rpTransportation.setTransporterSeqId(0);
					tempTransporterList.add(rpTransportation);
					
					rpTransportation = new RpTransportationBO();
					rpTransportation.setPlantId(rp.getPlantId());
					rpTransportation.setTransporterSeqId(0);
					tempTransporterList.add(rpTransportation);
					
					rpTransportation = new RpTransportationBO();
					rpTransportation.setPlantId(rp.getPlantId());
					rpTransportation.setTransporterSeqId(0);
					tempTransporterList.add(rpTransportation);
					
					rpTransportation = new RpTransportationBO();
					rpTransportation.setPlantId(rp.getPlantId());
					rpTransportation.setTransporterSeqId(0);
					tempTransporterList.add(rpTransportation);
					
					rpTransportation = new RpTransportationBO();
					rpTransportation.setPlantId(rp.getPlantId());
					rpTransportation.setTransporterSeqId(0);
					tempTransporterList.add(rpTransportation);
					rp.setTransporterList(tempTransporterList);
				}
				
				
				if(cmts!=null && cmts.size()>0){
					rp.setActionId(cmts.get(cmts.size()-1).getActionId());
					rp.setCommentId(cmts.get(cmts.size()-1).getComment_id());
					if(cmts.get(cmts.size()-1) != null && cmts.get(cmts.size()-1).getComment() != null){
						Blob blob =  cmts.get(cmts.size()-1).getComment();
	byte[] blobByte = blob.getBytes(1, (int) blob.length());
						String comments = new String(blobByte);
						comments.replaceAll("â‚¹", " Rs ");
						rp.setComments(comments);
	List<DocAttachment> docAttachmentList=findByCommentId(cmts.get(cmts.size()-1).getComment_id());
	List<String> attachmentList= new ArrayList<String>();
	for(DocAttachment docAttachment:docAttachmentList){
		attachmentList.add(docAttachment.getFileName());
	}

					rp.setDocAttachmentList(attachmentList);
				}

			}
				System.out.println("rp getApprovedRate>>>>>"+rp.getApprovedRate());
				System.out.println("rp getApprovedQuantity>>>>>"+rp.getApprovedQuantity());
			tempRps.add(rp);
		}
		/*
		 * for(RpTransportation rp:rps){ Plant plant=
		 * MasterPlantRepo.findByPlantId(rp.getPlantId());
		 * rp.setPlantName(plant.getPlantName());
		 * if(plantName.equals(rp.getPlantName())){ continue; }else{
		 * plantName=rp.getPlantName(); } if(cmts!=null && cmts.size()>0){
		 * rp.setActionId(cmts.get(cmts.size()-1).getActionId());
		 * rp.setCommentId(cmts.get(cmts.size()-1).getComment_id());
		 * if(cmts.get(cmts.size()-1) != null &&
		 * cmts.get(cmts.size()-1).getComment() != null){ Blob blob =
		 * cmts.get(cmts.size()-1).getComment(); byte[] blobByte =
		 * blob.getBytes(1, (int) blob.length()); String comments = new
		 * String(blobByte); comments.replaceAll("â‚¹", " Rs ");
		 * rp.setComments(comments); List<DocAttachment>
		 * docAttachmentList=findByCommentId(cmts.get(cmts.size()-1).
		 * getComment_id()); List<String> attachmentList= new
		 * ArrayList<String>(); for(DocAttachment
		 * docAttachment:docAttachmentList){
		 * attachmentList.add(docAttachment.getFileName()); }
		 * 
		 * rp.setDocAttachmentList(attachmentList); }
		 * 
		 * } rp.setTransporterList(rps); tempRps.add(rp); //break; }
		 */
		return tempRps;

	}

	@Override
	public List<DocumentMaster> getDocumentMasterByOriginalProposalDocId(Integer originalProposalDocId) {
		// TODO Auto-generated method stub
		return documentMasterRepository.findByOriginalProposalDocId(originalProposalDocId);
	}

	@Override
	public List<DocumentMaster> getDocumentMasterByTransportationJustification(
			String rpNumber) {
		// TODO Auto-generated method stub
		return documentMasterRepository.findByTransportationJustification(rpNumber);
	}

}
