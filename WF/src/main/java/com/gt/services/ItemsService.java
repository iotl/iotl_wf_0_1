package com.gt.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.controllers.Status;
import com.gt.bo.Items;
import com.gt.bo.SearchBO;
import com.gt.entity.Delegation;

public interface ItemsService {

	public abstract ArrayList<Items> getApprovedRPs(Integer userId,String roleName,String plantName);

	public abstract ArrayList<Items> getReleasedRPs(Integer userId,String roleName,String plantName);

	public abstract ArrayList<Items> getRPsApprovedByMe(Integer userId);

	public abstract ArrayList<Items> getRPsReleasedByMe(Integer userId);

	public abstract ArrayList<Items> getRejectedRPs(Integer userId);

	public abstract ArrayList<Items> getReferedRPs(Integer userId);

	public abstract ArrayList<Items> getPendingActions(Integer userId);

	public abstract ArrayList<Items> getInProcessRPs(Integer userId, String roleName);

	public abstract ArrayList<Items> getEndorsedRPs(Integer userId);
	
	public abstract ArrayList<Items> getEndorsedByApprover(Integer userId);

	public abstract ArrayList<Items> getDrafts(Integer userId,String roleName);

	public abstract ArrayList<Items> getCancelledRPs(Integer userId);

	public abstract ArrayList<Items> getDelegateRPs(Integer userId);
	
	public abstract ArrayList<Items> searchRPNumber(SearchBO searchBO);

	public abstract List<Delegation> findByToDateBefore(Date currentDate);

	public abstract void saveDelegation(Delegation delegation);

	public abstract Status isUserDelegated(Integer userId);
	
	public abstract List<Delegation> getIsActiveUserId(Integer userId);

	public abstract List<Delegation> getIsActiveUserIdOrderByToDate(
			Integer userId);

}