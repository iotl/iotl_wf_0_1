package com.gt.services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gt.bo.DocumentData;
import com.gt.bo.DocumentMetaData;
import com.gt.bo.DocumentMetaDataCreate;
import com.gt.bo.DocumentMetaXportationCreate;
import com.gt.entity.Comments;
import com.gt.entity.DocAttachment;
import com.gt.entity.DocUserMaster;
import com.gt.entity.DocumentMaster;
import com.gt.entity.RPNumber;
import com.gt.entity.RevenueProposal;
import com.gt.entity.RpTransportation;

public interface DocumentService {

	public ArrayList<RevenueProposal> getRevenueProposalByDocumentId(int documentId);
	public ArrayList<RevenueProposal> saveDocumentData(DocumentMetaDataCreate dmdc,DocumentMaster dm);
	
	public DocumentMaster getDocumentMasterByDocumentId(Integer documentId);
	public DocumentMaster saveDocumentMetaData(DocumentMetaDataCreate dmdc);
	public ArrayList<RPNumber> getRPNumbers();
	public RPNumber getRpNumberId(int rpNumberId);
	
	public Comments getCommentByAction_id(Integer actionId);
	public ArrayList<Comments> getCommentByDocumentId(Integer documentId);
	public ArrayList<DocumentMetaData> getDocumentMetada(Integer DocumentId,Integer userId) throws Exception;
	public ArrayList<DocumentData> getDocumentData(Integer DocumentId);
	public ArrayList<DocumentData> getDocumentUnits(Integer documentId);
	
	public DocAttachment getEntityObjByDocAccId(int p_Attachment_Id);
	
	public List<DocAttachment>  findByCommentId(int commentId);
	
	public List<DocAttachment>  findByDocumentId(int documentId);
	
	public byte[] getEntityByDocAccId(int p_Attachment_Id);
	public Boolean addBusinessRuleUsers(DocumentMetaDataCreate dmdc,DocumentMaster dm,ArrayList<RevenueProposal> rps,Boolean isReferredToInitiator);
	public ArrayList<DocumentData> getDocumentTotal(Integer documentId);
	
	public DocUserMaster getUserBydocumentIdAndDocRole(Integer documentId);
	public DocUserMaster getDocUserMasterByDocumentIdAndUserId(Integer documentId,Integer userId);
//	public ArrayList<Items> getDrafts(Integer userId );
	public Integer deleteByDocumentIdAndDocRoleIn(DocumentMaster dm);
	ArrayList<DocUserMaster> getDocUserMasterByDocumentIdAscending(int documentId);
	public ArrayList<RevenueProposal> getDraftDocumentData(Integer documentId) throws SQLException;
//	public ArrayList<CapitalProposal> getDraftDocumentDataCP(Integer documentId) throws SQLException;
	public ArrayList<RevenueProposal> getDraftDocumentTotal(Integer documentId);
	
	public Map<String, List<String>> getMailIdsToSend(DocumentMaster dm,
			String isCreate,String isCreateFinalApproved, String isApprove, String isRejected,
			String isRelease, String isEndorse, Integer userIdInt,Integer delegatdTo,Integer delegatdByInt,
			String rpNumber, List<RevenueProposal> revenueProposalList,
			String isReferedBack, String referedTo, String isReferedRp ,String isCancelled);
	public void updateReleaseRP(DocumentMetaDataCreate dmdc);
	public Integer deleteByFileNameAndCommentId(String fileName,Integer commentId);
	public DocAttachment getDocAttachmentByFileNameAndCommentId(String fileName,Integer commentId);
	public boolean isRPReleased(Integer documentId);
	public List<String> writeFilesToTempFolder(Integer documentId);
	
	public String getPlantName(Integer plantId);
	
	public DocUserMaster findByDocumentIdAndDocRole(Integer documentId,String docRole);
	
	public List<RevenueProposal> findByRpNumber(String rpNumber);
	
	public DocumentMaster saveDocumentMaster(DocumentMaster dm);
	
	public Integer getMaxSeqNo(Integer originalDocId);
	
	public RevenueProposal saveRP(RevenueProposal rp);
	
	public List<DocumentMaster> getDocumentMasterByOriginalDocId(Integer documentId);
	
	public abstract List<DocUserMaster> findByDocumentIdAndSerialNumberIsNotNullOrderBySerialNumberAsc(int documentId);
	
	public DocUserMaster saveDocUserMaster(DocUserMaster dum);
	
	public List<RevenueProposal> findRPByDocumentId(Integer docId);
	
//	public CapitalProposal saveCapitalProposal(CapitalProposal capitalProposal);
	

	
//	public Boolean addBusinessRuleUsersForCP(DocumentMetaDataCPCreate dmdc,DocumentMaster dm,ArrayList<CapitalProposal> rps);
	
//	public void saveCashFlows(int documentId, ArrayList<CashFlows> cashFlows);
	
//	public List<CapitalProposal> findByDocumentIdCp(Integer documentId);
	public List<DocUserMaster> findByDocumentIdAndNoAdvisory(int documentId);
	
	public List<DocUserMaster> findByDocumentIdAndDocRoleIn(int documentId,Collection<String> docRoleList);
	
//	public Boolean isRulePresent(DocumentMetaDataCreate dmdc);
	
	public Integer getMaxAttachmentId();
	
//	int updateTitleAndCategoryIdOfCapitalProposalByDocumentId(String titleCorp,int categoryIdCorp,int documentId);
	
	public Comments findCommentsByActionId(Integer actionId);
	
	
	public List<DocumentMaster> findByStatusIn(Collection<String> status);
	
	public HashMap<String,Object> getMisReport(Integer companyId,Integer businessId,Integer sbuId,Integer plantId,Integer materialId,String isRelParty, Integer userId,String isBudgeted
			,Date fromDate,Date toDate,Double upperLimit,Double lowerLimit,Boolean isPlantReport);
	
	public ArrayList<RpTransportation> saveRpTransportation(DocumentMetaDataCreate dmdc,DocumentMaster dm);
	
	public ArrayList<RevenueProposal> getDraftXportationData(Integer documentId) throws SQLException;
	
	public List<DocumentMaster> getDocumentMasterByOriginalProposalDocId(Integer documentId);
	public void updateFinalRP(DocumentMetaDataCreate dmdc);
	
	public List<DocumentMaster> getDocumentMasterByTransportationJustification(String rpNumber);
	
}

