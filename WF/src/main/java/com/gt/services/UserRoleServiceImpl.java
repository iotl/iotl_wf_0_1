package com.gt.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.gt.entity.UserRoles;
import com.gt.repository.UserRoleRepository;

/**
 * @author Abilash
 *
 */
@Component("userRoleService")
@Transactional
public class UserRoleServiceImpl implements UserRoleService {
	@Autowired
	UserRoleRepository userRoleRepo;

	@Override
	public ArrayList<UserRoles> findAll() {
		// TODO Auto-generated method stub
		return userRoleRepo.findAll();
	}

	@Override
	public ArrayList<UserRoles> findByUserId(int userId) {
		// TODO Auto-generated method stub
		return userRoleRepo.findByUserId(userId);
	}

	@Override
	public ArrayList<UserRoles> findByRoleId(int roleId) {
		// TODO Auto-generated method stub
		return userRoleRepo.findByRoleId(roleId);
	}

}
