package com.gt.services;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.model.KieBaseModel;
import org.kie.api.builder.model.KieModuleModel;
import org.kie.api.builder.model.KieSessionModel;
import org.kie.api.conf.EqualityBehaviorOption;
import org.kie.api.conf.EventProcessingOption;
import org.kie.api.io.Resource;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.conf.ClockTypeOption;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.gt.bo.DocumentMetaDataCreate;
import com.gt.bo.DocumentMetaXportationCreate;
import com.gt.drools.WFRules.Business;
import com.gt.drools.WFRules.Business.InitiatorPlant;
import com.gt.drools.WFRules.Message;
import com.gt.drools.WFRules.User;
import com.gt.entity.Cluster;
import com.gt.entity.DocUserAction;
import com.gt.entity.DocUserMaster;
import com.gt.entity.DocumentMaster;
import com.gt.entity.Material;
import com.gt.entity.Plant;
import com.gt.entity.RevenueProposal;
import com.gt.entity.RpTransportation;
import com.gt.entity.UserEndorsers;
import com.gt.entity.UserRoles;
import com.gt.entity.Users;
import com.gt.repository.ClustersRepository;
import com.gt.repository.DocUserActionRepository;
import com.gt.repository.DocUserMasterRepository;
import com.gt.repository.DocumentMasterRepository;
import com.gt.repository.MasterMaterialDataRepository;
import com.gt.repository.MasterPlantDataRepository;
import com.gt.repository.UserEndorserRepository;
import com.gt.repository.UserRepository;
import com.gt.repository.UserRoleRepository;
import com.gt.utils.DocRole;
import com.gt.utils.PropsUtil;

@Component("ruleEngineHelper")
@Transactional
public class RuleEngineHelper {

	private final DocUserMasterRepository docUserMasterRepo;
	private final UserEndorserRepository userEndorserRepo;
	private final MasterMaterialDataRepository masterMaterialRepo;
	private final ClustersRepository clusterRepo;
	private final DocumentMasterRepository documentMasterRepository;
	private final UserRoleRepository userRoleRepo;
	private final UserRepository userRepo;
	private final MasterPlantDataRepository plantRepo;
	private final DocUserActionRepository docUserActionRepository;
	private String doaOutputFromRuleEngine;
	private final MasterPlantDataRepository masterPlantDataRepository;
	 
	public static final String RENUKOOT_CLUSTER ="Renukoot Cluster";

	public String getDoaOutputFromRuleEngine() {
		return doaOutputFromRuleEngine;
	}

	@Autowired
	private UserService userService;

	public static final Logger logger = Logger
			.getLogger(RuleEngineHelper.class.getName());

	@Autowired
	public RuleEngineHelper(DocUserMasterRepository docUserMasterRepo,UserEndorserRepository userEndorserRepo,MasterMaterialDataRepository masterMaterialRepo
			,DocumentMasterRepository documentMasterRepository,UserRoleRepository userRoleRepo,UserRepository userRepo,MasterPlantDataRepository plantRepo, DocUserActionRepository docUserActionRepository, ClustersRepository clusterRepo,MasterPlantDataRepository masterPlantDataRepository){
		this.docUserMasterRepo=docUserMasterRepo;
		this.userEndorserRepo=userEndorserRepo;
		this.masterMaterialRepo=masterMaterialRepo;
		this.documentMasterRepository=documentMasterRepository;
		this.userRoleRepo=userRoleRepo;
		this.userRepo=userRepo;
		this.plantRepo=plantRepo;
		this.docUserActionRepository=docUserActionRepository;
		this.clusterRepo=clusterRepo;
		this.masterPlantDataRepository= masterPlantDataRepository;
	}

	public Boolean addBusinessRuleUsers(DocumentMetaDataCreate dmdc ,DocumentMaster dm, ArrayList<RevenueProposal> rps,Boolean isReferredToInitiator) {
		Double total=0.0;
		String isBudgeted=null;
		Boolean isRulePresent =true;
		ArrayList<RevenueProposal> rps1 = dmdc.getRpData();

		try {
			// load up the knowledge base
			//KieServices ks = KieServices.Factory.get();

			KieContainer kContainer = createKieContainerForProject();
			//KieContainer kContainer = ks.getKieClasspathContainer();
			//KieSession kSession = kContainer.newKieSession("ksession-rules");
			KieSession kSession = kContainer.newKieSession();

			// go !
			Message message = new Message();
			message.setMessage("Hello World");
			message.setStatus(Message.HELLO);
			kSession.insert(message);

			for(RevenueProposal rp:rps1){
				total += rp.getAmountNegotiated();
			}	

			for(RevenueProposal rp:rps1){
				if("1".equals(rp.getIsBudgeted())){
					isBudgeted="Planned";				
				}
				else{
					isBudgeted="Unplanned";
					break;
				}
			}
			Integer materialId = dmdc.getMaterial().getMaterialId();
			Boolean isXporter = false;
			if(dmdc.getXportation() != null &&(dmdc.getXportation().equalsIgnoreCase("true") || dmdc.getRpNumber().indexOf("TRPT")!=-1 || dmdc.getRpNumber().indexOf("Transportation")!=-1)){
				isXporter = true;
			}

			Business business = new Business();
			if(dmdc.getBusiness().getBusinessName().equalsIgnoreCase("Aluminium")||dmdc.getBusiness().getBusinessName().equalsIgnoreCase("Copper")){
				business.setBusinessType(dmdc.getBusiness().getBusinessName());
			}else{
				business.setBusinessType("All");
			}
			business.setIsXportation(isXporter);
			business.setRequestType(isBudgeted);
			business.setValue(total);
			business.setMaterialId(materialId);
			if(dmdc.getMaterialDetails()!=null){
			business.setMaterialDetails(dmdc.getMaterialDetails().getMatDet());
			}
			Material material=masterMaterialRepo.findByMaterialId(materialId);
			business.setMaterialName(material.getMaterialName());
			if(dmdc.getCategory()!=null){
				business.setCategoryId(dmdc.getCategory().getCategoryId());
			}
			if(material!=null && material.getDoa()!=null && !material.getDoa().isEmpty()){
				business.setDoa(material.getDoa());
			}else{
				business.setDoa("");
			}
			String plantName="";
			for(InitiatorPlant initiatorPlant:InitiatorPlant.values()){
				System.out.println("------------------------------has plant name"+dmdc.getPlantName());
				if(dmdc.getPlantName()!=null && initiatorPlant.toString().equalsIgnoreCase(dmdc.getPlantName()) && "Corporate".equals(dmdc.getPlantName())){
					System.out.println("------------------------------has plant name");
					business.setPlantName("Corporate");
					plantName="Corporate";
					//business.setInitiatorPlant(initiatorPlant);
				}else if(dmdc.getPlantName()!=null && initiatorPlant.toString().equalsIgnoreCase(dmdc.getPlantName()) && "Utkal".equals(dmdc.getPlantName())){
					System.out.println("------------------------------has plant name");
					business.setPlantName("Utkal");

					plantName="Utkal";
					//business.setInitiatorPlant(initiatorPlant);
				}else if(dmdc.getPlantName()!=null && !initiatorPlant.toString().equalsIgnoreCase(dmdc.getPlantName()) && !"Utkal".equals(dmdc.getPlantName()) && !initiatorPlant.toString().equalsIgnoreCase(dmdc.getPlantName()) && !"Corporate".equals(dmdc.getPlantName())){
					business.setBusinessType(dmdc.getBusiness().getBusinessName());
					business.setRequestType(isBudgeted);
					business.setValue(total);
					String drlPlantName = dmdc.getPlantList().get(0).getPlantName().trim();
					
					if (drlPlantName.equalsIgnoreCase(RENUKOOT_CLUSTER)){
						Integer plantId=userRepo.findByUserId(dmdc.getCreatedBy()).getDefaultPlantId();
						Plant userPlant=plantRepo.findByPlantId(plantId);
						drlPlantName=userPlant.getPlantName().trim();
					}
					
					business.setPlantName(drlPlantName);
					Integer clusterId= dmdc.getPlantList().get(0).getClusterId();
					if(clusterId!=null){
					business.setClusterId(clusterId);
					List<Cluster> cluster = clusterRepo.findByClusterId(clusterId);
					business.setClusterName(cluster.get(0).getClusterName());
					}
				}
			}
			Users usersEty = userService.findByUserid(dmdc.getCreatedBy());
			if(usersEty!=null && usersEty.getDeptId()!=null){
				business.setDeptId(usersEty.getDeptId());
				if(usersEty.getDeptId()!=0 && !business.getPlantName().equalsIgnoreCase("Corporate")){
					business.setPlantName(null);
				}
				
			}
			System.out.println("business "+business.getIsXportation());
			kSession.insert(business);



			kSession.fireAllRules();
			if(dm == null){
				if(business == null || (business!=null &&  business.getUsers()==null) || (business!=null &&  business.getUsers()!=null &&  business.getUsers().size() ==0)){
					isRulePresent=false;
					return isRulePresent;
				}else if(business != null && dm == null && rps == null){
					return isRulePresent;
				}
			}


			int listSize=1;
		/*	if(plantName.equalsIgnoreCase("Corporate")&& usersEty.getDeptId()!=null && usersEty.getDeptId()!=2){
				listSize=2;
			}
*/
			List<String> docRoleList= new ArrayList<String>();
			docRoleList.add(DocRole.APPROVER);
			docRoleList.add(DocRole.ENDORSER);
			docRoleList.add(DocRole.INITIATOR);
			docRoleList.add(DocRole.CONTROLLER);
			Collection<String> docRole = new ArrayList<String>(docRoleList);
			ArrayList<User> users = business.getUsers();
			boolean updateList=true;
			List<DocumentMaster> isAmendmentExists= null;
			List<DocUserMaster> userMasterList = docUserMasterRepo.findByDocumentIdAndDocRoleIn(dm.getDocumentId(), docRole);
			System.out.println("isReferredToInitiator >>>>"+isReferredToInitiator);
			System.out.println("users.size() >>>>"+users.size()+" listsize "+listSize);
			System.out.println("users.size() >>>>"+listSize);
			System.out.println("userMasterList.size() >>>>"+userMasterList.size());
			List<UserEndorsers> usrEndorser = userEndorserRepo.findByUserId(dm.getCreatedBy());	
			int usrEndorserSize=0;
			if(usrEndorser!=null && usrEndorser.size()<=1){
				usrEndorserSize=usrEndorser.size();
			}
			if(isReferredToInitiator!=null && isReferredToInitiator && users.size()+listSize+usrEndorserSize < userMasterList.size()){
				List<DocUserAction> duaList=docUserActionRepository.findByDocumentId(dm.getDocumentId());
				for(DocUserMaster dum:userMasterList){
					if(DocRole.INITIATOR.equals(dum.getDocRole())){
						continue;
					}
					boolean isExists =false;
					for(DocUserAction dua :duaList){
						if(DocRole.INITIATOR.equals(dua.getDocRole())){
							continue;
						}
						if(dum.getUserId() == dua.getUserId()){
							isExists =true;
							if(DocRole.ENDORSER.equals(dum.getDocRole())){
								dum.setDocRole(DocRole.EARLIER_ENDORSER);
							}else if(DocRole.CONTROLLER.equals(dum.getDocRole())){
								dum.setDocRole(DocRole.EARLIER_CONTROLLER); 
							}else{
								dum.setDocRole(DocRole.EARLIER_APPROVER); 
							}
							docUserMasterRepo.save(dum);
							break;

						}else{
							isExists=false;
						}
					}

					if(!isExists){
						// userList.add(dum.getUserId());
						docUserMasterRepo.deleteByUserIdAndDocumentId(dum.getUserId(), dm.getDocumentId());
					}

				}



	
				for(UserEndorsers usrEnd:usrEndorser){
					DocUserMaster dum = new DocUserMaster();
					dum.setDocumentId(dm.getDocumentId());
					dum.setUserId(usrEnd.getEndorserId());
					dum.setDocRole(DocRole.ENDORSER);
					dum.setSerialNumber(21);
					System.out.println("dm.getDocumentId() 1111>>>"+dm.getDocumentId());
					docUserMasterRepo.save(dum);
				}
				for(User tempUsers:users){
					boolean isController=false;
					Users user = null;
					String role = tempUsers.getRole();	 
										if(role!=null && role.indexOf(".")!=-1){
											if(DocRole.CONTROLLER.equalsIgnoreCase(role.substring(role.indexOf(".")+1))){
												isController=true;
												//role=DocRole.CONTROLLER;
											}
											role=role.substring(0,role.indexOf("."));
											System.out.println("role >>>> 1"+role);
											
										}       	
					List<Users> usersForRole = userService.findUserBasedOnRole(role);   	



					for(DocUserMaster dum:userMasterList){
						if(usersForRole != null && usersForRole.size()>0){
							user =usersForRole.get(0);
							if(!isController){
							if(dum.getUserId() == user.getUserId()){
								if(DocRole.APPROVER.equals(dum.getDocRole())){
									dum.setDocRole(DocRole.APPROVER);
								}else if(DocRole.CONTROLLER.equals(dum.getDocRole())){
									dum.setDocRole(DocRole.CONTROLLER); 
								}else{
									dum.setDocRole(DocRole.ENDORSER); 
								}
								DocUserMaster tempDum=new DocUserMaster();
								BeanUtils.copyProperties(dum, tempDum);
								System.out.println("dum."+dum.getUserId());
								docUserMasterRepo.save(tempDum);
							}
							}else{
									if(dum.getUserId() == user.getUserId()){
									dum.setDocRole(DocRole.CONTROLLER);
									DocUserMaster tempDum=new DocUserMaster();
									BeanUtils.copyProperties(dum, tempDum);
									System.out.println("dum."+dum.getUserId());
									docUserMasterRepo.save(tempDum);
														}
								}
						}
					}

				}
				Users user = null;
				String role = users.get(users.size()-1).getRole();	 
				if(role!=null && role.indexOf(".")!=-1){
						role=role.substring(0,role.indexOf("."));
						System.out.println("role >>>> 2"+role);
				}
								        	      	
				List<Users> usersForRole = userService.findUserBasedOnRole(role);   	


				if(usersForRole != null && usersForRole.size()>0){
					user = usersForRole.get(0);

				}
				DocUserMaster dum= docUserMasterRepo.findByDocumentIdAndUserId(dm.getDocumentId(), user.getUserId());
				if(dum!=null){
					dum.setDocRole(DocRole.APPROVER);
					docUserMasterRepo.save(dum);

				}
				return null;

			}
			if (users != null && userMasterList != null && userMasterList.size() > 0 && users.size()+listSize > 0) {
				if (dm != null && dm.getIsAmendment() != null && dm.getIsAmendment().equalsIgnoreCase("Yes")) {
					System.out.println("dm.getOriginalDocId()"+dm.getOriginalDocId());
					isAmendmentExists =documentMasterRepository.findByOriginalDocId(dm.getOriginalDocId());
					int documentId=dm.getOriginalDocId();
					for (DocumentMaster obj:isAmendmentExists){
						if(obj!=null && "Yes".equalsIgnoreCase(obj.getIsAmendment())){
							continue;
						}
						documentId=obj.getDocumentId();
					}
					updateList=false;
					List<DocUserMaster> userOriginalList = docUserMasterRepo
							.findByDocumentIdAndDocRoleIn(documentId, docRole);
					System.out.println("documentId" + documentId);
					System.out.println("userOriginalList" + userOriginalList.size());
					System.out.println("users.size" + users.size());
					System.out.println("userMasterList.size" + userMasterList.size());
					if (users.size()+listSize >= userOriginalList.size()) {
						//System.out.println("users.size" + users.size());
						updateList=true;
						boolean delsupportingEnodrser = false;
						for (User usr : users) {
							String role = usr.getRole();	
							if(role!=null && role.indexOf(".")!=-1){
								if(DocRole.CONTROLLER.equalsIgnoreCase(role.substring(role.indexOf(".")+1))){
									delsupportingEnodrser = true;
									break;
								}
								role=role.substring(0,role.indexOf("."));
								System.out.println("role >>>> 3"+role);
														
							}
							if (role.equals(DocRole.CONTROLLER)) {
								delsupportingEnodrser = true;
								break;
							}
						}
						if (delsupportingEnodrser) {
							docUserMasterRepo.deleteByDocumentId(dm.getDocumentId());
						} else {
							for (String docRoleName : docRoleList) {
								docUserMasterRepo.deleteAllByDocRoleAndDocumentId(docRoleName, dm.getDocumentId());
							}
						}
					} else {
						if (userMasterList.size() > userOriginalList.size()) {
							updateList=false;
							System.out.println("users.size" + users.size());
							// System.out.println("userOriginalList.size"+userOriginalList.size());
							System.out.println("userMasterList.size" + userMasterList.size());
							boolean delsupportingEnodrser = false;
							for (User usr : users) {
								String role = usr.getRole();	 
								if(role!=null && role.indexOf(".")!=-1){
									if(DocRole.CONTROLLER.equalsIgnoreCase(role.substring(role.indexOf(".")+1))){
										delsupportingEnodrser = true;
										break;
									}
									role=role.substring(0,role.indexOf("."));
									System.out.println("role >>>> 4"+role);
														
								}
								if (role.equals(DocRole.CONTROLLER)) {
									delsupportingEnodrser = true;
									break;
								}
							}
							System.out.println("delsupportingEnodrser.size" + delsupportingEnodrser);
							if (delsupportingEnodrser) {
								docUserMasterRepo.deleteByDocumentId(dm.getDocumentId());
							} else {
								for (String docRoleName : docRoleList) {
									System.out.println("docRoleName.size" + docRoleName);
									docUserMasterRepo.deleteAllByDocRoleAndDocumentId(docRoleName, dm.getDocumentId());
								}
							}

							for(DocUserMaster dua : userOriginalList){
								DocUserMaster dumTemp= new DocUserMaster();
								BeanUtils.copyProperties(dua, dumTemp);
								if (dua.getDocRole().equals(DocRole.INITIATOR)) {
									dumTemp.setUserId(dm.getCreatedBy());
								}
								dumTemp.setDocumentId(dm.getDocumentId());
								System.out.println("dumTemp.getDocRole()" + dumTemp.getDocRole()+"dm.getDocumentId()"+dm.getDocumentId());
								docUserMasterRepo.save(dumTemp);
							}
						}
					}
				} else {
					if (users.size()+listSize != userMasterList.size()) {
						System.out.println("users.size" + users.size());
						// System.out.println("userOriginalList.size"+userOriginalList.size());
						System.out.println("userMasterList.size" + userMasterList.size());
						boolean delsupportingEnodrser = false;
						for (User usr : users) {
							String role = usr.getRole();	 
								if(role!=null && role.indexOf(".")!=-1){
									if(DocRole.CONTROLLER.equalsIgnoreCase(role)){
										delsupportingEnodrser = true;
										break;
									}
								role=role.substring(0,role.indexOf("."));
								System.out.println("role >>>> 5"+role);
															
							}
							if (role.equals(DocRole.CONTROLLER)) {
								delsupportingEnodrser = true;
								break;
							}
						}
						System.out.println("delsupportingEnodrser " + delsupportingEnodrser);
						if (delsupportingEnodrser) {
							docUserMasterRepo.deleteByDocumentId(dm.getDocumentId());
						} else {
							for (String docRoleName : docRoleList) {
								System.out.println("docRoleName" + docRoleName);
								docUserMasterRepo.deleteAllByDocRoleAndDocumentId(docRoleName, dm.getDocumentId());
							}
						}


					}
				}
			}
			DocUserMaster dum= new DocUserMaster();
			dum.setDocRole(DocRole.INITIATOR);
			dum.setUserId(dm.getCreatedBy());
			dum.setDocumentId(dm.getDocumentId());
			dum.setSerialNumber(1);
			docUserMasterRepo.save(dum);
			if(updateList){
				if(dmdc.getSelectedImmediatedEndorser()!=100){
					usrEndorser = userEndorserRepo.findByUserId(dm.getCreatedBy());	
					//Integer documentId;
					List<Integer> userList= new ArrayList<Integer>();
					for(UserEndorsers ue:usrEndorser){
						userList.add(ue.getEndorserId());
					}

					Collection<Integer> userColl = new ArrayList<Integer>(userList);
					docUserMasterRepo.deleteByDocumentIdAndUserIdIn(dm.getDocumentId(), userColl);
					dum= new DocUserMaster();
					dum.setDocRole(DocRole.ENDORSER);
					if(dmdc.getSelectedImmediatedEndorser()!=null && dmdc.getSelectedImmediatedEndorser()!=0){
						dum.setUserId(dmdc.getSelectedImmediatedEndorser());
					}else{
						dum.setUserId(usrEndorser.get(0).getEndorserId());
					}
					boolean isInitEndorser=false;
					ArrayList<UserRoles> userRoles1 = userRoleRepo.findByUserId(dm.getCreatedBy());
					System.out.println("userRoles)"+userRoles1.size());
					if(userRoles1!=null && userRoles1.size()>1){
						isInitEndorser=true;
					}
					if (dm != null && dm.getIsAmendment() != null && dm.getIsAmendment().equalsIgnoreCase("Yes")) {
						DocumentMaster docUser=documentMasterRepository.findByDocumentId(dm.getOriginalDocId());
						ArrayList<UserRoles> userRoles = userRoleRepo.findByUserId(docUser.getCreatedBy());
						System.out.println("isAmendmentExists.get(0).getCreatedBy()"+docUser.getCreatedBy());
						System.out.println("userRoles)"+userRoles.size());
						if(userRoles!=null && userRoles.size()>1){
							isInitEndorser=true;
						}
						System.out.println("isInitEndorser)"+isInitEndorser);

					}
					if(!isInitEndorser){
						dum.setDocumentId(dm.getDocumentId());
						dum.setSerialNumber(21);	        	
						docUserMasterRepo.save(dum);
					}

				}else if (dmdc.getSelectedImmediatedEndorser()==100 && dm != null && dm.getIsAmendment() != null && dm.getIsAmendment().equalsIgnoreCase("Yes")) {
					DocumentMaster docUser=documentMasterRepository.findByDocumentId(dm.getOriginalDocId());
					List<DocUserMaster> dumImmediateEndorser = docUserMasterRepo.findBySerialNumberAndDocumentId(21, dm.getOriginalDocId());
					Integer immediateEndorserId =null;
					if(dumImmediateEndorser!=null && dumImmediateEndorser.size()>0){
					  immediateEndorserId= dumImmediateEndorser.get(0).getUserId();
					}
					//ArrayList<UserRoles> userRoles = userRoleRepo.findByUserId(docUser.getCreatedBy());
					System.out.println("dm.getCreatedBy()"+docUser.getCreatedBy());
					usrEndorser = userEndorserRepo.findByUserId(docUser.getCreatedBy());	
					if(usrEndorser!=null && immediateEndorserId !=null && usrEndorser.size()>0){
						//	        		 dum.setUserId(usrEndorser.get(0).getEndorserId()); 
						dum.setUserId(immediateEndorserId); 
						dum.setDocRole(DocRole.ENDORSER);
						dum.setDocumentId(dm.getDocumentId());
						dum.setSerialNumber(21);	        	
						docUserMasterRepo.save(dum);
					}
				}
			}

			int beforeControllerSeq=22;
			int afterControllerSeq=91;
			int controllerSeq=0;
			if(updateList){
				Boolean businessController=false;
				for (User businessUser : users) {
					String role = businessUser.getRole();	 
					if(role!=null && role.indexOf(".")!=-1){
						role=role.substring(0,role.indexOf("."));
						System.out.println("role >>>> 6"+role);
					}
					if (role.equals(DocRole.CONTROLLER)) {
						businessController=true;
					}
				}
				for(User businessUser : users){

					dum= new DocUserMaster();
					Users user = null;
					String role = businessUser.getRole();	 
					if(role!=null && role.indexOf(".")!=-1){
						role=role.substring(0,role.indexOf("."));
						System.out.println("role >>>> 7"+role);
											
					}	        	
					List<Users> usersForRole = userService.findUserBasedOnRole(role);   	


					if(usersForRole != null && usersForRole.size()>0){
						boolean isApprover=false;
						user = usersForRole.get(0);
						/*if(dm!= null && dm.getIsAmendment()!=null && dm.getIsAmendment().equalsIgnoreCase("Yes")){
	        			System.out.println("isApprover1"+isApprover+"isDifferentList"+isDifferentList);
		        		if(isDifferentList && businessUser.getSequence()==users.size()+2){
		        			System.out.println("isApprover2"+isApprover);
		        			isApprover=true;
		        		}else{
		        			System.out.println("isApprover3"+isApprover);
		        			isApprover=false;
		        		}
	        		}else*/ 

						if(businessUser.getSequence()==users.size()+listSize){
							isApprover=true;
						}



						if(isApprover){
							dum.setDocRole(DocRole.APPROVER);
							dum.setSerialNumber(100);
						} else if(DocRole.CONTROLLER.equals(role)||(role.indexOf(DocRole.CONTROLLER)!=-1 && !businessController)){
						//User role
							dum.setDocRole(DocRole.CONTROLLER);	//Doc Role
							dum.setSerialNumber(50);
							controllerSeq=businessUser.getSequence();
						} else{
							dum.setDocRole(DocRole.ENDORSER);
							if(DocRole.MINING_CONTROLLER.equals(role) && !businessController){	
								dum.setSerialNumber(50);
								controllerSeq=businessUser.getSequence();
							}else if(controllerSeq==0 || businessUser.getSequence() < controllerSeq){
								dum.setSerialNumber(beforeControllerSeq);
								beforeControllerSeq++;
							}else {
								dum.setSerialNumber(afterControllerSeq);
								afterControllerSeq++;
							}
						}
						if(businessUser.getRole()!=null && DocRole.CONTROLLER.equalsIgnoreCase(businessUser.getRole().substring(businessUser.getRole().indexOf(".")+1))){
							dum.setDocRole(DocRole.CONTROLLER);	//Doc Role
							dum.setSerialNumber(50);
							controllerSeq=businessUser.getSequence();
						}
						dum.setUserId(user.getUserId());
						dum.setDocumentId(dm.getDocumentId());

						docUserMasterRepo.save(dum);
					}

				}
			}

		} catch (Throwable t) {
			t.printStackTrace();
		}
		return isRulePresent;

	}

	public static KieContainer createKieContainerForProject() {
		KieServices kieServices = KieServices.Factory.get();
		// Create a module model
		KieModuleModel kieModuleModel = kieServices.newKieModuleModel();

		// Base Model from the module model
		KieBaseModel kieBaseModel = kieModuleModel.newKieBaseModel( "rules" )
				.setDefault( true )
				.setEqualsBehavior( EqualityBehaviorOption.EQUALITY)
				.setEventProcessingMode( EventProcessingOption.STREAM );                

		// Create session model for the Base Model
		KieSessionModel ksessionModel = kieBaseModel.newKieSessionModel( "ksession-rules" )
				.setDefault( true )
				.setType( KieSessionModel.KieSessionType.STATEFUL )
				.setClockType( ClockTypeOption.get("realtime") );

		// Create File System services
		KieFileSystem kFileSystem = kieServices.newKieFileSystem();

		//File file = new File("D:\\git-code\\11Dec-wf2\\11122015GIT\\DRules\\Sample.drl");\

		File folder = new File(System.getProperty("user.dir"));
		//File folder = new File(file.getParent());
		File sampleFile = null;
		/*if(folder.exists()){
			sampleFile = new File(folder.getAbsolutePath()+"//DRules/Sample.drl");
			//System.out.println("sample file location "+sampleFile.getAbsolutePath()+sampleFile.exists());

		}*/
		List<String> fileList=new ArrayList<String>();
		String drules = PropsUtil.getValue("drl.folder");
		fileList.add(folder.getAbsolutePath()+"//"+drules+"/Sample.drl");
		fileList.add(folder.getAbsolutePath()+"//"+drules+"/utkal.drl");
		fileList.add(folder.getAbsolutePath()+"//"+drules+"/aditya.drl");
		fileList.add(folder.getAbsolutePath()+"//"+drules+"/Corp-IT.drl");
		fileList.add(folder.getAbsolutePath()+"//"+drules+"/rpPlant.drl");
		fileList.add(folder.getAbsolutePath()+"//"+drules+"/hirakud.drl");
		fileList.add(folder.getAbsolutePath()+"//"+drules+"/belgaum.drl");
		fileList.add(folder.getAbsolutePath()+"//"+drules+"/muri.drl");
		fileList.add(folder.getAbsolutePath()+"//"+drules+"/mahan.drl");
		fileList.add(folder.getAbsolutePath()+"//"+drules+"/renukoot.drl");
		fileList.add(folder.getAbsolutePath()+"//"+drules+"/dahej.drl");
		fileList.add(folder.getAbsolutePath()+"//"+drules+"/MiningAndMinerals.drl");
		Resource resource =null;

		for(String fileName:fileList){
			if(folder.exists()){
				sampleFile = new File(fileName);
				resource=kieServices.getResources().newFileSystemResource(sampleFile).setResourceType(ResourceType.DRL);
				//System.out.println("sample file location "+sampleFile.getAbsolutePath()+sampleFile.exists());
				kFileSystem.write( resource );
			}
		}


		// Resource resource = kieServices.getResources().newFileSystemResource().setResourceType(ResourceType.DRL);
		// kFileSystem.write( resource ); 



		KieBuilder kbuilder = kieServices.newKieBuilder( kFileSystem );
		// kieModule is automatically deployed to KieRepository if successfully built.
		kbuilder.buildAll();

		if (kbuilder.getResults().hasMessages(org.kie.api.builder.Message.Level.ERROR)) {
			throw new RuntimeException("Build time Errors: " + kbuilder.getResults().toString());
		}
		KieContainer kContainer = kieServices.newKieContainer(kieServices.getRepository().getDefaultReleaseId());
		return kContainer;
	}



	@SuppressWarnings("unchecked")
	public Boolean addBusinessRuleUsersUT(DocumentMetaDataCreate dmdc ,DocumentMaster dm, ArrayList<RevenueProposal> rps,Boolean isReferredToInitiator) {
		Double total=0.0;
		String isBudgeted=null;
		Boolean isRulePresent =true;
		ArrayList<RevenueProposal> rps1 = dmdc.getRpData();

		try {
			// load up the knowledge base
			//KieServices ks = KieServices.Factory.get();

			KieContainer kContainer = createKieContainerForProject();
			//KieContainer kContainer = ks.getKieClasspathContainer();
			//KieSession kSession = kContainer.newKieSession("ksession-rules");
			KieSession kSession = kContainer.newKieSession();

			// go !
			Message message = new Message();
			message.setMessage("Hello World");
			message.setStatus(Message.HELLO);
			kSession.insert(message);

			for(RevenueProposal rp:rps1){
				total += rp.getAmountNegotiated();
			}	

			for(RevenueProposal rp:rps1){
				if("1".equals(rp.getIsBudgeted())){
					isBudgeted="Planned";				
				}
				else{
					isBudgeted="Unplanned";
					break;
				}
			}
			Integer materialId = dmdc.getMaterial().getMaterialId();


			Business business = new Business();
			if(dmdc.getBusiness().getBusinessName().equalsIgnoreCase("Aluminium")||dmdc.getBusiness().getBusinessName().equalsIgnoreCase("Copper")){
				business.setBusinessType(dmdc.getBusiness().getBusinessName());
			}else{
				business.setBusinessType("All");
			}
			business.setRequestType(isBudgeted);
			business.setValue(total);
			business.setMaterialId(materialId);
			Material material=masterMaterialRepo.findByMaterialId(materialId);
			business.setMaterialName(material.getMaterialName());
			if(material!=null && material.getDoa()!=null && !material.getDoa().isEmpty()){
				business.setDoa(material.getDoa());
			}else{
				business.setDoa("");
			}
			String plantName="";
			for(InitiatorPlant initiatorPlant:InitiatorPlant.values()){
				System.out.println("------------------------------has plant name"+dmdc.getPlantName());
				if(dmdc.getPlantName()!=null && initiatorPlant.toString().equalsIgnoreCase(dmdc.getPlantName()) && "Corporate".equals(dmdc.getPlantName())){
					System.out.println("------------------------------has plant name");
					business.setPlantName("Corporate");
					plantName="Corporate";
					//business.setInitiatorPlant(initiatorPlant);
				}else if(dmdc.getPlantName()!=null && initiatorPlant.toString().equalsIgnoreCase(dmdc.getPlantName()) && "Utkal".equals(dmdc.getPlantName())){
					System.out.println("------------------------------has plant name");
					business.setPlantName("Utkal");

					plantName="Utkal";
					//business.setInitiatorPlant(initiatorPlant);
				}else if(dmdc.getPlantName()!=null && !initiatorPlant.toString().equalsIgnoreCase(dmdc.getPlantName()) && !"Utkal".equals(dmdc.getPlantName()) && !initiatorPlant.toString().equalsIgnoreCase(dmdc.getPlantName()) && !"Corporate".equals(dmdc.getPlantName())){
					business.setBusinessType(dmdc.getBusiness().getBusinessName());
					business.setRequestType(isBudgeted);
					business.setValue(total);
					System.out.println("dmdc.getPlantList()  >>>>"+dmdc.getPlantList() );
					System.out.println("business.getPlantName()  >>>>"+business.getPlantName() );
					if(dmdc.getPlantList() == null){
						Plant plant=masterPlantDataRepository.findByPlantName(dmdc.getPlantName());
						business.setPlantName(plant.getPlantName().trim());
						Integer clusterId= plant.getClusterId();
						if(clusterId!=null){
						business.setClusterId(clusterId);
						List<Cluster> cluster = clusterRepo.findByClusterId(clusterId);
						business.setClusterName(cluster.get(0).getClusterName());
						}
					}else{
					business.setPlantName(dmdc.getPlantList().get(0).getPlantName().trim());
					Integer clusterId= dmdc.getPlantList().get(0).getClusterId();
					if(clusterId!=null){
					business.setClusterId(clusterId);
					List<Cluster> cluster = clusterRepo.findByClusterId(clusterId);
					business.setClusterName(cluster.get(0).getClusterName());
					}
					}
				}
			}
			Users usersEty = userService.findByUserid(dmdc.getCreatedBy());
			if(usersEty!=null && usersEty.getDeptId()!=null){
				business.setDeptId(usersEty.getDeptId());
				if(usersEty.getDeptId()!=0){
				business.setPlantName(null);
				}
			}
			kSession.insert(business);
			logger.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
			logger.info("## Amount Neg  "+business.getValue());
			logger.info("## un/planned  "+ business.getRequestType());
			logger.info("## Business  "+business.getBusinessType());
			logger.info("## Plant Name  "+business.getPlantName());
			logger.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
			logger.info("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			logger.info("!! DOA chain   "+business.getData());
			logger.info("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");


			kSession.fireAllRules();
			ArrayList<User> users = business.getUsers();
			String tempData="";
			String seperator="::";
			if(business.getData() == null && business.getUsers()!=null){
				for(User user :users){
					String role = user.getRole();	 
					if(role!=null && role.indexOf(".")!=-1){
						role=role.substring(0,role.indexOf("."));
						System.out.println("role >>>> 8"+role);
					}
					tempData=tempData+role+seperator;
				}
				tempData=tempData.trim();
				tempData=tempData.substring(0,tempData.lastIndexOf(seperator));
			}else{
				tempData= business.getData();
			}
			
			if(tempData == null || tempData.isEmpty()){
				tempData="NA";
			}
			//set the DOA string (provided by Rule Engine) for comparing in JUnit test
			this.doaOutputFromRuleEngine =tempData;
			/*if(dm == null){
				if(business == null || (business!=null &&  business.getUsers()==null) || (business!=null &&  business.getUsers()!=null &&  business.getUsers().size() ==0)){
					isRulePresent=false;
					return isRulePresent;
				}else if(business != null && dm == null && rps == null){
					return isRulePresent;
				}
			}*/


			



		} catch (Throwable t) {
			t.printStackTrace();
		}
		return isRulePresent;

	}


	

}
