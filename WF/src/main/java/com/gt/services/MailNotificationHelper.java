package com.gt.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.gt.entity.Delegation;
import com.gt.entity.DocUserAction;
import com.gt.entity.DocUserMaster;
import com.gt.entity.DocumentMaster;
import com.gt.entity.Material;
import com.gt.entity.Plant;
import com.gt.entity.RevenueProposal;
import com.gt.entity.Role;
import com.gt.entity.UserRoles;
import com.gt.entity.Users;
import com.gt.repository.DelegationRepository;
import com.gt.repository.DocUserActionRepository;
import com.gt.repository.DocUserMasterRepository;
import com.gt.repository.MasterMaterialDataRepository;
import com.gt.repository.MasterPlantDataRepository;
import com.gt.repository.RolesRepository;
import com.gt.repository.UserRepository;
import com.gt.repository.UserRoleRepository;
import com.gt.utils.ActionStatus;
import com.gt.utils.DeptHead.DepartmentHead;
import com.gt.utils.DocRole;
import com.gt.utils.PropsUtil;
import com.gt.utils.WFUtil;

@Component("mailNotificationHelper")
@Transactional
public class MailNotificationHelper {

	 private final static Logger logger = Logger.getLogger(MailNotificationHelper.class.getName());
	 
	 private final UserRepository userrepo;
	 private final DocUserMasterRepository docUserMasterRepo;
	 private final DocUserActionRepository docUserActionRepo;
	 private final MasterPlantDataRepository MasterPlantRepo;
	 private final MasterMaterialDataRepository MasterMaterialRepo;
	 private final UserRoleRepository userRoleRepo;
	 private final DelegationRepository delegateRepo;
	 private final RolesRepository rolesRepo;
	 
		@Autowired
		public MailNotificationHelper(DelegationRepository delegateRepo,DocUserActionRepository docUserActionRepo , UserRepository userrepo ,MasterMaterialDataRepository MasterMaterialRepo, MasterPlantDataRepository MasterPlantRepo, DocUserMasterRepository docUserMasterRepo,UserRoleRepository userRoleRepo,RolesRepository rolesRepo
				){
			this.docUserActionRepo=docUserActionRepo;
			this.docUserMasterRepo=docUserMasterRepo;
			this.userrepo = userrepo;
			this.MasterMaterialRepo=MasterMaterialRepo;
			this.MasterPlantRepo=MasterPlantRepo;
			this.userRoleRepo=userRoleRepo;
			this.delegateRepo=delegateRepo;
			this.rolesRepo=rolesRepo;
		}

		
		public Map<String,List<String>> getMailIds(DocumentMaster dm,String isCreate,String isCreateFinalApproved,String isApprove,String isRejected,String isRelease, String isEndorse,Integer userIdInt,Integer delegatedTo,Integer delegatedByInt, String rpNumber,List<RevenueProposal> revenueProposalList,String isReferedBack,String referedTo,String isReferedRp,String isCancelled) {
			List<String> toMailIds=new ArrayList<String>();
			List<String> ccMailIds=new ArrayList<String>();
			List<String> subjectList=new ArrayList<String>();
			Integer documentId =null;
			Boolean isExpress=null;
			String proposalType="";
			String subject=null;
			String plantName="";
			Plant plant =null;
			Integer refferTo;
			Boolean reply =false;
			String materialName=null;
			Material material =null;
			Map<String,List<String>> allLists= new HashMap<String, List<String>>();
			 String test = PropsUtil.getValue("release.testing");
			 String subjectAppend="";
			 
			 if(test.equalsIgnoreCase("true")){
				 subjectAppend="Testing e-TrackRev ";
			 }
			 
			 
			
			//Delegation Notification
			if(delegatedTo!=null){
				subject = "Temporary Delegation in e-TrackRev";
				Users usr;				
			    usr = userrepo.findByUserId(delegatedTo);
				toMailIds.add(usr.getEmailId());
				usr = userrepo.findByUserId(delegatedByInt);
				ccMailIds.add(usr.getEmailId());
				
			}
			
			if(dm!=null && ((revenueProposalList !=null && revenueProposalList.size()>0))){
			String documentType=dm.getDocumentType();
			if(documentType == null || documentType.isEmpty()){
					documentType="RP";
			}
			if(delegatedByInt!=null && delegatedByInt!=-1 &&delegatedByInt!=0){
				userIdInt=delegatedByInt;
			}	
			
			documentId=dm.getDocumentId();
			isExpress=dm.getIsExpress();
			proposalType=dm.getProposalType();
			if(referedTo!=null && !referedTo.equalsIgnoreCase("null")  && !referedTo.equalsIgnoreCase("false")){
				refferTo=Integer.parseInt(referedTo);
			}else{
				refferTo= -1;
			}
			if(isReferedRp!=null && !isReferedRp.equalsIgnoreCase("false")){
				reply = true;
			}
			 if(revenueProposalList!=null && revenueProposalList.size()>0){
			 int z=0;			
			 for(RevenueProposal rp:revenueProposalList){
				plant=MasterPlantRepo.findByPlantId(rp.getPlantId());
				if(z==0){
					plantName=plant.getPlantName();
				}else{
					plantName=plantName+", "+plant.getPlantName();
				}
				z++;
			 }
			 }
			 Integer categoryId=null;
			
				if(dm.getMaterialId()!=null){
					 material = MasterMaterialRepo.findByMaterialId(dm.getMaterialId());
					 materialName = material.getMaterialName();
				}
			
			 Map<String,List<String>> getCongfiguredMailIds;
			 List<DocUserMaster> docUserMasterList=(List<DocUserMaster>) docUserMasterRepo.findByDocumentIdAndSerialNumberIsNotNullOrderBySerialNumberAsc(documentId);
			 List<DocUserMaster> parallelEndorsersList=(List<DocUserMaster>) docUserMasterRepo.findByDocumentIdAndSerialNumberIsNull(documentId);
			 List<DocUserAction> duas = docUserActionRepo.findByDocumentIdAndActionStatus(documentId, ActionStatus.PENDING);
			 Boolean controllerPresent= false;
			 List<Role> miningContrlRole = rolesRepo.findByRoleName(DocRole.MINING_CONTROLLER);
			 Integer miningCtrlRoleId=miningContrlRole.get(0).getRoleId();
			 Integer miningCtrlId=userRoleRepo.findByRoleId(miningCtrlRoleId).get(0).getUserId();
			 for(DocUserMaster dum :docUserMasterList){
				 if(dum.getDocRole().equalsIgnoreCase(DocRole.CONTROLLER)){
					 controllerPresent= true;			
				 }else if(dm.getDeptId()!=null && dm.getDeptId()==2 && dum.getUserId()==miningCtrlId){
					 controllerPresent= true;	
				 }
			 }
			 
			 //CancelledRP Notification
			 
			 if(isCancelled!=null && isCancelled.equalsIgnoreCase("true")){
				 
				 if(isExpress!=null && isExpress){
					 subject="Express "+documentType+" for \'"+materialName+"\' for plant(s) \'"+plantName+"\' has been Cancelled";
					 String expressCancelledPendingRPTo=PropsUtil.getValue("rp.express.cancelled.mail.notification.to");					
					 String expressCancelledPendingRPCc=PropsUtil.getValue("rp.express.cancelled.mail.notification.cc");
					 getCongfiguredMailIds= configuredMailIds( expressCancelledPendingRPCc, expressCancelledPendingRPTo, dm, userIdInt,refferTo,reply,delegatedTo,delegatedByInt);
					 ccMailIds=getCongfiguredMailIds.get("Cc");
					 toMailIds=getCongfiguredMailIds.get("To");
				 }else{
					 subject=documentType+" for \'"+materialName+"\' for plant(s) \'"+plantName+"\' has been Cancelled";
					 String cancelledPendingRPTo=PropsUtil.getValue("rp.cancelled.mail.notification.to");					
					 String cancelledPendingRPCc=PropsUtil.getValue("rp.cancelled.mail.notification.cc");
					 getCongfiguredMailIds= configuredMailIds( cancelledPendingRPCc, cancelledPendingRPTo, dm, userIdInt,refferTo,reply,delegatedTo,delegatedByInt);
					 ccMailIds=getCongfiguredMailIds.get("Cc");
					 toMailIds=getCongfiguredMailIds.get("To"); 
				 }
			 }
			 
			 // RP Create
			 if(isCreate!=null && isCreate.equalsIgnoreCase("true")){
				
				 if(isExpress!=null && isExpress){
					 subject="Express "+documentType+" for \'"+materialName+"\' for plant(s) \'"+plantName+"\' awaiting your action";
					 if(parallelEndorsersList!=null && parallelEndorsersList.size()>0){
						 //Express RP Created, Parallel Endorser Added 
						 String initiatorAddedParallelEndorserTo=PropsUtil.getValue("rp.initiator.added.parallel.endorser.to");					
						 String initiatorAddedParallelEndorserCc=PropsUtil.getValue("rp.initiator.added.parallel.endorser.cc");
						 getCongfiguredMailIds= configuredMailIds( initiatorAddedParallelEndorserCc, initiatorAddedParallelEndorserTo, dm, userIdInt,refferTo,reply,delegatedTo,delegatedByInt);
						 ccMailIds=getCongfiguredMailIds.get("Cc");
						 toMailIds=getCongfiguredMailIds.get("To"); 
						 
					 }else{
						//Express RP Created(Dept/Corp Level), Parallel Endorser not added  
						
						 if(controllerPresent){							
								String expressCreateMailNotificationCC=PropsUtil.getValue("rp.express.creation.mail.notification.cc");							
								String expressCreateMailNotificationTo=PropsUtil.getValue("rp.express.creation.mail.notification.to");
								getCongfiguredMailIds=configuredMailIds( expressCreateMailNotificationCC, expressCreateMailNotificationTo, dm, userIdInt,refferTo,reply,delegatedTo,delegatedByInt);
								ccMailIds=getCongfiguredMailIds.get("Cc");
								toMailIds=getCongfiguredMailIds.get("To");
								
						 }else{						 
							 String createMailNotificationCC=PropsUtil.getValue("rp.creation.mail.notification.cc");
							 String createMailNotificationTo=PropsUtil.getValue("rp.creation.mail.notification.to");
							 getCongfiguredMailIds= configuredMailIds( createMailNotificationCC, createMailNotificationTo, dm, userIdInt,refferTo,reply,delegatedTo,delegatedByInt);
							 ccMailIds=getCongfiguredMailIds.get("Cc");
							 toMailIds=getCongfiguredMailIds.get("To");
						 }
						 
					 }
					 
				 }else{
					 subject=documentType+" for \'"+materialName+"\' for plant(s) \'"+plantName+"\' awaiting your action";
					 if(parallelEndorsersList!=null && parallelEndorsersList.size()>0){
						 //RP Created, Parallel Endorser Added 
						 String initiatorAddedParallelEndorserTo=PropsUtil.getValue("rp.initiator.added.parallel.endorser.to");					
						 String initiatorAddedParallelEndorserCc=PropsUtil.getValue("rp.initiator.added.parallel.endorser.cc");
						 getCongfiguredMailIds= configuredMailIds( initiatorAddedParallelEndorserCc, initiatorAddedParallelEndorserTo, dm, userIdInt,refferTo,reply,delegatedTo,delegatedByInt);
						 ccMailIds=getCongfiguredMailIds.get("Cc");
						 toMailIds=getCongfiguredMailIds.get("To");
						
					 }else{
						//RP Created, Parallel Endorser Not Added 
						 String createMailNotificationCC=PropsUtil.getValue("rp.creation.mail.notification.cc");
						 String createMailNotificationTo=PropsUtil.getValue("rp.creation.mail.notification.to");
						 getCongfiguredMailIds= configuredMailIds( createMailNotificationCC, createMailNotificationTo, dm, userIdInt,refferTo,reply,delegatedTo,delegatedByInt);
						 ccMailIds=getCongfiguredMailIds.get("Cc");
						 toMailIds=getCongfiguredMailIds.get("To");
						 
					 }
				 }
			 }
			 
			 
			 // Final RP Create
			 if(isCreateFinalApproved!=null && isCreateFinalApproved.equalsIgnoreCase("true")){
				
				 	subject="Final Approval "+documentType+" for \'"+materialName+"\' for plant(s) \'"+plantName+"\' awaiting your action";
					
						//RP Created, Parallel Endorser Not Added 
						 String createMailNotificationCC=PropsUtil.getValue("rp.final.creation.mail.notification.cc");
						 String createMailNotificationTo=PropsUtil.getValue("rp.final.creation.mail.notification.to");
						 getCongfiguredMailIds= configuredMailIds( createMailNotificationCC, createMailNotificationTo, dm, userIdInt,refferTo,reply,delegatedTo,delegatedByInt);
						 ccMailIds=getCongfiguredMailIds.get("Cc");
						 toMailIds=getCongfiguredMailIds.get("To");
						 
			 }
			 
			 
			 //Refered
			 if(isReferedBack!=null && isReferedBack.equalsIgnoreCase("true")){
				 if(isExpress!=null && isExpress){	
					 	subject="Express "+documentType+" for \'"+materialName+"\' for plant(s) \'"+plantName+"\' is Reffered back";
						String expressRefferBackMailNotificationTo=PropsUtil.getValue("rp.express.referBack.mail.notification.to");					
						String expressRefferBackMailNotificationCC=PropsUtil.getValue("rp.express.referBack.mail.notification.cc");
						 getCongfiguredMailIds= configuredMailIds( expressRefferBackMailNotificationCC, expressRefferBackMailNotificationTo, dm, userIdInt,refferTo,reply,delegatedTo,delegatedByInt);
						 ccMailIds=getCongfiguredMailIds.get("Cc");
						 toMailIds=getCongfiguredMailIds.get("To");
				 }else{
					 subject=documentType+" for \'"+materialName+"\' for plant(s) \'"+plantName+"\' is Reffered back";
					 String refferBackMailNotificationTo=PropsUtil.getValue("rp.referBack.mail.notification.to");
					 String refferBackMailNotificationCC=PropsUtil.getValue("rp.referBack.mail.notification.cc");
					 getCongfiguredMailIds= configuredMailIds( refferBackMailNotificationCC, refferBackMailNotificationTo, dm, userIdInt,refferTo,reply,delegatedTo,delegatedByInt);
					 ccMailIds=getCongfiguredMailIds.get("Cc");
					 toMailIds=getCongfiguredMailIds.get("To");
				 }
			 }
			 
			 //Reply
			 if(isReferedRp!=null && isReferedRp.equalsIgnoreCase("true")){
				 if(isExpress!=null && isExpress){	
					 if(dm.getDeptId()!=null && dm.getDeptId()==1){
						 subject="Express "+documentType+" for \'"+materialName+"\' for plant(s) \'"+plantName+"\' awaiting your action";
					 }else{
						 subject="Express "+documentType+" for \'"+materialName+"\' for plant(s) \'"+plantName+"\' is Replied";
					 }
						String expressReplyMailNotificationTo=PropsUtil.getValue("rp.express.reply.mail.notification.to");					
						String expressReplyMailNotificationCC=PropsUtil.getValue("rp.express.reply.mail.notification.cc");
						 getCongfiguredMailIds= configuredMailIds( expressReplyMailNotificationCC, expressReplyMailNotificationTo, dm, userIdInt,refferTo,reply,delegatedTo,delegatedByInt);
						 ccMailIds=getCongfiguredMailIds.get("Cc");
						 toMailIds=getCongfiguredMailIds.get("To");
				 }else{
					 if(dm.getDeptId()!=null && dm.getDeptId()==1){
						 subject=documentType+" for \'"+materialName+"\' for plant(s) \'"+plantName+"\' awaiting your action";
				     }else{
				    	subject=documentType+" for \'"+materialName+"\' for plant(s) \'"+plantName+"\' is Replied";
					 }
					 String replyMailNotificationTo=PropsUtil.getValue("rp.reply.mail.notification.to");
					 String replyMailNotificationCC=PropsUtil.getValue("rp.reply.mail.notification.cc");
					 getCongfiguredMailIds= configuredMailIds( replyMailNotificationCC, replyMailNotificationTo, dm, userIdInt,refferTo,reply,delegatedTo,delegatedByInt);
					 ccMailIds=getCongfiguredMailIds.get("Cc");
					 toMailIds=getCongfiguredMailIds.get("To");
				 }
			 }
			 
			 //Approve
			 if(isApprove!=null && isApprove.equalsIgnoreCase("true")){
				 if(isExpress!=null && isExpress){
					 	subject="Express "+documentType+" for \'"+materialName+"\' for plant(s) \'"+plantName+"\' is Approved";
					    String expressApproveMailNotificationTo=PropsUtil.getValue("rp.express.approve.mail.notification.to");
						String expressApproveMailNotificationCC=PropsUtil.getValue("rp.express.approve.mail.notification.cc");
						if(dm.getDeptId()!=null && dm.getDeptId()==2 && dm.getProposalType()!=null && dm.getProposalType().equalsIgnoreCase("Final")){
							subject="Final Approval Express "+documentType+" for \'"+materialName+"\' for plant(s) \'"+plantName+"\' is Released";
							expressApproveMailNotificationTo=PropsUtil.getValue("rp.final.release.mail.notification.to");					
							expressApproveMailNotificationCC=PropsUtil.getValue("rp.final.release.mail.notification.cc");
						}
						 getCongfiguredMailIds= configuredMailIds( expressApproveMailNotificationCC, expressApproveMailNotificationTo, dm, userIdInt,refferTo,reply,delegatedTo,delegatedByInt);
						 ccMailIds=getCongfiguredMailIds.get("Cc");
						 toMailIds=getCongfiguredMailIds.get("To");
				 }else{
					 	subject=documentType+" for \'"+materialName+"\' for plant(s) \'"+plantName+"\' is Approved";
					 	String approveMailNotificationTo=PropsUtil.getValue("rp.approve.mail.notification.to");
						String approveMailNotificationCC=PropsUtil.getValue("rp.approve.mail.notification.cc");
						if(dm.getDeptId()!=null && dm.getDeptId()==2 && dm.getProposalType()!=null && dm.getProposalType().equalsIgnoreCase("Final")){
							subject="Final Approval "+documentType+" for \'"+materialName+"\' for plant(s) \'"+plantName+"\' is Released";
							approveMailNotificationTo=PropsUtil.getValue("rp.final.release.mail.notification.to");					
							approveMailNotificationCC=PropsUtil.getValue("rp.final.release.mail.notification.cc");
						}
						 getCongfiguredMailIds= configuredMailIds( approveMailNotificationCC, approveMailNotificationTo, dm, userIdInt,refferTo,reply,delegatedTo,delegatedByInt);
						 ccMailIds=getCongfiguredMailIds.get("Cc");
						 toMailIds=getCongfiguredMailIds.get("To");
				 }
			 }
			 //Reject
			 if(isRejected!=null && isRejected.equalsIgnoreCase("true")){
				 if(isExpress!=null && isExpress){	
					 	subject="Express "+documentType+" for \'"+materialName+"\' for plant(s) \'"+plantName+"\' is Rejected";
						String expressRejectMailNotificationTo=PropsUtil.getValue("rp.express.reject.mail.notification.to");					
						String expressRejectMailNotificationCC=PropsUtil.getValue("rp.express.reject.mail.notification.cc");
						String itDeptHead=PropsUtil.getValue("it.dept.head.and.previous.mail.notification.cc");	
						if(dm.getDeptId()==1 && itDeptHead!=null && itDeptHead.equalsIgnoreCase("enable")){
							expressRejectMailNotificationCC=PropsUtil.getValue("rp.it.express.reject.mail.notification.cc");
						}
						 getCongfiguredMailIds= configuredMailIds( expressRejectMailNotificationCC, expressRejectMailNotificationTo, dm, userIdInt,refferTo,reply,delegatedTo,delegatedByInt);
						 ccMailIds=getCongfiguredMailIds.get("Cc");
						 toMailIds=getCongfiguredMailIds.get("To");
				 }else{
					 subject=documentType+" for \'"+materialName+"\' for plant(s) \'"+plantName+"\' is Rejected";
					 String rejectMailNotificationTo=PropsUtil.getValue("rp.reject.mail.notification.to");
					 String rejectMailNotificationCC=PropsUtil.getValue("rp.reject.mail.notification.cc");
					 String itDeptHead=PropsUtil.getValue("it.dept.head.and.previous.mail.notification.cc");	
						if(dm.getDeptId()!=null && dm.getDeptId()==1 && itDeptHead!=null && itDeptHead.equalsIgnoreCase("enable")){
							rejectMailNotificationCC=PropsUtil.getValue("rp.it.reject.mail.notification.cc");
						}
					 getCongfiguredMailIds= configuredMailIds( rejectMailNotificationCC, rejectMailNotificationTo, dm, userIdInt,refferTo,reply,delegatedTo,delegatedByInt);
					 ccMailIds=getCongfiguredMailIds.get("Cc");
					 toMailIds=getCongfiguredMailIds.get("To");
				 }
			 }
			 //Release
			 if(isRelease!=null && isRelease.equalsIgnoreCase("true")){
				 if(isExpress!=null && isExpress){
					 subject="Express "+documentType+" for \'"+materialName+"\' for plant(s) \'"+plantName+"\' is Released";
					 if(controllerPresent!=null && controllerPresent){
						 String corporateReleaseMailNotificationTo=PropsUtil.getValue("rp.release.mail.notification.CorporateLevel.to");					
						 String corporateReleaseMailNotificationCC=PropsUtil.getValue("rp.release.mail.notification.CorporateLevel.cc");
						 getCongfiguredMailIds= configuredMailIds( corporateReleaseMailNotificationCC, corporateReleaseMailNotificationTo, dm, userIdInt,refferTo,reply,delegatedTo,delegatedByInt);
						 ccMailIds=getCongfiguredMailIds.get("Cc");
						 toMailIds=getCongfiguredMailIds.get("To");
					 }else{
						 String releaseMailNotificationTo=PropsUtil.getValue("rp.release.mail.notification.to");					
						 String releaseMailNotificationCC=PropsUtil.getValue("rp.release.mail.notification.cc");
						 String itDeptHead=PropsUtil.getValue("it.dept.head.and.previous.mail.notification.cc");	
							if(dm.getDeptId()!=null && dm.getDeptId()==1 && itDeptHead!=null && itDeptHead.equalsIgnoreCase("enable")){
								releaseMailNotificationTo=PropsUtil.getValue("rp.it.release.mail.notification.to");					
								releaseMailNotificationCC=PropsUtil.getValue("rp.it.release.mail.notification.cc");
							}
						 getCongfiguredMailIds= configuredMailIds( releaseMailNotificationCC, releaseMailNotificationTo, dm, userIdInt,refferTo,reply,delegatedTo,delegatedByInt);
						 ccMailIds=getCongfiguredMailIds.get("Cc");
						 toMailIds=getCongfiguredMailIds.get("To");
					 }
					 	
					
				 }else{
					 subject=documentType+" for \'"+materialName+"\' for plant(s) \'"+plantName+"\' is Released";
					 if(controllerPresent!=null && controllerPresent){
						 String corporateReleaseMailNotificationTo=PropsUtil.getValue("rp.release.mail.notification.CorporateLevel.to");					
						 String corporateReleaseMailNotificationCC=PropsUtil.getValue("rp.release.mail.notification.CorporateLevel.cc");
						 getCongfiguredMailIds= configuredMailIds( corporateReleaseMailNotificationCC, corporateReleaseMailNotificationTo, dm, userIdInt,refferTo,reply,delegatedTo,delegatedByInt);
						 ccMailIds=getCongfiguredMailIds.get("Cc");
						 toMailIds=getCongfiguredMailIds.get("To");
					 }
					 else{
						 String releaseMailNotificationTo=PropsUtil.getValue("rp.release.mail.notification.to");					
						 String releaseMailNotificationCC=PropsUtil.getValue("rp.release.mail.notification.cc");
						 String itDeptHead=PropsUtil.getValue("it.dept.head.and.previous.mail.notification.cc");	
							if(dm.getDeptId()!=null && dm.getDeptId()==1 && itDeptHead!=null && itDeptHead.equalsIgnoreCase("enable")){
								releaseMailNotificationTo=PropsUtil.getValue("rp.it.release.mail.notification.to");					
								releaseMailNotificationCC=PropsUtil.getValue("rp.it.release.mail.notification.cc");
							}
						 getCongfiguredMailIds= configuredMailIds( releaseMailNotificationCC, releaseMailNotificationTo, dm, userIdInt,refferTo,reply,delegatedTo,delegatedByInt);
						 ccMailIds=getCongfiguredMailIds.get("Cc");
						 System.out.println("ccMailId" +ccMailIds);
						 toMailIds=getCongfiguredMailIds.get("To");
						 System.out.println("toMailId "+toMailIds);
					 }
				 }
			 }
			 
			 //Endorse

			 if(isEndorse!=null && isEndorse.equalsIgnoreCase("true")){
				 Users usr1 = userrepo.findByUserId(userIdInt);
				 Boolean isAdvisory = usr1.getIsSupportingEndorser();
				 DocUserMaster dum1 = docUserMasterRepo.findByDocumentIdAndUserId(documentId, userIdInt);
					 if(isExpress!=null && isExpress){
						 subject="Express "+documentType+" for \'"+materialName+"\' for plant(s) \'"+plantName+"\' awaiting your action";
						 if(isAdvisory!=null && isAdvisory && dum1.getSerialNumber()==null){	
							 if(parallelEndorsersList!=null && parallelEndorsersList.size()>0){
								 
								 String addedParallelEndorsedTo=PropsUtil.getValue("rp.added.parallel.endorser.endorsed.to");					
								 String addedParallelEndorsedCc=PropsUtil.getValue("rp.added.parallel.endorser.endorsed.cc");
								 getCongfiguredMailIds= configuredMailIds( addedParallelEndorsedCc, addedParallelEndorsedTo, dm, userIdInt,refferTo,reply,delegatedTo,delegatedByInt);
								 ccMailIds=getCongfiguredMailIds.get("Cc");
								 toMailIds=getCongfiguredMailIds.get("To");
								 
								 
							}
							
						 }else{
						
								 String expressEnodorsedMailNotificationTo=PropsUtil.getValue("rp.express.endorse.mail.notification.to");					
								 String expressEnodorsedMailNotificationCC=PropsUtil.getValue("rp.express.endorse.mail.notification.cc");
								 getCongfiguredMailIds= configuredMailIds( expressEnodorsedMailNotificationCC, expressEnodorsedMailNotificationTo, dm, userIdInt,refferTo,reply,delegatedTo,delegatedByInt);
								 ccMailIds=getCongfiguredMailIds.get("Cc");
								 toMailIds=getCongfiguredMailIds.get("To"); 
							 
						 }
					 }else{
						 subject=documentType+" for \'"+materialName+"\' for plant(s) \'"+plantName+"\' awaiting your action";
						 if(isAdvisory!=null && isAdvisory && dum1.getSerialNumber()==null){	
							 if(parallelEndorsersList!=null && parallelEndorsersList.size()>0){
								 
								 String addedParallelEndorsedTo=PropsUtil.getValue("rp.added.parallel.endorser.endorsed.to");					
								 String addedParallelEndorsedCc=PropsUtil.getValue("rp.added.parallel.endorser.endorsed.cc");
								 getCongfiguredMailIds= configuredMailIds( addedParallelEndorsedCc, addedParallelEndorsedTo, dm, userIdInt,refferTo,reply,delegatedTo,delegatedByInt);
								 ccMailIds=getCongfiguredMailIds.get("Cc");
								 toMailIds=getCongfiguredMailIds.get("To");
								 
								 }
							
						 }else{
						 
						 		if(controllerPresent!=null && controllerPresent && parallelEndorsersList!=null && parallelEndorsersList.size()>0 && duas!=null && (duas.get(0).getDocRole().equalsIgnoreCase(DocRole.CONTROLLER)||duas.get(0).getUserId().equals(miningCtrlId))){
						 			
						 			 String controllerAddedParallelEndorserTo=PropsUtil.getValue("rp.controller.added.parallel.endorser.to");					
									 String controllerAddedParallelEndorserCc=PropsUtil.getValue("rp.controller.added.parallel.endorser.cc");
									 getCongfiguredMailIds= configuredMailIds( controllerAddedParallelEndorserCc, controllerAddedParallelEndorserTo, dm, userIdInt,refferTo,reply,delegatedTo,delegatedByInt);
									 ccMailIds=getCongfiguredMailIds.get("Cc");
									 toMailIds=getCongfiguredMailIds.get("To"); 
											 	 
								 }else{
								 String enodorsedMailNotificationTo=PropsUtil.getValue("rp.endorse.mail.notification.to");					
								 String enodorsedMailNotificationCC=PropsUtil.getValue("rp.endorse.mail.notification.cc");
								 getCongfiguredMailIds= configuredMailIds( enodorsedMailNotificationCC, enodorsedMailNotificationTo, dm, userIdInt,refferTo,reply,delegatedTo,delegatedByInt);
								 ccMailIds=getCongfiguredMailIds.get("Cc");
								 toMailIds=getCongfiguredMailIds.get("To");
							 }
						 }
					  }
					 
				 
			 }
			}
			 
			subject=subjectAppend+subject;
		 
		 	List<String> plantNameList=new ArrayList<String>();
			List<String> materialList=new ArrayList<String>();
			plantNameList.add(plantName);
			materialList.add(materialName);
			Set<String> ccMailIdsSet = new HashSet<>(ccMailIds);
			Set<String> toMailIdsSet = new HashSet<>(toMailIds);
			List<String> tempCCList=new ArrayList<String>();
			tempCCList.addAll(ccMailIdsSet);
			List<String> tempToList=new ArrayList<String>();
			tempToList.addAll(toMailIdsSet);
			subjectList.add(subject);
			allLists.put("CC",tempCCList);
			allLists.put("TO",tempToList);
			allLists.put("SUBJECT",subjectList);
			allLists.put("PLANTNAME",plantNameList);
			allLists.put("MATERIAL",materialList);
		
		return allLists;
	}
	
	
	private Map<String,List<String>> configuredMailIds( String ccIds, String toIds, DocumentMaster dm, Integer currentUserId, Integer referTo, Boolean isReply,Integer delegatedTo,Integer delegatedByInt){
		System.out.println("cc "+ccIds);
		System.out.println("to "+toIds);
		Map<String,List<String>> mailIdList = new HashMap<String, List<String>>();
		Map<String,List<String>> getMailIds = null;
		List<String> toMailIds=new ArrayList<String>();
		List<String> ccMailIds=new ArrayList<String>();
		Integer documentId=dm.getDocumentId();
		List<DocUserMaster> dums = docUserMasterRepo.findByDocumentIdAndDocRoleIn(documentId, WFUtil.docRoleList());//allusers
		List<DocUserAction> dua = docUserActionRepo.findByDocumentIdAndActionStatus(documentId,ActionStatus.PENDING);
		List<DocUserAction> duaCancelled = docUserActionRepo.findByDocumentIdAndActionStatus(documentId,ActionStatus.CANCELLED);
		 List<DocUserMaster> parallelEndorsersList= docUserMasterRepo.findByDocumentIdAndSerialNumberIsNull(documentId);
		 List<DocUserAction> duaRPCancelledBy = docUserActionRepo.findByDocumentIdAndActionStatusAndUserIdOrderByDocActionSerialNumberDesc(documentId, ActionStatus.CANCELLED, currentUserId);
		
		List<DocUserAction> duas = docUserActionRepo.findByDocumentIdAndActionStatusAndUserIdOrderByDocActionSerialNumberDesc(documentId, ActionStatus.REPLIED, currentUserId);
		Integer srNo=docUserMasterRepo.findByDocumentIdAndUserId(documentId, currentUserId).getSerialNumber();
		DocUserMaster dum;
		Integer initiator =null;
		Integer controller=null;
		Integer controllerEndorsed=null;
		Integer approver=null;
		Integer nextEndorser=null;
		Integer replyToEndorser=null;
		 Integer replyCancelledTo =null;
		 
		 List<Role> poManagerRole = rolesRepo.findByRoleName("PO Manager");
		 Integer roleId=poManagerRole.get(0).getRoleId();
		 Integer poManagerId=userRoleRepo.findByRoleId(roleId).get(0).getUserId();
		 
		 
		 
		dum = docUserMasterRepo.findByDocumentIdAndDocRole(documentId, DocRole.INITIATOR);
		if(dum!=null){
			initiator = dum.getUserId();//initiator id
		}
		
		dum = docUserMasterRepo.findByDocumentIdAndDocRole(documentId, DocRole.CONTROLLER);
		 List<Role> miningContrlRole = rolesRepo.findByRoleName(DocRole.MINING_CONTROLLER);
		 Integer miningCtrlRoleId=miningContrlRole.get(0).getRoleId();
		 Integer miningCtrlId=userRoleRepo.findByRoleId(miningCtrlRoleId).get(0).getUserId();
		if(dum!=null||miningCtrlId!=null){
			List<DocUserAction> duaController=null;
			if(dum!=null){
			controller = dum.getUserId();//controller id
			duaController = docUserActionRepo.findByDocumentIdAndActionStatusAndUserIdOrderByDocActionSerialNumberAsc(documentId, ActionStatus.ENDORSED, controller);
			}
			List<DocUserAction> duaMiningController = null;
			if(miningCtrlId!=null){
				duaMiningController=docUserActionRepo.findByDocumentIdAndActionStatusAndUserIdOrderByDocActionSerialNumberAsc(documentId, ActionStatus.ENDORSED, miningCtrlId);
			}
				if(duaController!=null&& duaController.size()>0){
					controllerEndorsed = duaController.get(0).getUserId();
				}else if(dm.getDeptId()!=null &&dm.getDeptId()==2&& duaMiningController!=null&& duaMiningController.size()>0){
					controllerEndorsed = duaMiningController.get(0).getUserId();
				}
		}	


		
		dum = docUserMasterRepo.findByDocumentIdAndDocRole(documentId, DocRole.APPROVER);
		if(dum!=null){
			approver = dum.getUserId();//approver id
		}	
		
		if(dua!=null && dua.size()>0){
			DocUserAction dua1=dua.get(0);
			nextEndorser=dua1.getUserId();//next endorser id
		}
		
		if(isReply!=null && isReply && duas.size()>0){
			DocUserAction dua1 = duas.get(0);
			replyToEndorser= dua1.getFromUserAction();
		}else if(isReply!=null && isReply && duas.size()==0){
			duas=docUserActionRepo.findByDocumentIdAndActionStatus(documentId, ActionStatus.PENDING);
			if(duas!=null && duas.size() >0){
				DocUserAction dua1 = duas.get(0);
				replyToEndorser= dua1.getUserId();
			}
			
		}
		
		  if(duaRPCancelledBy!=null && duaRPCancelledBy.size()>0){
			  Integer referedFrom =duaRPCancelledBy.get(0).getFromUserAction();
			  if(!referedFrom.equals(currentUserId)){
				  replyCancelledTo= referedFrom;
			  }
		  }
		  Integer itDeptHeadSerialNumber= null;
		 if(dm.getDeptId()!=null && dm.getDeptId()==1){
			 List<Role> role = rolesRepo.findByRoleName("CIO");
			 ArrayList<UserRoles> usrRole = userRoleRepo.findByRoleId(role.get(0).getRoleId());
			 DocUserMaster dumIt = docUserMasterRepo.findByDocumentIdAndUserId(documentId, usrRole.get(0).getUserId());
			 itDeptHeadSerialNumber= dumIt.getSerialNumber();
			 
		 }
		String[] mailTo =  toIds.split(",");
		String[] mailCc =  ccIds.split(",");
		Users usr;
		for(int i= 0;i<mailTo.length;i++){
			System.out.println("controller >>>>"+controller);
			System.out.println("controllerEndorsed >>>>"+controllerEndorsed);
			if(mailTo.length == 1 && mailTo[i].equalsIgnoreCase("ControllerEndorsed") ){
				if(controllerEndorsed==null || controllerEndorsed.intValue() ==0){
				controllerEndorsed=controller;
				}
			}
			if(initiator!=null && mailTo[i].equalsIgnoreCase(DocRole.INITIATOR)){
				getMailIds=isDelegation(initiator, true,delegatedTo,delegatedByInt);
				 ccMailIds.addAll(getMailIds.get("Cc"));
				 toMailIds.addAll(getMailIds.get("To"));
			}
			if(controller !=null && mailTo[i].equalsIgnoreCase(DocRole.CONTROLLER)){
				getMailIds=isDelegation(controller, true,delegatedTo,delegatedByInt);
				ccMailIds.addAll(getMailIds.get("Cc"));
				toMailIds.addAll(getMailIds.get("To"));
			}
			if(controllerEndorsed !=null && mailTo[i].equalsIgnoreCase("ControllerEndorsed")){
				getMailIds=isDelegation(controllerEndorsed, true,delegatedTo,delegatedByInt);
				ccMailIds.addAll(getMailIds.get("Cc"));
				 toMailIds.addAll(getMailIds.get("To"));
			}
			if(approver !=null && mailTo[i].equalsIgnoreCase(DocRole.APPROVER)){
				getMailIds=isDelegation(approver, true,delegatedTo,delegatedByInt);
				ccMailIds.addAll(getMailIds.get("Cc"));
				toMailIds.addAll(getMailIds.get("To"));
			}
			if(nextEndorser !=null && mailTo[i].equalsIgnoreCase("NextEndorser")){
				getMailIds=isDelegation(nextEndorser, true,delegatedTo,delegatedByInt);
				ccMailIds.addAll(getMailIds.get("Cc"));
				toMailIds.addAll(getMailIds.get("To"));
			}
			if(currentUserId!=null && mailTo[i].equalsIgnoreCase("CurrentUser")){
				getMailIds=isDelegation(currentUserId, true,delegatedTo,delegatedByInt);
				ccMailIds.addAll(getMailIds.get("Cc"));
				toMailIds.addAll(getMailIds.get("To"));
			}
			if(referTo != null && mailTo[i].equalsIgnoreCase("ReferredTo")){
				getMailIds=isDelegation(referTo, true,delegatedTo,delegatedByInt);
				ccMailIds.addAll(getMailIds.get("Cc"));
				toMailIds.addAll(getMailIds.get("To"));
			}
			if(replyToEndorser != null && mailTo[i].equalsIgnoreCase("ReplyTo")){
				getMailIds=isDelegation(replyToEndorser, true,delegatedTo,delegatedByInt);
				ccMailIds.addAll(getMailIds.get("Cc"));
				toMailIds.addAll(getMailIds.get("To"));
			}
			if(mailTo[i].equalsIgnoreCase("AllUsers")){
				for(DocUserMaster dum1 :dums){
					Integer usrId = dum1.getUserId();
					if(!usrId.equals(currentUserId)){
						getMailIds=isDelegation(usrId, true,delegatedTo,delegatedByInt);
						ccMailIds.addAll(getMailIds.get("Cc"));
						toMailIds.addAll(getMailIds.get("To"));
					}
				}
			}
			if(dums!=null && mailTo[i].equalsIgnoreCase("AllPrevEndorser")){
			
				for(DocUserMaster prvuser :dums){
					if(srNo!=null && prvuser.getSerialNumber()!=null && prvuser.getSerialNumber()<srNo){
						getMailIds=isDelegation(prvuser.getUserId(), true,delegatedTo,delegatedByInt);
						ccMailIds.addAll(getMailIds.get("Cc"));
						toMailIds.addAll(getMailIds.get("To"));
					}
				}
			}
			if(parallelEndorsersList!=null && mailTo[i].equalsIgnoreCase("allAddedParallelEndorser")){
				
				for(DocUserMaster parallelEndorser :parallelEndorsersList){			
						getMailIds=isDelegation(parallelEndorser.getUserId(), true,delegatedTo,delegatedByInt);
						ccMailIds.addAll(getMailIds.get("Cc"));
						toMailIds.addAll(getMailIds.get("To"));
				}
			}
			if(dums!=null && mailTo[i].equalsIgnoreCase("InitiatorOrController")){
				 DocUserMaster dumFrom;
				 dumFrom = docUserMasterRepo.findByDocumentIdAndDocRole(documentId, DocRole.CONTROLLER);
				 Users usr1;
				 if(dumFrom!=null||(miningCtrlId!=null && dm.getDeptId()!=null && dm.getDeptId()== 2)){
					 if(dumFrom!=null){
					 getMailIds=isDelegation(dumFrom.getUserId(), true,delegatedTo,delegatedByInt);
					 }else if(dm.getDeptId()!=null && dm.getDeptId()== 2){
						 getMailIds=isDelegation(miningCtrlId, true,delegatedTo,delegatedByInt);
					 }
					 ccMailIds.addAll(getMailIds.get("Cc"));
					 toMailIds.addAll(getMailIds.get("To"));
				 }else{
					 dumFrom = docUserMasterRepo.findByDocumentIdAndDocRole(documentId, DocRole.INITIATOR);
					 getMailIds=isDelegation(dumFrom.getUserId(), true,delegatedTo,delegatedByInt);
					 ccMailIds.addAll(getMailIds.get("Cc"));
					 toMailIds.addAll(getMailIds.get("To"));
				 }
			}
			
			if(dums!=null && duaCancelled!=null && mailTo[i].equalsIgnoreCase("allCancelled")){
				for(DocUserAction duaCancel:duaCancelled){
					if(!duaCancel.getDocRole().equalsIgnoreCase(DocRole.INITIATOR)){
						 getMailIds=isDelegation(duaCancel.getUserId(), true,delegatedTo,delegatedByInt);
						 ccMailIds.addAll(getMailIds.get("Cc"));
						 toMailIds.addAll(getMailIds.get("To"));
					}
				}	
			}
			if( duaRPCancelledBy.size()>0 && replyCancelledTo!=null && duaCancelled!=null && mailTo[i].equalsIgnoreCase("replyCancel")){
						 getMailIds=isDelegation(replyCancelledTo, true,delegatedTo,delegatedByInt);
						 ccMailIds.addAll(getMailIds.get("Cc"));
						 toMailIds.addAll(getMailIds.get("To"));
					
			}
		
			
			
			
			
		}
		for(int i= 0;i<mailCc.length;i++){
			if(initiator!=null && mailCc[i].equalsIgnoreCase(DocRole.INITIATOR)){
				getMailIds=isDelegation(initiator, false,delegatedTo,delegatedByInt);
				ccMailIds.addAll(getMailIds.get("Cc"));
				toMailIds.addAll(getMailIds.get("To"));
			}
			if(controller!=null && mailCc[i].equalsIgnoreCase(DocRole.CONTROLLER) ){
				getMailIds=isDelegation(controller, false,delegatedTo,delegatedByInt);
				ccMailIds.addAll(getMailIds.get("Cc"));
				toMailIds.addAll(getMailIds.get("To"));
			}
			if(controllerEndorsed !=null && mailCc[i].equalsIgnoreCase("ControllerEndorsed")){
				getMailIds=isDelegation(controllerEndorsed, false,delegatedTo,delegatedByInt);
				ccMailIds.addAll(getMailIds.get("Cc"));
				toMailIds.addAll(getMailIds.get("To"));
			}
			if(approver!=null && mailCc[i].equalsIgnoreCase(DocRole.APPROVER)){
				getMailIds=isDelegation(approver, false,delegatedTo,delegatedByInt);
				ccMailIds.addAll(getMailIds.get("Cc"));
				toMailIds.addAll(getMailIds.get("To"));
			}
			if(nextEndorser!=null && mailCc[i].equalsIgnoreCase("NextEndorser")){
				getMailIds=isDelegation(nextEndorser, false,delegatedTo,delegatedByInt);
				ccMailIds.addAll(getMailIds.get("Cc"));
				toMailIds.addAll(getMailIds.get("To"));
			}
			if(currentUserId!= null && mailCc[i].equalsIgnoreCase("CurrentUser")){
				getMailIds=isDelegation(currentUserId, false,delegatedTo,delegatedByInt);
				ccMailIds.addAll(getMailIds.get("Cc"));
				toMailIds.addAll(getMailIds.get("To"));
			}
			if(referTo != null && mailCc[i].equalsIgnoreCase("ReferredTo")){
				getMailIds=isDelegation(referTo, false,delegatedTo,delegatedByInt);
				ccMailIds.addAll(getMailIds.get("Cc"));
				 toMailIds.addAll(getMailIds.get("To"));
			}
			if(replyToEndorser != null && mailCc[i].equalsIgnoreCase("ReplyTo")){
				getMailIds=isDelegation(replyToEndorser, false,delegatedTo,delegatedByInt);
				ccMailIds.addAll(getMailIds.get("Cc"));
				toMailIds.addAll(getMailIds.get("To"));
			}
			if(mailCc[i].equalsIgnoreCase("AllUsers")){
				for(DocUserMaster dum1 :dums){
					Integer usrId = dum1.getUserId();
					if(usrId!=currentUserId){
						getMailIds=isDelegation(usrId, false,delegatedTo,delegatedByInt);
						ccMailIds.addAll(getMailIds.get("Cc"));
						 toMailIds.addAll(getMailIds.get("To"));
					}
				}
			}
			if(dums!=null && mailCc[i].equalsIgnoreCase("AllPrevEndorser")){			
				for(DocUserMaster prvuser :dums){
					if(srNo!=null && prvuser.getSerialNumber()!=null && prvuser.getSerialNumber()<srNo){
						getMailIds=isDelegation(prvuser.getUserId(), false,delegatedTo,delegatedByInt);
						ccMailIds.addAll(getMailIds.get("Cc"));
						toMailIds.addAll(getMailIds.get("To"));
					}
				}
			}
			if(parallelEndorsersList!=null && mailCc[i].equalsIgnoreCase("allAddedParallelEndorser")){
				
				for(DocUserMaster parallelEndorser :parallelEndorsersList){		
						getMailIds=isDelegation(parallelEndorser.getUserId(), false,delegatedTo,delegatedByInt);
						ccMailIds.addAll(getMailIds.get("Cc"));
						 toMailIds.addAll(getMailIds.get("To"));
				}
			}
			
			if(dums!=null && mailCc[i].equalsIgnoreCase("InitiatorOrController")){
				 DocUserMaster dumFrom;
				 dumFrom = docUserMasterRepo.findByDocumentIdAndDocRole(documentId, DocRole.CONTROLLER);
				 Users usr1;
				 if(dumFrom!=null||miningCtrlId!=null){
					 if(dumFrom!=null){
					 getMailIds=isDelegation(dumFrom.getUserId(), true,delegatedTo,delegatedByInt);
					 }else{
						 getMailIds=isDelegation(miningCtrlId, true,delegatedTo,delegatedByInt);
					 }
					 ccMailIds.addAll(getMailIds.get("Cc"));
					 toMailIds.addAll(getMailIds.get("To"));
				 }else{
					 dumFrom = docUserMasterRepo.findByDocumentIdAndDocRole(documentId, DocRole.INITIATOR);
					 getMailIds=isDelegation(dumFrom.getUserId(), false,delegatedTo,delegatedByInt);
					 ccMailIds.addAll(getMailIds.get("Cc"));
					 toMailIds.addAll(getMailIds.get("To"));
				 }
			}
			
			if(dums!=null && duaCancelled!=null && mailCc[i].equalsIgnoreCase("allCancelled")){
				for(DocUserAction duaCancel:duaCancelled){
					if(!duaCancel.getDocRole().equalsIgnoreCase(DocRole.INITIATOR)){
						getMailIds=isDelegation(duaCancel.getUserId(), false,delegatedTo,delegatedByInt);
						ccMailIds.addAll(getMailIds.get("Cc"));
						toMailIds.addAll(getMailIds.get("To"));
					}
				}	
			}
			if(duaRPCancelledBy.size()>0  && replyCancelledTo!=null && duaCancelled!=null && mailCc[i].equalsIgnoreCase("replyCancel")){
				getMailIds=isDelegation(replyCancelledTo, false,delegatedTo,delegatedByInt);
				ccMailIds.addAll(getMailIds.get("Cc"));
				 toMailIds.addAll(getMailIds.get("To"));
			
			}
			
			if(dums!=null && mailCc[i].equalsIgnoreCase("allITDept")){			
				for(DocUserMaster prvuser :dums){
						getMailIds=isDelegation(prvuser.getUserId(), false,delegatedTo,delegatedByInt);
						ccMailIds.addAll(getMailIds.get("Cc"));
						toMailIds.addAll(getMailIds.get("To"));
				}
			}
			
			if( mailCc[i].equalsIgnoreCase("poManager")){
				getMailIds=isDelegation(poManagerId, false,delegatedTo,delegatedByInt);
				ccMailIds.addAll(getMailIds.get("Cc"));
				toMailIds.addAll(getMailIds.get("To"));
			}
			
		
		}
	if(dm.getDeptId()!= null && dm.getDeptId()== 0){
		String deptHead=PropsUtil.getValue("dept.head.mail.notification.cc");
		if(deptHead.equalsIgnoreCase("enable")){
			
			Integer deptHeadId = null;
			
			//if RP belongs to corporate
			DepartmentHead departmentHead = DepartmentHead.Head_RawMaterials; 
			
			if(departmentHead.equals(DepartmentHead.Head_RawMaterials)){
				//Retrieve the user-id for Head-RawMaterial
				List<Role> role = rolesRepo.findByRoleName("Head-RawMaterial");
				 ArrayList<UserRoles> usrs = userRoleRepo.findByRoleId(role.get(0).getRoleId());
				 if((!usrs.isEmpty()) && usrs!=null){
					 deptHeadId= usrs.get(0).getUserId();
				 }
			}
			
			
			if(deptHeadId!=null){
				 usr =userrepo.findByUserId(deptHeadId);
				ccMailIds.add(usr.getEmailId());
			}
		}
		String immediateEndorser=PropsUtil.getValue("immediate.endorser.mail.notification.cc");	
		if(immediateEndorser.equalsIgnoreCase("enable")){
			Integer immeEndorserId = null;
			List<DocUserMaster> dumImmediateEndorser = docUserMasterRepo.findBySerialNumberAndDocumentId(21, documentId);
			if(!dumImmediateEndorser.isEmpty()){
				immeEndorserId=dumImmediateEndorser.get(0).getUserId();
			}
			if(immeEndorserId!=null){
				usr =userrepo.findByUserId(immeEndorserId);
				ccMailIds.add(usr.getEmailId());
			}
		}
	}
		
		
		for(int i =0 ;i<ccMailIds.size();i++){
			String mailCc1=ccMailIds.get(i);
			for(int j= 0;j<toMailIds.size();j++){
				if(mailCc1.equals(toMailIds.get(j))){
					ccMailIds.remove(i);
					i--;
				}
			}
		}
		
		
		Set<String> ccMailIdsSet = new HashSet<>(ccMailIds);
		Set<String> toMailIdsSet = new HashSet<>(toMailIds);
		List<String> tempCCList=new ArrayList<String>();
		tempCCList.addAll(ccMailIdsSet);
		List<String> tempToList=new ArrayList<String>();
		tempToList.addAll(toMailIdsSet);
		
		mailIdList.put("Cc",tempCCList);
		mailIdList.put("To",tempToList);
			
		return mailIdList;
		
}
	private Map<String,List<String>> isDelegation(Integer userId, Boolean toId,Integer delegatedTo,Integer delegatedBy ){
		
		Map<String,List<String>> mailIdList = new HashMap<String, List<String>>();
		List<String> toMailIds=new ArrayList<String>();
		List<String> ccMailIds=new ArrayList<String>();
		
		List<Delegation> delegates = delegateRepo.findByUserIdAndIsActive(userId, true);
		Users usr ;
		String delegationEnabled=PropsUtil.getValue("rp.delegation");	
		if(delegates.size()>0 && delegationEnabled.equalsIgnoreCase("enable")){
			for(Delegation del :delegates){
				Date fromDate =del.getFromDate();
				Date toDate = del.getToDate();
				Date currentDate =new Date();
				
				boolean getMin = (currentDate.compareTo(toDate)<=0 &&currentDate.compareTo(fromDate)>=0);
				if(getMin){
					if(toId){
						
							usr= userrepo.findByUserId(del.getDelegateUserId());
							toMailIds.add(usr.getEmailId());
							usr = userrepo.findByUserId(userId);
							ccMailIds.add(usr.getEmailId());
							
					}else{
							usr = userrepo.findByUserId(userId);
							ccMailIds.add(usr.getEmailId());
					}
				}else{
					if(toId){
						usr = userrepo.findByUserId(userId);
						toMailIds.add(usr.getEmailId());
	
					}else{
						usr = userrepo.findByUserId(userId);
						ccMailIds.add(usr.getEmailId());
	
					}
				}
			}
		}else{
			if(toId){
				usr = userrepo.findByUserId(userId);
				toMailIds.add(usr.getEmailId());

			}else{
				usr = userrepo.findByUserId(userId);
				ccMailIds.add(usr.getEmailId());

			}
		}
		if(delegatedBy!=null && delegatedBy!=-1){
			usr = userrepo.findByUserId(delegatedBy);
			ccMailIds.add(usr.getEmailId());

		}
		
		Set<String> ccMailIdsSet = new HashSet<>(ccMailIds);
		Set<String> toMailIdsSet = new HashSet<>(toMailIds);
		List<String> tempCCList=new ArrayList<String>();
		tempCCList.addAll(ccMailIdsSet);
		List<String> tempToList=new ArrayList<String>();
		tempToList.addAll(toMailIdsSet);
		
		mailIdList.put("Cc",tempCCList);
		mailIdList.put("To",tempToList);
			
		return mailIdList;
	}
}
