package com.gt.services;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Blob;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.gt.bo.DocAttachmentBO;
import com.gt.bo.DocumentUserActionBO;
import com.gt.dao.DocAttachDao;
import com.gt.entity.Comments;
import com.gt.entity.Delegation;
import com.gt.entity.DocAttachment;
import com.gt.entity.DocUserAction;
import com.gt.entity.DocUserMaster;
import com.gt.entity.DocumentMaster;
import com.gt.entity.Role;
import com.gt.entity.UserEndorsers;
import com.gt.entity.UserRoles;
import com.gt.entity.Users;
import com.gt.repository.CommentRepository;
import com.gt.repository.DelegationRepository;
import com.gt.repository.DocAttachmentRepository;
import com.gt.repository.DocUserActionRepository;
import com.gt.repository.DocUserMasterRepository;
import com.gt.repository.DocumentMasterRepository;
import com.gt.repository.RPNumberRepository;
import com.gt.repository.RevenueProposalRepository;
import com.gt.repository.RolesRepository;
import com.gt.repository.UserEndorserRepository;
import com.gt.repository.UserRepository;
import com.gt.repository.UserRoleRepository;
import com.gt.utils.ActionStatus;
import com.gt.utils.DocRole;
import com.gt.utils.DocStatus;
import com.gt.utils.PropsUtil;
import com.gt.utils.WFUtil;

@Component("docUserActionService")
@Transactional
public class DocUserActionServiceImpl implements DocUserActionService {

	private final static Logger logger = Logger.getLogger(DocUserActionServiceImpl.class.getName());
	
	private final DocUserActionRepository docUserActionRepo;
	private final DocUserMasterRepository docUserMasterRepo;
	private final DocumentMasterRepository documentMasterRepository;
	private final CommentRepository CommentRepo;
	private final DocAttachmentRepository docAttachmentRepository;
	private final UserRepository userrepo;
	private final UserEndorserRepository userEndorserRepo;
	private final RevenueProposalRepository revenueProposalRepository;
	private final DelegationRepository delegateRepo;
	private final RolesRepository rolesRepo;
	private final UserRoleRepository userRoleRepo;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private DocAttachDao docAttachDao;
	
	@Autowired
	public DocUserActionServiceImpl(DelegationRepository delegateRepo,DocUserActionRepository docUserActionRepo ,RevenueProposalRepository revenueProposalRepository,DocumentMasterRepository documentMasterRepository, CommentRepository CommentRepo,DocUserMasterRepository docUserMasterRepo,DocAttachmentRepository docAttachmentRepository, RPNumberRepository rpNumRepo, UserRepository userrepo,UserEndorserRepository userEndorserRepo,RolesRepository rolesRepo, UserRoleRepository userRoleRepo){
		this.docUserActionRepo=docUserActionRepo;
		this.docUserMasterRepo=docUserMasterRepo;
		this.documentMasterRepository=documentMasterRepository;
		this.CommentRepo=CommentRepo;
		this.docAttachmentRepository=docAttachmentRepository;
		this.userrepo = userrepo;
		this.userEndorserRepo =userEndorserRepo;
		this.revenueProposalRepository =revenueProposalRepository;
		this.delegateRepo=delegateRepo;
		this.userRoleRepo= userRoleRepo;
		this.rolesRepo = rolesRepo;
	}
	
	DocumentMaster getDocumentMasterByDocumentId(
			Integer documentId) {
		
		return (DocumentMaster) documentMasterRepository.findByDocumentId(documentId);
	}
	
	/* (non-Javadoc)
	 * @see com.gt.services.DocUserActionService1#saveApprovedData(java.lang.Integer, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public DocUserAction saveApprovedData(Integer documentId, Integer userId, Integer delegatedByInt){
		DocumentMaster dm = getDocumentMasterByDocumentId(documentId);
		DocUserMaster dum =null;
		List<DocUserAction> docUsers = docUserActionRepo.findByDocumentIdAndActionStatus (documentId, ActionStatus.PENDING);
//		List<DocUserAction> initiator = docUserActionRepo.findByDocumentIdAndActionStatus (documentId, ActionStatus.INITIATED);
		List<DocUserAction> docUserActionAll = docUserActionRepo.findByDocumentId(documentId);
		DocUserAction userPendingAction = null;
		
		Integer delegatedTo =null;
		if(delegatedByInt!=null && delegatedByInt != -1 && (delegatedByInt != userId)){
			dum = docUserMasterRepo.findByDocumentIdAndUserId(documentId, delegatedByInt);
			delegatedTo=userId;
			userId=delegatedByInt;
		}
//		Integer relUserId =initiator.get(0).getUserId();
//		String docRole = DocRole.INITIATOR;
			
		
		for(DocUserAction docUserAction : docUsers){
			if (userId.equals(docUserAction.getUserId())){
				userPendingAction = docUserAction;
				break;
			}
		}
		Date date = new Date();
		Integer srNo =userPendingAction.getDocActionSerialNumber();
		userPendingAction.setActionTakenTimeStamp(date);
		userPendingAction.setActionStatus(ActionStatus.APPROVED);
		if(delegatedTo!=null){
			userPendingAction.setActionTakenBy(delegatedTo);
		}
		docUserActionRepo.save(userPendingAction);
		
		List<DocUserAction> docUsersParallel = docUserActionRepo.findByDocumentIdAndActionStatus (documentId, ActionStatus.PENDING_PARALLEL);
		if(docUsersParallel.size()!=0){
			for(DocUserAction dua:docUsersParallel){
				dua.setActionStatus(ActionStatus.PENDING_INVALID);
				dua.setIsDraft("No");
				docUserActionRepo.save(dua);
				System.out.println("docUserAction.getIsDraft() Approve DUA>>>"+dua.getIsDraft());
				logger.info("docUserAction.getIsDraft()actionId>>>"+dua.getActionId()+" Approve DUA>>>"+dua.getIsDraft());
			}			
		}
//		for(DocUserAction dua:docUserActionAll){
//			if(dua.getDocRole().equalsIgnoreCase(DocRole.CONTROLLER)){
//			relUserId=dua.getUserId();
//			docRole=dua.getDocRole();
//			
//			}	
//		}	
		String rpRelease =PropsUtil.getValue("rp.release");
//		String rpReleaseBy =PropsUtil.getValue("rp.release.By");
//		String rpReleaseCorporateIT= PropsUtil.getValue("rp.release.CorporateIT.By");
		if(dm.getDeptId()!=null && dm.getDeptId()!=0 && dm.getDeptId()!=1 && dm.getDeptId()!=2){
			rpRelease="Disable";
		}
		Boolean isFinal=false;
		if(dm.getProposalType()!=null && dm.getProposalType().equalsIgnoreCase("Final")){
			isFinal =true;
		}
		Integer relUserId = null;
		if(rpRelease.equalsIgnoreCase("Enable") && !isFinal ){
			logger.info("dm.setStatus saveApproved"+dm.getStatus()+" ProposalType "+ dm.getProposalType()+" DocId "+dm.getDocumentId());
			String rpReleaseBy= null;
			if(dm.getDeptId()== null || dm.getDeptId()== 0){
				rpReleaseBy =PropsUtil.getValue("rp.release.By");
				for(DocUserAction dua:docUserActionAll){
					if(dua.getDocRole().equalsIgnoreCase(DocRole.CONTROLLER)){
						 rpReleaseBy =PropsUtil.getValue("rp.release.CorporateLevel.By");				
					}	
				}
			}else if(dm.getDeptId()!= null && dm.getDeptId() == 1){
				rpReleaseBy = PropsUtil.getValue("rp.release.CorporateIT.By");
			}else if(dm.getDeptId()!= null && dm.getDeptId() == 2){
				rpReleaseBy =PropsUtil.getValue("rp.release.miningNMinerals.deptHead.By");
				for(DocUserAction dua:docUserActionAll){
					if(dua.getDocRole().equalsIgnoreCase(DocRole.CONTROLLER)){
						rpReleaseBy = PropsUtil.getValue("rp.release.miningNMinerals.By");
					}	
				}
			}
			
			
			
			
			dum = docUserMasterRepo.findByDocumentIdAndDocRole(documentId, DocRole.INITIATOR);
			
			if(dum!=null && rpReleaseBy!= null && rpReleaseBy.equalsIgnoreCase(DocRole.INITIATOR)){
				relUserId = dum.getUserId();//initiator id
			}
			
			dum = docUserMasterRepo.findByDocumentIdAndDocRole(documentId, DocRole.CONTROLLER);
			if(dum!=null  && rpReleaseBy!= null && rpReleaseBy.equalsIgnoreCase(DocRole.CONTROLLER)){
				relUserId = dum.getUserId();//controller id
				
			}	
			
			dum = docUserMasterRepo.findByDocumentIdAndDocRole(documentId, DocRole.APPROVER);
			if(dum!=null && rpReleaseBy!= null && rpReleaseBy.equalsIgnoreCase(DocRole.APPROVER)){
				relUserId = dum.getUserId();//approver id
			}
			if(dum!=null && rpReleaseBy!= null && rpReleaseBy.equalsIgnoreCase("MiningController")){
			List<Role> role = rolesRepo.findByRoleName(DocRole.MINING_CONTROLLER);
			 ArrayList<UserRoles> usrs = userRoleRepo.findByRoleId(role.get(0).getRoleId());
			 if((!usrs.isEmpty()) && usrs!=null){
				 relUserId= usrs.get(0).getUserId();
				 
			 }
			 rpReleaseBy= DocRole.ENDORSER;
			}
	/*		if(dm.getDocumentType().equalsIgnoreCase("CP")){
				Integer roleId=3;
				List<DocUserMaster> dumList=docUserMasterRepo.isControllerPresent(documentId, roleId);
				if(dumList!=null && dumList.size()>0)
				dum = dumList.get(0);
				if(dum!=null && rpReleaseBy!= null && rpReleaseBy.equalsIgnoreCase(DocRole.APPROVER)){
					relUserId = dum.getUserId();//approver id
				}
			}*/
			DocUserAction dua1= new DocUserAction();
			dua1.setDocumentId(documentId);
			dua1.setUserId(relUserId);
			dua1.setDocRole(rpReleaseBy);
			dua1.setActionStatus(ActionStatus.PENDING);
			dua1.setActionPendingTimeStamp(date);
			dua1.setDocActionSerialNumber(srNo);
			dua1.setFromUserAction(userId);
//			dua1.setPlantId(rp.getPlantId());
			docUserActionRepo.save(dua1);
			
	/*		List<RevenueProposal> rpList=revenueProposalRepository.findByDocumentId(documentId);
			Integer tempSrNo=srNo;
			for(RevenueProposal rp:rpList){
				tempSrNo++;
				System.out.println("test >>>>>"+tempSrNo);
				DocUserAction dua1= new DocUserAction();
				dua1.setDocumentId(documentId);
				dua1.setUserId(relUserId);
				dua1.setDocRole(rpReleaseBy);
				dua1.setActionStatus(ActionStatus.PENDING);
				dua1.setActionPendingTimeStamp(date);
				dua1.setDocActionSerialNumber(tempSrNo);
				dua1.setFromUserAction(userId);
				dua1.setPlantId(rp.getPlantId());
				docUserActionRepo.save(dua1);
			}*/
			
			
			
		}
		
		if(delegatedTo!=null && dum!=null){
			dum.setDelegateId(delegatedTo);
			docUserMasterRepo.save(dum);
		}
		
		
		if(rpRelease.equalsIgnoreCase("Enable") && !isFinal ){
			dm.setStatus(DocStatus.APPROVED);		
			dm.setCurrentlyWith(relUserId);
			logger.info("dm.setStatus saveApproved rpRelease && !isFinal"+dm.getProposalType()+" Status "+dm.getStatus()+" DocId "+dm.getDocumentId());
		}else{
			dm.setStatus(DocStatus.RELEASED);
			dm.setCurrentlyWith(null);
			logger.info("dm.setStatus saveApproved else rpRelease && !isFinal"+dm.getProposalType()+" Status "+dm.getStatus()+" DocID "+dm.getDocumentId());
		}
		
		
		
		dm.setIsAmendment("NO");
		documentMasterRepository.save(dm);
		logger.info("dm.setStatus saveApproved"+dm.getStatus());
		return userPendingAction;
		
	}

	/* (non-Javadoc)
	 * @see com.gt.services.DocUserActionService1#saveRejectedData(java.lang.Integer, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public DocUserAction saveRejectedData(Integer documentId, Integer userId ,Integer delegatedByInt){
		DocumentMaster dm = getDocumentMasterByDocumentId(documentId);
		List<DocUserAction> docUsers = docUserActionRepo.findByDocumentIdAndActionStatus (documentId, ActionStatus.PENDING);
		DocUserMaster dum = null;
		DocUserAction userPendingAction = null;
		Integer delegatedTo =null;
		if(delegatedByInt!=null && delegatedByInt!=-1 && delegatedByInt!=userId){
			dum =docUserMasterRepo.findByDocumentIdAndUserId(documentId, delegatedByInt);
			delegatedTo = userId;
			userId = delegatedByInt;
		}
		
		for(DocUserAction docUserAction : docUsers){
			if (userId.equals(docUserAction.getUserId())){
				userPendingAction = docUserAction;
				break;
			}
		}
		Date date = new Date();
		userPendingAction.getDocActionSerialNumber();
		userPendingAction.setActionTakenTimeStamp(date);
		userPendingAction.setActionStatus(ActionStatus.REJECTED);
		if(delegatedTo!=null){
			userPendingAction.setActionTakenBy(delegatedTo);
		}
		docUserActionRepo.save(userPendingAction);
		
		List<DocUserAction> docUsersParallel = docUserActionRepo.findByDocumentIdAndActionStatus (documentId, ActionStatus.PENDING_PARALLEL);
		if(docUsersParallel.size()!=0){
			for(DocUserAction dua:docUsersParallel){
				dua.setActionStatus(ActionStatus.PENDING_INVALID);
				dua.setIsDraft("No");
				docUserActionRepo.save(dua);
				System.out.println("docUserAction.getIsDraft() dua for Draft data DUA>>>"+dua.getIsDraft());
				logger.info("docUserAction.getIsDraft() dua for actionID>>>>>"+dua.getActionId()+" Draft data DUA>>>"+dua.getIsDraft());
				
			}			
		}
		
		if(dum!= null){
			dum.setDelegateId(delegatedTo);
			docUserMasterRepo.save(dum);
		}
		dm.setStatus(DocStatus.REJECTED);
		dm.setCurrentlyWith(null);
		dm.setIsAmendment("NO");
		documentMasterRepository.save(dm);
		
		return userPendingAction;
		
	}

	/* (non-Javadoc)
	 * @see com.gt.services.DocUserActionService1#saveReferedBackData(java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public DocUserAction saveReferedBackData(Integer userId,Integer delegatedBy,Integer documentId,Integer referedTo){
		
		
		List<DocUserAction> docUsers = docUserActionRepo.findByDocumentIdAndActionStatus (documentId, ActionStatus.PENDING);
		DocUserMaster dum1 =null;
		DocUserAction dua = docUsers.get(0);
		Date date = new Date();
		int srNo=dua.getDocActionSerialNumber();
		Integer delegatedTo= null;
		if(delegatedBy!=null && delegatedBy!= -1 &&(delegatedBy!=userId)){
			delegatedTo=userId;
			dum1 = docUserMasterRepo.findByDocumentIdAndUserId(documentId, delegatedBy);
		}

			dua.setActionTakenTimeStamp(date);
			dua.setActionStatus(ActionStatus.REFERRED);
			if(delegatedTo!=null){
				dua.setActionTakenBy(delegatedTo);
			}
			docUserActionRepo.save(dua);
				
			DocUserMaster dum = docUserMasterRepo.findByDocumentIdAndUserId(documentId, referedTo);
			DocUserAction dua1= new DocUserAction();
			dua1.setUserId(referedTo);
			dua1.setDocumentId(documentId);
			dua1.setActionStatus(ActionStatus.PENDING);
			dua1.setActionPendingTimeStamp(date);
			dua1.setDocActionSerialNumber(srNo+1);
			if(delegatedTo!=null){
				dua1.setFromUserAction(delegatedBy);
			}else{
				dua1.setFromUserAction(userId);
			}
			dua1.setDocRole(dum.getDocRole());
			docUserActionRepo.save(dua1);
			
			if(delegatedTo!=null && dum1!=null){
				dum1.setDelegateId(delegatedTo);
				docUserMasterRepo.save(dum1);
			}
			
			
			DocumentMaster dm = documentMasterRepository.findByDocumentId(documentId);
			dm.setCurrentlyWith(referedTo);
			dm.setStatus(DocStatus.IN_PROCESS);
			documentMasterRepository.save(dm);


		return dua;
		
	}

	/* (non-Javadoc)
	 * @see com.gt.services.DocUserActionService1#saveReply(java.lang.Integer, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public DocUserAction saveReply(Integer userId,Integer delegatedBy, Integer documentId,String isReferredToInitiator){
		
		List<DocUserAction> docUsers = docUserActionRepo.findByDocumentIdAndActionStatus (documentId, ActionStatus.PENDING);
		DocUserAction dua = docUsers.get(0);
		Date date = new Date();
		int srNo=dua.getDocActionSerialNumber();
		Integer delegatedTo =null;
		
		if(delegatedBy!=null && delegatedBy!=-1 && delegatedBy!=0){
			delegatedTo =userId;
		}

			int replyTo=dua.getFromUserAction();
			
			System.out.println("replyTo >>>>"+replyTo);
			//DocUserMaster tempDum = docUserMasterRepo.findByDocumentIdAndUserId(documentId, replyTo);
			
			dua.setActionTakenTimeStamp(date);
			dua.setActionStatus(ActionStatus.REPLIED);
			if(delegatedTo!=null){
				dua.setActionTakenBy(delegatedTo);
			}
			docUserActionRepo.save(dua);
//			LOGGER.debug(message);
			boolean delegated=false;
			DocUserMaster dum = docUserMasterRepo.findByDocumentIdAndUserId(documentId, replyTo);	
			List<Delegation> delegationList=delegateRepo.findByUserIdAndIsActive(delegatedBy, true);
			System.out.println("delegated >>>>"+delegated);
			for(Delegation del :delegationList){
				Date fromDate =del.getFromDate();
				Date toDate = del.getToDate();
				Date currentDate =new Date();
				
				boolean getMin = (currentDate.compareTo(toDate)<=0 &&currentDate.compareTo(fromDate)>=0);
				if(getMin){
					if(del.getIsActive()){
					delegated =true;
					/*item = new Items();
					item.setIsDelegated(delegated);
					items.add(item);*/
					break;
					}
				}
			}
			System.out.println("delegated >>>>"+delegated);
			System.out.println("userId >>>>"+userId);
			DocUserAction dua1= new DocUserAction();
			dua1.setUserId(replyTo);
			dua1.setDocumentId(documentId);
			dua1.setActionStatus(ActionStatus.PENDING);
			dua1.setActionPendingTimeStamp(date);
			dua1.setDocActionSerialNumber(srNo+1);
			if(delegated){
				dua1.setFromUserAction(delegatedBy);
			}else{
				dua1.setFromUserAction(userId);
			}
			
			dua1.setDocRole(dum.getDocRole());
			docUserActionRepo.save(dua1);
			
			DocUserMaster dum1 = docUserMasterRepo.findByDocumentIdAndUserId(documentId, delegatedBy);
			if(delegatedTo!=null){
				dum1.setDelegateId(delegatedTo);
				docUserMasterRepo.save(dum1);
			}
			
			
			DocumentMaster dm = documentMasterRepository.findByDocumentId(documentId);
			dm.setCurrentlyWith(replyTo);
			dm.setStatus(DocStatus.IN_PROCESS);
			documentMasterRepository.save(dm);


		return dua;
		
	}


	@Override
	public DocAttachment saveDocAttachment(DocAttachment docAttachment,File file) throws Exception {
		
		Session session = sessionFactory.openSession();
		FileInputStream inputStream;
		Blob blob = null;
		try {
			inputStream = new FileInputStream(file);
			blob = Hibernate.getLobCreator(session).createBlob(inputStream, file.length());
			System.out.println("doc attach imp ");
			System.out.println("filename "+docAttachment.getFileName());
			docAttachment.setContent(blob);
			//blob.free();
			return docAttachmentRepository.save(docAttachment);
		} catch (Exception e) {
			
			System.out.println("Exception");
			e.printStackTrace();
		}finally{
			if(blob!=null){
				blob.free();
			}
			if(file!=null){
				file.delete();
			}
			
			if(session!=null){
				session.close();
			}
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see com.gt.services.DocUserActionService1#saveComments(com.gt.entity.Comments, java.lang.String)
	 */
	@Override
	public Comments saveComments(Comments comments,String safeHtml) {
		
		Comments comment = CommentRepo.findByActionId(comments.getActionId());
		if(comment!=null){
			comments=comment;
		}

		Session session = sessionFactory.openSession();
		
		/*
		 * Fix for Issue 403
		 */
			
		/*if(safeHtml!=null && !safeHtml.isEmpty()){
			safeHtml=safeHtml.replaceAll("'", "");
		}*/
		 
		Blob blob = Hibernate.getLobCreator(session).createBlob(safeHtml.getBytes());
		comments.setComment(blob);
		Comments tempComments = CommentRepo.save(comments);

		session.close();
		return tempComments;
	}
	
	@Override
	public void saveCancelledData(Integer userId,Integer documentId) {
		//DocUserAction dua = null;
		List<DocUserAction> duaPending = docUserActionRepo.findByDocumentIdAndActionStatus(documentId, ActionStatus.PENDING);
		List<DocUserAction> duaDraft = docUserActionRepo.findByDocumentIdAndActionStatus(documentId, ActionStatus.DRAFT);
		List<DocUserAction> duaPendingExpress = docUserActionRepo.findByDocumentIdAndActionStatus(documentId, ActionStatus.PENDING_EXPRESS);
		List<DocUserAction> duaPendingParallel = docUserActionRepo.findByDocumentIdAndActionStatus(documentId, ActionStatus.PENDING_PARALLEL);
		DocumentMaster dm = documentMasterRepository.findByDocumentId(documentId);
		
		Date date = new Date();
		
		if(duaPending!=null && duaPending.size()>0){
			for(DocUserAction dua1:duaPending){
				
					dua1.setActionStatus(ActionStatus.CANCELLED);
					dua1.setActionTakenTimeStamp(date);
					dua1.setIsDraft("No");
					docUserActionRepo.save(dua1);
					System.out.println("docUserAction.getIsDraft() cancel data DUA>>>"+dua1.getIsDraft());
					logger.info("docUserAction.getIsDraft() actionId>>>>"+dua1.getActionId()+"cancel data DUA>>>"+dua1.getIsDraft());
				
			}
		}
		if(duaDraft!=null && duaDraft.size()>0){
			for(DocUserAction dua1:duaDraft){
				
					dua1.setActionStatus(ActionStatus.CANCELLED);
					dua1.setActionTakenTimeStamp(date);
					dua1.setIsDraft("No");
					docUserActionRepo.save(dua1);
				
			}
		}
		if(duaPendingParallel!=null && duaPendingParallel.size()>0){
			for(DocUserAction dua1:duaPendingParallel){
				
					dua1.setActionStatus(ActionStatus.CANCELLED);
					dua1.setActionTakenTimeStamp(date);
					dua1.setIsDraft("No");
					docUserActionRepo.save(dua1);
				
			}
		}
		if(duaPendingExpress!=null && duaPendingExpress.size()>0){
			for(DocUserAction dua1:duaPendingExpress){
				
					dua1.setActionStatus(ActionStatus.CANCELLED);
					dua1.setActionTakenTimeStamp(date);
					dua1.setIsDraft("No");
					docUserActionRepo.save(dua1);
				
			}
		}
		
		
		
		dm.setCurrentlyWith(null);
		dm.setStatus(DocStatus.CANCELLED);
		dm.setModifiedTimeStamp(date);
		dm.setModifiedBy(userId);
		documentMasterRepository.save(dm);
		
	}

	@Override
	public	DocUserAction saveDocUserAction(Integer documentId, String isRPExpress,Integer actionId){
		DocumentMaster dm = getDocumentMasterByDocumentId(documentId);
		DocUserAction dua= new DocUserAction();
		Integer srNo=1;
		Date date = new Date();
		try{
		dua.setDocumentId(documentId);
	
		List<DocUserMaster> dums = docUserMasterRepo.findByDocumentIdOrderBySerialNumberAsc(documentId);
		List<DocUserMaster> docUserMasters = docUserMasterRepo.findBySerialNumberAndDocumentId(null, documentId);
		Integer initiatorSrNo= docUserMasters.size();
		dua.setUserId(dums.get(initiatorSrNo).getUserId());
		dua.setDocRole(dums.get(initiatorSrNo).getDocRole());
		dua.setActionStatus(ActionStatus.INITIATED);
////		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		dua.setActionTakenTimeStamp(date);
		dua.setActionPendingTimeStamp(date);
		if(actionId!=null && actionId>0){
		dua.setActionId(actionId);
		}
		
		dua.setDocActionSerialNumber(srNo);
		dua.setFromUserAction(dums.get(initiatorSrNo).getUserId());
		
		docUserActionRepo.save(dua);
		srNo+=1;
		if("true".equals(isRPExpress)){
			int controllerId=0;
			DocUserAction dua1= new DocUserAction();
			dua1.setDocumentId(documentId);
			dua1.setUserId(dums.get(initiatorSrNo+1).getUserId());
			dua1.setDocRole(dums.get(initiatorSrNo+1).getDocRole());
			dua1.setActionStatus(ActionStatus.PENDING);
			dua1.setActionPendingTimeStamp(date);
			dua1.setDocActionSerialNumber(srNo);
			dua1.setFromUserAction(dums.get(initiatorSrNo).getUserId());
			docUserActionRepo.save(dua1);
			dm.setStatus(DocStatus.IN_PROCESS);
			dm.setCurrentlyWith(dums.get(initiatorSrNo+1).getUserId());
			documentMasterRepository.save(dm);
			
			for(DocUserMaster dum : dums){
				if(dum.getDocRole().equals(DocRole.CONTROLLER)){
					controllerId = dum.getUserId();
				}
			}
			if(controllerId!=0){
			dua1= new DocUserAction();
			dua1.setDocumentId(documentId);
			dua1.setUserId(controllerId);
			dua1.setDocRole(DocRole.CONTROLLER);
			//dua1.setActionStatus(ActionStatus.PENDING_EXPRESS);
			if(dums.get(initiatorSrNo+1).getUserId() == controllerId){
				dua1.setActionStatus(ActionStatus.PENDING_INVALID);	
			}else{
			dua1.setActionStatus(ActionStatus.PENDING_EXPRESS);
			}
			dua1.setActionPendingTimeStamp(date);
			dua1.setDocActionSerialNumber(srNo+1);
			dua1.setFromUserAction(dums.get(initiatorSrNo).getUserId());
			docUserActionRepo.save(dua1);
			}
			
		}else{		
			if(docUserMasters.size()!=0){
				for(DocUserMaster dum:docUserMasters){
					DocUserAction dua1= new DocUserAction();
					dua1.setDocumentId(documentId);
					dua1.setUserId(dum.getUserId());
					dua1.setDocRole(dum.getDocRole());
					dua1.setActionStatus(ActionStatus.PENDING_PARALLEL);
					dua1.setActionPendingTimeStamp(date);
					dua1.setDocActionSerialNumber(srNo);
					dua1.setFromUserAction(dums.get(initiatorSrNo).getUserId());
					docUserActionRepo.save(dua1);
					srNo++;
				}
				DocUserAction dua1= new DocUserAction();
				dua1.setDocumentId(documentId);
				dua1.setUserId(dums.get(initiatorSrNo).getUserId());
				dua1.setDocRole(dums.get(initiatorSrNo).getDocRole());
				dua1.setActionStatus(ActionStatus.PENDING);
				dua1.setActionPendingTimeStamp(date);
				dua1.setDocActionSerialNumber(srNo);
				dua1.setFromUserAction(dums.get(initiatorSrNo).getUserId());
				docUserActionRepo.save(dua1);
				
				dm.setStatus(DocStatus.IN_PROCESS_PARALLEL);
				dm.setCurrentlyWith(dums.get(initiatorSrNo).getUserId());
				documentMasterRepository.save(dm);
			}else{
				DocUserAction dua1= new DocUserAction();
				dua1.setDocumentId(documentId);
				dua1.setUserId(dums.get(initiatorSrNo+1).getUserId());
				dua1.setDocRole(dums.get(initiatorSrNo+1).getDocRole());
				dua1.setActionStatus(ActionStatus.PENDING);
				dua1.setActionPendingTimeStamp(date);
				dua1.setDocActionSerialNumber(srNo);
				dua1.setFromUserAction(dums.get(initiatorSrNo).getUserId());
				docUserActionRepo.save(dua1);
				dm.setStatus(DocStatus.IN_PROCESS);
				dm.setCurrentlyWith(dums.get(initiatorSrNo+1).getUserId());
				documentMasterRepository.save(dm);
			}
		}
		
		return dua;
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return dua;
	}


	List<DocUserAction> getDocUserActionByDocumentIdAndActionStatus(Integer documentId,String actionStatus){	
		return   docUserActionRepo.findByDocumentIdAndActionStatus(documentId,actionStatus);
	}
	
	/* (non-Javadoc)
	 * @see com.gt.services.DocUserActionService1#saveDocUserActionView(java.lang.Integer, java.lang.Integer, java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@Override
	public DocUserAction saveDocUserActionView(Integer documentId ,Integer userId, String parallelFinished, String parallelStarted, Integer delegatedByUserId){
		Integer nextSerialNumber= null;
		Integer nextUserId=null;
		String docRole=null;
		String docRoleParllel=null;
		DocUserAction dua;
		DocumentMaster dm = getDocumentMasterByDocumentId(documentId);
		Integer delegatedToUserId=userId;
		if(delegatedByUserId!= null && delegatedByUserId != -1){
			userId=delegatedByUserId;
		}
			
		DocUserMaster dum1 = docUserMasterRepo.findByDocumentIdAndUserId(documentId,userId);
		List<DocUserMaster> dums = docUserMasterRepo.findByDocumentIdAndDocRoleNotIn(documentId, WFUtil.getDocRoles());	
		Collections.sort(dums, new Comparator<DocUserMaster>() {

			@Override
			public int compare(DocUserMaster dum1, DocUserMaster dum2) {
				Integer srNo1 = dum1.getSerialNumber();
				Integer srNo2 = dum2.getSerialNumber();
				if(srNo1 == null){
					srNo1 =-1;
				}
				if(srNo2 == null){
					srNo2 =-1;
				}
				if(srNo1 != null && srNo2!= null ){
					if (srNo1 > srNo2)
					return 1;
				else if (srNo1 < srNo2)
					return -1;
				}
				//}
				return 0;
			}
	    });
		boolean foundCurrentUserId = false;
		for(DocUserMaster dum : dums){
			if(foundCurrentUserId){
				nextSerialNumber = dum.getSerialNumber();
				nextUserId=dum.getUserId();
				docRole=dum.getDocRole();
				break;
			} else if(dum.getUserId() == userId)
				foundCurrentUserId = true;
				docRoleParllel=dum.getDocRole();
				
		}
		
		logger.info("<<<< nextUserId >>>>"+nextUserId);
		
		List<DocUserAction> docUsers = docUserActionRepo.findByDocumentIdAndActionStatus (documentId, ActionStatus.PENDING);
		List<DocUserAction> parallelDocUsers = docUserActionRepo.findByDocumentIdAndActionStatus (documentId, ActionStatus.PENDING_PARALLEL);
		List<DocUserMaster> docUserMasters = docUserMasterRepo.findBySerialNumberAndDocumentId(null, documentId);
		DocUserAction userPendingAction = null;
		Date date = new Date();
		boolean isAdvisoryInParallel=false;
		for(DocUserAction docUserAction : docUsers){
			if (userId.equals(docUserAction.getUserId())){
				userPendingAction = docUserAction;
				break;
			}
		}
		DocUserAction userPendingParallelAction = null;
		for(DocUserAction docUserAction : parallelDocUsers){
			if (userId.equals(docUserAction.getUserId())){
				userPendingParallelAction = docUserAction;
				isAdvisoryInParallel=true;
				break;
			}
		}
		if(isAdvisoryInParallel){
			userPendingParallelAction.setActionTakenTimeStamp(date);
			userPendingParallelAction.setActionStatus(ActionStatus.ENDORSED);
			if(delegatedByUserId != -1){
				userPendingAction.setActionTakenBy(delegatedToUserId);
			}
			docUserActionRepo.save(userPendingParallelAction);
			return userPendingParallelAction;
		}else{
			if((parallelFinished == null && parallelStarted.equals("false")) || ("true".equalsIgnoreCase(parallelFinished)) ){
				int srNo=userPendingAction.getDocActionSerialNumber();		
				userPendingAction.setActionTakenTimeStamp(date);
				userPendingAction.setActionStatus(ActionStatus.ENDORSED);
				if(delegatedByUserId!=null && delegatedByUserId != -1){
					userPendingAction.setActionTakenBy(delegatedToUserId);
				}
				docUserActionRepo.save(userPendingAction);
				List<DocUserAction> docUserActions = getDocUserActionByDocumentIdAndActionStatus(documentId, ActionStatus.PENDING_EXPRESS);
				
					if(docUserActions.size()!=0 && nextUserId.equals(docUserActions.get(0).getUserId())){
						docUserActions.get(0).setActionStatus(ActionStatus.PENDING_INVALID);
						docUserActionRepo.save(docUserActions.get(0));
					}
				System.out.println("<<<< nextUserId >>>>"+nextUserId);
				dua= new DocUserAction();
				dua.setDocumentId(documentId);
				dua.setUserId(nextUserId);
				dua.setDocActionSerialNumber(srNo+1);
				dua.setDocRole(docRole);
				dua.setActionStatus(ActionStatus.PENDING);
				dua.setActionPendingTimeStamp(date);
				dua.setFromUserAction(userId);
				docUserActionRepo.save(dua);
				
				dm.setStatus(DocStatus.IN_PROCESS);
				dm.setCurrentlyWith(nextUserId);
				documentMasterRepository.save(dm);
			}else{
				int srNo=userPendingAction.getDocActionSerialNumber();	
				userPendingAction.setActionTakenTimeStamp(date);
				userPendingAction.setActionStatus(ActionStatus.ENDORSED_PARALLEL);
				if(delegatedByUserId != -1){
					userPendingAction.setActionTakenBy(delegatedToUserId);
				}
				docUserActionRepo.save(userPendingAction);
				for(DocUserMaster dum:docUserMasters){
					DocUserAction dua1= new DocUserAction();
					dua1.setDocumentId(documentId);
					dua1.setUserId(dum.getUserId());
					dua1.setDocRole(dum.getDocRole());
					dua1.setActionStatus(ActionStatus.PENDING_PARALLEL);
					dua1.setActionPendingTimeStamp(date);
					dua1.setDocActionSerialNumber(srNo);
					dua1.setFromUserAction(userId);
					docUserActionRepo.save(dua1);
					srNo++;
				}
				DocUserAction dua1= new DocUserAction();
				dua1.setDocumentId(documentId);
				dua1.setUserId(userId);
				dua1.setDocRole(docRoleParllel);
				dua1.setActionStatus(ActionStatus.PENDING);
				dua1.setActionPendingTimeStamp(date);
				dua1.setDocActionSerialNumber(srNo);
				dua1.setFromUserAction(userId);
				docUserActionRepo.save(dua1);
				
				dm.setStatus(DocStatus.IN_PROCESS_PARALLEL);
				dm.setCurrentlyWith(userId);
				documentMasterRepository.save(dm);
			}
		}
		if(delegatedByUserId!= null && delegatedByUserId != -1){
			dum1.setDelegateId(delegatedToUserId);
			docUserMasterRepo.save(dum1);
		}
		return userPendingAction;
	}

	@Override
	public DocUserAction saveReleaseData(Integer userId,Integer delegatedBy,
			Integer documentId) {
		DocUserAction dua = null;
		List<DocUserAction> duas = docUserActionRepo.findByDocumentIdAndActionStatus(documentId, ActionStatus.PENDING);
		DocumentMaster dm = documentMasterRepository.findByDocumentId(documentId);
		DocUserMaster dum= null;
		Integer delegatedTo =null;
		if(delegatedBy!=null && delegatedBy!=-1 && (delegatedBy!=userId)){
			delegatedTo= userId;
			userId = delegatedBy;
			dum= docUserMasterRepo.findByDocumentIdAndUserId(documentId, delegatedBy);
		}
		Date date = new Date();
		Integer releaseSerialNumber = 0;
		for(DocUserAction dua1:duas){
			if(dua1.getUserId().equals(userId)){
				dua1.setActionStatus(ActionStatus.RELEASED);
				dua1.setActionTakenTimeStamp(date);
				if(delegatedBy!=null && delegatedBy!=-1){
					dua1.setActionTakenBy(delegatedTo);
				}
				releaseSerialNumber=dua1.getDocActionSerialNumber();
				dua=dua1;
				docUserActionRepo.save(dua1);
			}
		}
		Integer initiatorId=docUserMasterRepo.findByDocumentIdAndDocRole(documentId, DocRole.INITIATOR).getUserId();
		if(dm.getProposalType()!=null && dm.getProposalType().equalsIgnoreCase("In-Principle")){
			DocUserAction duaPendingAction = new DocUserAction();
			duaPendingAction.setDocumentId(documentId);
			duaPendingAction.setUserId(initiatorId);
			duaPendingAction.setDocRole(DocRole.INITIATOR);
			duaPendingAction.setActionStatus(ActionStatus.PENDING);
			duaPendingAction.setActionPendingTimeStamp(date);
			duaPendingAction.setDocActionSerialNumber(releaseSerialNumber+1);
			duaPendingAction.setFromUserAction(userId);
			docUserActionRepo.save(duaPendingAction);
			
//			dm.setProposalType("Final");
		}
		
		
	
		if(delegatedTo!=null && dum != null){
			dum.setDelegateId(delegatedTo);
			docUserMasterRepo.save(dum);
		}
		
		dm.setCurrentlyWith(null);
		dm.setStatus(DocStatus.RELEASED);
		dm.setIsAmendment("NO");
		documentMasterRepository.save(dm);
		System.out.println("dm.Status() save Released>>>"+dm.getStatus());
		logger.info("dm.Status() save Released>>>"+dm.getStatus());
		
		
		return dua;
	}
	
	private List<DocAttachment> findByCommentId(int commentId) {
		
		return docAttachmentRepository.findByCommentId(commentId);
	}
	
	/* (non-Javadoc)
	 * @see com.gt.services.DocUserActionService1#getDocumentUserAction(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public ArrayList<DocumentUserActionBO> getDocumentUserAction( Integer documentId,Integer userId) throws Exception {
		ArrayList<DocumentUserActionBO> documentUserActions =new ArrayList<DocumentUserActionBO>();
		DocumentUserActionBO documentUserAction = null;
		DocumentMaster dm =getDocumentMasterByDocumentId(documentId);  
		ArrayList<DocUserAction> docUserActions =(ArrayList<DocUserAction>) docUserActionRepo.findByDocumentId(documentId);
		Collections.sort(docUserActions, new Comparator<DocUserAction>() {
		          
			@Override
			public int compare(DocUserAction dua1, DocUserAction dua2) {
				Integer srNo1 = dua1.getDocActionSerialNumber();
				Integer srNo2 = dua2.getDocActionSerialNumber();
				
				if(srNo1 != null && srNo2!= null ){
					if (srNo1 > srNo2)
						return 1;
					else if (srNo1 < srNo2)
						return -1;
				}
				
				
				return 0;
			}
		});
		for(DocUserAction docUserAction:docUserActions){

			System.out.println("docUserAction.getUserId()"+docUserAction.getUserId());
			Users user =userrepo.findByUserId(docUserAction.getUserId());
			
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss a");
			
			
			if(docUserAction.getActionTakenTimeStamp()!=null){
						String dateStr=dateFormat.format(docUserAction.getActionTakenTimeStamp());
					
					int actionId = docUserAction.getActionId();
					System.out.println("actionId"+actionId);
					Comments comment =  CommentRepo.findByActionId(actionId);
		
					if(comment != null && comment.getComment() != null){
						Blob blob =  comment.getComment();
						
						byte[] blobByte = blob.getBytes(1, (int) blob.length());
						String comments = new String(blobByte);
						documentUserAction = new DocumentUserActionBO();
						documentUserAction.setDocRole(docUserAction.getDocRole());
						documentUserAction.setPersonName(user.getFirstName()+" "+user.getLastName());
						documentUserAction.setComment(comments.replaceAll("₹", " Rs "));
						documentUserAction.setDateStr(dateStr);
						documentUserAction.setUserId(user.getUserId());
						documentUserAction.setDocStatus(docUserAction.getActionStatus());//Initiated,Endorser,Controller,
						documentUserAction.setIsParallel(dm.getStatus());
						documentUserAction.setAction(false);
//						System.out.println("docUserAction.getUserId() >>>>>"+docUserAction.getUserId() +"userId >>>"+userId+"dm.getDeptId() "+dm.getDeptId());
						if(docUserAction.getUserId().longValue()== userId.longValue() && dm.getDeptId()!= null  && dm.getDeptId().longValue()>0 && dm.getDeptId().longValue()!=2){
//							System.out.println("docUserAction.getUserId() dasd>>>>>"+docUserAction.getUserId() +"userId >>>"+userId+"dm.getDeptId() "+dm.getDeptId());
							List<DocUserAction> docuaList=docUserActionRepo.findByDocRoleAndDocumentIdAndActionStatus(DocRole.INITIATOR, dm.getDocumentId(), ActionStatus.ENDORSED);
							if(docuaList!=null && docuaList.size()>0){
								if(docUserActions.get(docUserActions.size()-2).getActionStatus().equalsIgnoreCase(ActionStatus.REFERRED)){
									documentUserAction.setIsRefferedToInitiator(false);	
								}else{
								documentUserAction.setIsRefferedToInitiator(true);	
								}
							}else{
								documentUserAction.setIsRefferedToInitiator(false);	
							}
								
							}else if(docUserAction.getUserId().longValue()== userId.longValue() && dm.getDeptId()!= null  && dm.getDeptId().longValue()==2l){
//								System.out.println("docUserAction.getUserId() vic>>>>>"+docUserAction.getUserId() +"userId >>>"+userId+"dm.getDeptId() "+dm.getDeptId());
								List<DocUserAction> docuacList=docUserActionRepo.findByDocRoleAndDocumentIdAndActionStatus(DocRole.INITIATOR, dm.getDocumentId(), ActionStatus.INITIATED);
								System.out.println("docuacList >>>>>"+docuacList.size());
								if(docuacList!=null && docuacList.size()>1){
									List<DocUserAction> docusacList=docUserActionRepo.findByDocRoleAndDocumentIdAndActionStatus(DocRole.INITIATOR, dm.getDocumentId(), ActionStatus.PENDING);
//									System.out.println("docusacList >>>>>"+docusacList.size());
									if(docusacList!=null && docusacList.size()>0){
										documentUserAction.setIsRefferedToInitiator(true);	
									}else{
										documentUserAction.setIsRefferedToInitiator(false);	
									}
									}else{
										documentUserAction.setIsRefferedToInitiator(false);	
									}
							}else{
//								System.out.println("docUserAction.getUserId() pac>>>>>"+docUserAction.getUserId() +"userId >>>"+userId+"dm.getDeptId() "+dm.getDeptId());
								documentUserAction.setIsRefferedToInitiator(false);
							}
						if(docUserAction.getActionTakenBy()!=null){
							documentUserAction.setActionTakenBy(docUserAction.getActionTakenBy());
							Users usr =userrepo.findByUserId(docUserAction.getActionTakenBy());
							documentUserAction.setActionTakenByName(usr.getFirstName()+" "+usr.getLastName());
						}else{
							documentUserAction.setActionTakenBy(0);
//							Users usr =userrepo.findByUserId(docUserAction.getActionTakenBy());
							documentUserAction.setActionTakenByName(" ");
						}
						
						String activeStatus=docUserAction.getActionStatus();
						String rpStatus="RP Created On";
		
						if(ActionStatus.INITIATED.equalsIgnoreCase(activeStatus)){
						rpStatus="RP Created On";

						}else if(ActionStatus.ENDORSED.equalsIgnoreCase(activeStatus)){
						rpStatus="Endorsed On";
						}else if(ActionStatus.APPROVED.equalsIgnoreCase(activeStatus)){
						rpStatus="Approved On";
						}else if(ActionStatus.REJECTED.equalsIgnoreCase(activeStatus)){
						rpStatus="Rejected On";
						}else if(ActionStatus.REFERRED.equalsIgnoreCase(activeStatus)){
							rpStatus="Referred On";
							Integer referredActionSerialNo= docUserAction.getDocActionSerialNumber();
							if(referredActionSerialNo!=null && dm.getDeptId()!=null && dm.getDeptId()==1){
								referredActionSerialNo=referredActionSerialNo+1;
								for(DocUserAction dua:docUserActions){
									if(dua.getDocActionSerialNumber()!=null && dua.getDocActionSerialNumber()==referredActionSerialNo){
										Users usr = userrepo.findByUserId(dua.getUserId());
										documentUserAction.setReferredTo(usr.getFirstName()+" "+usr.getLastName());
										System.out.println(usr.getFirstName()+" "+usr.getLastName());
									}
								}
								
							}
						}else if(ActionStatus.REPLIED.equalsIgnoreCase(activeStatus)){
							rpStatus="Replied On";
						}else if(ActionStatus.RELEASED.equalsIgnoreCase(activeStatus)){
							rpStatus="Released On";
						}else if(ActionStatus.DRAFT.equalsIgnoreCase(activeStatus)){
							rpStatus="RP Draft Created On";
							if(docUserActions!=null && docUserActions.size()==1 && dm.getStatus().equalsIgnoreCase(ActionStatus.CANCELLED) ){
								rpStatus="RP Cancelled On";
						}
						}else if(ActionStatus.CANCELLED.equalsIgnoreCase(activeStatus)){
							rpStatus="RP Cancelled On";
						}else {
							rpStatus="Endorsed On";
							}
						
						documentUserAction.setActionId(comment.getActionId());
						documentUserAction.setRpStatus(rpStatus);
						List<DocAttachment> docAttachmentList=findByCommentId(comment.getComment_id());
						List<DocAttachmentBO> docAttachmentBOList=new ArrayList<DocAttachmentBO>();
						if(docAttachmentList!=null && docAttachmentList.size()>0){
							for(DocAttachment docAttachment : docAttachmentList){
							int attachmentId=docAttachment.getpAttachmentId();
							String fileName=docAttachment.getFileName();
							DocAttachmentBO docAttachmentBO = new DocAttachmentBO();
							docAttachmentBO.setAttachmentId(attachmentId);
							docAttachmentBO.setFileName(fileName);
							docAttachmentBOList.add(docAttachmentBO);
							}
						}/*else{
							DocAttachmentBO docAttachmentBO = new DocAttachmentBO();
							docAttachmentBOList.add(docAttachmentBO);
							
						}*/
						documentUserAction.setDocAttachmentBOList(docAttachmentBOList);
					
						documentUserActions.add(documentUserAction);
						
						
					}
					
			}else if(docUserAction.getActionTakenTimeStamp()==null && ActionStatus.PENDING_PARALLEL.equalsIgnoreCase(docUserAction.getActionStatus()) && !docUserAction.getUserId().equals(userId)){
				documentUserAction = new DocumentUserActionBO();
				documentUserAction.setDocRole(docUserAction.getDocRole());
				documentUserAction.setPersonName(user.getFirstName()+" "+user.getLastName());
				documentUserAction.setComment("Pending endorsement");
				documentUserAction.setUserId(user.getUserId());
				documentUserAction.setRpStatus("To be Endorsed");
				documentUserAction.setAction(true);
				documentUserAction.setActionTakenBy(0);
				documentUserAction.setActionId(0);
				documentUserAction.setIsRefferedToInitiator(false);
//				Users usr =userrepo.findByUserId(docUserAction.getActionTakenBy());
				documentUserAction.setActionTakenByName(" ");
//				documentUserAction.setDocStatus(docUserAction.getActionStatus());
				documentUserActions.add(documentUserAction);
			}
			

		}
		
		return documentUserActions;
	}

	/* (non-Javadoc)
	 * @see com.gt.services.DocUserActionService1#getDocumentUserActionCurrentUser(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public ArrayList<DocumentUserActionBO> getDocumentUserActionCurrentUser(
			Integer documentId,Integer userId) {
		ArrayList<DocUserAction> docUserActions =(ArrayList<DocUserAction>) docUserActionRepo.findByDocumentId(documentId);	
		DocUserMaster dum = docUserMasterRepo.findByDocumentIdAndUserId(documentId, userId);
		ArrayList<DocumentUserActionBO> documentUserActions =new ArrayList<DocumentUserActionBO>();
		DocumentUserActionBO documentUserAction;
		for(DocUserAction doc:docUserActions){
			String status= doc.getActionStatus();
			Integer CurrentUserId=doc.getUserId();
			System.out.println("CurrentUserId"+CurrentUserId);
			System.out.println("userId"+userId);
			System.out.println("status"+status);
			
			if((CurrentUserId!=null && userId!=null) && (CurrentUserId.intValue()== userId.intValue()) 
					&&(status.equals(ActionStatus.PENDING)||status.equals(ActionStatus.PENDING_PARALLEL) ||status.equals(ActionStatus.RELEASED))){	
				documentUserAction=new DocumentUserActionBO();
				Users user =userrepo.findByUserId(doc.getUserId());
				ArrayList<UserRoles> userrRole=userRoleRepo.findByUserId(user.getUserId());
//				System.out.println("user.getUserId() >>>>"+user.getUserId());
				String roleName=rolesRepo.findByRoleId(userrRole.get(0).getRoleId()).getRoleName();
//			System.out.println("userrRole.get(0).getRoleId() >>>>"+userrRole.get(0).getRoleId());
//				System.out.println("roleName >>>>"+roleName);
//				System.out.println("roleName >>>>"+roleName.indexOf("SE-Mining & Minerals-Controller"));
				DocUserMaster advisoryLink = docUserMasterRepo.findByDocumentIdAndDocRole(documentId, DocRole.CONTROLLER);
//				System.out.println("advisoryLink >>>>"+advisoryLink);
//				System.out.println("advisoryLink.getDocumentId() >>>>"+advisoryLink.getDocumentId());
				if(roleName!=null && roleName.indexOf("SE-Mining & Minerals-Controller")!=-1 && advisoryLink!=null && advisoryLink.getDocumentId() >0
						&& advisoryLink.getUserId()!=user.getUserId()){
					documentUserAction.setEnableLink(true);	
				}else{
					documentUserAction.setEnableLink(false);	
				}
				documentUserAction.setUserId(userId);
				documentUserAction.setDocRole(doc.getDocRole());
				documentUserAction.setPersonName(user.getFirstName()+" "+user.getLastName());				
				int seqNum = dum.getSerialNumber() == null ? 0 : dum.getSerialNumber();
				documentUserAction.setDocSequence(seqNum);				
				documentUserActions.add(documentUserAction);
			}
		}
		return documentUserActions;
	}
	
	
	
	@Override
	public ArrayList<DocumentUserActionBO> getDocumentUserMasterInQueue(Integer documentId, Boolean isViewOnlyIT){
//		List<DocUserMaster> dums = docUserMasterRepo.findByDocumentIdAndSerialNumberIsNotNullOrderBySerialNumberAsc(documentId);
		/*List<String> docRoleList = new ArrayList<String>();
		docRoleList.add(DocRole.APPROVER);
		docRoleList.add(DocRole.ENDORSER);
		docRoleList.add(DocRole.INITIATOR);
		docRoleList.add(DocRole.CONTROLLER);
		Collection<String> docRole = new ArrayList<String>(docRoleList);*/
		List<DocUserMaster> dums = docUserMasterRepo.findByDocumentIdAndDocRoleInAndSerialNumberIsNotNullOrderBySerialNumberAsc(documentId, WFUtil.docRoleList());
		List<DocUserAction> docUserList=docUserActionRepo.findByDocRoleAndDocumentIdAndActionStatus(DocRole.APPROVER, documentId, ActionStatus.APPROVED);
		List<DocUserAction> docUAList=docUserActionRepo.findByDocRoleAndDocumentIdAndActionStatus(DocRole.INITIATOR, documentId, ActionStatus.PENDING);
		if(docUserList!= null && docUserList.size() >0 && docUAList!=null && docUAList.size() >0 ){
			return null;
		}
		DocumentMaster dm = documentMasterRepository.findByDocumentId(documentId);
		Integer currentUserId=dm.getCurrentlyWith();
		ArrayList<DocumentUserActionBO> inQueue = new ArrayList<DocumentUserActionBO>();
		Integer currentUserSerialNo= null;
		if(currentUserId== null){
			return null;
		}
		for(DocUserMaster dum:dums){
			System.out.println("dum.getUserId()" + dum.getUserId());
			System.out.println("currentUserId" + currentUserId);
			if(dum.getUserId() ==currentUserId.intValue()){
				currentUserSerialNo = dum.getSerialNumber();
				System.out.println("currentUserSerialNo" + currentUserSerialNo);
			}
		}
		DocumentUserActionBO person;
		if(isViewOnlyIT){
			for(DocUserMaster dum:dums){
				if(dum.getSerialNumber()>=currentUserSerialNo){
			
				person = new DocumentUserActionBO();
				Users user=userrepo.findByUserId(dum.getUserId());
				person.setUserId(user.getUserId());
				person.setPersonName(user.getFirstName()+" "+user.getLastName());
				person.setDocRole(dum.getDocRole());
				person.setDocSequence(dum.getSerialNumber());
				if(dum.getSerialNumber()==null){
					person.setIsParallel("true");
				}else{
					person.setIsParallel("false");
				}
				inQueue.add(person);
				}
			}
		}else{
			for(DocUserMaster dum:dums){
				System.out.println("dum.getSerialNumber()" + dum.getSerialNumber());
				System.out.println("currentUserSerialNo" + currentUserSerialNo);
				if(dum.getSerialNumber()>currentUserSerialNo){
			
				person = new DocumentUserActionBO();
				Users user=userrepo.findByUserId(dum.getUserId());
				person.setUserId(user.getUserId());
				person.setPersonName(user.getFirstName()+" "+user.getLastName());
				person.setDocRole(dum.getDocRole());
				person.setDocSequence(dum.getSerialNumber());
				if(dum.getSerialNumber()==null){
					person.setIsParallel("true");
				}else{
					person.setIsParallel("false");
				}
				inQueue.add(person);
				}
			}
		}
		Collections.sort(inQueue, new Comparator<DocumentUserActionBO>() {

			@Override
			public int compare(DocumentUserActionBO dua1, DocumentUserActionBO dua2) {
				Integer srNo1 = dua1.getDocSequence();
				Integer srNo2 = dua2.getDocSequence();
			
				if(srNo1 != null && srNo2!= null ){
					if (srNo1 > srNo2)
					return 1;
				else if (srNo1 < srNo2)
					return -1;
				}
				return 0;
			}
	    });
		return inQueue;
	}


	
	@Override
	public ArrayList<DocumentUserActionBO> geUsersInQueue(Integer documentId,
			Integer userId, Boolean isSupportingEndorsers,
			Boolean isReferredInitiator,Boolean isPDF) {
		
		ArrayList<DocumentUserActionBO> inQueue = new ArrayList<DocumentUserActionBO>();
		List<DocUserMaster> mainlineEndorser = docUserMasterRepo.findByDocumentIdAndDocRoleIn(documentId, WFUtil.docRoleList());
		List<DocUserMaster> supportingEndorser = docUserMasterRepo.findByDocumentIdAndDocRoleNotIn(documentId, WFUtil.docRoleListForSupportingEndorser());
		DocumentMaster dm = documentMasterRepository.findByDocumentId(documentId);
		List<DocUserAction> docActions = docUserActionRepo.findByDocumentId(documentId);
		List<DocUserAction> docList=docUserActionRepo.findByDocRoleAndDocumentIdAndActionStatus(DocRole.INITIATOR, documentId, ActionStatus.INITIATED);
		List<DocUserAction> docusrList=docUserActionRepo.findByDocRoleAndDocumentIdAndActionStatus(DocRole.INITIATOR, documentId, ActionStatus.PENDING);
		List<DocUserAction> docaprList=docUserActionRepo.findByDocRoleAndDocumentIdAndActionStatus(DocRole.APPROVER, documentId, ActionStatus.REFERRED);
		Map<Integer, DocUserMaster> userIdVsDUM = new HashMap<Integer, DocUserMaster>();
		if(!isSupportingEndorsers && !isReferredInitiator){
			for(DocUserMaster dum : mainlineEndorser){
				userIdVsDUM.put(dum.getUserId(), dum);
	
			}
		
		}else if(isReferredInitiator!=null && isReferredInitiator){
			Integer srNoCurrentUserInDum= null;
			for(DocUserMaster dum : mainlineEndorser){
				System.out.println("dum.getUserId() >>>>"+dum.getUserId());
						userIdVsDUM.put(dum.getUserId(), dum);
						if(userId!=null && userId.equals(dum.getUserId())){
							
							srNoCurrentUserInDum=dum.getSerialNumber();
							System.out.println("<<<< srNoCurrentUserInDum >>>>"+srNoCurrentUserInDum);
						}
			}
			for(DocUserMaster dum : mainlineEndorser){
				System.out.println("<<<< srNoCurrentUserInDum >>>>"+srNoCurrentUserInDum);
				System.out.println("<<<< dum.getSerialNumber() >>>>"+dum.getSerialNumber());
				if(srNoCurrentUserInDum>dum.getSerialNumber()||(userId.equals(dum.getUserId()))){
					System.out.println("<<<< dum.getUserId() >>>>"+dum.getUserId());
					userIdVsDUM.remove(dum.getUserId());
				}
			}
			
		
		}else{
			for(DocUserMaster dum : supportingEndorser){
				userIdVsDUM.put(dum.getUserId(), dum);
	
			}
		}
		if(!isReferredInitiator){
			for(DocUserAction dua : docActions){
				if((!dua.getActionStatus().equalsIgnoreCase(ActionStatus.PENDING_EXPRESS) &&!dua.getActionStatus().equalsIgnoreCase(ActionStatus.REFERRED) &&!dua.getActionStatus().equalsIgnoreCase(ActionStatus.PENDING))||( !isPDF && dua.getActionStatus().equalsIgnoreCase(ActionStatus.PENDING)&& userId!=null && dua.getUserId().equals(userId))){
					if(isPDF!=null && isPDF){
						if(dm.getCurrentlyWith()==null ){
							userIdVsDUM.remove(dua.getUserId());
						}else if(!(dm.getStatus().equalsIgnoreCase(DocStatus.IN_PROCESS_PARALLEL) &&dua.getUserId().equals(dm.getCurrentlyWith()))&& !dm.getCurrentlyWith().equals(dua.getUserId())){
							userIdVsDUM.remove(dua.getUserId());
						}
					}else if(!isPDF ){
						System.out.println("dm.getStatus() >>>>"+dm.getStatus());
						if(!(dm.getStatus().equalsIgnoreCase(DocStatus.IN_PROCESS_PARALLEL) &&dua.getUserId().equals(dm.getCurrentlyWith())&& userId!=null && !dua.getUserId().equals(userId))||(dua.getActionStatus().equalsIgnoreCase(ActionStatus.PENDING)&& userId!=null && dua.getUserId().equals(userId)&& dm.getCurrentlyWith().equals(dua.getUserId()))){
							System.out.println("dm.getStatus() dua.getUserId()>>>>"+dua.getUserId());
							userIdVsDUM.remove(dua.getUserId());
						}
					}
					}
			}
		}

		if(!isSupportingEndorsers && !isReferredInitiator){
		if(docList!=null && docList.size()>1 && docusrList!=null && docusrList.size() >0 && docaprList!=null && docaprList.size()>0){
		for(DocUserMaster dum : mainlineEndorser){
			System.out.println("docaprList.get(0).getUserId() >>>>"+docaprList.get(0).getUserId()+"sdds"+dum.getUserId());
			//for(Users users : usersList){
				if(docaprList.get(0).getUserId()==dum.getUserId() ){
					System.out.println("docaprList.get(0).getUserId() >>>>"+docaprList.get(0).getUserId()+"test"+dum.getUserId());
					userIdVsDUM.put(dum.getUserId(), dum);
				}
		//	}
			}
		}
		}
		ArrayList<DocUserMaster> docUserMasters = new ArrayList<DocUserMaster>(userIdVsDUM.values());
		System.out.println("dm.getStatus() dua.getUserId()>>>>"+docUserMasters.size());
		System.out.println("userIdVsDUM >>>>>"+userIdVsDUM);
		DocumentUserActionBO person;
		for(DocUserMaster dum : docUserMasters){
		
			person = new DocumentUserActionBO();
			Users user=userrepo.findByUserId(dum.getUserId());
			person.setUserId(user.getUserId());
			person.setPersonName(user.getFirstName()+" "+user.getLastName());
			person.setDocRole(dum.getDocRole());
			person.setDocSequence(dum.getSerialNumber());
			if(dum.getSerialNumber()==null){
				person.setIsParallel("true");
			}else{
				person.setIsParallel("false");
			}
			inQueue.add(person);
			
		}
		
		System.out.println("<<<< isSupportingEndorsers >>>>>"+isSupportingEndorsers+"docUserMasters>>>>>>"+docUserMasters.size());
		Collections.sort(inQueue, new Comparator<DocumentUserActionBO>() {

			@Override
			public int compare(DocumentUserActionBO dua1, DocumentUserActionBO dua2) {
				Integer srNo1 = dua1.getDocSequence();
				Integer srNo2 = dua2.getDocSequence();
			
				if(srNo1 != null && srNo2!= null ){
					if (srNo1 > srNo2)
					return 1;
				else if (srNo1 < srNo2)
					return -1;
				}
				return 0;
			}
	    });
		return inQueue;
	}
	
	/* (non-Javadoc)
	 * @see com.gt.services.DocUserActionService1#getDocumentUserActionInQueue(java.lang.Integer, java.lang.Boolean)
	 */
	@Override
	public ArrayList<DocumentUserActionBO> getDocumentUserActionInQueue(
			Integer documentId,Integer userId, Boolean isSupportingEndorsers,Boolean isReferredInitiator) {
		ArrayList<DocumentUserActionBO> inQueue = new ArrayList<DocumentUserActionBO>();
		List<DocUserMaster> dums = docUserMasterRepo.findByDocumentIdAndDocRoleNotIn(documentId, WFUtil.getDocRoles());
		List<DocUserAction> duas = docUserActionRepo.findByDocumentId(documentId);
		List<Users> usersList = userrepo.findByIsSupportingEndorser(true);
		
		Map<Integer, DocUserMaster> userIdVsDUM = new HashMap<Integer, DocUserMaster>();
		
		
		System.out.println("dums.size() >>>>"+dums.size());
		
		System.out.println("isReferredInitiator >>>>"+isReferredInitiator);
		
		System.out.println("isSupportingEndorsers >>>>"+isSupportingEndorsers);
		
		if(!isSupportingEndorsers && !isReferredInitiator){
			Integer srNo=null;
			DocumentMaster dm =documentMasterRepository.findByDocumentId(documentId);
			if(dm.getDeptId()!= null  && dm.getDeptId()!=0){
				List<DocUserAction> docuaList=docUserActionRepo.findByDocRoleAndDocumentIdAndActionStatus(DocRole.INITIATOR, dm.getDocumentId(), ActionStatus.ENDORSED);
				if(docuaList!=null && docuaList.size()>0){
					srNo=docuaList.get(docuaList.size()-1).getDocActionSerialNumber();
				}
			}
			for(DocUserMaster dum : dums){
				userIdVsDUM.put(dum.getUserId(), dum);
			}
			Users usr = userrepo.findByUserId(userId);
			if(usr.getIsSupportingEndorser()!=null && usr.getIsSupportingEndorser()){
				for(DocUserAction dua : duas){
					
					if(!dua.getActionStatus().equalsIgnoreCase(ActionStatus.PENDING_EXPRESS) && !dua.getActionStatus().equalsIgnoreCase(ActionStatus.PENDING)){
						userIdVsDUM.remove(dua.getUserId());
					}else{
						for(DocUserMaster dum : dums){
							if(dua.getUserId() == dum.getUserId()){
								userIdVsDUM.put(dum.getUserId(), dum);
							}
							
						}
					}
				}
			}else{
				for(DocUserAction dua : duas){
					if(!dua.getActionStatus().equalsIgnoreCase(ActionStatus.PENDING_EXPRESS)){
						if(srNo == null){
						userIdVsDUM.remove(dua.getUserId());
						}else if(srNo != null && dua.getDocActionSerialNumber() >= srNo ){
							userIdVsDUM.remove(dua.getUserId());
						}
					}
				}
			}
			for(Users users : usersList){
				userIdVsDUM.remove(users.getUserId());
			}
		}else if(isReferredInitiator!=null && isReferredInitiator){
			for(DocUserMaster dum : dums){
				System.out.println("dum.getUserId() >>>>"+dum.getUserId());
				//for(Users users : usersList){
					//if(users.getUserId()==dum.getUserId() ){
						userIdVsDUM.put(dum.getUserId(), dum);
					//}
			//	}
				}
				for(DocUserAction dua : duas){
					if(dua.getDocRole().equals(DocRole.INITIATOR)){
						System.out.println("dua.getUserId() >>>>"+dua.getUserId());
					userIdVsDUM.remove(dua.getUserId());
				}
				}
		}else{
			for(DocUserMaster dum : dums){
				
			for(Users users : usersList){
				if(users.getUserId()==dum.getUserId() ){
					userIdVsDUM.put(dum.getUserId(), dum);
				}
			}
			}
			for(DocUserAction dua : duas){
				userIdVsDUM.remove(dua.getUserId());
			}
		}
		
		ArrayList<DocUserMaster> docUserMasters = new ArrayList<DocUserMaster>(userIdVsDUM.values());
		
		DocumentUserActionBO person;
		for(DocUserMaster dum : docUserMasters){
		
			person = new DocumentUserActionBO();
			Users user=userrepo.findByUserId(dum.getUserId());
			person.setUserId(user.getUserId());
			person.setPersonName(user.getFirstName()+" "+user.getLastName());
			person.setDocRole(dum.getDocRole());
			person.setDocSequence(dum.getSerialNumber());
			if(dum.getSerialNumber()==null){
				person.setIsParallel("true");
			}else{
				person.setIsParallel("false");
			}
			inQueue.add(person);
			
		}
		Collections.sort(inQueue, new Comparator<DocumentUserActionBO>() {

			@Override
			public int compare(DocumentUserActionBO dua1, DocumentUserActionBO dua2) {
				Integer srNo1 = dua1.getDocSequence();
				Integer srNo2 = dua2.getDocSequence();
			
				if(srNo1 != null && srNo2!= null ){
					if (srNo1 > srNo2)
					return 1;
				else if (srNo1 < srNo2)
					return -1;
				}
				return 0;
			}
	    });
		return inQueue;
	}

	@Override
	public ArrayList<DocumentUserActionBO> getDocumentUserActionDocumentAllUsersInQueue(
			Integer documentId,Boolean isSupportingEndorsers,Boolean isReferredInitiator) {

		ArrayList<DocumentUserActionBO> inQueue = new ArrayList<DocumentUserActionBO>();
		List<DocUserMaster> dums = docUserMasterRepo.findByDocumentIdAndDocRoleNotIn(documentId, WFUtil.getDocRoles());
		List<DocUserAction> duas = docUserActionRepo.findByDocumentId(documentId);
		List<Users> usersList = userrepo.findByIsSupportingEndorser(true);
		DocumentMaster documentMaster = documentMasterRepository.findByDocumentId(documentId);
		Map<Integer, DocUserMaster> userIdVsDUM = new HashMap<Integer, DocUserMaster>();
		
		if(!isSupportingEndorsers && !isReferredInitiator){
			Integer srNo=null;
			DocumentMaster dm =documentMasterRepository.findByDocumentId(documentId);
			if(dm.getDeptId()!= null  && dm.getDeptId()!=0){
				List<DocUserAction> docuaList=docUserActionRepo.findByDocRoleAndDocumentIdAndActionStatus(DocRole.INITIATOR, dm.getDocumentId(), ActionStatus.ENDORSED);
				if(docuaList!=null && docuaList.size()>0){
					srNo=docuaList.get(docuaList.size()-1).getDocActionSerialNumber();
				}
			}
			for(DocUserMaster dum : dums){
				userIdVsDUM.put(dum.getUserId(), dum);
			}
			for(DocUserAction dua : duas){
				if((!dua.getActionStatus().equalsIgnoreCase(ActionStatus.PENDING_EXPRESS))){
					if(documentMaster.getCurrentlyWith() == null || (documentMaster.getCurrentlyWith()!=null &&documentMaster.getCurrentlyWith()!=dua.getUserId())){
						//userIdVsDUM.remove(dua.getUserId());
						if(srNo == null){
							userIdVsDUM.remove(dua.getUserId());
							}else if(srNo != null && dua.getDocActionSerialNumber() >= srNo ){
								userIdVsDUM.remove(dua.getUserId());
							}
					}
					
				}
				
			}
			for(Users users : usersList){
				userIdVsDUM.remove(users.getUserId());
			}
		}else if(isReferredInitiator!=null && isReferredInitiator){
			for(DocUserMaster dum : dums){
				System.out.println("dum.getUserId() >>>>"+dum.getUserId());
				//for(Users users : usersList){
					//if(users.getUserId()==dum.getUserId() ){
						userIdVsDUM.put(dum.getUserId(), dum);
					//}
			//	}
				}
				for(DocUserAction dua : duas){
					if(dua.getDocRole().equals(DocRole.INITIATOR)){
						System.out.println("dua.getUserId() >>>>"+dua.getUserId());
					userIdVsDUM.remove(dua.getUserId());
				}
				}
		}else{
			for(DocUserMaster dum : dums){
				
			for(Users users : usersList){
				if(users.getUserId()==dum.getUserId() ){
					userIdVsDUM.put(dum.getUserId(), dum);
				}
			}
			}
			for(DocUserAction dua : duas){
				if(!dua.getActionStatus().equalsIgnoreCase(ActionStatus.PENDING)){
					userIdVsDUM.remove(dua.getUserId());
				}
			}
		}
		
		ArrayList<DocUserMaster> docUserMasters = new ArrayList<DocUserMaster>(userIdVsDUM.values());
		
		DocumentUserActionBO person;
		for(DocUserMaster dum : docUserMasters){
		
			person = new DocumentUserActionBO();
			Users user=userrepo.findByUserId(dum.getUserId());
			person.setPersonName(user.getFirstName()+" "+user.getLastName());
			person.setDocRole(dum.getDocRole());
			person.setDocSequence(dum.getSerialNumber());
			if(dum.getSerialNumber()==null){
				person.setIsParallel("true");
			}else{
				person.setIsParallel("false");
			}
			inQueue.add(person);
			
		}
		Collections.sort(inQueue, new Comparator<DocumentUserActionBO>() {

			@Override
			public int compare(DocumentUserActionBO dua1, DocumentUserActionBO dua2) {
				Integer srNo1 = dua1.getDocSequence();
				Integer srNo2 = dua2.getDocSequence();
			
				if(srNo1 != null && srNo2!= null ){
					if (srNo1 > srNo2)
					return 1;
				else if (srNo1 < srNo2)
					return -1;
				}
				return 0;
			}
	    });
		return inQueue;
	
	}
	
	/* (non-Javadoc)
	 * @see com.gt.services.DocUserActionService1#saveDocUserActionForDraft(java.lang.Integer)
	 */
	@Override
	public DocUserAction saveDocUserActionForDraft(Integer documentId){
		DocUserAction dua= new DocUserAction();
		Integer srNo=1;
		List<DocUserMaster> dums = docUserMasterRepo.findByDocumentIdOrderBySerialNumberAsc(documentId);
		List<DocUserMaster> docUserMasters = docUserMasterRepo.findBySerialNumberAndDocumentId(null, documentId);
		Integer initiatorSrNo= docUserMasters.size();
		dua.setUserId(dums.get(initiatorSrNo).getUserId());
		dua.setDocRole(dums.get(initiatorSrNo).getDocRole());
		dua.setActionStatus(ActionStatus.DRAFT);
		dua.setIsDraft("Yes");
//		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		dua.setActionPendingTimeStamp(date);
		dua.setActionTakenTimeStamp(date);
		dua.setDocActionSerialNumber(srNo);
		dua.setFromUserAction(dums.get(initiatorSrNo).getUserId());
		dua.setDocumentId(documentId);
		docUserActionRepo.save(dua);
		System.out.println("docUserAction.getIsDraft() dua for Draft data DUA>>>"+dua.getIsDraft());
		logger.info("docUserAction.getIsDraft() dua for actionID>>>"+dua.getActionId()+"Draft data DUA>>>"+dua.getIsDraft());
		return dua;
	}
	
	@Override
	public ArrayList<DocUserAction> getDocUserActionByDocumentId(Integer documentId) {
	
		return (ArrayList<DocUserAction>) docUserActionRepo.findByDocumentId(documentId);
	}
	
	@Override
	public ArrayList<DocumentUserActionBO> getImmediateNextEndorsers(Integer userId) {
		
		ArrayList<DocumentUserActionBO> immediateEndorsers =new ArrayList<DocumentUserActionBO>();
		
		List<UserEndorsers> userEndorser = userEndorserRepo.findByUserId(userId);
		for(UserEndorsers usr:userEndorser){
			Integer endorserId=usr.getEndorserId();
			Users endorser=userrepo.findByUserId(endorserId);
			DocumentUserActionBO immEndorser =new DocumentUserActionBO();
			immEndorser.setPersonName(endorser.getFirstName()+" "+endorser.getLastName());		
			immEndorser.setUserId(endorser.getUserId());
			immediateEndorsers.add(immEndorser);
			
		}
		
		return  immediateEndorsers;
	}
	
	@Override
	public List<DocUserAction> findByDocumentIdAndActionStatus(
			Integer documentIdInt, String released) {
		// TODO Auto-generated method stub
		
		List<DocUserAction> duaList=docUserActionRepo.findByDocumentIdAndActionStatus(documentIdInt, released);
		return duaList;
	}

	@Override
	public DocUserAction findByActionId(Integer actionId) {
		// TODO Auto-generated method stub
		return docUserActionRepo.findByActionId(actionId);
	}

	@Override
	public DocUserAction updateDocUserAction(DocUserAction docUserAction) {
		// TODO Auto-generated method stub
		return docUserActionRepo.save(docUserAction);
	}

	@Override
	public ArrayList<DocUserAction> findByUserIdOrderByActionIdDesc(
			Integer userId) {
		// TODO Auto-generated method stub
		return docUserActionRepo.findByUserIdOrderByActionIdDesc(userId);
	}
	
	@Override
	public List<DocUserAction> findByUserIdAndIsDraft(
			Integer userId,String isDraft) {
		// TODO Auto-generated method stub
		return docUserActionRepo.findByUserIdAndIsDraft(userId,isDraft);
	}

	@Override
	public List<DocUserAction> findByUserIdAndDocumentId(Integer userId,
			Integer documentId) {
		// TODO Auto-generated method stub
		return docUserActionRepo.findByUserIdAndDocumentId(userId,documentId);
	}


	@Override
	public ArrayList<DocUserAction> getMnMApprovedReleasedList() {
		// TODO Auto-generated method stub
		return (ArrayList<DocUserAction>) docUserActionRepo.findByDocumentIdAndActionStatus();
	}

	@Override
	public DocUserAction saveCreateFinalApprovedData(Integer documentId, Integer userId,
			Integer delegatedBy) {
		

		DocUserAction dua = null;
		List<DocUserAction> duas = docUserActionRepo.findByDocumentIdAndActionStatus(documentId, ActionStatus.PENDING);
		DocumentMaster dm = documentMasterRepository.findByDocumentId(documentId);
		DocUserMaster dum= null;
		Integer delegatedTo =null;
		if(delegatedBy!=null && delegatedBy!=-1 && (delegatedBy!=userId)){
			delegatedTo= userId;
			userId = delegatedBy;
			dum= docUserMasterRepo.findByDocumentIdAndUserId(documentId, delegatedBy);
		}
		Date date = new Date();
		Integer releaseSerialNumber = 0;
		for(DocUserAction dua1:duas){
			if(dua1.getUserId().equals(userId)){
				dua1.setActionStatus(ActionStatus.INITIATED);
				dua1.setActionTakenTimeStamp(date);
				if(delegatedBy!=null && delegatedBy!=-1){
					dua1.setActionTakenBy(delegatedTo);
				}
				releaseSerialNumber=dua1.getDocActionSerialNumber();
				dua=dua1;
				docUserActionRepo.save(dua1);
			}
		}
		Integer approverId=docUserMasterRepo.findByDocumentIdAndDocRole(documentId, DocRole.APPROVER).getUserId();
			DocUserAction duaPendingAction = new DocUserAction();
			duaPendingAction.setDocumentId(documentId);
			duaPendingAction.setUserId(approverId);
			duaPendingAction.setDocRole(DocRole.APPROVER);
			duaPendingAction.setActionStatus(ActionStatus.PENDING);
			duaPendingAction.setActionPendingTimeStamp(date);
			duaPendingAction.setDocActionSerialNumber(releaseSerialNumber+1);
			duaPendingAction.setFromUserAction(userId);
			docUserActionRepo.save(duaPendingAction);
			
		
		
	
		if(delegatedTo!=null && dum != null){
			dum.setDelegateId(delegatedTo);
			docUserMasterRepo.save(dum);
		}
		
		dm.setCurrentlyWith(approverId);
		dm.setStatus(DocStatus.IN_PROCESS);
		dm.setProposalType("Final");
		dm.setIsAmendment("NO");
		documentMasterRepository.save(dm);
		logger.info("DocumentMaster Status save create "+dm.getStatus()+" Proposal Type "+dm.getProposalType()+" DocId "+dm.getDocumentId());
		
		return dua;
	
	}


		
}
