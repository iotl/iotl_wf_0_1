package com.gt.services;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.gt.bo.DocumentUserActionBO;
import com.gt.entity.Comments;
import com.gt.entity.DocAttachment;
import com.gt.entity.DocUserAction;

public interface DocUserActionService {
	public ArrayList<DocUserAction> getDocUserActionByDocumentId(Integer documentId);
	public abstract DocUserAction saveApprovedData(Integer documentId, Integer userId, Integer delegatedByInt);

	public abstract DocUserAction saveRejectedData(Integer documentId, Integer userId, Integer delegatedByInt);

	public abstract DocUserAction saveReferedBackData(Integer userId, Integer delegatedBy, Integer documentId,
			Integer referedTo);

	public abstract DocUserAction saveReply(Integer userId, Integer delegatedBy, Integer documentId,String isReferredToInitiator);

	public abstract DocAttachment saveDocAttachment(DocAttachment docAttachment, File file) throws Exception;

	public abstract Comments saveComments(Comments comments, String safeHtml);

	public abstract void saveCancelledData(Integer userId, Integer documentId);

	public abstract DocUserAction saveDocUserAction(Integer documentId, String isRPExpress, Integer actionId);

	public abstract DocUserAction saveDocUserActionView(Integer documentId, Integer userId, String parallelFinished,
			String parallelStarted, Integer delegatedByUserId);

	public abstract DocUserAction saveReleaseData(Integer userId, Integer delegatedBy, Integer documentId);

	public abstract ArrayList<DocumentUserActionBO> getDocumentUserAction(Integer documentId, Integer userId)
			throws Exception;

	public abstract ArrayList<DocumentUserActionBO> getDocumentUserActionCurrentUser(Integer documentId, Integer userId);

	public abstract ArrayList<DocumentUserActionBO> getDocumentUserActionInQueue(Integer documentId, Integer userId,
			Boolean isSupportingEndorsers,Boolean isReferredInitiator);
	
	public abstract ArrayList<DocumentUserActionBO> geUsersInQueue(Integer documentId, Integer userId,
			Boolean isSupportingEndorsers,Boolean isReferredInitiator,Boolean isPDF);

	public abstract ArrayList<DocumentUserActionBO> getDocumentUserActionDocumentAllUsersInQueue(Integer documentId,
			Boolean isSupportingEndorsers,Boolean isReferredInitiator);

	public abstract ArrayList<DocumentUserActionBO> getImmediateNextEndorsers(Integer userId);
	/**
	 * @param documentId
	 * @return
	 */
	public abstract DocUserAction saveDocUserActionForDraft(Integer documentId);
	
	public abstract List<DocUserAction> findByDocumentIdAndActionStatus(
			Integer documentIdInt, String released);
	
	public abstract DocUserAction findByActionId(Integer actionId);
	
	public abstract DocUserAction updateDocUserAction(DocUserAction docUserAction);
	
	public abstract ArrayList<DocUserAction> findByUserIdOrderByActionIdDesc(Integer userId);
	
	public abstract List<DocUserAction> findByUserIdAndIsDraft(Integer userId,String isDraft);
	
	public abstract List<DocUserAction> findByUserIdAndDocumentId(Integer userId,Integer documentId);
	
	public abstract ArrayList<DocumentUserActionBO> getDocumentUserMasterInQueue(Integer documentId, Boolean isViewOnlyIT);
	
	public ArrayList<DocUserAction> getMnMApprovedReleasedList();
	
	public DocUserAction saveCreateFinalApprovedData(Integer documentId, Integer userId,Integer delegatedBy);

}