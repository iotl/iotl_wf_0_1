package com.gt.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.data.repository.query.Param;

import com.gt.bo.MasterBO;
import com.gt.entity.Businesses;
import com.gt.entity.Category;
import com.gt.entity.Cluster;
import com.gt.entity.Companies;
import com.gt.entity.Material;
import com.gt.entity.Plant;
import com.gt.entity.Sbus;

public interface MasterData {
	
	public ArrayList<Companies> getCompanies();
	public Companies getCompaniesById(int id);
	
	public ArrayList<Businesses> getBusinesses();
	public Businesses getBusinessesById(int id);
	
	public ArrayList<Plant> getPlantData();
	public Plant getPlantById(int id);
	public ArrayList<Plant> getPlantByClusterId(Integer clusterId);
	
	public ArrayList<Sbus> getSbuData();
	public Sbus getSbusById(int id);
	
	public ArrayList<Material> getMaterialData();
	public Material getMaterialById(int id);
	public ArrayList<Material> getMaterialsForCluster(Integer plantId);
	public MasterBO getMasterDataForPlant(Integer plantId);
	public List<Material> getMaterialsByBusinessIdAndDeptId(Integer plantId,Integer deptId);
	public ArrayList<Cluster> getClusterSbuData(Integer plantId);
	public ArrayList<Plant> getPlantNameByClusterId(Integer clusterId);
	public List<Material> getMaterialsByDeptId(Integer deptId);
   
	public ArrayList<Businesses> findBusinessesByCompanyId(int companyId);
	public ArrayList<Sbus> getSBUsbyCompanyIdandBusinessId(int companyId,int businessId);
	public ArrayList<Plant> getPlantsbyCompanyIdandBusinessIdandSbuId(int companyId,int businessId,int sbusId);
	
	public ArrayList<Businesses> findbyNotInCompanyIdAndBusinessIdNotInBusinessCollection(int companyId,List<Integer> businessIdList);
	
	public ArrayList<Plant> findAllRpPlants();
	
	public ArrayList<Plant> findNotMatchingRecordsInPlantsId(int companyId, int businessId,int sbuId,List<Integer> plantListId);
	
	public ArrayList<Businesses> findAllBusiness();
	
	public ArrayList<Category> getCategories();

}
