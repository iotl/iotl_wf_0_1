package com.gt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import com.gt.entity.Companies;
import com.gt.entity.Users;

public interface MasterCompaniesDataRepository extends Repository<Companies, String> {
	List<Companies> findAll();
	Companies findByCompanyId(Integer id);
     public <S extends Companies> S save(S entity);
	 @Query("select max(c.companyId) from Companies c")
     Integer findMaxCompanyId();
	List<Companies> findByAbbreviationAndCompanyIdNotIn(String abbreviation, int companyId);
	List<Companies> findByCompanyNameAndCompanyIdNotIn(String companyName, int companyId);
	 @Query("select companyId from Companies c")
	 List<Integer> findAllCompanyId();

}
