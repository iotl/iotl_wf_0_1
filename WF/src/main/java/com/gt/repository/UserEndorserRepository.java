package com.gt.repository;

import java.util.List;

import org.springframework.data.repository.Repository;

import com.gt.entity.UserEndorsers;

public interface UserEndorserRepository extends Repository<UserEndorsers, String> {
	
	 List<UserEndorsers> findByUserId(int userId);
	 
	 UserEndorsers save(UserEndorsers userEndorser);
	 UserEndorsers delete(UserEndorsers userEndoser);
	 Integer deleteByUserId(Integer userId);


}