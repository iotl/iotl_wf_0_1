package com.gt.repository;


import java.util.ArrayList;
import java.util.List;

import org.springframework.data.repository.Repository;

import com.gt.entity.Cluster;
import java.lang.String;

public interface ClustersRepository extends Repository<Cluster, String> {
		
	ArrayList<Cluster> findAll();
	List<Cluster> findByClusterName(String clustername);
	List<Cluster> findByClusterId(int clusterid);
		

}


