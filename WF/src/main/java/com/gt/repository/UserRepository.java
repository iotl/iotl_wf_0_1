package com.gt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.gt.entity.Users;

public interface  UserRepository extends CrudRepository<Users, String> {
	   void delete(Users deleted);

	    List<Users> findAll();
	    
	    Users findByUserId(int id);
	
        List<Users> findByIsSupportingEndorser(Boolean isSupportingEndorser);
        List<Users> findByEmailId(String emailId);
        @Override
        public <S extends Users> S save(S entity);
        
        Users findByAuthorizationKey(String authorizationKey);
	    
        @Query("select max(t.userId) from Users t")
        Integer findMaxUserId();
        
}

		