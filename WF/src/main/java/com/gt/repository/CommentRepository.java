package com.gt.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gt.entity.Comments;

public interface CommentRepository extends CrudRepository<Comments,String>{
	
	List<Comments> findAll();
//	Comments findByAction_id(Integer action_Id);
//	List<Comments> findByDocument_id(Integer documentid);
	//Comments save(Comments entity);
//	Comments findByComment_Id(Integer commentId);
	@Override
	public <S extends Comments> S save(S entity);
	
	Comments findByActionId(Integer actionId);
	
	List<Comments> findByDocumentId(Integer documentid);
	
	//Comments findByComment_id(Integer commentId);
	
	
}
