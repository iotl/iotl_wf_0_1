package com.gt.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gt.entity.RPNumber;


public interface RPNumberRepository extends CrudRepository<RPNumber, String>{
	
	List<RPNumber> findAll();
	
	RPNumber findByRpNumberId(int rpNumberId);
	
	List<RPNumber> findByPlantIdOrderByRpNumberIdDesc(int plantId);
	List<RPNumber> findByPlantIdAndFinancialYearOrderByRpNumberIdDesc(int plantId,int fy);
	
	@Override
	public <S extends RPNumber> S save(S entity);

}
