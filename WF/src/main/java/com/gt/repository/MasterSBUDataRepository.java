package com.gt.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import com.gt.entity.SbuBusinessCompany;
import com.gt.entity.Sbus;

public interface MasterSBUDataRepository extends Repository<Sbus, SbuBusinessCompany> {
	
	@Query("select s from Sbus s group by s.sbuId")
	List<Sbus> findAll();
	
	@Query("select s from Sbus s where s.sbuId = :sbuId group by s.sbuId"  )
	Sbus findBySbuId(@Param("sbuId")int sbuId);
	
	List<Sbus> findByCompanyIdAndBusinessId(int companyId,int businessId);
	
	public <S extends Sbus> S save(S entity);
	
	@Query("select s from Sbus s where s.sbuName = :sbuName and s.abbreviation = :abbreviation group by s.sbuId")
	Sbus findBySbuNameAndAbbreviation(@Param("sbuName")String sbuName,@Param("abbreviation")String abbreviation);
	
	 @Query("select max(s.sbuId) from Sbus s")
     Integer findMaxSbuId();
	 
	 @Query("select sb from Sbus sb where sb.companyId!= :companyId OR sb.businessId != :businessId AND sb.sbuId NOT IN :sbuIdList  GROUP BY sb.sbuId")
	 List<Sbus> findbyNotInCompanyIdAndNotInBusinessId(@Param("companyId")int companyId,@Param("businessId")int businessId,@Param("sbuIdList")List<Integer> sbuIdList);
      //business Id in ony one so use !=
	 //run now

	void delete(Sbus entity);
	
	@Modifying
	@Query("update Sbus sb set sb.sbuName = ?1 , sb.abbreviation= ?2 where sb.sbuId= ?3")
	int updateSbuNameBySbuId(String sbuName,String abbreviation,int sbuId);
	
	Integer countBySbuId(int sbuId);

	@Query("SELECT  s.sbuId FROM Sbus s where s.companyId in :companyIds and s.businessId in :businessIds GROUP BY s.sbuId")
	List<Integer> findAllSbuIdByCompanyIdInAndBusinessIdIn(@Param("companyIds")Collection<Integer> companyIds,@Param("businessIds")Collection<Integer> businessIds);
	
	@Query("SELECT  s FROM Sbus s where s.companyId in :companyIds and s.businessId in :businessIds GROUP BY s.sbuId")
	List<Sbus> findAllSbuByCompanyIdInAndBusinessIdIn(@Param("companyIds")Collection<Integer> companyIds,@Param("businessIds")Collection<Integer> businessIds);

}
