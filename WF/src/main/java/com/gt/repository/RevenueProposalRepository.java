package com.gt.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gt.entity.RevenueProposal;

public interface RevenueProposalRepository extends CrudRepository<RevenueProposal, String> {

	List<RevenueProposal> findAll();
	List<RevenueProposal> findByDocumentId(int DocId);
	RevenueProposal findByDocumentIdAndPlantId(int docId,int plantId);
	@Override
	public <S extends RevenueProposal> S save(S entity);
	
	@Override
	public <S extends RevenueProposal> Iterable<S> save(Iterable<S> entities);
	
	List<RevenueProposal> findByDocumentIdInAndRpNumberContaining(Collection<Integer> documentList,String rpNumber);
	
	List<RevenueProposal> findByRpNumber(String rpNumber);
	
	List<RevenueProposal> findByDocumentIdInAndAmountNegotiatedBetween(Collection<Integer> documentList,Double lowerLimt,Double upperLimit);
	
	List<RevenueProposal> findByDocumentIdIn(Collection<Integer> documentList);
}

