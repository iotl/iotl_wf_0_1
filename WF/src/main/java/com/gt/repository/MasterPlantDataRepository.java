package com.gt.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import com.gt.entity.Plant;


public interface MasterPlantDataRepository extends Repository<Plant, String> {
	
	@Query("select p from Plant p group by p.plantId")
	List<Plant> findAll();
	
	@Query("select p from Plant p where p.plantId = :plantId group by p.plantId"  )
	Plant findByPlantId(@Param("plantId")int plantId);
	
	List<Plant> findByPlantNameIgnoreCaseContaining(@Param("plantName")String plantName);
	
	List<Plant> findByClusterId(int clusterId);
	List<Plant> findByClusterIdAndCorpLogin(int clusterId,boolean isCorp);
	List<Plant> findByClusterIdAndPlantLogin(int clusterId,boolean isPlantLogin);
	
	List<Plant> findByCompanyIdAndBusinessIdAndSbuId(int companyId,int businessId,int sbuId);
	List<Plant> findByCompanyIdAndBusinessIdAndSbuIdAndCorpLogin(int companyId,int businessId,int sbuId,Boolean isCorp);
	List<Plant> findByCompanyIdAndBusinessIdAndSbuIdAndPlantLogin(int companyId,int businessId,int sbuId,Boolean isPlantLogin);
	public <S extends Plant> S save(S entity);
	
	@Query("select p from Plant p where p.plantName = :plantName and p.abbreviation = :abbreviation group by p.plantId")
	Plant findByPlantNameAndAbbreviation(@Param("plantName")String plantName,@Param("abbreviation")String abbreviation);
	
	 @Query("select max(p.plantId) from Plant p")
     Integer findMaxPlantId();
	 
	 @Query("select max(p.pkId) from Plant p")
     Integer findMaxPkId();
	 
	 @Query("select p from Plant p where p.sbuId is not null and p.businessId is not null and p.companyId is not null group by p.plantId")
	 List<Plant> findAllRpPlants();
	 
	 void delete(Plant entity);
	 
	 @Query("select p from Plant p where (p.companyId != :companyId OR p.businessId != :businessId OR p.sbuId != :sbuId) and (p.plantId NOT IN (:plantListId)) and (p.businessId is not null and p.companyId is not null) group by p.plantId")
	 List<Plant> findNotMatchingRecordsInMatchedPlants(@Param("companyId")int companyId,@Param("businessId") int businessId,@Param("sbuId")int sbuId,@Param("plantListId")List<Integer> plantListId);
	 
	 @Modifying
	 @Query("update Plant p set p.plantName = ?1 , p.abbreviation= ?2 where p.plantId= ?3")
	 int updatePlantNameByPlantId(String plantName,String abbreviation,int plantId);
	 
	 Integer countByPlantId(int plantId);
	 
	 Plant findByPlantName(String plantName);
	 
	 @Query("select p from Plant p where p.corpLogin = :isCorp group by p.plantId")
	 List<Plant> findByCorpLogin(@Param("isCorp")Boolean isCorp);
	 
		List<Plant> findByPlantLogin(boolean isPlantLogin);

	@Query("SELECT  p.plantId FROM Plant p where p.companyId in :companyIds and p.businessId in :businessIds and p.sbuId in :sbuIds and p.corpLogin =:corpLogin GROUP BY p.plantId")	
	List<Integer> findAllPlantIdByCompanyIdInAndBusinessIdInAndSbuIdIn(@Param("companyIds")Collection<Integer> companyIds,@Param("businessIds")Collection<Integer> businessIds, @Param("sbuIds")Collection<Integer> sbuIds,@Param("corpLogin") Boolean corpLogin);
	
	@Query("SELECT  p FROM Plant p where p.companyId in :companyIds and p.businessId in :businessIds and p.sbuId in :sbuIds and p.corpLogin =:corpLogin GROUP BY p.plantId")	
	List<Plant> findAllPlantByCompanyIdInAndBusinessIdInAndSbuIdIn(@Param("companyIds")Collection<Integer> companyIds,@Param("businessIds")Collection<Integer> businessIds, @Param("sbuIds")Collection<Integer> sbuIds,@Param("corpLogin") Boolean corpLogin);

	

}
