package com.gt.repository;

import java.util.ArrayList;

import org.springframework.data.repository.Repository;
import com.gt.entity.UserDelegates;
import java.lang.Integer;
import java.util.List;


public interface UserDelegatesRepository extends Repository<UserDelegates,String> {
		ArrayList<UserDelegates> findAll();
		List<UserDelegates> findByUserId(Integer userid);
}
