package com.gt.repository;

import java.util.List;

import org.springframework.data.repository.Repository;

import com.gt.entity.Category;



public interface MasterCategoryDataRepository  extends  Repository<Category, String> {
	
	List<Category> findAll();
	Category findByCategoryId(Integer id);

}
