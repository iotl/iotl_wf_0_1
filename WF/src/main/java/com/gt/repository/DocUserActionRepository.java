package com.gt.repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.gt.entity.DocUserAction;

public interface DocUserActionRepository extends CrudRepository<DocUserAction, String> {
	
//	 public final static String FIND_BY_DcoumentId_PendingRP_QUERY = "SELECT dua"+" FROM DocUserAction dua "+ "where Document_Id=:documentId and Action_Status like\"Pending%\" and Action_Status!=\"Pending_Invalid\"";
//
//	 @Query(FIND_BY_DcoumentId_PendingRP_QUERY)
//	 public List<DocUserAction> findByDocumentIdAndPendingRP(@Param("documentId") Integer documentId);
	
	List<DocUserAction> findAll();
	DocUserAction findByActionId(int actionId);
	List<DocUserAction> findByUserId(Integer userid);
	List<DocUserAction> findByDocumentId(Integer documentId);
	List<DocUserAction> findByActionStatus(String ActionStatus);
	List<DocUserAction> findByUserIdAndActionStatus(Integer userid, String ActionStatus);
	List<DocUserAction> findByUserIdAndActionStatusOrderByActionPendingTimeStampDesc(Integer userid, String ActionStatus);
	List<DocUserAction> findByUserIdAndActionStatusOrderByActionTakenTimeStampDesc(Integer userId, String ActionStatus);
	List<DocUserAction> findByUserIdAndActionStatusAndDocRoleOrderByActionTakenTimeStampDesc(Integer userId, String ActionStatus,String DocRole);
	List<DocUserAction> findByActionTakenBy(Integer actionTakenBy);
	DocUserAction findByDocumentIdAndDocActionSerialNumber(Integer docId, Integer srNo);
//	List<DocUserAction> findByUserIdAndActionStatus(Users userid, String ActionStatus);
	List<DocUserAction> findByDocumentIdAndActionStatus(Integer docId, String ActionStatus);
	List<DocUserAction> findByDocumentIdAndActionStatusAndUserIdOrderByDocActionSerialNumberAsc(Integer docId, String ActionStatus,Integer userId);
	List<DocUserAction> findByDocumentIdAndActionStatusAndUserIdOrderByDocActionSerialNumberDesc(Integer docId, String ActionStatus,Integer userId);
	List<DocUserAction> findByDocRoleAndDocumentIdAndActionStatus(String docRole,Integer documentId,String ActionStatus);
	
	List<DocUserAction> findByDocumentIdAndDocRoleNotIn(Integer documentId,Collection<String> docRole);
	
	@Query("Select rp FROM DocUserAction dua,DocumentMaster dm,RevenueProposal rp WHERE dm.documentId=dua.documentId AND rp.documentId=dua.documentId AND dm.status IN ('Approved','Released') AND dm.deptId='2' AND (dm.transportationJustification IS NULL OR dm.transportationJustification='')"
			+ "AND dm.documentId NOT IN (SELECT dma.documentId FROM DocumentMaster dma WHERE dma.status='Released'  AND dma.deptId='2' AND dma.categoryId=1 AND dma.proposalType='In-Principle') "
			+ "AND dm.documentId NOT IN (SELECT dmt.documentId FROM DocumentMaster dmt WHERE dm.status='Approved' AND  dm.deptId='2' AND dm.categoryId=1) GROUP BY dua.documentId")
	List<DocUserAction> findByDocumentIdAndActionStatus();
	
	@Override
	public <S extends DocUserAction> S save(S entity);
	
	ArrayList<DocUserAction> findByUserIdOrderByActionIdDesc(
			Integer userId);
	
	List<DocUserAction> findByUserIdAndIsDraft(
			Integer userId,String isDraft);
	
	List<DocUserAction> findByUserIdAndDocumentId(Integer userId,Integer documentId);
	List<DocUserAction> findByDocumentIdAndActionStatusOrderByDocActionSerialNumberDesc(Integer documentId,String ActionStatus);
}


