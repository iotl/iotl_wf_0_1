package com.gt.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.gt.entity.RpTransportation;

public interface RPTransportationRepository extends CrudRepository<RpTransportation, String> {

	ArrayList<RpTransportation> findAll();
	ArrayList<RpTransportation> findByDocumentId(Integer DocId);
	ArrayList<RpTransportation> findByDocumentIdAndPlantId(int docId,int plantId);
	@Override
	public <S extends RpTransportation> S save(S entity);
	
	@Override
	public <S extends RpTransportation> Iterable<S> save(Iterable<S> entities);
	
	@Query("Select COALESCE(MAX(rpt.transporterSeqId),0)+1 FROM RpTransportation rpt WHERE rpt.documentId IN (SELECT dm.documentId FROM DocumentMaster dm WHERE dm.originalProposalDocId=:originalProposalDocId)")
	Integer findByTransporterSeqIdWithOriginalProposalDocId(@Param("originalProposalDocId") Integer originalProposalDocId);
	
}

