package com.gt.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gt.entity.FinalApproval;





public interface FinalApprovalRepository  extends CrudRepository<FinalApproval, String> {
	
	@Override
	public <S extends FinalApproval> S save(S entity);
	List<FinalApproval> findByDocumentId(int DocId);
   FinalApproval findByDocumentIdAndPlantIdAndProposalSequenceNumber(int DocId,int plantId, int proposalSeqNo);

}
