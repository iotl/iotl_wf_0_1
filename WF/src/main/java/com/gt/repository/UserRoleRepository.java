package com.gt.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
//import org.aspectj.apache.bcel.util.Repository;
import org.springframework.data.repository.Repository;

import com.gt.entity.UserRoles;

public interface UserRoleRepository extends Repository<UserRoles, String> {
	
	
	 ArrayList<UserRoles> findAll();
	 
	 ArrayList<UserRoles> findByUserId(int userId); //int id
     ArrayList<UserRoles> findByRoleId(int roleId);
	 
     UserRoles save(UserRoles userRole);
     
     Integer deleteByUserId(int userId);
     
   


}
 