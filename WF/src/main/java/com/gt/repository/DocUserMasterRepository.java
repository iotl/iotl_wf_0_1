package com.gt.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.gt.entity.DocUserMaster;

public interface DocUserMasterRepository extends CrudRepository<DocUserMaster, String> {

	List<DocUserMaster> findAll();
	List<DocUserMaster> findByDocumentId(int documentId);
	List<DocUserMaster> findByDocumentIdAndDocRoleNotIn(Integer documentId,Collection<String> docRole);
	@Override
	public <S extends DocUserMaster> S save(S entity);
	DocUserMaster findByDocumentIdAndUserId(Integer documentId, Integer userId);
//	DocUserMaster findBySerialNumberAndDocumentId(int actionStatus, Integer documentId);
	List<DocUserMaster> findBySerialNumberAndDocumentId(Integer srNo, Integer documentId);
	DocUserMaster findByDocumentIdAndDocRole(Integer documentId, String string);
	List<DocUserMaster> findByDocumentIdOrderBySerialNumberAsc(int documentId);
	Integer deleteByUserIdAndDocumentId(Integer userId,Integer documentId);
	List<DocUserMaster> findByDocumentIdOrderBySerialNumberDesc(int documentId);
	Integer deleteAllByDocRoleAndDocumentId(String docRole,Integer documentId);
	Integer deleteByDocumentIdAndDocRoleIn(Integer documentId,Collection<String> docRole);
	Integer deleteByDocumentId(Integer documentId);
	List<DocUserMaster> findByDocumentIdAndDocRoleIn(Integer documentId,Collection<String> docRole);
	List<DocUserMaster> findByDocumentIdAndDocRoleInAndSerialNumberIsNotNullOrderBySerialNumberAsc(Integer documentId,Collection<String> docRole);
	List<DocUserMaster> findByDocumentIdAndSerialNumberIsNull(Integer documentId);
	List<DocUserMaster> findByDocumentIdAndSerialNumberIsNotNullOrderBySerialNumberDesc(int documentId);
	List<DocUserMaster> findByDocumentIdAndSerialNumberIsNotNullOrderBySerialNumberAsc(int documentId);
	Integer deleteByDocumentIdAndUserIdIn(Integer documentId,Collection<Integer> userId);
	
	@Query("select dm from DocUserMaster dm,UserRoles ur,Users usr WHERE dm.documentId=:documentId AND dm.userId=usr.userId AND ur.userId=usr.userId AND ur.roleId=:roleId")
	List<DocUserMaster> isControllerPresent(@Param("documentId") Integer documentId,@Param("roleId")Integer roleId);
	
	List<DocUserMaster> findByUserId(int userId);

}
