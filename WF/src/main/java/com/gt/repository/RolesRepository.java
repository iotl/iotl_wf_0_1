package com.gt.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import com.gt.entity.Role;

public interface RolesRepository extends Repository<Role, String> {

	ArrayList<Role> findAll();

	Role findByRoleId(int roleId);

	List<Role> findByRoleName(String rolename);

	Role save(Role role);
	
	@Query("select max(t.roleId) from Role t")
	Integer findMaxRoleId();

}
