package com.gt.repository;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gt.entity.DocumentMaster;


public interface DocumentMasterRepository  extends JpaRepository<DocumentMaster,String>,
	JpaSpecificationExecutor<DocumentMaster> {  
	    @Query("select coalesce(max(dm.seqNo),0)+1 from DocumentMaster dm WHERE dm.originalDocId=:originalDocId")
	    Integer getMaxSeqNo(@Param("originalDocId") Integer originalDocId);


	
	List<DocumentMaster> findAll();
//	DocumentMaster findByDocumentId(int DocumentId);
	DocumentMaster findByDocumentId(Integer DocumentId);
@Override
	public <S extends DocumentMaster> S save(S entity);

List<DocumentMaster> findByCreatedBy(int userId);
List<DocumentMaster> findByCreatedByOrderByCreatedTimeStampDesc(int userId);
List<DocumentMaster> findByCreatedByAndStatusOrderByCreatedTimeStampDesc(int userId,String status);
List<DocumentMaster> findByCreatedByAndStatusOrderByModifiedTimeStampDesc(int userId,String status);
List<DocumentMaster> findByDocumentIdAndStatus(Integer documentId,String status);

List<DocumentMaster> findByOriginalDocId(Integer docId);
List<DocumentMaster> findByOriginalProposalDocId(Integer originalProposalDocId);
List<DocumentMaster> findByClusterIdIsNullOrClusterIdAndDeptId(Integer clusterId,Integer deptId);
List<DocumentMaster> findByClusterId(Integer clusterId);
@Query("select dm from DocumentMaster dm WHERE dm.deptId=:deptId AND (dm.clusterId=:clusterId OR clusterId IS NULL)")
List<DocumentMaster> getDocListByDeptIdAndClusterId(@Param("deptId") Integer deptId,@Param("clusterId")Integer clusterId);

@Query("select dm from DocumentMaster dm WHERE (dm.deptId=:deptId OR dm.deptId is NULL) AND (dm.clusterId=:clusterId OR clusterId IS NULL)")
List<DocumentMaster> getDocListByDeptIdIsNullAndClusterId(@Param("deptId") Integer deptId,@Param("clusterId")Integer clusterId);

List<DocumentMaster> findByClusterIdIsNullOrClusterId(Integer clusterId);

@Query("select count(dm) from DocumentMaster dm where dm.companyId=:CompanyId")
int findDocumentMasterCountByCompanyId(@Param("CompanyId") int companyId);

@Query("select count(dm) from DocumentMaster dm where dm.businessId=:BusinessId")
int findDocumentMasterCountByBusinessId(@Param("BusinessId") int businessId);

@Query("select count(dm) from DocumentMaster dm where dm.sbuId=:SbuId")
int findDocumentMasterCountBySbuId(@Param("SbuId") int sbuId);

@Query("select count(dm) from DocumentMaster dm where dm.plantId=:PlantId")
int findDocumentMasterCountByPlantId(@Param("PlantId") int plantId);


List<DocumentMaster> findByStatusIn(Collection<String> status);

@Query("select dm from DocumentMaster dm WHERE dm.status in :status AND (dm.deptId!=:deptId OR dm.deptId is NULL)")
List<DocumentMaster> findByStatusInAndDeptIdNot(@Param("status")Collection<String> status, @Param("deptId") Integer deptId);

@Query("select dm from DocumentMaster dm WHERE dm.status in :status AND (dm.deptId in :deptId OR dm.deptId is NULL) AND dm.initiatorPlantId is NULL")
List<DocumentMaster> findByStatusInAndDeptIdInAndInitiatorPlantIdIsNUll(@Param("status")Collection<String> status, @Param("deptId")Collection<Integer> deptId);

//List<DocumentMaster> findByStatusInAndDeptIdNot(Collection<String> status,Integer deptId);

/*List<DocumentMaster> findByCompanyIdAndBusinessIdAndSbuIdAndPlantIdAndMaterialIdAndIsRelatedPartyAndStatusIn(Integer companyId,Integer businessId,Integer sbuId,Integer plantId ,Integer materialId, Boolean relParty, Collection<String> status);

List<DocumentMaster> findByCompanyIdAndBusinessIdAndSbuIdAndPlantIdAndMaterialIdAndIsRelatedPartyAndStatusInAndDocumentIdIn(Integer companyId,Integer businessId,Integer sbuId,Integer plantId ,Integer materialId, Boolean relParty, Collection<String> status,Collection<Integer> documentId);

List<DocumentMaster> findByIsRelatedPartyAndStatusIn( Boolean relParty, Collection<String> status);

List<DocumentMaster> findByIsRelatedPartyAndStatusInAndDocumentIdIn( Boolean relParty, Collection<String> status,Collection<Integer> documentId);

List<DocumentMaster> findByMaterialIdAndIsRelatedPartyAndStatusInAndDocumentIdIn(Integer materialId, Boolean relParty, Collection<String> status,Collection<Integer> documentId);

List<DocumentMaster> findByMaterialIdAndIsRelatedPartyAndStatusIn(Integer materialId, Boolean relParty, Collection<String> status);

List<DocumentMaster> findByMaterialIdAndPlantIdAndIsRelatedPartyAndStatusIn( Integer materialId ,Integer plantId,Boolean relParty, Collection<String> status);

List<DocumentMaster> findByMaterialIdAndPlantIdAndIsRelatedPartyAndStatusInAndDocumentIdIn(Integer materialId ,Integer plantId, Boolean relParty, Collection<String> status,Collection<Integer> documentId);

List<DocumentMaster> findByMaterialIdAndPlantIdAndSbuIdAndIsRelatedPartyAndStatusInAndDocumentIdIn(Integer materialId,Integer plantId,Integer sbuId, Boolean relParty, Collection<String> status,Collection<Integer> documentId);

List<DocumentMaster> findByMaterialIdAndPlantIdAndSbuIdAndIsRelatedPartyAndStatusIn(Integer materialId,Integer plantId,Integer sbuId, Boolean relParty, Collection<String> status);

List<DocumentMaster> findByBusinessIdAndSbuIdAndPlantIdAndMaterialIdAndIsRelatedPartyAndStatusIn(Integer businessId,Integer sbuId, Integer plantId ,Integer materialId,Boolean relParty, Collection<String> status);
*/
List<DocumentMaster> findByCompanyIdInAndBusinessIdInAndSbuIdInAndPlantIdInAndMaterialIdInAndIsRelatedPartyInAndStatusInAndCreatedTimeStampBetween(Collection<Integer> companyId,Collection<Integer> businessId,Collection<Integer> sbuId,Collection<Integer> plantId ,Collection<Integer> materialId, Collection<Boolean> relParty, Collection<String> status,Date fromDate,Date toDate);

/*List<DocumentMaster> findByBusinessIdAndSbuIdAndPlantIdAndMaterialIdAndIsRelatedPartyAndStatusInAndDocumentIdIn(Integer businessId,Integer sbuId, Integer plantId ,Integer materialId, Boolean relParty, Collection<String> status,Collection<Integer> documentId);*/

List<DocumentMaster> findByTransportationJustification(String rpNumber);
	}

