package com.gt.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gt.entity.Delegation;

public interface DelegationRepository extends CrudRepository<Delegation, String>{

	ArrayList<Delegation> findAll();
	Delegation findByDelegationId(int delegationId);
	List<Delegation> findByDelegateUserId(int delegateUserId);
	List<Delegation> findByUserId(Integer userId);
	@Override
	public <S extends Delegation> S save(S entity);
	List<Delegation> findByToDateBeforeAndIsActive(Date toDate,boolean isActive);
	List<Delegation> findByUserIdAndIsActive(Integer userId,boolean isActive);
	List<Delegation> findByUserIdAndIsActiveOrderByToDateAsc(Integer userId,boolean isActive);
	List<Delegation> findByDelegateUserIdAndIsActive(Integer userId,boolean isActive);
	
}

