package com.gt.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import com.gt.entity.BusinessCompany;
import com.gt.entity.Businesses;

public interface MasterBusinessDataRepository extends Repository<Businesses, BusinessCompany> {
	
	@Query("SELECT  b FROM Businesses b group by b.businessId")
	List<Businesses> findAll();
	
	@Query("SELECT  b FROM Businesses b where b.businessId = :businessId group by b.businessId")
	Businesses findByBusinessId(@Param("businessId")int businessId);
	
	List<Businesses> findByCompanyId(int companyId);
	
	
	public <S extends Businesses> S save(S entity);
	@Query("select max(bb.businessId) from Businesses bb")
    Integer findMaxBusinessId();
	
	@Query("select bs from Businesses bs where bs.businessName = :businessName and bs.abbreviation = :abbreviation group by bs.businessId")
	Businesses findByBusinessNameAndAbbreviation(@Param("businessName")String businessName,@Param("abbreviation")String abbreviation);
	
	void delete(Businesses entity); 
	
	@Query("select bs from Businesses bs where bs.companyId!= :companyId and bs.businessId NOT IN :businessIdList group by bs.businessId")
	List<Businesses> findbyCompanyIdNotInAndBusinessIdNotInBusinessCollection(@Param("companyId")int companyId,@Param("businessIdList")List<Integer> businessIdList);
	
	@Modifying
	@Query("update Businesses bs set bs.businessName = ?1 , bs.abbreviation= ?2 where bs.businessId= ?3")
	int updateBusinessNameByBusinessId(String businessName,String abbreviation,int businessId);
	
	Integer countByBusinessId(int businessId);
	
	@Query("select bb.businessId from Businesses bb group by bb.businessId")
    List<Integer> findAllBusinessId();
	
	@Query("SELECT  b.businessId FROM Businesses b where b.companyId = :companyId group by b.businessId")
	List<Integer> findAllBusinessIdByCompanyId(@Param("companyId")int companyId);
	
	
	
	@Query("SELECT  b.businessId FROM Businesses b where b.companyId in :companyIds group by b.businessId")
	List<Integer> findAllBusinessIdByCompanyIdIn(@Param("companyIds")Collection<Integer> companyIds);
	
	@Query("SELECT  b FROM Businesses b where b.companyId in :companyIds group by b.businessId")
	List<Businesses> findAllBusinessByCompanyIdIn(@Param("companyIds")Collection<Integer> companyIds);

	
	
	
}