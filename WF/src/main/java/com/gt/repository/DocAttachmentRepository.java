package com.gt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import com.gt.entity.DocAttachment;
import com.gt.entity.DocumentMaster;

public interface DocAttachmentRepository extends  JpaRepository<DocAttachment,String>,
JpaSpecificationExecutor<DocAttachment> {  
	
	 @Query("select coalesce(max(dm.pAttachmentId),0)+1 from DocAttachment dm")
	    Integer getMaxId();

	  //  DocAttachment save(DocAttachment docAttachment);
	 
	 @Override
		public <S extends DocAttachment> S save(S entity);
	    
	    DocAttachment findByPAttachmentId(int id);
	    
	    List<DocAttachment> findByCommentId(int commentId);
	    
	    Integer deleteByFileNameAndCommentId(String fileName,Integer commentId); 
	    
	    DocAttachment findByFileNameAndCommentId(String fileName,Integer commentId); 
	    
	    List<DocAttachment> findByDocumentIdOrderByCommentIdAsc(int documentId);
}

		