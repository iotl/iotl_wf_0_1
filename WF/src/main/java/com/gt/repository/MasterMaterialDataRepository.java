package com.gt.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.gt.entity.Material;

public interface MasterMaterialDataRepository extends CrudRepository<Material,String> {
	List<Material> findAll();
	Material findByMaterialId(int materialId);
	@Override
	public <S extends Material> S save(S entity);
	
	List<Material> findByBusinessId(Integer businessId);

	Integer deleteByMaterialId(Integer materialId);
	
	List<Material> findByBusinessIdIsNull();
	List<Material> findByClusterId(Integer clusterid);
	
	List<Material> findByBusinessIdAndDeptId(Integer businessId,Integer deptId);
	
	List<Material> findByBusinessIdAndDeptIdIsNull(Integer businessId);
	
	List<Material> findByAbbreviationAndMaterialIdNotIn(String abbreviation,Integer materialId);
	List<Material> findByMaterialNameAndMaterialIdNotIn(String materialname,Integer materialId);
//	 @Query("SELECT mat FROM Material m where m.Material_Name=:materialName and m.Material_Id !=: materialId")
//	 List<Material> isMaterialNamePresent(@Param("materialName") String materialName,@Param("materialId") Integer materialId);
//	 
//	 @Query("SELECT mat FROM Material m where m.Abbreviation=:abbreviation and m.Material_Id !=: materialId")
//	 List<Material> isMaterialAbbreviationPresent(@Param("abbreviation") String abbreviation,@Param("materialId") Integer materialId); 
	List<Material> findByBusinessIdAndDeptIdAndIsActive(Integer businessId, Integer deptId, Boolean isActive);
	List<Material> findByDeptIdAndIsActive(Integer deptId, Boolean isActive);
	
	List<Material> findByBusinessIdInAndDeptIdAndIsActive(Collection<Integer> businessId, Integer deptId, Boolean isActive);
	List<Material> findByBusinessIdInAndDeptIdIsNull(Collection<Integer> businessId);
	
	List<Material> findByBusinessIdIn(Collection<Integer> businessId);
	
	@Query("SELECT  m.materialId FROM Material m where m.businessId in :businessIds and m.deptId = 0 group by m.materialId")
	List<Integer> findAllMaterialIdByBusinessIdIn(@Param("businessIds")Collection<Integer> businessIds);
	
	@Query("SELECT  m.materialId FROM Material m where m.businessId in :businessIds and m.deptId != 1 group by m.materialId")
	List<Integer> findAllMaterialIdByBusinessIdInAndDeptIdNotEqualtoOne(@Param("businessIds")Collection<Integer> businessIds);
	
	List<Material> findByClusterIdIn(Collection<Integer> clusterId);
}
