package com.gt.dao;

import java.io.File;



import java.util.List;

import com.gt.entity.Comments;
import com.gt.entity.DocAttachment;

public interface DocAttachDao {

	public boolean addEntity(DocAttachment docAttachment,File file) throws Exception; 
	public byte[]  getEntityById(Integer p_Attachment_Id) throws Exception;
	public DocAttachment getEntityObjById(Integer p_Attachment_Id)			throws Exception ;
	public Comments saveComments(Comments commentEty);
	public Comments findByActionId(Integer action_id) ;
	public List<Comments> findByDocumentId(Integer document_id);
}
