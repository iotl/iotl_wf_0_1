package com.gt.dao;

import java.util.List;

 
import com.gt.entity.Wf_User;

public interface Wf_UserDao {

	public boolean addEntity(Wf_User wf_User) throws Exception;
	public Wf_User getEntityById(Integer userid) throws Exception;
	public boolean editEntityById(Wf_User wf_User) throws Exception;
	public List<Wf_User> getEntityList() throws Exception;
	public boolean deleteEntity(Integer userid) throws Exception;
}
