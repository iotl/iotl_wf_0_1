package com.gt.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gt.entity.Users;

 
@Component("userdao")
public class UserDaoImpl  implements UserDao {
	@Autowired
	SessionFactory sessionFactory;

	Session session = null;
	Transaction tx = null;
	@Override
	public boolean addEntity(Users wf_User) throws Exception {
		 
		session = sessionFactory.openSession();
		tx = session.beginTransaction();
		session.save(wf_User);
		tx.commit();
		session.close();

		return false;
	}

	@Override
	public Users getEntityById(Integer userid) throws Exception {
		 
		session = sessionFactory.openSession();
	 
		Users wf_user = (Users) session.get(Users.class, userid);
		tx = session.getTransaction();
		session.beginTransaction();
		//tx.commit();
		session.close();
		return wf_user;
	}

	@Override
	public boolean editEntityById(Users user) throws Exception {
		session = sessionFactory.openSession();
		/*User userObj = (User) session.load(User.class, new Integer(user.getUserid()));
		userObj = user;*/
		session.update(user);	
		tx = session.getTransaction();
		session.beginTransaction();
		tx.commit();
		
		return false;
	}

	
 

	@Override
	public List<Users> getEntityList() throws Exception {
		System.out.println("inside list");
		session = sessionFactory.openSession();
		tx = session.beginTransaction();
		List<Users> userList = session.createCriteria(Users.class).list();
		/*for (Iterator<User> iterator = userList.iterator(); iterator.hasNext();)
		{
			User user = userList.iterator<User>();
			Roles roles = user.getUserRoles();
		}*/
		tx.commit();
		session.close();
		return userList;
	}

	@Override
	public boolean deleteEntity(Integer userid) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

}
