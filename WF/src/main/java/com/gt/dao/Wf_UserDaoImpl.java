package com.gt.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.gt.entity.Wf_User;
@Component("wf_UserDao")
@Transactional
public class Wf_UserDaoImpl implements Wf_UserDao {
	
	@Autowired
	SessionFactory sessionFactory;

	Session session = null;
	Transaction tx = null;
	@Override
	public boolean addEntity(Wf_User wf_User) throws Exception {
		 
		session = sessionFactory.openSession();
		tx = session.beginTransaction();
		session.save(wf_User);
		tx.commit();
		session.close();

		return false;
	}

	@Override
	public Wf_User getEntityById(Integer userid) throws Exception {
		 
		session = sessionFactory.openSession();
	 
		Wf_User wf_user = (Wf_User) session.get(Wf_User.class, userid);
		tx = session.getTransaction();
		session.beginTransaction();
		//tx.commit();
		session.close();
		return wf_user;
	}

	@Override
	public boolean editEntityById(Wf_User user) throws Exception {
		session = sessionFactory.openSession();
		/*User userObj = (User) session.load(User.class, new Integer(user.getUserid()));
		userObj = user;*/
		session.update(user);	
		tx = session.getTransaction();
		session.beginTransaction();
		tx.commit();
		
		return false;
	}

	
 

	@Override
	public List<Wf_User> getEntityList() throws Exception {
		 
		session = sessionFactory.openSession();
		tx = session.beginTransaction();
		List<Wf_User> userList = session.createCriteria(Wf_User.class).list();
		/*for (Iterator<User> iterator = userList.iterator(); iterator.hasNext();)
		{
			User user = userList.iterator<User>();
			Roles roles = user.getUserRoles();
		}*/
		tx.commit();
		session.close();
		return userList;
	}

	@Override
	public boolean deleteEntity(Integer userid) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

}
