package com.gt.dao;
import java.io.File;
import java.io.FileInputStream;
import java.math.BigInteger;
import java.sql.Blob;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gt.entity.Comments;
import com.gt.entity.DocAttachment;

@Component("docattachdao")
public class DocAttachDaoImpl implements DocAttachDao {


	@Autowired
	SessionFactory sessionFactory;

	Session session = null;
	Transaction tx = null;
	DocAttachment docAttachment = new DocAttachment();

	@Override
	public boolean addEntity(DocAttachment docAttachment,File file) throws Exception 
	{
		
		session = sessionFactory.openSession();
		tx = session.beginTransaction();
		//S//tring FileWithFullPath = docAttachment.getFile_Name() + "." + docAttachment.getAttachment_Type(); 
		//File file = new File(FileWithFullPath);
		FileInputStream inputStream = new FileInputStream(file);
		Blob blob = Hibernate.getLobCreator(session).createBlob(inputStream, file.length());
		System.out.println("doc attach imp ");
		
			docAttachment.setContent(blob);
			//String fileName = FileWithFullPath.substring(FileWithFullPath.lastIndexOf("/") + 1);
//			docAttachment.setFileName(docAttachment.getFileName());
			session.save(docAttachment);
			tx.commit();
			session.close();
			blob.free();
		
		System.out.println("document uploaded");
		return false;
	}
	@Override	
	public byte[]  getEntityById(Integer p_Attachment_Id) throws Exception
	{
		session = sessionFactory.openSession();
		//tx = session.beginTransaction();
		DocAttachment docAttachment = new DocAttachment();
		
		docAttachment = (DocAttachment) session.get(DocAttachment.class, p_Attachment_Id);
		Blob blob = docAttachment.getContent();
		byte[] blobByte = blob.getBytes(1, (int) blob.length());
		//String path = "E://testDownload//" + docAttachment.getFile_Name();
		
		// outputStream;
		byte[] buff = blob.getBytes(1, (int)blob.length());
		//tx.c
		session.close();
		
		return buff;
	}
	@Override
	public DocAttachment getEntityObjById(Integer p_Attachment_Id)
			throws Exception {
		// TODO Auto-generated method stub
		session = sessionFactory.openSession();
		tx = session.beginTransaction();
		DocAttachment docAttachment = new DocAttachment();
		
		docAttachment = (DocAttachment) session.get(DocAttachment.class, p_Attachment_Id);
		return docAttachment;
	}
	
	@Override
	public Comments saveComments(Comments comments) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.save(comments);
		tx.commit();
		Long lastId = ((BigInteger) session.createSQLQuery("SELECT LAST_INSERT_ID()").uniqueResult()).longValue();
		comments.setComment_id(lastId.intValue());
		session.close();
		return comments;
	}
	
	@Override
	public Comments findByActionId(Integer action_id)
			{
		// TODO Auto-generated method stub
		session = sessionFactory.openSession();
		tx = session.beginTransaction();
		Comments comments = new Comments();
		String sql = "from Comments where action_id = :action_id";
		Query q = session.createQuery(sql);
		q.setString("action_id", String.valueOf(action_id));
		if(q!=null && q.list()!=null && q.list().size()>0){
		comments = (Comments) q.list().get(0);
		}
		return comments;
	}
	
	
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Comments> findByDocumentId(Integer document_id)
			{
		List<Comments> commentsList =null;
		// TODO Auto-generated method stub
		session = sessionFactory.openSession();
		tx = session.beginTransaction();
		String sql = "from Comments where Document_id =:document_id";
		Query q = session.createQuery(sql);
		q.setString("action_id", String.valueOf(document_id));
		
		if(q!=null && q.list()!=null && q.list().size()>0){
			 commentsList =q.list();
		}
		return commentsList;
	}
	
	
}
