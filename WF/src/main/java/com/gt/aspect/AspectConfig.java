package com.gt.aspect;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy
public class AspectConfig {
	private final static Logger LOGGER = Logger
			.getLogger(AspectConfig.class.getName());
    @Bean
    public LoggingAspect myAspect() {
    	LOGGER.info("<<<< Inside LoggingAspect >>>>");
        return new LoggingAspect();
    }
}