package com.gt.aspect;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;

import com.gt.utils.PropsUtil;

@Aspect
public class LoggingAspect {
	
	private final static Logger LOGGER = Logger
			.getLogger(LoggingAspect.class.getName());
	
	private static String logAfter="false";
	private static String logAfterReturning="false";
	static{
		logAfter=PropsUtil.getValue("log.after");
		logAfterReturning=PropsUtil.getValue("log.after.returning");
	}
	
	
	@After("within(com.gt.services.* || com.gt.utility.* || com.controllers.*)")
	public void logAfter(JoinPoint joinPoint) {
		if("true".equals(logAfter)){
		LOGGER.info("logAfter() is running!");
		LOGGER.info("<<<< Method Name >>>>>" + joinPoint.getSignature().getName());
		LOGGER.info("<<<< Class And Package Name >>>>>" + joinPoint.getTarget().getClass());
		}
 
	}
	
	@AfterReturning(
		      pointcut = "within(com.gt.services.* || com.gt.utility.* || com.controllers.*)",
		      returning= "result")
		   public void logAfterReturning(JoinPoint joinPoint, Object result) {
		if("true".equals(logAfterReturning)){
			LOGGER.info("logAfterReturning() is running!");
			LOGGER.info("<<<< Method Name >>>>>" + joinPoint.getSignature().getName());
			LOGGER.info("<<<< Class And Package Name >>>>>" + joinPoint.getTarget().getClass());
			LOGGER.info("<<<<  Method returned value is  >>>>>: " + result);
		}
		   }
	
	@AfterThrowing(
			  pointcut = "within(com.gt.services.* || com.gt.utility.* || com.controllers.*)",
		      throwing= "error")
		    public void logAfterThrowing(JoinPoint joinPoint, Throwable error) {
		 
			LOGGER.fatal("logAfterThrowing() is running!");
			LOGGER.fatal("<<<< Method Name >>>>>" + joinPoint.getSignature().getName());
			LOGGER.info("<<<< Class And Package Name >>>>>" + joinPoint.getTarget().getClass());
			LOGGER.fatal("<<<< Exception  >>>>> " + error);
			StringWriter stack = new StringWriter();
			error.printStackTrace(new PrintWriter(stack));
			LOGGER.fatal("Caught exception; decorating with appropriate status template : " + stack.toString());
		    }
		 
 
    /*  @Before("allMethodsPointcut()")
    public void allServiceMethodsAdvice(){
    	LOGGER.info("Before executing service method");
    }*/
     
    //Pointcut to execute on all the methods of classes in a package
   /* @Pointcut("within(com.gt.services.*)")
    public void allMethodsPointcut(){
    	LOGGER.info("Pointcut");
    }*/
     
}
