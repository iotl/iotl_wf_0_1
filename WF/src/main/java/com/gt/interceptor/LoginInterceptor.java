/**
 * 
 */
package com.gt.interceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.security.crypto.codec.Utf8;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.gt.entity.Users;
import com.gt.exception.NotSignedInException;
import com.gt.services.Wf_UserService;

/**
 * @author 
 * 
 */
public class LoginInterceptor implements HandlerInterceptor {

	/**
	 * 
	 */

	private final static Logger LOGGER = Logger
			.getLogger(LoginInterceptor.class.getName());
	
	@Autowired
	Wf_UserService wf_UserService;


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.web.servlet.HandlerInterceptor#afterCompletion(javax
	 * .servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * java.lang.Object, java.lang.Exception)
	 */
	@Override
	public void afterCompletion(HttpServletRequest arg0,
			HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// localInfo("<<<<< afterCompletion >>>>");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.web.servlet.HandlerInterceptor#postHandle(javax.servlet
	 * .http.HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * java.lang.Object, org.springframework.web.servlet.ModelAndView)
	 */
	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1,
			Object arg2, ModelAndView arg3) throws Exception {
		// localInfo("<<<<< postHandle >>>>");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.web.servlet.HandlerInterceptor#preHandle(javax.servlet
	 * .http.HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * java.lang.Object)
	 */
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object arg2) throws Exception {
//		response.setHeader("Access-Control-Allow-Origin", "*");
//	    response.setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
//	    response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
//	    response.setHeader("Access-Control-Max-Age", "3600");
		localInfo("<<<< Is Logged In >>>>"+isLoggedIn(request));
		if (!isLoggedIn(request)) {
			String requestedURI = request.getRequestURI();
			String requestedURL = request.getRequestURL().toString();
			
//			localInfo("<<<< requestedURI >>>> " + requestedURI); 
//			localInfo("<<<< requestedURL >>>> " + requestedURL); 
		//	localInfo("<<<< requestedURI.endsWith(NotSigned) >>>> " + requestedURI.endsWith("login")); 
			//arg1.sendRedirect("http://localhost:8080/loginC/NotSigned");
			if(!requestedURI.endsWith("login") && !requestedURI.endsWith("ForgetPassword") && !requestedURI.endsWith("resetPassword") && !requestedURI.endsWith("ForgetPasslogin")){
			throw new NotSignedInException();
			}
		}
		return true;
	}

	/**
	 * @param request
	 * @return
	 */
	private boolean isLoggedIn(HttpServletRequest request) {
		try {
			String userId=request.getParameter("userId");	
			if(userId!=null && !userId.isEmpty()){
				userId=userId.indexOf("#") == -1 ? userId :userId.substring(0, userId.indexOf("#"));
				//System.out.println(userId);
			}
			Cookie[] cookies=request.getCookies();
//			System.out.println("cookies"+cookies);
			if(cookies!=null){
				for (int i = 0; i < cookies.length; i++) {
					  String name = cookies[i].getName();
					  String value = cookies[i].getValue();
//					  System.out.println(name);
//					  System.out.println(value);
					  if("name".equals(name)){
						String authorizationKey=b64decode(value).toString();
//						System.out.println(authorizationKey);
						 Users user=wf_UserService.getEntityByAuthorizationKey(authorizationKey);
						 if(user!=null){
							 if(userId!=null && !userId.isEmpty()){
								 int userIdInt=Integer.parseInt(userId);
//								 System.out.println("fdgdfg"+userIdInt);
//								 System.out.println("fdgg"+user.getUserId());
								 if(userIdInt==user.getUserId()){
									 return true; 
								 }else{
									 return false; 
								 }
							 }else{
								 return true; 
							 }
							 
						 }
					  }
					}
			}else{
				String name =request.getParameter("name");
				if(name!=null && !name.isEmpty()){
				String authorizationKey=b64decode(name).toString();
//				System.out.println(authorizationKey);
				 Users user=wf_UserService.getEntityByAuthorizationKey(authorizationKey);
				 if(user!=null){
					 if(userId!=null && !userId.isEmpty()){
						 int userIdInt=Integer.parseInt(userId);
//						 System.out.println("fdgdfg"+userIdInt);
//						 System.out.println("fdgg"+user.getUserId());
						 if(userIdInt==user.getUserId()){
							 return true; 
						 }else{
							 return false; 
						 }
					 }else{
						 return true; 
					 }
					 
				 }
				}
			}
			
		} catch (Exception e) {
			localFatal("Exception at isLoggedIn(1) >>>> " + e.toString());
			e.printStackTrace();
		}
		return false;
	}
	
	private static String b64decode(String a) {
		return Utf8.decode(Base64.decode(a.getBytes()));

	}
	
	

	/**
	 * @return the wf_UserService
	 */
	public Wf_UserService getWf_UserService() {
		return wf_UserService;
	}

	/**
	 * @param wf_UserService the wf_UserService to set
	 */
	public void setWf_UserService(Wf_UserService wf_UserService) {
		this.wf_UserService = wf_UserService;
	}

	private static void localInfo(String str) {
		LOGGER.info(str);
	}

	private static void localFatal(String str) {
		LOGGER.fatal(str);
	}

}
