/**
 * 
 */
package com.gt.exception;

public class NotSignedInException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8713484654672010724L;

	/**
	 * 
	 */
	public NotSignedInException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public NotSignedInException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public NotSignedInException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public NotSignedInException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public NotSignedInException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
