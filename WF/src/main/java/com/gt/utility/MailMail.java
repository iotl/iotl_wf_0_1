package com.gt.utility;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import com.controllers.Status;
import com.gt.utils.PropsUtil;

@Component("mailmail")
public class MailMail {
	public static final Logger logger = Logger.getLogger(MailMail.class
			.getName());

	private final JavaMailSender javaMailSender;

	@Autowired
	MailMail(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
	}

	public void sendMail(String from, String to, String subject, String msg) {

		SimpleMailMessage message = new SimpleMailMessage();

		message.setFrom(from);
		message.setTo(to);
		message.setSubject(subject);
		message.setText(msg);
		javaMailSender.send(message);
	}

	public Status sendHtmlMail(String from, String to, String cc,
			String subject, String msg, String mailHighPriority) {

		MimeMessage message = javaMailSender.createMimeMessage();

		MimeMessageHelper helper = new MimeMessageHelper(message);
		try {

			System.out.println(to);
			System.out.println(cc);
			//to = "vigneshraja23@gmail.com";
			//cc = "vigneshraja23@gmail.com";
			InternetAddress[] toArr = InternetAddress.parse(to);

			message.setFrom(new InternetAddress(
					"hil.notification@adityabirla.com"));
			message.setRecipients(Message.RecipientType.TO, toArr);
			if (cc != null) {
				InternetAddress[] ccArr = InternetAddress.parse(cc);
				message.setRecipients(Message.RecipientType.CC, ccArr);
			}

			message.setSubject(subject);
			message.setHeader("X-Priority", mailHighPriority);
			helper.setText(msg);
			message.setContent(msg, "text/html");
			try {
				if (logger.isTraceEnabled()) {
					logger.trace("From " + message.getFrom());
					logger.trace("To "
							+ message.getRecipients(Message.RecipientType.TO));
					logger.trace("Cc "
							+ message.getRecipients(Message.RecipientType.CC));
					logger.trace("Subject " + message.getSubject());
					logger.trace("message == "
							+ message.getContent().toString());

				}
			} catch (IOException e) {
				logger.error(e.getStackTrace());
			}
			try {
				javaMailSender.send(message);
			} catch (MailException e) {
				logger.error("Error in Sending Mail " + "to:" + to + "cc:" + cc
						+ "from:" + from + "subject" + subject, e);
				return new Status(-1, "Mail Authentication Failed");
			}
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		return new Status(1, "Success");
	}

	/**
	 * @param subject
	 * @param bodyContent
	 * @param isHTML
	 * @param filePath
	 * @param to
	 * @param cc
	 * @param priority
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public Status sendMailWithAttachment(String subject, String bodyContent,
			boolean isHTML, List<String> filePath, String to, String cc,
			int priority) {

		MimeMessage message = null;

		try {

			//System.out.println(to);
			//System.out.println(cc);
			//to = "vigneshraja23@gmail.com";
			//cc = "vigneshraja23@gmail.com";
			message = javaMailSender.createMimeMessage();

			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom("hil.notification@adityabirla.com");
			InternetAddress[] toArr = InternetAddress.parse(to);
			message.setRecipients(Message.RecipientType.TO, toArr);
			if (cc != null) {
				InternetAddress[] ccArr = InternetAddress.parse(cc);
				message.setRecipients(Message.RecipientType.CC, ccArr);
			}
			/*InternetAddress[] replyToAddress = InternetAddress
					.parse("surendran.friend4u@gmail.com");
			message.setReplyTo(replyToAddress);*/
			helper.setSubject(subject);
			bodyContent = bodyContent == null ? "" : bodyContent.trim();
			if (!bodyContent.isEmpty()) {
				helper.setText(bodyContent, isHTML);
			}
			helper.setPriority(priority);
			for (String fileName : filePath) {
				FileSystemResource file = new FileSystemResource(fileName);
				helper.addAttachment(file.getFilename(), file);
			}
			try {
				javaMailSender.send(message);
				System.out.println("Mail Sent");
			} catch (MailException e) {
				logger.error("Error in Sending Mail " + "to:" + to + "cc:" + cc
						+ "from:" + "subject" + subject, e);
				return new Status(-1, "Mail Authentication Failed");
			}
		} catch (MessagingException e) {
			e.printStackTrace();
		} finally {
			if (filePath != null && filePath.size() > 0) {
				for (String fileName : filePath) {
					File f = new File(fileName);
					if (f != null && f.exists()) {
						f.delete();
					}
				}
			}
		}
		return new Status(1, "Success");
	}

	/**
	 * @param subject
	 * @param bodyContent
	 * @param isHTML
	 * @param filePath
	 * @param to
	 * @param cc
	 * @param priority
	 * @param aliasName
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public Status sendMailWithAttachmentForEndorsers(String subject,
			String bodyContent, boolean isHTML, List<String> filePath,
			String to, String cc, int priority, String aliasName)
			throws UnsupportedEncodingException {

		MimeMessage message = null;

		try {

			System.out.println(to);
			System.out.println(cc);
			//to = "vigneshraja23@gmail.com";
			//cc = "vigneshraja23@gmail.com";
			message = javaMailSender.createMimeMessage();

			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			String fromAddress = PropsUtil.getValue("mail.username");
			helper.setFrom("hil.notification@adityabirla.com");
			message.setFrom(new InternetAddress(fromAddress, aliasName));
			InternetAddress[] toArr = InternetAddress.parse(to);
			message.setRecipients(Message.RecipientType.TO, toArr);
			if (cc != null) {
				InternetAddress[] ccArr = InternetAddress.parse(cc);
				message.setRecipients(Message.RecipientType.CC, ccArr);
			}
			InternetAddress[] replyToAddress = InternetAddress.parse(aliasName);
			message.setReplyTo(replyToAddress);
			helper.setSubject(subject);
			bodyContent = bodyContent == null ? "" : bodyContent.trim();
			if (!bodyContent.isEmpty()) {
				helper.setText(bodyContent, isHTML);
			}
			helper.setPriority(priority);
			for (String fileName : filePath) {
				FileSystemResource file = new FileSystemResource(fileName);
				helper.addAttachment(file.getFilename(), file);
			}
			try {
				javaMailSender.send(message);
				System.out.println("Mail Sent");
			} catch (MailException e) {
				logger.error("Error in Sending Mail " + "to:" + to + "cc:" + cc
						+ "from:" + "subject" + subject, e);
				return new Status(-1, "Mail Authentication Failed");
			}
		} catch (MessagingException e) {
			e.printStackTrace();
		} finally {
			if (filePath != null && filePath.size() > 0) {
				for (String fileName : filePath) {
					File f = new File(fileName);
					if (f != null && f.exists()) {
						f.delete();
					}
				}
			}
		}
		return new Status(1, "Success");
	}

}