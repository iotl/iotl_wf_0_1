package com.gt.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "wf_plant")
public class Plant {

private String plantName;
private String abbreviation;

private Integer clusterId;
Integer SeqNo;


private Integer plantId;


private Integer sbuId;

private Integer businessId;

private Integer companyId;

private Boolean corpLogin;
private Boolean plantLogin;


@Id
private int pkId;

/**
 * @return the plantName
 */
public String getPlantName() {
	return plantName;
}

/**
 * @param plantName the plantName to set
 */
public void setPlantName(String plantName) {
	this.plantName = plantName;
}

/**
 * @return the abbreviation
 */
public String getAbbreviation() {
	return abbreviation;
}

/**
 * @param abbreviation the abbreviation to set
 */
public void setAbbreviation(String abbreviation) {
	this.abbreviation = abbreviation;
}

/**
 * @return the clusterId
 */
public Integer getClusterId() {
	return clusterId;
}

/**
 * @param clusterId the clusterId to set
 */
public void setClusterId(Integer clusterId) {
	this.clusterId = clusterId;
}

/**
 * @return the seqNo
 */
public Integer getSeqNo() {
	return SeqNo;
}

/**
 * @param seqNo the seqNo to set
 */
public void setSeqNo(Integer seqNo) {
	SeqNo = seqNo;
}

/**
 * @return the plantId
 */
public Integer getPlantId() {
	return plantId;
}

/**
 * @param plantId the plantId to set
 */
public void setPlantId(Integer plantId) {
	this.plantId = plantId;
}

/**
 * @return the sbuId
 */
public Integer getSbuId() {
	return sbuId;
}

/**
 * @param sbuId the sbuId to set
 */
public void setSbuId(Integer sbuId) {
	this.sbuId = sbuId;
}

/**
 * @return the businessId
 */
public Integer getBusinessId() {
	return businessId;
}

/**
 * @param businessId the businessId to set
 */
public void setBusinessId(Integer businessId) {
	this.businessId = businessId;
}

/**
 * @return the companyId
 */
public Integer getCompanyId() {
	return companyId;
}

/**
 * @param companyId the companyId to set
 */
public void setCompanyId(Integer companyId) {
	this.companyId = companyId;
}

/**
 * @return the pkId
 */
public int getPkId() {
	return pkId;
}

/**
 * @param pkId the pkId to set
 */
public void setPkId(int pkId) {
	this.pkId = pkId;
}

public Boolean getCorpLogin() {
	return corpLogin;
}

public void setCorpLogin(Boolean corpLogin) {
	this.corpLogin = corpLogin;
}

public Boolean getPlantLogin() {
	return plantLogin;
}

public void setPlantLogin(Boolean plantLogin) {
	this.plantLogin = plantLogin;
}










}
