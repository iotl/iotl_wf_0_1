package com.gt.entity;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
//@NamedQuery(name="Integer.findMaxRoleId",query="select max(t.roleId) from Role t")
@Table(name = "wf_rp_transportation")
@IdClass(RPTransportationKey.class)
public class RpTransportation {
	
	
	/**
	 * 
	 */
	private Integer documentId;
	
	/**
	 * 
	 */
	private Integer plantId;
	
	/**
	 * 
	 */
	@Column(name="PROPOSAL_SEQUENCE_NUMBER")
	private Integer transporterSeqId;
	
	/**
	 * 
	 */
	private Integer transporterId;
	
	/**
	 * 
	 */
	private String transporterName;
	
	/**
	 * 
	 */
	private String modesOfTransport;
	
	/**
	 * 
	 */
	private Double proposedQuantity;
	
	/**
	 * 
	 */
	private String proposedQuantityUnit;
	
	/**
	 * 
	 */
	private Double proposedRate;
	
	/**
	 * 
	 */
	private String proposedRateUnit;
	
	/**
	 * 
	 */
	private Double proposedValue;
	
	/**
	 * 
	 */
	private String proposedValueUnit;
	
	private ArrayList<RpTransportation> transporterList;
	
	private Double averagePnb;
	
	private Double averageFreightRate;
	
	private Double averageProposedRate;
	
	private List<String> docAttachmentList= new ArrayList<String>();
	private String comments;
	private int actionId;
	private int commentId;
	
	private String plantName;
	
	private String remark;
	
	@OneToOne
    private DocumentMaster documentMaster;

	public  RpTransportation(){
		
	}






	/**
	 * @return the transporterName
	 */
	public String getTransporterName() {
		return transporterName;
	}


	@Id
	public Integer getDocumentId() {
		return documentId;
	}



	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}


	@Id
	public Integer getPlantId() {
		return plantId;
	}



	public void setPlantId(Integer plantId) {
		this.plantId = plantId;
	}


	@Id
	@Column(name="PROPOSAL_SEQUENCE_NUMBER")
	public Integer getTransporterSeqId() {
		return transporterSeqId;
	}



	public void setTransporterSeqId(Integer transporterSeqId) {
		this.transporterSeqId = transporterSeqId;
	}


	@Id
	public Integer getTransporterId() {
		return transporterId;
	}



	public void setTransporterId(Integer transporterId) {
		this.transporterId = transporterId;
	}



	/**
	 * @param transporterName the transporterName to set
	 */
	public void setTransporterName(String transporterName) {
		this.transporterName = transporterName;
	}



	/**
	 * @return the modesOfTransport
	 */
	public String getModesOfTransport() {
		return modesOfTransport;
	}



	/**
	 * @param modesOfTransport the modesOfTransport to set
	 */
	public void setModesOfTransport(String modesOfTransport) {
		this.modesOfTransport = modesOfTransport;
	}



	/**
	 * @return the proposedQuantity
	 */
	public Double getProposedQuantity() {
		return proposedQuantity;
	}



	/**
	 * @param proposedQuantity the proposedQuantity to set
	 */
	public void setProposedQuantity(Double proposedQuantity) {
		this.proposedQuantity = proposedQuantity;
	}



	/**
	 * @return the proposedQuantityUnit
	 */
	public String getProposedQuantityUnit() {
		return proposedQuantityUnit;
	}



	/**
	 * @param proposedQuantityUnit the proposedQuantityUnit to set
	 */
	public void setProposedQuantityUnit(String proposedQuantityUnit) {
		this.proposedQuantityUnit = proposedQuantityUnit;
	}



	/**
	 * @return the proposedRate
	 */
	public Double getProposedRate() {
		return proposedRate;
	}



	/**
	 * @param proposedRate the proposedRate to set
	 */
	public void setProposedRate(Double proposedRate) {
		this.proposedRate = proposedRate;
	}



	/**
	 * @return the proposedRateUnit
	 */
	public String getProposedRateUnit() {
		return proposedRateUnit;
	}



	/**
	 * @param proposedRateUnit the proposedRateUnit to set
	 */
	public void setProposedRateUnit(String proposedRateUnit) {
		this.proposedRateUnit = proposedRateUnit;
	}



	/**
	 * @return the proposedValue
	 */
	public Double getProposedValue() {
		return proposedValue;
	}



	/**
	 * @param proposedValue the proposedValue to set
	 */
	public void setProposedValue(Double proposedValue) {
		this.proposedValue = proposedValue;
	}



	/**
	 * @return the proposedValueUnit
	 */
	public String getProposedValueUnit() {
		return proposedValueUnit;
	}



	/**
	 * @param proposedValueUnit the proposedValueUnit to set
	 */
	public void setProposedValueUnit(String proposedValueUnit) {
		this.proposedValueUnit = proposedValueUnit;
	}



	/**
	 * @return the transporterList
	 */
	@Transient
	public ArrayList<RpTransportation> getTransporterList() {
		return transporterList;
	}



	/**
	 * @param transporterList the transporterList to set
	 */
	public void setTransporterList(ArrayList<RpTransportation> transporterList) {
		this.transporterList = transporterList;
	}






	public Double getAveragePnb() {
		return averagePnb;
	}






	public void setAveragePnb(Double averagePnb) {
		this.averagePnb = averagePnb;
	}






	public Double getAverageFreightRate() {
		return averageFreightRate;
	}






	public void setAverageFreightRate(Double averageFreightRate) {
		this.averageFreightRate = averageFreightRate;
	}






	public Double getAverageProposedRate() {
		return averageProposedRate;
	}






	public void setAverageProposedRate(Double averageProposedRate) {
		this.averageProposedRate = averageProposedRate;
	}





	@Transient
	public List<String> getDocAttachmentList() {
		return docAttachmentList;
	}






	public void setDocAttachmentList(List<String> docAttachmentList) {
		this.docAttachmentList = docAttachmentList;
	}





	@Transient
	public String getComments() {
		return comments;
	}






	public void setComments(String comments) {
		this.comments = comments;
	}





	@Transient
	public int getActionId() {
		return actionId;
	}






	public void setActionId(int actionId) {
		this.actionId = actionId;
	}





	@Transient
	public int getCommentId() {
		return commentId;
	}






	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}





	@Transient
	public String getPlantName() {
		return plantName;
	}





	
	public void setPlantName(String plantName) {
		this.plantName = plantName;
	}






	public String getRemark() {
		return remark;
	}






	public void setRemark(String remark) {
		this.remark = remark;
	}



	

}
