package com.gt.entity;

import java.io.Serializable;

public class RevenueProposalKey implements Serializable {
	int DocumentId;
	int PlantId;
	
	
	public int getDocumentId() {
		return DocumentId;
	}
	public void setDocumentId(int documentId) {
		DocumentId = documentId;
	}
	public int getPlantId() {
		return PlantId;
	}
	public void setPlantId(int plantId) {
		PlantId = plantId;
	}
	

}
