package com.gt.entity;

import java.io.Serializable;
import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "wf_doccomments")
public class Comments implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2739383184118306828L;
	@Id
	@Column(name = "Comment_Id", unique = true, nullable = false)
	int Comment_id;
	@Column(name = "Comment_Type")
	String Comment_type;
	@Column(name = "action_id")
	Integer actionId;
	@Column(name = "Document_id")
	Integer documentId;
	@Column(name = "Comment")
	Blob Comment;
	
	
	/**
	 * @return the comment_id
	 */
	@Id
	@GeneratedValue
	public int getComment_id() {
		return Comment_id;
	}
	/**
	 * @param comment_id the comment_id to set
	 */
	public void setComment_id(int comment_id) {
		Comment_id = comment_id;
	}
	/**
	 * @return the comment_type
	 */
	public String getComment_type() {
		return Comment_type;
	}
	/**
	 * @param comment_type the comment_type to set
	 */
	public void setComment_type(String comment_type) {
		Comment_type = comment_type;
	}
	
	
	/**
	 * @return the actionId
	 */
	public Integer getActionId() {
		return actionId;
	}
	/**
	 * @param actionId the actionId to set
	 */
	public void setActionId(Integer actionId) {
		this.actionId = actionId;
	}
	/**
	 * @return the documentId
	 */
	public Integer getDocumentId() {
		return documentId;
	}
	/**
	 * @param documentId the documentId to set
	 */
	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}
	public Blob getComment() {
		return Comment;
	}
	public void setComment(Blob comment) {
		Comment = comment;
	}
	 
	
//	private DocAttachment docAttachment;
	
	

}
