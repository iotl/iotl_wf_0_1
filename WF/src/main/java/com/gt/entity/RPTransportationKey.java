package com.gt.entity;

import java.io.Serializable;

public class RPTransportationKey implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7242084122625936403L;

	/**
	 * 
	 */
	private Integer documentId;
	
	/**
	 * 
	 */
	private Integer plantId;
	
	/**
	 * 
	 */
	private Integer transporterSeqId;
	
	/**
	 * 
	 */
	private Integer transporterId;

	/**
	 * @return the documentId
	 */
	public Integer getDocumentId() {
		return documentId;
	}

	/**
	 * @param documentId the documentId to set
	 */
	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}

	/**
	 * @return the plantId
	 */
	public Integer getPlantId() {
		return plantId;
	}

	/**
	 * @param plantId the plantId to set
	 */
	public void setPlantId(Integer plantId) {
		this.plantId = plantId;
	}

	/**
	 * @return the transporterSeqId
	 */
	public Integer getTransporterSeqId() {
		return transporterSeqId;
	}

	/**
	 * @param transporterSeqId the transporterSeqId to set
	 */
	public void setTransporterSeqId(Integer transporterSeqId) {
		this.transporterSeqId = transporterSeqId;
	}

	/**
	 * @return the transporterId
	 */
	public Integer getTransporterId() {
		return transporterId;
	}

	/**
	 * @param transporterId the transporterId to set
	 */
	public void setTransporterId(Integer transporterId) {
		this.transporterId = transporterId;
	}
	
	
	
	
}
