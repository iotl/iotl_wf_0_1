package com.gt.entity;





import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "wf_documentmaster")
public class DocumentMaster {
	
	int DocumentId;
	String DocumentType;
	Integer CompanyId;
	Integer BusinessId;
	Integer SbuId;
	Integer PlantId;
	Integer MaterialId;
	String Title;
	Boolean IsExpress;
	Boolean IsRelatedParty;
//	Boolean IsMultiPlant;
	Integer CreatedBy;
	Date CreatedTimeStamp;
	Integer ModifiedBy;
	Date ModifiedTimeStamp;
	Integer CurrentlyWith;
	String Status;
	String isAmendment;
	Integer originalDocId;
	Integer seqNo;
	Integer clusterId;
	Integer deptId;
	String otherMaterialName;
	Integer initiatorPlantId;
	String vendorName;
	String materialDetails;
	String SourceName;
	Integer CategoryId;
	String proposalType;
	Double Total_PnB_Rate_mkcal;
	Double Total_Proposed_Rate_mkcal;
	Double Total_Approved_Rate_mkcal;
	Double Total_Final_Rate_mkcal;
	
	String transportationJustification;
	Integer originalProposalDocId;
	@Id
	@GeneratedValue
	@Column(name = "Document_Id")
	public int getDocumentId() {
		return DocumentId;
	}
	public void setDocumentId(int documentId) {
		DocumentId = documentId;
	}
	@Column(name = "Document_Type")
	public String getDocumentType() {
		return DocumentType;
	}

	public void setDocumentType(String documentType) {
		DocumentType = documentType;
	}
	@Column(name = "Company_Id")
	public Integer getCompanyId() {
		return CompanyId;
	}
	public void setCompanyId(Integer companyId) {
		CompanyId = companyId;
	}
	@Column(name = "Business_Id")
	public Integer getBusinessId() {
		return BusinessId;
	}
	public void setBusinessId(Integer businessId) {
		BusinessId = businessId;
	}
	@Column(name = "Sbu_Id")
	public Integer getSbuId() {
		return SbuId;
	}
	public void setSbuId(Integer sbuId) {
		SbuId = sbuId;
	}
	@Column(name = "Plant_Id")
	public Integer getPlantId() {
		return PlantId;
	}
	public void setPlantId(Integer plantId) {
		PlantId = plantId;
	}
	@Column(name = "Material_Id")
	public Integer getMaterialId() {
		return MaterialId;
	}
	public void setMaterialId(Integer materialId) {
		MaterialId = materialId;
	}
	@Column(name = "Title")
	public String getTitle() {
		return Title;
	}
	public void setTitle(String title) {
		Title = title;
	}
	@Column(name = "Is_Express")
	public Boolean getIsExpress() {
		return IsExpress;
	}
	public void setIsExpress(Boolean isExpress) {
		IsExpress = isExpress;
	}
	@Column(name = "Is_Related_Party")
	public Boolean getIsRelatedParty() {
		return IsRelatedParty;
	}
	public void setIsRelatedParty(Boolean isRelatedParty) {
		IsRelatedParty = isRelatedParty;
	}
	
/*	public Boolean getIsMultiPlant() {
		return IsMultiPlant;
	}
	public void setIsMultiPlant(Boolean isMultiPlant) {
		IsMultiPlant = isMultiPlant;
	}*/
	@Column(name = "Created_By")
	public Integer getCreatedBy() {
		return CreatedBy;
	}
	public void setCreatedBy(Integer createdBy) {
		CreatedBy = createdBy;
	}
	@Column(name = "Created_Time_Stamp")
	public Date getCreatedTimeStamp() {
		return CreatedTimeStamp;
	}
	public void setCreatedTimeStamp(Date date) {
		CreatedTimeStamp = date;
	}
	@Column(name = "Modified_By")
	public Integer getModifiedBy() {
		return ModifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		ModifiedBy = modifiedBy;
	}
	@Column(name = "Modified_Time_Stamp")
	public Date getModifiedTimeStamp() {
		return ModifiedTimeStamp;
	}
	public void setModifiedTimeStamp(Date modifiedTimeStamp) {
		ModifiedTimeStamp = modifiedTimeStamp;
	}
	@Column(name = "Currently_With")
	public Integer getCurrentlyWith() {
		return CurrentlyWith;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public void setCurrentlyWith(Integer currentlyWith) {
		CurrentlyWith = currentlyWith;
	}
	@Column(name = "Status")
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getIsAmendment() {
		return isAmendment;
	}
	public void setIsAmendment(String isAmendment) {
		this.isAmendment = isAmendment;
	}
	public Integer getOriginalDocId() {
		return originalDocId;
	}
	public void setOriginalDocId(Integer originalDocId) {
		this.originalDocId = originalDocId;
	}
	public Integer getSeqNo() {
		return seqNo;
	}
	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}
	public Integer getClusterId() {
		return clusterId;
	}
	public void setClusterId(Integer clusterId) {
		this.clusterId = clusterId;
	}
	public Integer getDeptId() {
		return deptId;
	}
	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}
	public String getOtherMaterialName() {
		return otherMaterialName;
	}
	public void setOtherMaterialName(String otherMaterialName) {
		this.otherMaterialName = otherMaterialName;
	}
	public Integer getInitiatorPlantId() {
		return initiatorPlantId;
	}
	public void setInitiatorPlantId(Integer initiatorPlantId) {
		this.initiatorPlantId = initiatorPlantId;
	}
	public String getMaterialDetails() {
		return materialDetails;
	}
	public void setMaterialDetails(String materialDetails) {
		this.materialDetails = materialDetails;
	}

	public String getSourceName() {
		return SourceName;
	}
	public void setSourceName(String sourceName) {
		SourceName = sourceName;
	}
	
	public Integer getCategoryId() {
		return CategoryId;
	}
	public void setCategoryId(Integer categoryId) {
		CategoryId = categoryId;
	}
	
	public String getProposalType() {
		return proposalType;
	}
	public void setProposalType(String proposalType) {
		this.proposalType = proposalType;
	}
	/**
	 * @return the transportationJustification
	 */
	public String getTransportationJustification() {
		return transportationJustification;
	}
	/**
	 * @param transportationJustification the transportationJustification to set
	 */
	public void setTransportationJustification(String transportationJustification) {
		this.transportationJustification = transportationJustification;
	}
	/**
	 * @return the originalProposalDocId
	 */
	public Integer getOriginalProposalDocId() {
		return originalProposalDocId;
	}
	/**
	 * @param originalProposalDocId the originalProposalDocId to set
	 */
	public void setOriginalProposalDocId(Integer originalProposalDocId) {
		this.originalProposalDocId = originalProposalDocId;

	}
	public Double getTotal_PnB_Rate_mkcal() {
		return Total_PnB_Rate_mkcal;
	}
	public void setTotal_PnB_Rate_mkcal(Double total_PnB_Rate_mkcal) {
		Total_PnB_Rate_mkcal = total_PnB_Rate_mkcal;
	}
	public Double getTotal_Proposed_Rate_mkcal() {
		return Total_Proposed_Rate_mkcal;
	}
	public void setTotal_Proposed_Rate_mkcal(Double total_Proposed_Rate_mkcal) {
		Total_Proposed_Rate_mkcal = total_Proposed_Rate_mkcal;
	}
	
	public Double getTotal_Approved_Rate_mkcal() {
		return Total_Approved_Rate_mkcal;
	}
	public void setTotal_Approved_Rate_mkcal(Double total_Approved_Rate_mkcal) {
		Total_Approved_Rate_mkcal = total_Approved_Rate_mkcal;
	}
	public Double getTotal_Final_Rate_mkcal() {
		return Total_Final_Rate_mkcal;
	}
	public void setTotal_Final_Rate_mkcal(Double total_Final_Rate_mkcal) {
		Total_Final_Rate_mkcal = total_Final_Rate_mkcal;
	}
	

	

}
