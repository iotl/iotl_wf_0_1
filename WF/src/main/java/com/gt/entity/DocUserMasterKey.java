package com.gt.entity;

import java.io.Serializable;

public class DocUserMasterKey implements Serializable {
	int DocumentId;
	int UserId;
	public int getDocumentId() {
		return DocumentId;
	}
	public void setDocumentId(int documentId) {
		DocumentId = documentId;
	}
	public int getUserId() {
		return UserId;
	}
	public void setUserId(int userId) {
		UserId = userId;
	}
	
	

}
