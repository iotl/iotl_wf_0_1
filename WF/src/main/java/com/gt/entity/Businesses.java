package com.gt.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "wf_business")
@NamedQuery(name="Integer.findMaxBusinessId",query="select max(bb.businessId) from Businesses bb")
@IdClass(BusinessCompany.class)
public class Businesses {


String businessName;
String abbreviation;
@Id
private int businessId;
@Id
private int companyId;



public String getBusinessName() {
	return businessName;
}

public void setBusinessName(String businessName) {
	this.businessName = businessName;
}

public String getAbbreviation() {
	return abbreviation;
}

public void setAbbreviation(String abbreviation) {
	this.abbreviation = abbreviation;
}

public int getBusinessId() {
	return businessId;
}

public void setBusinessId(int businessId) {
	this.businessId = businessId;
}

public int getCompanyId() {
	return companyId;
}

public void setCompanyId(int companyId) {
	this.companyId = companyId;
}




}
