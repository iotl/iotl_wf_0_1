package com.gt.entity;

import java.util.Date;

import javax.persistence.*;

@Entity
@NamedQuery(name="Integer.findMaxUserId",query="select max(t.userId) from Users t")
@Table(name = "wf_user")
public class Users {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "User_Id", unique = true, nullable = false)
	int UserId;
	String firstName;
	String lastName;
	String address;
	Integer alternateContactPerson;
	@Column(name = "Email_Id", unique = true)
	String emailId;
	String phoneNumber;
	String password;
	Boolean isSupportingEndorser;
	@Column(name = "Access_Token")
	private String accessToken;
	@Column(name = "Access_Token_Validity")
	private Date AccessTokenValidity;
	String authorizationKey;
	String existingPassword;
	Integer deptId;
//	Boolean cpIsSupportingEndorser;
	
	// added on 291015 for utkal login
		Integer plantId;
		Integer defaultPlantId;

		/**
		 * added for utkal login
		 * 
		 * @return
		 */
		
		@Column(name = "Plant_Id")
		public Integer getPlantId() {
			return plantId;
		}

		public void setPlantId(Integer plantId) {
			this.plantId = plantId;
		}

	
		@Column(name = "Default_Plant_Id")
		public Integer getDefaultPlantId() {
			return defaultPlantId;
		}

		public void setDefaultPlantId(Integer defaultPlantId) {
			this.defaultPlantId = defaultPlantId;
		}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public Date getAccessTokenValidity() {
		return AccessTokenValidity;
	}

	public void setAccessTokenValidity(Date accessTokenValidity) {
		AccessTokenValidity = accessTokenValidity;
	}

	public Users(int userId, String firstName, String lastName, String address,
			Integer alternateContactPerson, String emailId, String phoneNumber,
			String password, Boolean isSupportingEndorser, String accessToken,
			Date accessTokenValidity) {
		super();
		UserId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.alternateContactPerson = alternateContactPerson;
		this.emailId = emailId;
		this.phoneNumber = phoneNumber;
		this.password = password;
		//this.isSupportingEndorser = isSupportingEndorser;
		this.accessToken = accessToken;
		AccessTokenValidity = accessTokenValidity;
	}

	public Users() {

	}
	
	public Users(int P_UserId) {
		this.UserId = P_UserId;

	}
	

	@Id
	public int getUserId() {
		return UserId;
	}

	public void setUserId(int userId) {
		UserId = userId;
	}
	
	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}



	public Integer getAlternateContactPerson() {
		return alternateContactPerson;
	}



	public void setAlternateContactPerson(Integer alternateContactPerson) {
		this.alternateContactPerson = alternateContactPerson;
	}
	
	
	public Boolean getIsSupportingEndorser() {
		return isSupportingEndorser;
	}

	public void setIsSupportingEndorser(Boolean isSupportingEndorser) {
		this.isSupportingEndorser = isSupportingEndorser;
	}

	/**
	 * @return the authorizationKey
	 */
	public String getAuthorizationKey() {
		return authorizationKey;
	}

	/**
	 * @param authorizationKey the authorizationKey to set
	 */
	public void setAuthorizationKey(String authorizationKey) {
		this.authorizationKey = authorizationKey;
	}
	@Transient
	public String getExistingPassword() {
		return existingPassword;
	}

	public void setExistingPassword(String existingPassword) {
		this.existingPassword = existingPassword;
	}

	public Integer getDeptId() {
		return deptId;
	}

	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}

/*	*//**
	 * @return the cpIsSupportingEndorser
	 *//*
	public Boolean getCpIsSupportingEndorser() {
		return cpIsSupportingEndorser;
	}

	*//**
	 * @param cpIsSupportingEndorser the cpIsSupportingEndorser to set
	 *//*
	public void setCpIsSupportingEndorser(Boolean cpIsSupportingEndorser) {
		this.cpIsSupportingEndorser = cpIsSupportingEndorser;
	}*/


}
