package com.gt.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "wf_docusermaster")
@IdClass(DocUserMasterKey.class)
public class DocUserMaster{

	int DocumentId;
	int UserId;
	String docRole;
	Integer serialNumber;
	Integer DelegateId;
	
	@Id
	public int getDocumentId() {
		return DocumentId;
	}
	public void setDocumentId(int documentId) {
		DocumentId = documentId;
	}
	
	@Id
	public int getUserId() {
		return UserId;
	}
	public void setUserId(int userId) {
		UserId = userId;
	}
	
	public String getDocRole() {
		return docRole;
	}
	public void setDocRole(String docRole) {
		this.docRole = docRole;
	}
	public Integer getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(Integer serialNumber) {
		this.serialNumber = serialNumber;
	}
	public Integer getDelegateId() {
		return DelegateId;
	}
	public void setDelegateId(Integer delegateId) {
		DelegateId = delegateId;
	}

}
