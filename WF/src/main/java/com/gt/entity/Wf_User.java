package com.gt.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
@Entity
@Table(name = "wf_user", catalog = "wf", uniqueConstraints = {
		@UniqueConstraint(columnNames = "User_Id")})
/*@Embeddable*/
public class Wf_User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "User_Id", unique = true, nullable = false)
	private Integer userid;
	@Column(name = "FIRST_NAME")
	private String firstname;
	@Column(name = "LAST_NAME")
	private String lastname;
	@Column(name = "ADDRESS")
	private String address;
	@Column(name = "ALTERNATE_CONTACT_PERSON")
	private String alternatecontactperson;
	
	@Column(name = "EMAIL_ID")
	private String emailId;
	@Column(name = "PHONE_NUMBER")
	private String phoneNumber;
	@Column(name = "Password")
	private String password;
	public Wf_User() {
		super();
	}
	public Wf_User(Integer userid, String firstname, String lastname,
			String address, String alternatecontactperson, String emailId,
			String phoneNumber, String password, String accessToken,
			Date accessTokenValidity) {
		super();
		this.userid = userid;
		this.firstname = firstname;
		this.lastname = lastname;
		this.address = address;
		this.alternatecontactperson = alternatecontactperson;
		this.emailId = emailId;
		this.phoneNumber = phoneNumber;
		this.password = password;
		this.accessToken = accessToken;
		AccessTokenValidity = accessTokenValidity;
	}
	@Column(name = "Access_Token")
	private String accessToken;
	@Column(name = "Access_Token_Validity")
	private Date AccessTokenValidity;
	public Wf_User(Integer userid, String firstname, String lastname,
			String address, String alternatecontactperson, String emailId,
			String phoneNumber, String password) {
		super();
		this.userid = userid;
		this.firstname = firstname;
		this.lastname = lastname;
		this.address = address;
		this.alternatecontactperson = alternatecontactperson;
		this.emailId = emailId;
		this.phoneNumber = phoneNumber;
		this.password = password;
	}
	public Integer getUserid() {
		return userid;
	}
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAlternatecontactperson() {
		return alternatecontactperson;
	}
	public void setAlternatecontactperson(String alternatecontactperson) {
		this.alternatecontactperson = alternatecontactperson;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public Date getAccessTokenValidity() {
		return AccessTokenValidity;
	}
	public void setAccessTokenValidity(Date accessTokenValidity) {
		AccessTokenValidity = accessTokenValidity;
	}
	


}
