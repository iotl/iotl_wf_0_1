package com.gt.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
@Entity
@Table(name = "wf_userroles")
//@IdClass(UserRolesKey.class)
@IdClass(UserRoles.class)
public class UserRoles implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -921676455368394878L;
	
	int userId;
	int RoleId;
	

	@Id
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	@Id
	@Column (name="Role_Id")
	public int getRoleId() {
		return RoleId;
	}

	public void setRoleId(int roleId) {
		this.RoleId = roleId;
	}
	
	
	
}
