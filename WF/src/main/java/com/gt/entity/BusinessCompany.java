package com.gt.entity;

import java.io.Serializable;

import javax.persistence.IdClass;


@IdClass(BusinessCompany.class)
public class BusinessCompany  implements Serializable  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int businessId;
	private int companyId;
	public int getBusinessId() {
		return businessId;
	}
	public void setBusinessId(int businessId) {
		this.businessId = businessId;
	}
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	

	

}
