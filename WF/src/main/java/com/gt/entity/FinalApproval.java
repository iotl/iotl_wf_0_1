package com.gt.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "wf_rp_finalapproval")
@IdClass(FinalApprovalKey.class)
public class FinalApproval {
	
	int DocumentId;
	int PlantId;
	int proposalSequenceNumber;
	Double finalRate;
	Double finalQuantity;
	Double finalApprovedAmount;
	Double finalRate_mkcal;
	Double finalPremium;
	
	
	
	@Id
	public int getDocumentId() {
		return DocumentId;
	}
	public void setDocumentId(int documentId) {
		DocumentId = documentId;
	}
	
	@Id
	public int getPlantId() {
		return PlantId;
	}
	public void setPlantId(int plantId) {
		PlantId = plantId;
	}
	
	@Id
	public int getProposalSequenceNumber() {
		return proposalSequenceNumber;
	}
	public void setProposalSequenceNumber(int proposalSequenceNumber) {
		this.proposalSequenceNumber = proposalSequenceNumber;
	}
	
	public Double getFinalRate() {
		return finalRate;
	}
	public void setFinalRate(Double finalRate) {
		this.finalRate = finalRate;
	}
	public Double getFinalQuantity() {
		return finalQuantity;
	}
	public void setFinalQuantity(Double finalQuantity) {
		this.finalQuantity = finalQuantity;
	}
	public Double getFinalApprovedAmount() {
		return finalApprovedAmount;
	}
	public void setFinalApprovedAmount(Double finalApprovedAmount) {
		this.finalApprovedAmount = finalApprovedAmount;
	}

	public Double getFinalRate_mkcal() {
		return finalRate_mkcal;
	}
	public void setFinalRate_mkcal(Double finalRate_mkcal) {
		this.finalRate_mkcal = finalRate_mkcal;
	}
	public Double getFinalPremium() {
		return finalPremium;
	}
	public void setFinalPremium(Double finalPremium) {
		this.finalPremium = finalPremium;
	}
	
	
	
	

}
