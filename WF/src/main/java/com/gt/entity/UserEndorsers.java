package com.gt.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "wf_userendorsers")
//@IdClass(UserRolesKey.class)
@IdClass(UserEndorsers.class)
public class UserEndorsers implements Serializable{
	
	int userId;
	int endorserId;
	
	
	@Id
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	@Id
	public int getEndorserId() {
		return endorserId;
	}
	public void setEndorserId(int endorserId) {
		this.endorserId = endorserId;
	}
	
	

}
