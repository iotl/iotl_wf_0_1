package com.gt.entity;

import java.io.File;
import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="wf_docattachment")
public class DocAttachment implements java.io.Serializable{

	/*public DocAttachment() {
		super();
	}
	public DocAttachment(int p_Attachment_Id, int comment_Id, int document_Id,
			String attachmentType, String file_Name) {
		super();
		this.p_Attachment_Id = p_Attachment_Id;
//		this.comment_Id = comment_Id;
//		this.document_Id = document_Id;
		//this.attachmentType = attachmentType;
		this.file_Name = file_Name;
	}*/
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "P_Attachmentid", unique = true, nullable = false)
	private int pAttachmentId;
	
	@Column(name="Commentid")
	private int commentId;
	
	@Column(name = "Documentid")
	private int documentId;
	
	@Column(name = "Attachmenttype")
	private String attachmentType;
	
	@Column(name = "Content")
	private Blob content;
	
	@Column(name = "Filename")
	private String fileName;
	
	@Transient
	private File uploadFile;
	
	
	
	
	
	/**
	 * @return the commentId
	 */
	public int getCommentId() {
		return commentId;
	}
	/**
	 * @param commentId the commentId to set
	 */
	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}
	/**
	 * @return the documentId
	 */
	public int getDocumentId() {
		return documentId;
	}
	/**
	 * @param documentId the documentId to set
	 */
	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}


	/**
	 * @return the pAttachmentId
	 */
	public int getpAttachmentId() {
		return pAttachmentId;
	}
	/**
	 * @param pAttachmentId the pAttachmentId to set
	 */
	public void setpAttachmentId(int pAttachmentId) {
		this.pAttachmentId = pAttachmentId;
	}
	/**
	 * @return the attachmentType
	 */
	public String getAttachmentType() {
		return attachmentType;
	}
	/**
	 * @param attachmentType the attachmentType to set
	 */
	public void setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
	}
	public Blob getContent() {
		return content;
	}
	public void setContent(Blob content) {
		this.content = content;
	}
	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/**
	 * @return the uploadFile
	 */
	public File getUploadFile() {
		return uploadFile;
	}
	/**
	 * @param uploadFile the uploadFile to set
	 */
	public void setUploadFile(File uploadFile) {
		this.uploadFile = uploadFile;
	}
	
	
}