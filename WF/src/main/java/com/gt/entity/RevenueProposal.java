package com.gt.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.gt.bo.RpTransportationBO;


@Entity
@Table(name = "wf_revenueproposal")
@IdClass(RevenueProposalKey.class)
public class RevenueProposal {
	
int DocumentId;
int PlantId;
String IsBudgeted;
Double AnnualQuantity;
Double OrderQuantity;
Double BudgetedRate;
Double PolestarRate;
Double NegotiatedRate;
Double SavingsBudgeted;
Double SavingsPolestar;
Double AmountNegotiated;
String AnnualQuantityUnit;
String OrderQuantityUnit;
String BudgetRateUnit;
String PolestarRateUnit;
String NegotiatedRateUnit;
String plantName;
Double approvedPeriod;
Double approvedQuantity;
Double amountApproved;
Double approvedRate;
int actionId;
int commentId;
Float amountPretax;
Float taxPercent;
Double orderPeriod;
String orderPeriodUnit;
Double AnnualPeriod;
Double taxAmount;
Double approvedTaxAmount;
Double BudgetedRate_mkcal;
Double NegotiatedRate_mkcal;
Double ProposedPremium;
Double ApprovedNegotiatedRate_mkcal;
Double ApprovedPremium;


private String savingsBudgetedStr;
private String savingsPolestarStr;
private String amountNegotiatedStr;
private String amountPretaxStr;
Float negotiatedRateTotal;

String rpNumber;

List<String> docAttachmentList= new ArrayList<String>();
String comments;

private ArrayList<RpTransportationBO> transporterList;

private Double averagePnb;

private Double averageFreightRate;

private Double averageProposedRate;

String remarks;


@Id
public int getDocumentId() {
	return DocumentId;
}
public void setDocumentId(int documentId) {
	DocumentId = documentId;
}
@Id
public int getPlantId() {
	return PlantId;
}
public void setPlantId(int plantId) {
	PlantId = plantId;
}


public Double getAnnualQuantity() {
	if(AnnualQuantity!=null){
		  BigDecimal bd = new BigDecimal(Double.toString(AnnualQuantity));
	        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
		return  bd.doubleValue();
	}else{
		return  AnnualQuantity;	
	}
}
public void setAnnualQuantity(Double annualQuantity) {
	AnnualQuantity = annualQuantity;
}
public Double getOrderQuantity() {
	if(OrderQuantity!=null){
		  BigDecimal bd = new BigDecimal(Double.toString(OrderQuantity));
	        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
		return  bd.doubleValue();
	}else{
		return  OrderQuantity;	
	}
}
public void setOrderQuantity(Double orderQuantity) {
	OrderQuantity = orderQuantity;
}


/**
 * @return the isBudgeted
 */
public String getIsBudgeted() {
	return IsBudgeted;
}
/**
 * @param isBudgeted the isBudgeted to set
 */
public void setIsBudgeted(String isBudgeted) {
	IsBudgeted = isBudgeted;
}
public Double getBudgetedRate() {
	if(BudgetedRate!=null){
		 BigDecimal bd = new BigDecimal(Double.toString(BudgetedRate));
	        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
		return  bd.doubleValue();
	}else{
		return  BudgetedRate;	
	}
}
public void setBudgetedRate(Double budgetedRate) {
	BudgetedRate = budgetedRate;
}
public Double getPolestarRate() {
	if(PolestarRate!=null){
		 BigDecimal bd = new BigDecimal(Double.toString(PolestarRate));
	        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
		return  bd.doubleValue();
	}else{
		return  PolestarRate;	
	}
}
public void setPolestarRate(Double polestarRate) {
	PolestarRate = polestarRate;
}
public Double getNegotiatedRate() {
	if(NegotiatedRate!=null){
		 BigDecimal bd = new BigDecimal(Double.toString(NegotiatedRate));
	        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
		return  bd.doubleValue();
	}else{
		return  NegotiatedRate;	
	}
}
public void setNegotiatedRate(Double negotiatedRate) {
	NegotiatedRate = negotiatedRate;
}
public Double getSavingsBudgeted() {
	if(SavingsBudgeted!=null){
		 BigDecimal bd = new BigDecimal(Double.toString(SavingsBudgeted));
	        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
		return  bd.doubleValue();
	}else{
		return  SavingsBudgeted;	
	}
}
public void setSavingsBudgeted(Double savingsBudgeted) {
	SavingsBudgeted = savingsBudgeted;
}
public Double getSavingsPolestar() {
	if(SavingsPolestar!=null){
		 BigDecimal bd = new BigDecimal(Double.toString(SavingsPolestar));
	        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
		return  bd.doubleValue();
	}else{
		return  SavingsPolestar;	
	}
}
public void setSavingsPolestar(Double savingsPolestar) {
	SavingsPolestar = savingsPolestar;
}
public Double getAmountNegotiated() {
	if(AmountNegotiated!=null){
		 BigDecimal bd = new BigDecimal(Double.toString(AmountNegotiated));
	        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
		return  bd.doubleValue();
	}else{
		return  AmountNegotiated;	
	}
}
public void setAmountNegotiated(Double amountNegotiated) {
	AmountNegotiated = amountNegotiated;
}
public String getAnnualQuantityUnit() {
	return AnnualQuantityUnit;
}
public void setAnnualQuantityUnit(String annualQuantityUnit) {
	AnnualQuantityUnit = annualQuantityUnit;
}
public String getOrderQuantityUnit() {
	return OrderQuantityUnit;
}
public void setOrderQuantityUnit(String orderQuantityUnit) {
	OrderQuantityUnit = orderQuantityUnit;
}
public String getBudgetRateUnit() {
	return BudgetRateUnit;
}
public void setBudgetRateUnit(String budgetRateUnit) {
	BudgetRateUnit = budgetRateUnit;
}
public String getPolestarRateUnit() {
	return PolestarRateUnit;
}
public void setPolestarRateUnit(String polestarRateUnit) {
	PolestarRateUnit = polestarRateUnit;
}
public String getNegotiatedRateUnit() {
	return NegotiatedRateUnit;
}
public void setNegotiatedRateUnit(String negotiatedRateUnit) {
	NegotiatedRateUnit = negotiatedRateUnit;
}
/**
 * @return the plantName
 */
@Transient
public String getPlantName() {
	return plantName;
}
/**
 * @param plantName the plantName to set
 */
public void setPlantName(String plantName) {
	this.plantName = plantName;
}
/**
 * @return the savingsBudgetedStr
 */
@Transient
public String getSavingsBudgetedStr() {
	return savingsBudgetedStr;
}
/**
 * @param savingsBudgetedStr the savingsBudgetedStr to set
 */

public void setSavingsBudgetedStr(String savingsBudgetedStr) {
	this.savingsBudgetedStr = savingsBudgetedStr;
}
/**
 * @return the savingsPolestarStr
 */
@Transient
public String getSavingsPolestarStr() {
	return savingsPolestarStr;
}
/**
 * @param savingsPolestarStr the savingsPolestarStr to set
 */
public void setSavingsPolestarStr(String savingsPolestarStr) {
	this.savingsPolestarStr = savingsPolestarStr;
}
/**
 * @return the amountNegotiatedStr
 */
@Transient
public String getAmountNegotiatedStr() {
	return amountNegotiatedStr;
}
/**
 * @param amountNegotiatedStr the amountNegotiatedStr to set
 */
public void setAmountNegotiatedStr(String amountNegotiatedStr) {
	this.amountNegotiatedStr = amountNegotiatedStr;
}
public Double getApprovedQuantity() {
	if(approvedQuantity!=null){
		 BigDecimal bd = new BigDecimal(Double.toString(approvedQuantity));
	        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
		return  bd.doubleValue();
	}else{
		return  approvedQuantity;	
	}
}
public void setApprovedQuantity(Double approvedQuantity) {
	this.approvedQuantity = approvedQuantity;
}
public Double getAmountApproved() {
	if(amountApproved!=null){
		 BigDecimal bd = new BigDecimal(Double.toString(amountApproved));
	        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
		return  bd.doubleValue();
	}else{
		return  amountApproved;	
	}
}
public void setAmountApproved(Double amountApproved) {
	this.amountApproved = amountApproved;
}
public Double getApprovedRate() {
	if(approvedRate!=null){
		 BigDecimal bd = new BigDecimal(Double.toString(approvedRate));
	        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
		return  bd.doubleValue();
	}else{
		return  approvedRate;	
	}
}
public void setApprovedRate(Double approvedRate) {
	this.approvedRate = approvedRate;
}
@Transient
public int getActionId() {
	return actionId;
}
public void setActionId(int actionId) {
	this.actionId = actionId;
}
@Transient
public int getCommentId() {
	return commentId;
}
public void setCommentId(int commentId) {
	this.commentId = commentId;
}

@Transient
public String getComments() {
	return comments;
}
public void setComments(String comments) {
	this.comments = comments;
}
@Transient
public List<String> getDocAttachmentList() {
	return docAttachmentList;
}
public void setDocAttachmentList(List<String> docAttachmentList) {
	this.docAttachmentList = docAttachmentList;
}
@Column(name="RPNumber")
public String getRpNumber() {
	return rpNumber;
}
public void setRpNumber(String rpNumber) {
	this.rpNumber = rpNumber;
}
public Float getAmountPretax() {
	if(amountPretax!=null){
		 BigDecimal bd = new BigDecimal(Float.toString(amountPretax));
	        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
		return  bd.floatValue();
	}else{
		return  amountPretax;	
	}
}
public void setAmountPretax(Float amountPretax) {
	this.amountPretax = amountPretax;
}
public Float getTaxPercent() {
	if(taxPercent!=null){
		 BigDecimal bd = new BigDecimal(Float.toString(taxPercent));
	        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
		return  bd.floatValue();
	}else{
		return  taxPercent;	
	}
}
public void setTaxPercent(Float taxPercent) {
	this.taxPercent = taxPercent;
}
@Transient
public String getAmountPretaxStr() {
	return amountPretaxStr;
}
public void setAmountPretaxStr(String amountPretaxStr) {
	this.amountPretaxStr = amountPretaxStr;
}
@Transient
public Float getNegotiatedRateTotal() {
	if(negotiatedRateTotal!=null){
		  BigDecimal bd = new BigDecimal(Float.toString(negotiatedRateTotal));
	        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
		return  bd.floatValue();
	}else{
		return  negotiatedRateTotal;	
	}
}
public void setNegotiatedRateTotal(Float negotiatedRateTotal) {
	this.negotiatedRateTotal = negotiatedRateTotal;
}
public Double getOrderPeriod() {
	return orderPeriod;
}
public void setOrderPeriod(Double orderPeriod) {
	this.orderPeriod = orderPeriod;
}
public String getOrderPeriodUnit() {
	return orderPeriodUnit;
}
public void setOrderPeriodUnit(String orderPeriodUnit) {
	this.orderPeriodUnit = orderPeriodUnit;
}
public Double getApprovedPeriod() {
	return approvedPeriod;
}
public void setApprovedPeriod(Double approvedPeriod) {
	this.approvedPeriod = approvedPeriod;
}
public Double getAnnualPeriod() {
	return AnnualPeriod;
}
public void setAnnualPeriod(Double annualPeriod) {
	AnnualPeriod = annualPeriod;
}



public Double getTaxAmount() {
	return taxAmount;
}
public void setTaxAmount(Double taxAmount) {
	this.taxAmount = taxAmount;
}
public Double getApprovedTaxAmount() {
	return approvedTaxAmount;
}
public void setApprovedTaxAmount(Double approvedTaxAmount) {
	this.approvedTaxAmount = approvedTaxAmount;
}


public Double getBudgetedRate_mkcal() {
	return BudgetedRate_mkcal;
}
public void setBudgetedRate_mkcal(Double budgetedRate_mkcal) {
	BudgetedRate_mkcal = budgetedRate_mkcal;
}

public Double getNegotiatedRate_mkcal() {
	return NegotiatedRate_mkcal;
}
public void setNegotiatedRate_mkcal(Double negotiatedRate_mkcal) {
	NegotiatedRate_mkcal = negotiatedRate_mkcal;
}
public Double getProposedPremium() {
	return ProposedPremium;
}
public void setProposedPremium(Double proposedPremium) {
	ProposedPremium = proposedPremium;
}
@Transient
public ArrayList<RpTransportationBO> getTransporterList() {
	return transporterList;
}
public void setTransporterList(ArrayList<RpTransportationBO> transporterList) {
	this.transporterList = transporterList;
}
@Transient
public Double getAveragePnb() {
	return averagePnb;
}
public void setAveragePnb(Double averagePnb) {
	this.averagePnb = averagePnb;
}
@Transient
public Double getAverageFreightRate() {
	return averageFreightRate;
}
public void setAverageFreightRate(Double averageFreightRate) {
	this.averageFreightRate = averageFreightRate;
}
@Transient
public Double getAverageProposedRate() {
	return averageProposedRate;
}
public void setAverageProposedRate(Double averageProposedRate) {
	this.averageProposedRate = averageProposedRate;
}
public Double getApprovedNegotiatedRate_mkcal() {
	return ApprovedNegotiatedRate_mkcal;
}
public void setApprovedNegotiatedRate_mkcal(Double approvedNegotiatedRate_mkcal) {
	ApprovedNegotiatedRate_mkcal = approvedNegotiatedRate_mkcal;
}
public Double getApprovedPremium() {
	return ApprovedPremium;
}
public void setApprovedPremium(Double approvedPremium) {
	ApprovedPremium = approvedPremium;
}
@Column(name="REMARKS")
public String getRemarks() {
	return remarks;
}
public void setRemarks(String remarks) {
	this.remarks = remarks;
}






}
