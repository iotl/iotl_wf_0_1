package com.gt.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "wf_rpnumber")
public class RPNumber {
	int id;
	int RpNumberId;
	int PlantId;
	
	@Column(name="Financial_Year")
	int financialYear;
	public int getPlantId() {
		return PlantId;
	}

	public void setPlantId(int plantId) {
		PlantId = plantId;
	}
	
	public int getRpNumberId() {
		return RpNumberId;
	}

	public void setRpNumberId(int RpNumberId) {
		this.RpNumberId = RpNumberId;
	}

	/**
	 * @return the id
	 */
	@Id
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	public int getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(int financialYear) {
		this.financialYear = financialYear;
	}
	
	
}
