package com.gt.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "wf_userdelegates")
@IdClass(UserDelegates.class)
public class UserDelegates implements Serializable {
	int UserId;
	int DelegateId;
	
	
	@Id
	public int getUserId() {
		return UserId;
	}
	public void setUserId(int userId) {
		UserId = userId;
	}
	@Id
	public int getDelegateId() {
		return DelegateId;
	}
	public void setDelegateId(int delegateId) {
		DelegateId = delegateId;
	}
	


}
