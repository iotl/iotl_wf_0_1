package com.gt.entity;





import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "wf_docuseractions")
public class DocUserAction  {

	int ActionId;
	Integer DocumentId;
	Integer UserId;
//	Users UserId;
	String DocRole;
	String ActionStatus;
	Date ActionPendingTimeStamp;
	Date ActionTakenTimeStamp;
	Integer DocActionSerialNumber;
	Integer FromUserAction; 
	Integer ActionTakenBy;
	Integer plantId;
	String isDraft;
	
	@ManyToOne
    private DocumentMaster documentMaster;
	
	@ManyToMany
    private RevenueProposal revenueProposal;


	

	@Id
	@GeneratedValue
	public int getActionId() {
		return ActionId;
	}
	public void setActionId(int actionId) {
		ActionId = actionId;
	}
	
	public Integer getDocumentId() {
		return DocumentId;
	}
	
	public void setDocumentId(Integer documentId) {
		DocumentId = documentId;
	}
	
		
	/*@ManyToOne
	public Users getUserId() {
		return UserId;
	}
	public void setUserId(Users userId) {
		UserId = userId;
	}*/
	
	public Integer getUserId() {
		return UserId;
	}
	public void setUserId(Integer userId) {
		UserId = userId;
	}
	public String getDocRole() {
		return DocRole;
	}
	public void setDocRole(String docRole) {
		DocRole = docRole;
	}
	public String getActionStatus() {
		return ActionStatus;
	}
	public void setActionStatus(String actionStatus) {
		ActionStatus = actionStatus;
	}
	public Date getActionPendingTimeStamp() {
		return ActionPendingTimeStamp;
	}
	public void setActionPendingTimeStamp(Date date) {
		ActionPendingTimeStamp = date;
	}
	public Date getActionTakenTimeStamp() {
		return ActionTakenTimeStamp;
	}
	public void setActionTakenTimeStamp(Date actionTakenTimeStamp) {
		ActionTakenTimeStamp = actionTakenTimeStamp;
	}
	
	public Integer getFromUserAction() {
		return FromUserAction;
	}
	public void setFromUserAction(Integer fromUserAction) {
		FromUserAction = fromUserAction;
	}
	public Integer getDocActionSerialNumber() {
		return DocActionSerialNumber;
	}
	public void setDocActionSerialNumber(Integer docActionSerialNumber) {
		DocActionSerialNumber = docActionSerialNumber;
	}
	public Integer getActionTakenBy() {
		return ActionTakenBy;
	}
	public void setActionTakenBy(Integer actionTakenBy) {
		ActionTakenBy = actionTakenBy;
	}
	public Integer getPlantId() {
		return plantId;
	}
	public void setPlantId(Integer plantId) {
		this.plantId = plantId;
	}
	public String getIsDraft() {
		return isDraft;
	}
	public void setIsDraft(String isDraft) {
		this.isDraft = isDraft;
	}
	
	
	
}
