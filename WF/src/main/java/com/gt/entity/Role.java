package com.gt.entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@NamedQuery(name="Integer.findMaxRoleId",query="select max(t.roleId) from Role t")
@Table(name = "wf_role")

public class Role {
	
	
	int RoleId;
	String RoleName;
	String Dashboard_Type;
	
     public String getDashboard_Type() {
		return Dashboard_Type;
	}

	public void setDashboard_Type(String dashboard_Type) {
		Dashboard_Type = dashboard_Type;
	}

	public  Role(){
		
	}
	
	@Id
	public int getRoleId() {
		return RoleId;
	}

	public void setRoleId(int roleId) {
		RoleId = roleId;
	}
	@Column (name="Role_Name")
	public String getRoleName() {
		return RoleName;
	}

	public void setRoleName(String roleName) {
		RoleName = roleName;
	}

	

	
	

}
