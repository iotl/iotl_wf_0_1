package com.gt.entity;

import java.io.Serializable;

public class FinalApprovalKey implements Serializable{
	
	int DocumentId;
	int PlantId;
	int proposalSequenceNumber;
	
	public int getDocumentId() {
		return DocumentId;
	}
	public void setDocumentId(int documentId) {
		DocumentId = documentId;
	}
	public int getPlantId() {
		return PlantId;
	}
	public void setPlantId(int plantId) {
		PlantId = plantId;
	}
	public int getProposalSequenceNumber() {
		return proposalSequenceNumber;
	}
	public void setProposalSequenceNumber(int proposalSequenceNumber) {
		this.proposalSequenceNumber = proposalSequenceNumber;
	}

	
}
