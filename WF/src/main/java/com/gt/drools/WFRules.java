/*
 * Copyright 2010 JBoss Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gt.drools;

import java.util.ArrayList;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;


/**
 * This is a sample file to launch a rule package from a rule source file.
 */
public class WFRules {

	public static final void main(String[] args) {
        try {
            // load up the knowledge base
	        KieServices ks = KieServices.Factory.get();
    	    KieContainer kContainer = ks.getKieClasspathContainer();
        	KieSession kSession = kContainer.newKieSession("ksession-rules");

            // go !
            Message message = new Message();
            message.setMessage("Hello World");
            message.setStatus(Message.HELLO);
            kSession.insert(message);
            kSession.fireAllRules();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
	
	

	


    public static class Message {

        public static final int HELLO = 0;
        public static final int GOODBYE = 1;

        private String message;

        private int status;

        public String getMessage() {
            return this.message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getStatus() {
            return this.status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

    }
    
    public static class Business {
    	
    	public static enum InitiatorPlant {
    		Corporate, Utkal 
    		}

        private String businessType;
        private String requestType;
        private double value;
        private Integer materialId;
        private String materialName;
        private ArrayList<User> users; 
//        private InitiatorPlant plantName;
        private String plantName;
        private String doa;
        private String data;
        private Integer deptId;
        private Integer clusterId;
        private Integer plantId;
		private String clusterName;
		private Integer materialDetails;
		private Integer categoryId;
		private Boolean isXportation;
		
        public Integer getCategoryId() {
			return categoryId;
		}
		public void setCategoryId(Integer categoryId) {
			this.categoryId = categoryId;
		}
		public String getBusinessType() {
			return businessType;
		}
		public void setBusinessType(String businessType) {
			this.businessType = businessType;
		}
		public String getRequestType() {
			return requestType;
		}
		public void setRequestType(String requestType) {
			this.requestType = requestType;
		}
		public double getValue() {
			return value;
		}
		public void setValue(double value) {
			this.value = value;
		}
		public Integer getMaterialId() {
			return materialId;
		}
		public void setMaterialId(Integer materialId) {
			this.materialId = materialId;
		}
		public ArrayList<User> getUsers() {
			return users;
		}
		public void setUsers(ArrayList<User> users) {
			this.users = users;
		}
		/*public InitiatorPlant getPlantName() {
			return plantName;
		}
		public void setPlantName(InitiatorPlant plantName) {
			this.plantName = plantName;
		}*/
		public String getPlantName() {
			return plantName;
		}
		public void setPlantName(String plantName) {
			this.plantName = plantName;
		}
		public String getDoa() {
			return doa;
		}
		public void setDoa(String doa) {
			this.doa = doa;
		}
		public String getData() {
			return data;
		}
		public void setData(String data) {

			this.data = data;
			String role[] =data.split("::");
			ArrayList<User> list =new ArrayList();
			User usr;
//			Integer sequenceNo= 3;
//			if((getDeptId()!=null && getDeptId()==1 )|| (getDeptId()!=null&& getDeptId()==2)){
//				sequenceNo=2;
//			}else if(getClusterName()!=null && !getClusterName().isEmpty()){
			Integer	sequenceNo=2;
//			}
			for(int i=0;i<role.length;i++){
				System.out.println("ArrayList Role Length "+role.length + "Role 1"+ role[i] +"sequenceNo  "+sequenceNo);
					
				usr = new User();
				usr.setRole(role[i]);
				usr.setSequence(sequenceNo);
				list.add(usr);
				sequenceNo++;
			}
			setUsers(list);
		
		}
		public String getMaterialName() {
			return materialName;
		}
		public void setMaterialName(String materialName) {
			this.materialName = materialName;
		}
		public Integer getDeptId() {
			return deptId;
		}
		public void setDeptId(Integer deptId) {
			this.deptId = deptId;
		}
		public Integer getClusterId() {
			return clusterId;
		}
		public void setClusterId(Integer clusterId) {
			this.clusterId = clusterId;
		}
		public Integer getPlantId() {
			return plantId;
		}
		public void setPlantId(Integer plantId) {
			this.plantId = plantId;
		}
		public String getClusterName() {
			return clusterName;
		}
		public void setClusterName(String clusterName) {
			this.clusterName = clusterName;
		}
		/*public String getMaterialDetails() {
			return materialDetails;
		}
		public void setMaterialDetails(String materialDetails) {
			this.materialDetails = materialDetails;
		}*/
		public Integer getMaterialDetails() {
			return materialDetails;
		}
		public void setMaterialDetails(Integer materialDetails) {
			this.materialDetails = materialDetails;
		}
		public Boolean getIsXportation() {
			return isXportation;
		}
		public void setIsXportation(Boolean isXportation) {
			this.isXportation = isXportation;
		}
		
        

    }
    
    public static class User {

        private String role;
        private int status;
        private String message;
        private int sequence;
        
        public int getStatus() {
			return status;
		}
		public void setStatus(int status) {
			this.status = status;
		}
		public String getRole() {
			return role;
		}
		public void setRole(String role) {
			this.role = role;
		}

		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		public int getSequence() {
			return sequence;
		}
		public void setSequence(int sequence) {
			this.sequence = sequence;
		}

		

    }
}
