package com;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.gt.utils.PropsUtil;

@Configuration
@EnableTransactionManagement
public class DatabaseConfig {

  /*@Value("${db.driver}")
  private String DB_DRIVER;
  
  @Value("${db.password}")
  private String DB_PASSWORD;
  
  @Value("${db.url}")
  private String DB_URL;
  
  @Value("${db.username}")
  private String DB_USERNAME;

  @Value("${hibernate.dialect}")
  private String HIBERNATE_DIALECT;
  
  @Value("${hibernate.show_sql}")
  private String HIBERNATE_SHOW_SQL;
  
  @Value("${hibernate.hbm2ddl.auto}")
  private String HIBERNATE_HBM2DDL_AUTO;

  @Value("${entitymanager.packagesToScan}")
  private String ENTITYMANAGER_PACKAGES_TO_SCAN;*/
  
  @Bean
  public DataSource dataSource() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName(PropsUtil.getValue("jdbc.driver"));
    dataSource.setUrl(PropsUtil.getValue("jdbc.url"));
    dataSource.setUsername(PropsUtil.getValue("jdbc.username"));
    dataSource.setPassword(PropsUtil.getValue("jdbc.password"));
    return dataSource;
  } 

  @Bean
  public LocalSessionFactoryBean sessionFactory() {
    LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
    sessionFactoryBean.setDataSource(dataSource());
    sessionFactoryBean.setPackagesToScan(PropsUtil.getValue("packages.to.scan"));
    Properties hibernateProperties = new Properties();
    hibernateProperties.put(PropsUtil.getValue("hibernate.dialect.key"), PropsUtil.getValue("hibernate.dialect.value"));
    hibernateProperties.put(PropsUtil.getValue("hibernate.show_sql.key"), PropsUtil.getValue("hibernate.show_sql.value"));
    hibernateProperties.put(PropsUtil.getValue("hibernate.hbm2ddl.auto.key"), PropsUtil.getValue("hibernate.hbm2ddl.auto.value"));
    hibernateProperties.put(PropsUtil.getValue("connection.pool_size.key"), PropsUtil.getValue("connection.pool_size.value"));
    sessionFactoryBean.setHibernateProperties(hibernateProperties);
    
    return sessionFactoryBean;
  }

  @Bean
  public HibernateTransactionManager transactionManager() {
    HibernateTransactionManager transactionManager = 
        new HibernateTransactionManager();
    transactionManager.setSessionFactory(sessionFactory().getObject());
    return transactionManager;
  }

} // class DatabaseConfig
