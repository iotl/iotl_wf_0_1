package com.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gt.bo.Folder;
import com.gt.bo.Navigation;
import com.gt.entity.Delegation;
import com.gt.entity.DocUserAction;
import com.gt.entity.Users;
import com.gt.services.DocUserActionService;
import com.gt.services.ItemsService;
import com.gt.services.UserService;
import com.gt.utils.PropsUtil;

@RestController
public class NavController {
	
	private String role;
	
	@Autowired
	private DocUserActionService docUserActionService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ItemsService itemsService;
	
	@RequestMapping("/nav/getNav")
    public Navigation getNav(@RequestParam(value="role", defaultValue="admin") String name) {
        return new Navigation(role);
    }
	
	@RequestMapping("/nav/getFolders")
    public  @ResponseBody ArrayList<Folder> getFolders(@RequestParam(value="role", defaultValue="Initiator") String role,@RequestParam(value="userId") String userId,@RequestParam(value="dbRole") String dbRole) {

		
		String cancelRP = PropsUtil.getValue("rp.cancel.enabled");
		String releaseEnable=PropsUtil.getValue("rp.release");
		String delegationEnable=PropsUtil.getValue("rp.delegation");
		String includeDraftForEndorsers= PropsUtil.getValue("include.draft.forendorsers");
		Date currentDate = new Date();
		List<Delegation> delegationList=itemsService.findByToDateBefore(currentDate);
		if(delegationList!=null && delegationList.size()>0){
			for(Delegation delegation : delegationList){
				delegation.setIsActive(false);
				itemsService.saveDelegation(delegation);
			}
		}
		boolean isDraft=false;
		Users user=null;
		if(userId!=null && !userId.isEmpty()){
			 user=userService.findByUserid(Integer.parseInt(userId));
			
			List<DocUserAction> dua=docUserActionService.findByUserIdOrderByActionIdDesc(Integer.parseInt(userId));
			if(dua!=null && dua.size()>0){
				DocUserAction docUserAction=dua.get(0);
				System.out.println(docUserAction.getActionId());
				System.out.println(docUserAction.getIsDraft());
				String isDraftStr=docUserAction.getIsDraft();
				if("Yes".equalsIgnoreCase(isDraftStr)){
					isDraft=true;
				}
			}
		}
		
		ArrayList<Folder> foldersList = new ArrayList<Folder>();
		Folder f;
		switch(role){
			case "Initiator":
				
				f = 	new Folder("drafts");
				f.setDisplayName("Drafts");
				foldersList.add(f);
				
				f = 	new Folder("pendingRPs");
				f.setDisplayName("Pending");
				foldersList.add(f);
				
				f = 	new Folder("inProcessRPs");
				f.setDisplayName("In Process");
				foldersList.add(f);
				
				f = 	new Folder("approvedRPs");
				f.setDisplayName("Approved");
				foldersList.add(f);
				if(user==null || user.getDeptId()==null || user.getDeptId()==1 || user.getDeptId()==2 ||user.getDeptId()==0){
					if(releaseEnable.equalsIgnoreCase("enable")){
						f = 	new Folder("releasedRPs");
						f.setDisplayName("Released");
						foldersList.add(f);
					}
				}
				
				if(delegationEnable.equalsIgnoreCase("enable")){
					f = 	new Folder("delegateRPs");
					f.setDisplayName("Delegated");
					foldersList.add(f);
				}
				
				if("true".equalsIgnoreCase(cancelRP)){
				f = 	new Folder("cancelledRPs");
				f.setDisplayName("Cancelled");
				foldersList.add(f);
				}
				
				break;
			case "Endorser":
				if(includeDraftForEndorsers.equalsIgnoreCase("enable")){
				f = 	new Folder("drafts");
				f.setDisplayName("Drafts");
				if(isDraft){
				f.setActive(true);	
				}
				foldersList.add(f);
				}
				f = 	new Folder("pendingRPs");
				f.setDisplayName("Pending");
				if(!isDraft){
					f.setActive(true);	
					}
				foldersList.add(f);
				
				if(delegationEnable.equalsIgnoreCase("enable")){
					f = 	new Folder("delegateRPs");
					f.setDisplayName("Delegated");
					foldersList.add(f);
				}
				
				f = 	new Folder("endorsedByMe");
				f.setDisplayName("Endorsed");
				foldersList.add(f);
				
				f = 	new Folder("referedByMe");
				f.setDisplayName("Referred");
				foldersList.add(f);
				
			
				
				break;
				
			case "Viewer":
				
				f = 	new Folder("inProcessRPs");
				f.setDisplayName("In Process");
				foldersList.add(f);
				
				f = 	new Folder("approvedRPs");
				f.setDisplayName("Approved");
				foldersList.add(f);
				
				if(user==null || user.getDeptId()==null || user.getDeptId()==1 ||user.getDeptId()==0){
					if(releaseEnable.equalsIgnoreCase("enable")){
						f = 	new Folder("releasedRPs");
						f.setDisplayName("Released");
						foldersList.add(f);
					}
				}
			
				break;	
			case "POViewer":
				
	
				
				if(user==null || user.getDeptId()==null || user.getDeptId()==1 ||user.getDeptId()==0){
					if(releaseEnable.equalsIgnoreCase("enable")){
						f = 	new Folder("releasedRPs");
						f.setDisplayName("Released");
						foldersList.add(f);
					}
				}
			
				break;	
			case "Temporary-Delegatee":
			
				
				if(delegationEnable.equalsIgnoreCase("enable")){
					f = 	new Folder("delegateRPs");
					f.setDisplayName("Delegated");
					foldersList.add(f);
				}

			
				
				break;
				
				case "InitEndorser":
					
					f = 	new Folder("drafts");
					f.setDisplayName("Drafts");
					foldersList.add(f);
					
					f = 	new Folder("pendingRPs");
					f.setDisplayName("Pending");
					foldersList.add(f);
					
					if(delegationEnable.equalsIgnoreCase("enable")){
						f = 	new Folder("delegateRPs");
						f.setDisplayName("Delegated");
						foldersList.add(f);
					}
					
					f = 	new Folder("inProcessRPs");
					f.setDisplayName("In Process");
					foldersList.add(f);
					
					f = 	new Folder("referedByMe");
					f.setDisplayName("Referred");
					foldersList.add(f);
					
					f = 	new Folder("approvedRPs");
					f.setDisplayName("Approved");
					foldersList.add(f);
					
					if(user==null || user.getDeptId()==null || user.getDeptId()==1 || user.getDeptId()==0){
					if(releaseEnable.equalsIgnoreCase("enable")){
						f = 	new Folder("releasedRPs");
						f.setDisplayName("Released");
						foldersList.add(f);
					}
					}
					
					if("true".equalsIgnoreCase(cancelRP)){
					f = 	new Folder("cancelledRPs");
					f.setDisplayName("Cancelled");
					foldersList.add(f);
					}			
				
					break;	
			case "Controller":
				if(includeDraftForEndorsers.equalsIgnoreCase("enable")){
					f = 	new Folder("drafts");
					f.setDisplayName("Drafts");
					if(isDraft){
						f.setActive(true);	
						}
					foldersList.add(f);
					}
				f = 	new Folder("pendingRPs");
				f.setDisplayName("Pending");
				if(!isDraft){
					f.setActive(true);	
					}
				foldersList.add(f);
				
				if(delegationEnable.equalsIgnoreCase("enable")){
					f = 	new Folder("delegateRPs");
					f.setDisplayName("Delegated");
					foldersList.add(f);
				}
				
				f = 	new Folder("referedByMe");
				f.setDisplayName("Referred");
				foldersList.add(f);
				
				f = 	new Folder("endorsedByApprover");
				f.setDisplayName("Endorsed");
				foldersList.add(f);
				
				f = 	new Folder("approvedByMe");
				f.setDisplayName("Approved");
				foldersList.add(f);
				
				f = 	new Folder("rejectedByMe");
				f.setDisplayName("Rejected");
				foldersList.add(f);
				
				if(releaseEnable.equalsIgnoreCase("enable")){
					f = 	new Folder("releasedRPsBy");
					f.setDisplayName("Released");
					foldersList.add(f);
				}
				
				
				
				break;
				
				
		
				
			case "Approver":
				if(includeDraftForEndorsers.equalsIgnoreCase("enable")){
					f = 	new Folder("drafts");
					f.setDisplayName("Drafts");
					if(isDraft){
						f.setActive(true);	
						}
					foldersList.add(f);
					}
				f = 	new Folder("pendingRPs");
				f.setDisplayName("Pending");
				if(!isDraft){
					f.setActive(true);	
					}
				foldersList.add(f);
				
				if(delegationEnable.equalsIgnoreCase("enable")){
					f = 	new Folder("delegateRPs");
					f.setDisplayName("Delegated");
					foldersList.add(f);
				}
				
				f = 	new Folder("referedByMe");
				f.setDisplayName("Referred");
				foldersList.add(f);
				if(!dbRole.equalsIgnoreCase("MD")){
				f = 	new Folder("endorsedByApprover");
				f.setDisplayName("Endorsed");
				foldersList.add(f);
				}
				
				f = 	new Folder("approvedByMe");
				f.setDisplayName("Approved");
				foldersList.add(f);
				
				f = 	new Folder("rejectedByMe");
				f.setDisplayName("Rejected");
				foldersList.add(f);
				
				if(releaseEnable.equalsIgnoreCase("enable")){
					f = 	new Folder("releasedRPsBy");
					f.setDisplayName("Released");
					foldersList.add(f);
				}
				
				break;
				
			/*case "CP-Admin":
				f = 	new Folder("drafts");
				f.setDisplayName("Drafts");
				foldersList.add(f);
				
				f = 	new Folder("pendingRPs");
				f.setDisplayName("Pending");
				foldersList.add(f);
				
				f = 	new Folder("inProcessRPs");
				f.setDisplayName("In Process");
				foldersList.add(f);
				
				f = 	new Folder("approvedRPs");
				f.setDisplayName("Approved");
				foldersList.add(f);
				
				if(releaseEnable.equalsIgnoreCase("enable")){
					f = 	new Folder("releasedRPs");
					f.setDisplayName("Released");
					foldersList.add(f);
				}
				if(delegationEnable.equalsIgnoreCase("enable")){
					f = 	new Folder("delegateRPs");
					f.setDisplayName("Delegated");
					foldersList.add(f);
				}
				
				if("true".equalsIgnoreCase(cancelRP)){
				f = 	new Folder("cancelledRPs");
				f.setDisplayName("Cancelled");
				foldersList.add(f);
				}
				
				
				break;*/
		}

        return foldersList;
    }
	
	
}
