
package com.controllers;

import java.util.ArrayList;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gt.drools.WFRules.Business;
import com.gt.drools.WFRules.Message;
import com.gt.drools.WFRules.User;
import com.gt.services.UserService;
@RestController
public class RulesController {

	@Autowired
	UserService userService;
	
	@RequestMapping("/rule/callRule")
	public @ResponseBody String callRule(){
		callRuleEngine();
		return "Done";
	} 
	private void callRuleEngine(){

		try {
			// load up the knowledge base
			KieServices ks = KieServices.Factory.get();
			KieContainer kContainer = ks.getKieClasspathContainer();
			KieSession kSession = kContainer.newKieSession("ksession-rules");

			// go !
			Message message = new Message();
			message.setMessage("Hello World");
			message.setStatus(Message.HELLO);
			kSession.insert(message);
		
			Business business = new Business();
	        business.setBusinessType("Aluminium");
	        business.setRequestType("Planned");
	        business.setValue(1);
	        kSession.insert(business);
	        
	        
	        kSession.fireAllRules();


	        System.out.println("Length :"+business.getUsers().size());

	        ArrayList<User> users = business.getUsers();

	        userService.findUserBasedOnRole(users.get(0).getRole());
	        
			
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}
	
	
}
