package com.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gt.bo.BusinessesBO;
import com.gt.bo.MasterBO;
import com.gt.bo.PlantsBO;
import com.gt.bo.UsersBO;
import com.gt.entity.Businesses;
import com.gt.entity.Category;
import com.gt.entity.Cluster;
import com.gt.entity.Companies;
import com.gt.entity.Material;
import com.gt.entity.Plant;
import com.gt.entity.Sbus;
import com.gt.services.MasterData;

@RestController

public class MasterDataController {
	
	
	@Autowired
	private MasterData masterData ;
	
	@RequestMapping("/masterdata/getMasterDataForPlant")
	public MasterBO getMasterDataForPlant(@RequestParam(value="plantId") Integer plantId) {
				return masterData.getMasterDataForPlant(plantId);
	}
	
	@RequestMapping("/masterdata/getPlantByClusterId")
	public ArrayList<Plant> getPlantByClusterId(@RequestParam(value="plantId") Integer plantId) {
				return masterData.getPlantByClusterId(plantId);
	}
	
	@RequestMapping("/masterdata/getMaterialsForCluster")
	public ArrayList<Material> getMaterialsForCluster(@RequestParam(value="plantId") Integer plantid) {
				return masterData.getMaterialsForCluster(plantid);
	}
	
	@RequestMapping("/masterdata/getMaterialsByBusinessId")
	public List<Material> getMaterialsByBusinessId(@RequestParam(value="businessId") Integer businessId,@RequestParam(value="deptId") String deptId) {
		System.out.println("deptId >>>>"+deptId);
		Integer deptIdInt=null;
		if(!deptId.equalsIgnoreCase("null") && !deptId.isEmpty() && !deptId.equalsIgnoreCase("undefined")){
			deptIdInt=Integer.parseInt(deptId);
		}
		System.out.println("deptId qwre>>>>"+deptId);
				return masterData.getMaterialsByBusinessIdAndDeptId(businessId,deptIdInt);
	}
	
	@RequestMapping("/masterdata/getCompanies")
    public @ResponseBody ArrayList<Companies> getCompanies() {
			ArrayList<Companies> comp = masterData.getCompanies(); 
			return comp;
	}
	
	@RequestMapping("/masterdata/getBusinesses")
    public @ResponseBody ArrayList<Businesses> getBusinesses() {
		ArrayList<Businesses> busi = masterData.getBusinesses();
		return busi;
	}
	
	@RequestMapping("/masterdata/getSBUs")
    public @ResponseBody ArrayList<Sbus>  getSBUs() {
		ArrayList<Sbus> sbu = masterData.getSbuData();
		return sbu;
	
	}
	
	@RequestMapping("/masterdata/getPlant")
    public @ResponseBody ArrayList<Plant> getPlant() {
		ArrayList<Plant> plants = masterData.getPlantData();
		return plants;
	
	}
	
	@RequestMapping("/masterdata/getMaterial")
    public @ResponseBody ArrayList<Material> getMaterial() {
		ArrayList<Material> materials= masterData.getMaterialData();
		return materials;
	
	}
	
	
	
	@RequestMapping("/masterdata/getClusterSBUs")
    public @ResponseBody ArrayList<Cluster>  getClusterSBUs(@RequestParam(value="plantId") Integer plantId) {
		ArrayList<Cluster> sbu = masterData.getClusterSbuData(plantId);
		System.out.println("sbu"+sbu);
		return sbu;
	
	}
	
	@RequestMapping("/masterdata/getPlantNameByClusterId")
	public ArrayList<Plant> name(@RequestParam(value="clusterId") Integer clusterId) {
				return masterData.getPlantNameByClusterId(clusterId);
	}
	
	
	@RequestMapping("/masterdata/getMaterials")
	public List<Material> getMaterials(@RequestParam(value="deptId") String deptId) {
		System.out.println("deptId >>>>"+deptId);
		Integer deptIdInt=null;
		if(!deptId.equalsIgnoreCase("null") && !deptId.isEmpty() && !deptId.equalsIgnoreCase("undefined")){
			deptIdInt=Integer.parseInt(deptId);
		}
		System.out.println("deptId qwre>>>>"+deptId);
				return masterData.getMaterialsByDeptId(deptIdInt);
	}
	
	//method to fetch businesses by companyId
	@RequestMapping("/masterdata/getBusinessesbyCompanyId")
	public @ResponseBody ArrayList<Businesses> getBusinessesbyCompanyId(@RequestParam(value="companyId") Integer companyId) {

		ArrayList<Businesses> busi = masterData.findBusinessesByCompanyId(companyId);
		return busi;
	}
	
	//method to fetch Sbus based on companyId and businessId
	@RequestMapping("/masterdata/getSBUsbyCompanyIdandBusinessId")
    public @ResponseBody ArrayList<Sbus>  getSBUsbyCompanyIdandBusinessId(@RequestParam(value="companyId") Integer companyId,@RequestParam(value="businessId") Integer businessId) {
		ArrayList<Sbus> sbu = masterData.getSBUsbyCompanyIdandBusinessId(companyId,businessId);
		return sbu;
	
	}
	
	//method to fetch Plants based on companyId and BusinessId and SbuId
	@RequestMapping("/masterdata/getPlantsbyCompanyIdandBusinessIdandSbuId")
    public @ResponseBody ArrayList<Plant>  getPlantsbyCompanyIdandBusinessIdandSbuId(@RequestParam(value="companyId") Integer companyId,@RequestParam(value="businessId") Integer businessId,@RequestParam(value="sbuId") Integer sbuId) {
		ArrayList<Plant> plantList = masterData.getPlantsbyCompanyIdandBusinessIdandSbuId(companyId,businessId,sbuId);
		return plantList;
	
	}
	
	//method to fetch business by businessId,companyId
		@RequestMapping(value="/masterdata/getBusinessNotINbyBusinessId",method=RequestMethod.POST)
		public @ResponseBody ArrayList<Businesses> getBusinessNotINbyBusinessId(@RequestBody BusinessesBO businessBO) {
			
			//SELECT * FROM wf.wf_business where Company_Id != 1 and business_Id  NOT IN (1,2,3) group by Business_Id;
			
			List<Integer> businessIdList = new ArrayList<Integer>();
			List<Businesses> businessList = businessBO.getPreviousBusinessList();
			ArrayList<Businesses> busi = new ArrayList<Businesses>();
			if(businessList.size()==0){
				busi = masterData.findAllBusiness();
			}
			else{
			for(Businesses business :businessList){
				businessIdList.add(business.getBusinessId());
				
			}
			
			 busi  = masterData.findbyNotInCompanyIdAndBusinessIdNotInBusinessCollection(businessBO.getCompanyId(), businessIdList);
			}
			return busi;
			
			
		}
		
	@RequestMapping("/masterdata/findAllRpPlants")
	public @ResponseBody ArrayList<Plant> findAllRpPlants(){
		
		return masterData.findAllRpPlants();
		
	}
	
	@RequestMapping(value="/masterdata/getPlantsNotMatchingInPlantsIdList",method=RequestMethod.POST)
	public @ResponseBody ArrayList<Plant> getPlantsNotMatchingInPlantsIdList(@RequestBody PlantsBO plantBO){
		int companyId = plantBO.getCompanyId();
		int businessId= plantBO.getBusinessId();
		int sbuId=plantBO.getSbuId();
		
		
		List<Integer> plantIdList = new ArrayList<Integer>();
		List<Plant> plantMapList = plantBO.getPreviousPlantList();
		ArrayList<Plant> plantRetList = new ArrayList<>();
		if(plantMapList.size()==0){
			plantRetList = masterData.findAllRpPlants();
		}
		else{
		
		for(Plant plant :plantMapList){
			plantIdList.add(plant.getPlantId());
			
		}
		//plantIdList.add(15);plantIdList.add(10);plantIdList.add(11);
		
		plantRetList = masterData.findNotMatchingRecordsInPlantsId(companyId, businessId, sbuId, plantIdList);
		}
		return plantRetList;
 		
		
	}
	
	@RequestMapping("/masterdata/getCategories")
    public @ResponseBody ArrayList<Category> getCategories() {
			ArrayList<Category> category = masterData.getCategories(); 
			return category;
	}

}
