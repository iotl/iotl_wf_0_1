package com.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.sql.Blob;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.ws.rs.Consumes;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gt.bo.CommentBlock;
import com.gt.bo.DocumentData;
import com.gt.bo.DocumentMetaData;
import com.gt.bo.DocumentMetaDataCreate;
import com.gt.bo.DocumentUserActionBO;
import com.gt.entity.Comments;
import com.gt.entity.DocAttachment;
import com.gt.entity.DocUserAction;
import com.gt.entity.DocUserMaster;
import com.gt.entity.DocumentMaster;
import com.gt.entity.RPNumber;
import com.gt.entity.RevenueProposal;
import com.gt.entity.Users;
import com.gt.exception.NotSignedInException;
import com.gt.repository.DocumentMasterRepository;
import com.gt.repository.UserRepository;
import com.gt.services.DocUserActionService;
//import com.gt.bo.TestJsonData;
import com.gt.services.DocumentService;
import com.gt.services.UserService;
import com.gt.utility.MailMail;
import com.gt.utils.ActionStatus;
import com.gt.utils.DocAttachmentUtil;
import com.gt.utils.DocRole;
import com.gt.utils.ExcelUtils;
import com.gt.utils.PropsUtil;
import com.gt.utils.RPDetailsGeneration;
import com.gt.utils.VelocityStringUtil;
import com.gt.utils.WFUtil;
import com.itextpdf.text.DocumentException;

@RestController
public class DocumentController {

	public static final Logger logger = Logger
			.getLogger(DocumentController.class.getName());

	@Autowired
	private DocumentService documentService;
	
	@Autowired
	private DocUserActionService docUserActionService;

	
	@Autowired
	private DocAttachmentUtil docAttachmentUtil;
	
	@Autowired
	private UserService userservice;

	@Autowired
	MailMail mailMail;
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private DocumentMasterRepository documentMasterRepo;
	
	@Autowired
	private RPDetailsGeneration rpDetailsGeneration;

	// testing
	@RequestMapping("/documentService/getDocumentMetada")
	public ArrayList<DocumentMetaData> getDocumentMetada(
			@RequestParam(value = "documentId") Integer documentId,Integer userId) throws Exception {

		return documentService.getDocumentMetada(documentId,userId);
	}
	
	@RequestMapping("/documentService/rpDetailsGeneration")
	public void rpDetailsGeneration(
			) throws Exception {

		rpDetailsGeneration.generateRpDetailsAsExcel();;
	}
	
	


	@RequestMapping("/documentService/getDocumentUnits")
	public ArrayList<DocumentData> getDocumentUnits(
			@RequestParam(value = "documentId") Integer documentId) {
		return documentService.getDocumentUnits(documentId);

	}

	@RequestMapping("/documentService/getDocumentData")
	public ArrayList<DocumentData> getDocumentData(
			@RequestParam(value = "documentId") Integer documentId) {

		return documentService.getDocumentData(documentId);
	}

	@RequestMapping("/documentService/getDocumentTotal")
	public ArrayList<DocumentData> getDocumentTotal(
			@RequestParam(value = "documentId") Integer documentId) {

		return documentService.getDocumentTotal(documentId);
	}

	@RequestMapping("/documentService/getDraftDocumentData")
	public ArrayList<RevenueProposal> getDraftDocumentData(
			@RequestParam(value = "documentId") Integer documentId) throws SQLException{
		return documentService.getDraftDocumentData(documentId);
	}
	
	@RequestMapping("/documentService/getDraftXportationData")
	public ArrayList<RevenueProposal> getDraftXportationData(
			@RequestParam(value = "documentId") Integer documentId) throws SQLException{
		return documentService.getDraftXportationData(documentId);
	}
	

	@RequestMapping("/documentService/getDraftDocumentTotal")
	public ArrayList<RevenueProposal> getDraftDocumentTotal(
			@RequestParam(value = "documentId") Integer documentId) {

		return documentService.getDraftDocumentTotal(documentId);
	}

	@RequestMapping("/documentService/getdocUserActionByDocumentId")
	public ArrayList<DocUserAction> getDocUserActionByDocumentId(
			@RequestParam(value = "documentId") Integer documentId) {
		return docUserActionService.getDocUserActionByDocumentId(documentId);
	}

//	@RequestMapping("/documentService/getComment")
//	public ArrayList<Comments> getComment() {
//		return documentService.getComment();
//	}

	@RequestMapping("/documentService/getDocumentUserAction")
	public ArrayList<DocumentUserActionBO> getDocumentUserAction(
			@RequestParam(value = "documentId") Integer documentId,
			@RequestParam(value = "userId") Integer userId) throws Exception{
		return docUserActionService.getDocumentUserAction(documentId, userId);

	}

	@RequestMapping("/documentService/getDocumentUserActionCurrentUser")
	public ArrayList<DocumentUserActionBO> getDocumentUserActionCurrentUser(
			@RequestParam(value = "documentId") Integer documentId,
			@RequestParam(value = "currentId") Integer userId) {
		return docUserActionService.getDocumentUserActionCurrentUser(documentId,
				userId);
	}

	@RequestMapping("/documentService/getDocumentUserActionInQueue")
	public ArrayList<DocumentUserActionBO> getDocumentUserActionInQueue(
			@RequestParam(value = "documentId") Integer documentId,@RequestParam(value = "currentId") Integer userId
			,@RequestParam(value ="isReferredInitiator") Boolean isReferredInitiator) {
		/*Boolean isReferredInitiator=false; 
		if(isSave!=null && isSave.equals("true")){
			isReferredInitiator=true;
		}*/
		System.out.println("isReferredInitiator"+isReferredInitiator);
		//System.out.println("isSave"+isSave);
		return docUserActionService.geUsersInQueue(documentId,userId, false,isReferredInitiator,false);
	}

	@RequestMapping("/documentService/getDocumentUserMasterInQueue")
	public ArrayList<DocumentUserActionBO> getDocumentUserMasterInQueue(
			@RequestParam(value = "documentId") Integer documentId,@RequestParam(value = "isViewOnlyIT") Boolean isViewOnlyIT) {
		/*Boolean isReferredInitiator=false; 
		if(isSave!=null && isSave.equals("true")){
			isReferredInitiator=true;
		}*/
//		System.out.println("isReferredInitiator"+isReferredInitiator);
		//System.out.println("isSave"+isSave);
		return docUserActionService.getDocumentUserMasterInQueue(documentId, isViewOnlyIT);
	}

	
	@RequestMapping("/documentService/getSupportingEndorsers")
	public ArrayList<DocumentUserActionBO> getSupportingEndorsers(
			@RequestParam(value = "documentId") Integer documentId, @RequestParam(value = "currentId") Integer userId) {
		return docUserActionService.geUsersInQueue(documentId,userId, true,false,false);
	}
	
	
	@RequestMapping("/documentService/getImmediatedEndorser")
	public ArrayList<DocumentUserActionBO> getImmediatedEndorser(
			@RequestParam(value = "userId") Integer userId) {
		return docUserActionService.getImmediateNextEndorsers(userId);
	}

	@RequestMapping("/doc/getDocAction")
	public CommentBlock getDocAction(
			@RequestParam(value = "userid") String userid) {
		return new CommentBlock(userid);
	}

	@RequestMapping(value = "/documentService/createRPData", method = RequestMethod.POST)
	public @ResponseBody/* String */Status createRPData(
			@RequestBody DocumentMetaDataCreate dmdc) {
		
		System.out.println("sucess");
		DocumentMaster tempDm = documentService.getDocumentMasterByDocumentId(dmdc.getDocumentId());
//		Boolean isRulePresent=documentService.isRulePresent(dmdc);
		if(tempDm==null || tempDm.getIsAmendment() == null || !tempDm.getIsAmendment().equalsIgnoreCase("Yes")){
		Boolean isRulePresent=	documentService.addBusinessRuleUsers(dmdc, null, null,null);
		System.out.println("isRulePresent"+isRulePresent);
		if(isRulePresent!=null && !isRulePresent){
			return new Status(-1, "No Rules");
		}
		}
		DocumentMaster dm = documentService.saveDocumentMetaData(dmdc);
		ArrayList<RevenueProposal> rps = documentService.saveDocumentData(dmdc,
				dm);
		// documentService.creatingRPNumber(dmdc);
		
		

		documentService.addBusinessRuleUsers(dmdc, dm, rps,null);

		// to show rpnumber in the create rp screen after the save & continue
		// button is clicked.
		String rpNum = null;
		if (rps != null && rps.size() > 0) {
			rpNum = rps.get(0).getRpNumber();
		}

		//

		System.out.println("posted on server: success " + dmdc.getTitle() + " "
				+ dmdc.getCompany().getCompanyName() + " "
				+ dmdc.getBusiness().getBusinessName() + " "
				+ dmdc.getSbu().getSbuName() + " "/*
												 * +dmdc.getPlants().getPlantName
												 * ()
												 */+ " "
				+ dmdc.getMaterial().getMaterialName() + " " + dmdc.getIsExp()
				+ " " + dmdc.getIsRelPar());
		
		DocUserAction dua=docUserActionService.saveDocUserActionForDraft(dm.getDocumentId());
		Comments commentEty = new Comments();
		commentEty.setDocumentId(dm.getDocumentId());

		commentEty.setActionId(dua.getActionId());
		
		String comments="";
				
		Comments commentsEntity = docUserActionService.saveComments(commentEty,comments);


		return new Status(1, rpNum, dm.getDocumentId(),commentsEntity.getComment_id(),commentEty.getActionId());
	}

	@RequestMapping(value = "/documentService/updateRPData", method = RequestMethod.POST)
	public @ResponseBody/* String */Status updateRPData(
			@RequestBody DocumentMetaDataCreate dmdc) throws  DocumentException,
			IOException  {
		Boolean isRulePresent=true;
		Boolean isReply = dmdc.getIsReferredToInitiator();

		System.out.println("sucess"+isReply);
		DocumentMaster tempDm = documentService.getDocumentMasterByDocumentId(dmdc.getDocumentId());
//		Boolean isRulePresent=documentService.isRulePresent(dmdc);
		if(tempDm==null || tempDm.getIsAmendment() == null || !tempDm.getIsAmendment().equalsIgnoreCase("Yes")){
		isRulePresent=	documentService.addBusinessRuleUsers(dmdc, null, null,isReply);
		System.out.println("isRulePresent"+isRulePresent);
		if(isRulePresent!=null && !isRulePresent){
			return new Status(-1, "No Rules");
		}
		}
		DocumentMaster dm = documentService.saveDocumentMetaData(dmdc);
		ArrayList<RevenueProposal> rps = documentService.saveDocumentData(dmdc,
				dm);

		// documentService.deleteDouUserMaster(dm);
		if(isRulePresent!=null){
		documentService.addBusinessRuleUsers(dmdc, dm, rps,isReply);
		}

		// to show rpnumber in the create rp screen after the save & continue
		// button is clicked.
		String rpNum = null;
		if (rps != null && rps.size() > 0) {
			rpNum = rps.get(0).getRpNumber();
		}
		System.out.println("posted on server: success " + dmdc.getTitle() + " "
				+ dmdc.getCompany().getCompanyName() + " "
				+ dmdc.getBusiness().getBusinessName() + " "
				+ dmdc.getSbu().getSbuName() + " "/*
												 * +dmdc.getPlants().getPlantName
												 * ()
												 */+ " "
				+ dmdc.getMaterial().getMaterialName() + " " + dmdc.getIsExp()
				+ " " + dmdc.getIsRelPar());

		int commentId=0;
		int actionId=0;
		ArrayList<DocUserAction> duaList=docUserActionService.getDocUserActionByDocumentId(dm.getDocumentId());
		if(duaList== null || (duaList!=null && duaList.size()==0)){
		DocUserAction dua=docUserActionService.saveDocUserActionForDraft(dm.getDocumentId());
		actionId =dua.getActionId();
		Comments commentEty = new Comments();
		commentEty.setDocumentId(dm.getDocumentId());

		commentEty.setActionId(dua.getActionId());
		
		String comments="";
				
		Comments commentEntity = docUserActionService.saveComments(commentEty,comments);
		commentId=commentEntity.getComment_id();
		}else{
			actionId =duaList.get(duaList.size()-1).getActionId();
			Comments commentEntity=documentService.getCommentByAction_id(actionId);
			if(commentEntity!=null){
				commentId=commentEntity.getComment_id();
			}
		}
		System.out.println("<<<< commentId >>>>"+commentId);
		System.out.println("<<<< actionId >>>>"+actionId);
		
		
		return new Status(1, rpNum, dm.getDocumentId(),commentId,actionId);
	}


	/**
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 * @throws ServletException
	 * @throws Exception
	 */
	@RequestMapping(value = "/documentService/uploadDoc", method = RequestMethod.POST)
	public synchronized @ResponseBody Status performDocUserAction(
			HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException,
			Exception {
		logger.info("Inside doccontroller");
		System.out.println("Success 2");
		String documentId = request.getParameter("documentId");
		String isRPExpress = request.getParameter("isRPExpress");
		String isCreate = request.getParameter("isCreateScreen");
		String isApprove = request.getParameter("isApproved");
		String isCreateFinalApproved = request.getParameter("isCreateFinalApproved");
		String isRejected = request.getParameter("isRejected");
		String isReferedBack = request.getParameter("isRefered");
		String referedTo = request.getParameter("referedTo");
		String isReferedRP = request.getParameter("isReferedRP");
		String isRelease = request.getParameter("isRelease");
		String parallelFinished = request.getParameter("parallelFinished");
		String parallelStarted = request.getParameter("parallelStarted");
		String loggedInUserId = request.getParameter("userId");
//		String loggedInUserId = request.getParameter("userId");
		String delegatedBy = request.getParameter("delegatedBy");
		String comments = request.getParameter("comments");
	//	String fileCount = request.getParameter("fileCount");
		String dbRole = request.getParameter("dbRole");
		String rpNum=request.getParameter("rpNum");
		String isEndorse = null;
		String commentIdStr=request.getParameter("commentId");
		String actionIdStr=request.getParameter("actionId");
		String includeComments=request.getParameter("includeComments");
		String includeAttachment=request.getParameter("includeAttachment");
		String saveClicked= request.getParameter("saveClicked");
		String isReferredToInitiator=request.getParameter("isReferredToInitiator");
		String titleCorp = request.getParameter("titleCorp");
		String categoryIdCorp = request.getParameter("categoryIdCorp");
		String actualRoleName = request.getParameter("actualRoleName");
		Comments commentEty = new Comments();
		boolean insertComments=true;
		// commentEty.setComment(comments);
		System.out.println("Delegated By " + delegatedBy);
		System.out.println("isReferredToInitiator " + isReferredToInitiator);
		Integer delegatedByInt =null;
		if(delegatedBy!= null){
			delegatedByInt = Integer.parseInt(delegatedBy);
		}else{
			delegatedByInt= -1;
		}
		if(saveClicked!=null && (saveClicked.equals("false") || saveClicked.equals("true"))){
			updateComments(request, response,false);
		}
		
		Integer actionIdInt  = null;
		if(actionIdStr!=null && !actionIdStr.isEmpty() && !actionIdStr.equals("null") && !actionIdStr.equals("undefined")){
			actionIdInt  =Integer.parseInt(actionIdStr);
		}
		
		Integer loggedInUserIdInt = Integer.parseInt(loggedInUserId);
		Integer documentIdInt = Integer.parseInt(documentId);
		commentEty.setDocumentId(Integer.parseInt(documentId));
		DocUserAction dua = null;
		List<DocUserAction> duaList =null;
		if(!"true".equals(isCreate)){
			
			List<DocUserAction> duaParallel=docUserActionService.findByDocumentIdAndActionStatus(documentIdInt, ActionStatus.PENDING_PARALLEL);
			for(DocUserAction duas:duaParallel){
				if(duas.getUserId() == loggedInUserIdInt){
					insertComments=false;
				}
			}
		if ("true".equals(isApprove)) {
			dua = docUserActionService.saveApprovedData(documentIdInt,
					loggedInUserIdInt,delegatedByInt);
		}else if ("true".equals(isCreateFinalApproved)) {
			dua = docUserActionService.saveCreateFinalApprovedData(documentIdInt,
					loggedInUserIdInt,delegatedByInt);
		}else if ("true".equals(isRejected)) {
			dua = docUserActionService.saveRejectedData(documentIdInt,
					loggedInUserIdInt,delegatedByInt);
		} else if ("true".equals(isReferedBack)) {
			// System.out.println("ReferedBack");
			dua = docUserActionService.saveReferedBackData(loggedInUserIdInt,delegatedByInt,
					documentIdInt, Integer.parseInt(referedTo));
		} else if ("true".equals(isReferedRP)) {
			//dua = docUserActionService.saveReply(loggedInUserIdInt,delegatedByInt, documentIdInt,isReferredToInitiator);
			if(isReferredToInitiator!=null && isReferredToInitiator.equalsIgnoreCase("true")){
				System.out.println("isReferredToInitiator");
				dua = docUserActionService.saveDocUserActionView(documentIdInt, loggedInUserIdInt, null, "false", null);
				
			}else{
				dua = docUserActionService.saveReply(loggedInUserIdInt,delegatedByInt, documentIdInt,isReferredToInitiator);
			}
		} else if ("true".equals(isRelease)) {
			dua = docUserActionService.saveReleaseData(loggedInUserIdInt,delegatedByInt,
					documentIdInt);
		} else if ("true".equals(isCreate)) {
			dua = docUserActionService.saveDocUserAction(documentIdInt, isRPExpress,actionIdInt);
		} else {
			isEndorse = "true";
			System.out.println("parallelFinished >>>>"+parallelFinished);
			System.out.println("parallelStarted >>>>"+parallelStarted);
			dua = docUserActionService.saveDocUserActionView(documentIdInt,
					loggedInUserIdInt, parallelFinished, parallelStarted,
					delegatedByInt);
		}
		
		Comments tempComments =null;
		int commentId=0;
		System.out.println("isRelease >>>>"+isRelease);
		logger.info("isRelease "+isRelease+" isApproved "+ isApprove );
		duaList =docUserActionService.findByDocumentIdAndActionStatus(documentIdInt, ActionStatus.RELEASED);
			commentEty.setActionId(dua.getActionId());
			
		    if(comments!=null && !comments.isEmpty()){
		    	comments=comments.replaceFirst("<table", "<table width= \"100%\" border= \"1\" ");
		    	comments=comments.replaceAll("<td", "<td style=\"border-width: 1px; border-style: solid;\" ");
		        }
		    System.out.println("commentIdStr >>>>"+commentIdStr);
		    System.out.println("commentIdStr >>>>"+commentIdStr);
		    if (commentIdStr!=null && !commentIdStr.isEmpty() && !commentIdStr.equals("null") && !commentIdStr.equals("undefined")) {
				commentId=Integer.parseInt(commentIdStr);
				commentEty.setComment_id(commentId);;
			}
				DocUserAction docUserAction=docUserActionService.findByActionId(dua.getActionId());
				docUserAction.setIsDraft("No");
				docUserActionService.updateDocUserAction(docUserAction);
				System.out.println("docuserAction Draft perform DUA"+docUserAction.getIsDraft());
				logger.info("docUserAction.getIsDraft() perform DUA>>>>>"+dua.getActionId()+"isDraft>>>"+docUserAction.getIsDraft());
			tempComments = docUserActionService.saveComments(commentEty,comments);
			commentId= tempComments.getComment_id();

			
	    
	   DocumentMaster dm=documentMasterRepo.findByDocumentId(documentIdInt);
	   logger.info("Dm Status 2"+dm.getStatus());
	   logger.info("isRelease "+isRelease+" isApproved "+ isApprove );
		if ("true".equals(isRelease) ||("true".equals(isApprove)&&dm.getDeptId()!=null && dm.getDeptId()==2 && dm.getProposalType()!=null && dm.getProposalType().equalsIgnoreCase("Final"))) {
			logger.info("isRelease 2 "+isRelease+" isApproved 2 "+ isApprove );
			boolean isMail=true;
				docAttachmentUtil.writePdf(request,rpNum, loggedInUserId, documentId,includeComments,actionIdInt,isMail,null);

			
		}
		}else if ("true".equals(isCreate)){
				dua = docUserActionService.saveDocUserAction(documentIdInt, isRPExpress,actionIdInt);
		}
		
		 saveComments(documentIdInt,loggedInUserIdInt,insertComments);

		/**
		 * Mail sending code starts here
		 */
		
		Status status=sendMail(loggedInUserIdInt, documentIdInt, delegatedByInt, isCreate, isCreateFinalApproved, isRejected, isApprove, isReferedBack, 
				isReferedRP, isRelease, dbRole, isEndorse, referedTo,includeComments,includeAttachment,duaList,isReferredToInitiator);
		
		return status;
	}
	
	
	/**
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 * @throws ServletException
	 * @throws Exception
	 */
	@RequestMapping(value = "/documentService/sendMail", method = RequestMethod.POST)
	 public @ResponseBody Status sendMail(
	   HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException,
	   Exception {
	  String ccAddress=request.getParameter("ccAddress");
	  String toAddress=request.getParameter("toAddress");
	  String userId=request.getParameter("userId");
	  String subject=request.getParameter("subject");
	  String mailContent=request.getParameter("mBody");
	  String aliasName=null;
	  
	  String test = PropsUtil.getValue("release.testing");
		 String subjectAppend="";
		 
		 if(test.equalsIgnoreCase("true")){
			 subjectAppend="Testing e-TrackRev ";
		 }
		 subject=subjectAppend+subject;
	  Users user=userRepo.findByUserId(Integer.parseInt(userId));
	 
	   if(user!=null){
		   aliasName=user.getEmailId();
		   if(ccAddress!=null && !ccAddress.isEmpty() && !ccAddress.equalsIgnoreCase("undefined") && !ccAddress.equalsIgnoreCase("null")){
			   ccAddress=ccAddress+","+user.getEmailId();
		   }else{
			   ccAddress=user.getEmailId();
		   }
	  }
	  
//	  String domainName=PropsUtil.getValue("app.domain");
	  StringBuilder sb=new StringBuilder();
	  sb.append("\r\n");
	  sb.append("\r\n");
	  sb.append("\r\n");
//	  sb.append("Please login at: ");
//	  sb.append(domainName);
	  sb.append("\r\n");
	  sb.append("Administrator,");
	  sb.append("\r\n");
	  sb.append("e-TrackRev Application,");
	  sb.append("\r\n");
	  sb.append("Hindalco & UAIL.");
		  if(mailContent==null  || mailContent.isEmpty() || (mailContent!=null && (mailContent.equalsIgnoreCase("undefined"))) || mailContent.equalsIgnoreCase("null")){
			  mailContent="PFA";
		  }
		  mailContent=mailContent+sb.toString();
	  writeRPASPDF(request);
	  List<String> filePathList= new ArrayList<String>();
	  String rpNum= request.getParameter("rpNum");
	  String specialChars2 = "[$&+,.:;=?@#|'<>^*()%!-/]";
	  rpNum = rpNum.replaceAll(specialChars2, "_");
	   String fileName = rpNum + ".pdf";
	     String filePath = PropsUtil.getValue("temp.path");
	     filePath = filePath + "\\pdf" + userId;
	     fileName = rpNum + ".pdf";
	     filePath = filePath + "\\" + fileName;
	     filePathList.add(filePath);
	     return  mailMail.sendMailWithAttachmentForEndorsers(subject, mailContent, false, filePathList, toAddress, ccAddress, 1,aliasName);
	  //String toAddress=request.getParameter("toAddress");
	 // return new Status(1,null);
	  
	 }
	
	/**
	 * @param loggedInUserIdInt
	 * @param documentIdInt
	 * @param delegatedByInt
	 * @param isCreate
	 * @param isRejected
	 * @param isApprove
	 * @param isReferedBack
	 * @param isReferedRP
	 * @param isRelease
	 * @param dbRole
	 * @param isEndorse
	 * @param referedTo
	 * @return
	 */
	private Status sendMail(Integer loggedInUserIdInt,Integer documentIdInt,Integer delegatedByInt,String isCreate,String isCreateFinalApproved,String isRejected,String isApprove,String isReferedBack,String isReferedRP,String isRelease,String dbRole,
			String isEndorse,String referedTo,String includeComments,String includeAttachment,List<DocUserAction> duaList,String isReferredToInitiator){
		System.out.println("isReferredToInitiator 12>>>>>"+isReferredToInitiator);
		Status status = null;
		DocumentMaster dm = documentService
				.getDocumentMasterByDocumentId(documentIdInt);
		List<RevenueProposal> revenueProposalList = documentService
				.getRevenueProposalByDocumentId(documentIdInt);
		String rpNumber = null;
		if (revenueProposalList != null && revenueProposalList.size() > 0) {
			rpNumber = revenueProposalList.get(0).getRpNumber();
		}
		String documentType=dm.getDocumentType();
		if(documentType == null || documentType.isEmpty()){
			documentType="RP";
		}
		String subject = "";
		String templateName = "/com/gt/templates/mailTemplate.html";
		templateName = WFUtil.getFileContent(templateName, true);
		String message = "This is to inform you that a new "+documentType+" with "+documentType+" No. <b>'"
				+ rpNumber + "'</b> has been Created & awaiting your action.";
		if ("true".equals(isCreate)) {
			message = "This is to inform you that a new "+documentType+" with "+documentType+" No. <b>'"
					+ rpNumber + "'</b> has been Created & awaiting your action.";
			
		} else if ("true".equals(isCreateFinalApproved)) {
			message = "This is to inform you that a Final Approval "+documentType+" with "+documentType+" No. <b>'"
					+ rpNumber + "'</b> is awaiting your action.";
			
		}else if ("true".equals(isRejected)) {
			message = "This is to inform you that "+documentType+" No. <b>'" + rpNumber
					+ "'</b> has been Rejected.";
		} else if ("true".equals(isApprove)) {
			 if(dm.getDeptId()!=null && dm.getDeptId()==2 && dm.getProposalType()!=null && dm.getProposalType().equalsIgnoreCase("Final")){
				 message = "This is to inform you that Final Approval "+documentType+" No. <b>'" + rpNumber
							+ "'</b> has been Released.";
			 }else{
			message = "This is to inform you that "+documentType+" No. <b>'" + rpNumber
					+ "'</b> has been Approved.";
			 }
		} else if ("true".equals(isReferedBack)) {
			message = "This is to inform you that "+documentType+" No. <b>'" + rpNumber
					+ "'</b> has been Reffered Back & awaiting your action.";
		} else if ("true".equals(isReferedRP)) {
			 if(dm.getDeptId()!=null && dm.getDeptId()==1){
				 message = "This is to inform you that "+documentType+" No. <b>'" + rpNumber
							+ "'</b> has been Endorsed & awaiting your action.";
			 }else{
				 message = "This is to inform you that "+documentType+" No. <b>'" + rpNumber
					+ "'</b> has been Replied & awaiting your action.";
			 }
		} else if ("true".equals(isRelease)) {
			// System.out.println("ReferedBack");
			
			message = "This is to inform you that "+documentType+" No. <b>'" + rpNumber
					+ "'</b> has been Released.";
			 
		} else {

			message = "This is to inform you that "+documentType+" No. <b>'" + rpNumber
					+ "'</b> has been Endorsed & awaiting your action.";
		}
		logger.info("isRelease "+isRelease+" isApproved "+ isApprove );
		System.out.println("isReferredToInitiator 123>>>>>"+isReferredToInitiator);
		String plantName = null;
		String material = null;
	/*	if (dbRole != null && !dbRole.isEmpty() && "Initiator".equals(dbRole)) {
			isCreate = "true";
			isEndorse = "false";
		}*/
		Integer delegatedTo= null;
		String isCancelled = null;
		Map<String, List<String>> getMailIds = documentService
				.getMailIdsToSend(dm, isCreate, isCreateFinalApproved, isApprove, isRejected,
						isRelease, isEndorse, loggedInUserIdInt,delegatedTo, delegatedByInt, rpNumber,
						revenueProposalList, isReferedBack, referedTo,
						isReferedRP,isCancelled);
		if (getMailIds != null) {
			String mailHighPriority;
			if (dm.getIsExpress() != null && dm.getIsExpress()) {
				mailHighPriority = "1";
			} else {
				mailHighPriority = "3";
			}
			List<String> toMailIds = getMailIds.get("TO");
			List<String> ccMailIds = getMailIds.get("CC");
			List<String> subjectList = getMailIds.get("SUBJECT");
			List<String> plantNameList = getMailIds.get("PLANTNAME");
			List<String> materialList = getMailIds.get("MATERIAL");
			if (subjectList != null && subjectList.size() > 0) {
				subject = subjectList.get(0);
			}
			if (plantNameList != null && plantNameList.size() > 0) {
				plantName = plantNameList.get(0);
			}
			if (materialList != null && materialList.size() > 0) {
				material = materialList.get(0);
			}
			String[] toMailIdsArr = toMailIds.toArray(new String[toMailIds
					.size()]);
			String[] ccMailIdsArr = ccMailIds.toArray(new String[ccMailIds
					.size()]);
			String toAddress = null;
			String ccAddress = null;

			if (toMailIdsArr != null && toMailIdsArr.length > 0
					/*&& ccMailIdsArr != null && ccMailIdsArr.length > 0*/) {
				toAddress = Arrays.toString(toMailIdsArr);

				StringTokenizer removeBraces = new StringTokenizer(toAddress,
						"[]");
				toAddress = removeBraces.nextToken();
				if( ccMailIdsArr != null && ccMailIdsArr.length > 0){
					ccAddress = Arrays.toString(ccMailIdsArr);
					removeBraces = new StringTokenizer(ccAddress, "[]");
					ccAddress = removeBraces.nextToken();
				}
				System.out.println(toAddress);
				System.out.println(ccAddress);
				System.out.println(subject);
				
				if(isReferredToInitiator!= null && isReferredToInitiator.trim().equalsIgnoreCase("true")){
					System.out.println("isReferredToInitiator >>>>>"+isReferredToInitiator);
					List<DocUserMaster> dumList =documentService.findByDocumentIdAndDocRoleIn(documentIdInt, WFUtil.getDocRoles());
					for(DocUserMaster dum :dumList ){
						Users usr=userservice.findByUserid(dum.getUserId());
						ccAddress=ccAddress+","+usr.getEmailId();
					}
				}
				
				System.out.println(ccAddress);
				
				String appURL = PropsUtil.getValue("app.domain")+"/login";
				
				Map<String, Object> nameValueMap = new HashMap<String, Object>();
				
				nameValueMap.put("message", message);
				nameValueMap.put("plantName", plantName);
				nameValueMap.put("materials", material);
				nameValueMap.put("hostName", appURL);
				nameValueMap.put("delegatedByName", "null");
				if(documentType.equalsIgnoreCase("RP")){
					nameValueMap.put("labelName", "Materials");
				}else{
					nameValueMap.put("labelName", "Category");
				}

				String mailContent = VelocityStringUtil.getFinalContent("SM",
						templateName, nameValueMap);
				// System.out.println("mailContent >>>>>"+mailContent);
				System.out.println("includeAttachment >>>>>" + includeAttachment);
//				status = mailMail.sendHtmlMail(null, toAddress, ccAddress, subject,
//						mailContent, mailHighPriority);
				if("true".equalsIgnoreCase(isRelease) ||("true".equals(isApprove)&&dm.getDeptId()!=null && dm.getDeptId()==2 && dm.getProposalType()!=null && dm.getProposalType().equalsIgnoreCase("Final"))){
					templateName = "/com/gt/templates/releaseRPMailTemplate.html";
					templateName = WFUtil.getFileContent(templateName, true);

					/*if(("true".equals(isApprove)&&dm.getDeptId()!=null && dm.getDeptId()==2 && dm.getProposalType()!=null && dm.getProposalType().equalsIgnoreCase("Final"))){
						includeAttachment="true";
					}*/
					System.out.println("mailContent >>>>>" + mailContent);
					DocUserMaster dum= null;
					Users user= null;
				/*	if(dm.getDeptId()!=null && dm.getDeptId()!=1){
						 dum = documentService.findByDocumentIdAndDocRole(documentIdInt, DocRole.CONTROLLER);
						if(dum == null){
							dum = documentService.findByDocumentIdAndDocRole(documentIdInt, DocRole.APPROVER);
						}
						
						 user=userservice.findByUserid(dum.getUserId());
						toAddress=user.getEmailId();
					}*/
				//	for(DocUserAction dua : duaList){
						//+ "_"+dua.getPlantId()
						String fileName = rpNumber + ".pdf";
						String filePath = PropsUtil.getValue("temp.path");
//						String plantNames=documentService.getPlantName(dua.getPlantId());
						String proposalType= dm.getProposalType();
						String currentSub="";
						if(proposalType!=null && proposalType.equalsIgnoreCase("Final")){
							currentSub="Intimation of Final RP Approval";
						}else{
						    currentSub="Intimation of RP Approval";
						}
//						String currentSub="Intimation of RP Approval for '"+material+"' for'"+plantNames+"' plant";
						
						filePath = filePath + "\\" + fileName.replaceAll("/", "_");
						List<String> filePathList= new ArrayList<String>();
						filePathList.add(filePath);
						if(includeAttachment.equals("true") ){
							List<String> tempList=documentService.writeFilesToTempFolder(documentIdInt);
							filePathList.addAll(tempList);
						}
//						if(dm.getDeptId()!=null && dm.getDeptId()!=1){
//							dum = documentService.findByDocumentIdAndDocRole(documentIdInt, DocRole.INITIATOR);
//							user=userservice.findByUserid(dum.getUserId());
//							ccAddress=user.getEmailId();
//						}
//						nameValueMap.put("plantName", plantNames);
						mailContent=VelocityStringUtil.getFinalContent("SM",templateName, nameValueMap);
						System.out.println("mailContent >>>>>"+mailContent);
						System.out.println("currentSub >>>>>"+currentSub);
						status = mailMail.sendMailWithAttachment(currentSub, mailContent, true, filePathList, toAddress, ccAddress, 1);
//						status = mailMail.sendHtmlMail(null, toAddress, ccAddress, currentSub,
//								mailContent, mailHighPriority);
					//}
					
				}else{
					System.out.println("mailContent >>>>>"+mailContent);
					System.out.println("subject >>>>>"+subject);
					status = mailMail.sendHtmlMail(null, toAddress, ccAddress, subject,
							mailContent, mailHighPriority);
				}
			}

		}
		return status;
	}
	
	
	/**
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/documentService/removeAttachmentForCreate")
	public synchronized @ResponseBody Status removeAttachmentForCreateRP(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String fileName = request.getParameter("fileName");
		//System.out.println("fileName  fileName>>>"+fileName);
		String specialChars2 = "[$&+,:;=?@#|'<>^*()%!-]";
		fileName = fileName.replaceAll(specialChars2, "_");
		String commentId = request.getParameter("commentId");
		//System.out.println("fileName >>>"+fileName);
		//System.out.println("commentId >>>"+commentId);
		int deleted=documentService.deleteByFileNameAndCommentId(fileName, Integer.parseInt(commentId));
		return new Status(deleted, fileName);
	}


/**
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@Consumes("multipart/form-data")
	@RequestMapping(value = "/documentService/uploadAttachmentForCreate", method = RequestMethod.POST)
	public synchronized @ResponseBody Status uploadAttachmentForCreateRP(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		//Declaration Part
		String documentId=request.getParameter("documentId");
		String commentId=request.getParameter("commentId");
		String userId = request.getParameter("userId");
		String userAgent = request.getHeader("User-Agent");
		Integer documentIdInt=Integer.parseInt(documentId);
		Integer commentIdInt=Integer.parseInt(commentId);
		String filePath = PropsUtil.getValue("temp.path");
		Part input = request.getPart("file1");
		
		
		
		//Main code
		InputStream inputStream = input.getInputStream();
		
		// convert the uploaded file to inputstream

		byte[] bytes = IOUtils.toByteArray(inputStream);
		// constructs upload file path
		String fileName = WFUtil.getFileName(input);
		String specialChars2 = "[$&+,:;=?@#|'<>^*()%!-]";
		fileName = fileName.replaceAll(specialChars2, "_");
		System.out.println("fileName >>>>>"+fileName);
		File orgFile = new File(filePath);
		if (orgFile != null) {
			// tempFile.deleteOnExit();
			orgFile.mkdir();
		}
		
		filePath = filePath + "\\vwatt" + userId;
		File tempFile = new File(filePath);
		if (tempFile != null) {
			System.out.println("inside tempfile");
			tempFile.mkdir();
		}
		
		File file = new File(filePath + "\\" + fileName);
		// Create a file in the folder
		if (!file.exists()) {
			file.createNewFile();
		}

		//String fileTempName = file.getAbsolutePath();
		FileOutputStream fop = new FileOutputStream(file);
		fop.write(bytes);
		fop.flush();
		fop.close();
		DocAttachment docAttachment = new DocAttachment();
		docAttachment.setDocumentId(documentIdInt);
		docAttachment.setCommentId(commentIdInt);
		String tempFileName=WFUtil.getFileName(input);
		tempFileName = tempFileName.replaceAll(specialChars2, "_");
		
		
		String attachmentType = null;
//		attachmentType = fileName.substring(tempFileName
//				.indexOf('.') + 1);
		String fileNameSplit[] = fileName.split("\\.");
		System.out.println(fileNameSplit.length);
		Integer size = fileNameSplit.length;
		attachmentType = fileNameSplit[size-1];
		System.out.println("attachmentType "+attachmentType);
		Integer maxAttachmentId=documentService.getMaxAttachmentId();
		if(userAgent!=null && (userAgent.indexOf("iPhone")!=-1 || userAgent.indexOf("iPod")!=-1 || userAgent.indexOf("iPad")!=-1)){
			if(tempFileName!=null && tempFileName.indexOf("image")!=-1){
				tempFileName=tempFileName.substring(0,tempFileName.lastIndexOf("."))+maxAttachmentId+"."+attachmentType;
			}
		}
		docAttachment.setFileName(tempFileName);
		docAttachment.setAttachmentType(attachmentType);
		int deleted=documentService.deleteByFileNameAndCommentId(fileName, Integer.parseInt(commentId));
		docUserActionService.saveDocAttachment(docAttachment, file);
		return new Status(deleted, tempFileName);
	}
	
	/**
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/documentService/updateComments", method = RequestMethod.POST)
	public synchronized @ResponseBody Status updateComments(
			HttpServletRequest request, HttpServletResponse response,Boolean isCancel)
			throws Exception {
		String comments = request.getParameter("comments");
		String commentId = request.getParameter("commentId");
		String documentId = request.getParameter("documentId");
		String actionId = request.getParameter("actionId");
		String titleCorp = request.getParameter("titleCorp");
		String categoryIdCorp = request.getParameter("categoryIdCorp");
		String actualRoleName = request.getParameter("actualRoleName");
		String isCreate = request.getParameter("isCreateScreen");
		if(actionId==null || actionId.isEmpty() || actionId.equals("null")  || actionId.equals("undefined")){
		List<DocUserAction> dua=docUserActionService.findByDocumentIdAndActionStatus(Integer.parseInt(documentId), ActionStatus.PENDING);
		actionId=String.valueOf(dua.get(0).getActionId());
		}
		Comments commentEntity=new Comments();
		if(commentId!=null && !commentId.isEmpty() && !commentId.equals("undefined") && !commentId.equals("null")){
		commentEntity.setComment_id(Integer.parseInt(commentId));
		}
		if(actionId!=null && !actionId.isEmpty() && !actionId.equals("null")  && !actionId.equals("undefined")){
			DocUserAction docUserAction=docUserActionService.findByActionId(Integer.parseInt(actionId));
			if(isCancel!=null && isCancel){
				docUserAction.setIsDraft("NO");
			}else{
				docUserAction.setIsDraft("Yes");
			}
			
			docUserActionService.updateDocUserAction(docUserAction);
			System.out.println("docuserAction comment draft>>>>> "+docUserAction.getIsDraft());
			logger.info("docUserAction.getIsDraft() comment actionId>>>"+actionId+"draft>>>"+docUserAction.getIsDraft());
			}
		commentEntity.setDocumentId(Integer.parseInt(documentId));
		commentEntity.setActionId(Integer.parseInt(actionId));
		Comments tempComments=docUserActionService.saveComments(commentEntity, comments);
		
		int documentIdInt = Integer.parseInt(documentId);
		DocumentMaster dm = documentService
				.getDocumentMasterByDocumentId(documentIdInt);
		
		
		return new Status(0, String.valueOf(tempComments.getComment_id()));
	}
	
	
	/**
	 * @param documentId
	 * @return
	 * @throws Exception
	 */
	private void saveComments(Integer documentId,Integer userId,boolean insertComments)
			throws Exception {
		Integer actionId=null;
		List<DocUserAction> dua=docUserActionService.findByDocumentIdAndActionStatus(documentId, ActionStatus.PENDING);
		if(dua!=null && dua.size()>0){
			
			String comments="";
			List<DocUserAction> duaParallel=docUserActionService.findByDocumentIdAndActionStatus(documentId, ActionStatus.PENDING_PARALLEL);
			if(duaParallel!=null && duaParallel.size()>0){
				System.out.println("(dua.get(0).getDocRole() >>>>"+dua.get(0).getDocRole());
				Users usr = userRepo.findByUserId(userId);
				 List<Users> miningController = userservice.findUserBasedOnRole(DocRole.MINING_CONTROLLER);
				 Boolean isMiningController=false;
				 if(miningController.get(0).getUserId()== usr.getUserId()){
					 isMiningController=true;
				 }
	
		
				System.out.println("(dua.get(0).getDocRole() >>>>"+usr.getIsSupportingEndorser());
				if(usr==null ||  usr.getIsSupportingEndorser()==null || (usr!=null && usr.getIsSupportingEndorser()!=null && !usr.getIsSupportingEndorser())|| (usr!=null && usr.getIsSupportingEndorser()!=null && usr.getIsSupportingEndorser() && isMiningController )){
					boolean isComments = false;
					for(DocUserAction duaRef:duaParallel){
						Comments cmts=documentService.findCommentsByActionId(duaRef.getActionId());
						if(cmts!=null){
							isComments = true;
							break;
						}else{
							break;
						}
					}
					
					if(!isComments){
						for(DocUserAction duaction:duaParallel){
							Comments commentEntity=new Comments();
							commentEntity.setDocumentId(documentId);
							commentEntity.setActionId(duaction.getActionId());
							docUserActionService.saveComments(commentEntity, comments);
						}
						
					}
				}
			}
			
		if(insertComments){
			actionId=dua.get(0).getActionId();
		Comments commentEntity=new Comments();
		commentEntity.setDocumentId(documentId);
		commentEntity.setActionId(actionId);
		docUserActionService.saveComments(commentEntity, comments);
		}
		}
	}
	
	
	/*
	private synchronized void uploadAttachments(HttpServletRequest request, HttpServletResponse response,String documentId,Integer commentId)
			throws Exception {
		String loggedInUserId = request.getParameter("userId");
		String fileCount = request.getParameter("fileCount");
		Integer loggedInUserIdInt = Integer.parseInt(loggedInUserId);
		String fileName = "";
		if (fileCount != null && !fileCount.isEmpty()) {
			int fileCountInt = Integer.parseInt(fileCount);
			if (fileCountInt > 0) {
				for (int i = 0; i < fileCountInt; i++) {
					DocAttachment docAttachment = new DocAttachment();
					docAttachment.setDocumentId(Integer.parseInt(documentId));
					docAttachment.setCommentId(commentId);
					Part input = request.getPart("file" + i);
					System.out.println("fileCountInt" + fileCountInt);
					System.out.println("i" + i);
					System.out.println("documentId" + documentId);
					System.out.println("input" + input);
					
					fileName = WFUtil.getFileName(input);
					System.out.println("dsf" + fileName);
					String specialChars2 = "[$&+,:;=?@#|'<>^*()%!-]";
					fileName = fileName.replaceAll(specialChars2, "_");
					System.out.println("dsf" + fileName);
					if (fileName != null && !fileName.isEmpty()) {
						docAttachment.setFileName(fileName);

						System.out.println("getFileName"
								+ docAttachment.getFileName());

						String attachmentType = null;
						attachmentType = fileName.substring(fileName
								.indexOf('.') + 1);
						docAttachment.setAttachmentType(attachmentType);
						String filePath = PropsUtil.getValue("temp.path");
						filePath = filePath + "\\vwatt" + loggedInUserIdInt + "\\" + fileName;
						File file = new File(filePath);
						docUserActionService.saveDocAttachment(docAttachment, file);
						
						
					}

				}
				String filePath = PropsUtil.getValue("temp.path");
				filePath = filePath + "\\vwatt" + loggedInUserId;
				try {
					FileUtils.cleanDirectory(new File(filePath));
				} catch (Throwable e) {
					logger.error("Error While Cleaning Directory: " + filePath, e);
				}
			}

		}
	
	}*/




  

	
	/**
	 * @param request
	 * @param res
	 * @return
	 */
	@RequestMapping(value = "/documentService/downloadFileForCreate")
	public @ResponseBody Status downloadFileForCreateRP(HttpServletRequest request,HttpServletResponse res) {

		try {
			String fileName = request.getParameter("fileName");
			String specialChars2 = "[$&+,:;=?@#|'<>^*()%!-]";
			fileName = fileName.replaceAll(specialChars2, "_");
			String commentId = request.getParameter("commentId");
			System.out.println("fileName >>>>>"+fileName);
			System.out.println("commentId >>>>>"+commentId);
			DocAttachment docAttachment = documentService.getDocAttachmentByFileNameAndCommentId(fileName, Integer.parseInt(commentId));
			Blob blob = docAttachment.getContent();
			byte[] byteArray = blob.getBytes(1, (int)blob.length());
			if (docAttachment.getAttachmentType().equalsIgnoreCase("pdf"))
				res.setContentType("application/pdf");
			else if (docAttachment.getAttachmentType().equalsIgnoreCase("doc"))
				res.setContentType("application/msword");
			else if (docAttachment.getAttachmentType().equalsIgnoreCase("xls"))
				res.setContentType("application/vnd.ms-excel");
			else if (docAttachment.getAttachmentType().equalsIgnoreCase("tiff")
					|| docAttachment.getAttachmentType()
							.equalsIgnoreCase("gif")
					|| docAttachment.getAttachmentType().equalsIgnoreCase(
							"jpeg")
					|| (docAttachment.getAttachmentType()
							.equalsIgnoreCase("png")))
				res.setContentType("image/" + docAttachment.getAttachmentType());
			else
				res.setContentType("application/octet-stream");
			res.setContentLength(byteArray.length);
			res.setHeader("Content-Disposition", "attachment; filename="
					+ docAttachment.getFileName());

			res.getOutputStream().write(byteArray);

			return new Status(1, "Document downloaded Successfully !");
		} catch (Exception e) {
			e.printStackTrace();
			return new Status(0, e.toString());
		}

	}


	/**
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@Consumes("multipart/form-data")
	@RequestMapping(value = "/documentService/uploadAttachment", method = RequestMethod.POST)
	public synchronized @ResponseBody Status uploadAttachment(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Part input = request.getPart("file1");
		InputStream inputStream = input.getInputStream();
		// convert the uploaded file to inputstream

		byte[] bytes = IOUtils.toByteArray(inputStream);
		// constructs upload file path
		String fileName = WFUtil.getFileName(input);
		String specialChars2 = "[$&+,:;=?@#|'<>^*()%!-]";
		fileName = fileName.replaceAll(specialChars2, "_");
		System.out.println("dsf" + fileName);

		String filePath = PropsUtil.getValue("temp.path");
		File orgFile = new File(filePath);
		if (orgFile != null) {
			// tempFile.deleteOnExit();
			orgFile.mkdir();
		}
		String userId = request.getParameter("userId");
		filePath = filePath + "\\vwatt" + userId;
		File tempFile = new File(filePath);
		if (tempFile != null) {
			// tempFile.deleteOnExit();
			System.out.println("inside tempfile");
			tempFile.mkdir();
		}
		File file = new File(filePath + "\\" + fileName);
		System.out.println(filePath + "\\" + fileName);
		if (!file.exists()) {
			file.createNewFile();
		}

		String fileTempName = file.getAbsolutePath();
		FileOutputStream fop = new FileOutputStream(file);
		fop.write(bytes);
		fop.flush();
		fop.close();
		return new Status(0, fileTempName);
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/documentService/removeAttachment")
	public synchronized @ResponseBody Status removeAttachmentFile(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String fileName = request.getParameter("fileName");
		String filePath = PropsUtil.getValue("temp.path");
		String userId = request.getParameter("userId");
		filePath = filePath + "\\vwatt" + userId;
		File tempFile = new File(filePath + "\\" + fileName);
		System.out.println(filePath + "\\" + fileName);
		if (tempFile != null && tempFile.exists()) {
			tempFile.delete();
		}

		return new Status(0, fileName);
	}

	/**
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/documentService/viewAttachmentForIE")
	public synchronized @ResponseBody Status viewAttachmentForIE(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String fileName = request.getParameter("fileName");
		if (fileName != null) {
			fileName = fileName.trim();
		}
		System.out.println("<<<< fileName >>>>" + fileName);
		String fileDirPath = fileName.substring(0, fileName.lastIndexOf("\\"));
		String fileNameWithouext = fileName.substring(
				fileName.lastIndexOf("\\") + 1, fileName.lastIndexOf("."));
		String fileType = fileName.substring(fileName.lastIndexOf(".") + 1);
		System.out.println("<<<< fileDirPath >>>>" + fileDirPath);
		System.out.println("<<<< fileNameWithouext >>>>" + fileNameWithouext);
		System.out.println("<<<< fileType >>>>" + fileType);
		String contentType = "";
		if (fileType != null && fileType.trim().equalsIgnoreCase("pdf")) {
			contentType = "pdf";
		} else if (fileType.trim().equalsIgnoreCase("doc")
				|| fileType.trim().equalsIgnoreCase("docx")) {
			contentType = "winword";
		} else if (fileType.trim().equalsIgnoreCase("xls")
				|| fileType.trim().equalsIgnoreCase("xlsx")) {
			contentType = "excel";
		} else if (fileType.trim().equalsIgnoreCase("tiff")
				|| fileType.trim().equalsIgnoreCase("gif")
				|| fileType.trim().equalsIgnoreCase("jpeg")
				|| fileType.trim().equalsIgnoreCase("jpg")
				|| fileType.trim().equalsIgnoreCase("png")) {

			contentType = "mspaint";
		} else if (fileType.trim().equalsIgnoreCase("txt")) {
			contentType = "notepad";
		}
		System.out.println("<<<< contentType >>>>" + contentType);
		Runtime rt = Runtime.getRuntime();
		String file = contentType + " " + fileNameWithouext + "." + fileType;
		File fileDir = new File(fileDirPath);
		Process p = rt.exec(file, null, fileDir);
		return null;
	}

	/**
	 * @param response
	 * @param filePath
	 * @param mimeType
	 * @param removeAfterDownload
	 */
	@RequestMapping(value = "/documentService/downloadFile")
	public void downloadFile(HttpServletRequest request,
			HttpServletResponse response, String mimeType,
			boolean removeAfterDownload) throws Exception {
		// localInfo("entering into showFile()");
		FileInputStream fileInputStream = null;
		// PrintWriter out = null;
		File fileToDownload = null;
		try {
			String fileName = request.getParameter("fileName");

			String specialChars2 = "[$&+,:;=?@#|'<>^*()%!-]";
			fileName = fileName.replaceAll(specialChars2, "_");
			System.out.println("dsf" + fileName);

			String filePath = PropsUtil.getValue("temp.path");
			String userId = request.getParameter("userId");
			filePath = filePath + "\\vwatt" + userId + "\\" + fileName;
			System.out.println("<<<< fileName >>>>" + fileName);
			System.out.println("<<<< filePath >>>>" + filePath);
			// String filePath = request.getParameter("filePath");
			// String fileName = new File(filePath).getName();
			if (mimeType == null || mimeType.trim().length() == 0) {
				// mimeType = "application/x-unknown";
				mimeType = "application/download";
			}
			response.setContentType(mimeType);

			response.setHeader("Content-disposition", "attachment; filename=\""
					+ fileName + "\"");

			/*
			 * response.setHeader("Pragma", "public");
			 * response.setHeader("Cache-Control", "max-age=0");
			 */

			// transfer the file byte-by-byte to the response object
			fileToDownload = new File(filePath);

			fileInputStream = new FileInputStream(fileToDownload);
			// out = response.getWriter();
			int i;
			while ((i = fileInputStream.read()) != -1) {
				response.getOutputStream().write(i);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				if (fileInputStream != null) {
					fileInputStream.close();
				}
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				// e1.printStackTrace();
			}
			try {
				if (removeAfterDownload) {
					if (fileToDownload != null && fileToDownload.exists()) {
						fileToDownload.delete();
					}
				}
			} catch (Exception e) {
				//
			}
		}
	}


	/**
	 * @param content
	 * @param filename
	 * @return
	 * @throws IOException
	 *//*
	private File writeFile(byte[] content, String filename, String userId)
			throws IOException {

		String filePath = PropsUtil.getValue("temp.path");
		filePath = filePath + "\\vwatt" + userId + "\\" + filename;
		File file = new File(filePath);
		if (file != null) {
			if (file.exists()) {
				file.delete();
			}
			file.createNewFile();
		}

		FileOutputStream fop = new FileOutputStream(file);
		fop.write(content);
		fop.flush();
		fop.close();
		// file.delete();
		return file;

	}*/


	@RequestMapping(value = "/downloadDoc", method = RequestMethod.GET)
	public @ResponseBody Status getDocument(HttpServletResponse res,
			@RequestParam("attachmentId") int p_Attachment_Id) {

		try {
			System.out.println("p_Attachment_Id" + p_Attachment_Id);
			byte[] byteArray = documentService
					.getEntityByDocAccId(p_Attachment_Id);
			DocAttachment docAttachment = documentService
					.getEntityObjByDocAccId(p_Attachment_Id);
			if (docAttachment.getAttachmentType().equalsIgnoreCase("pdf"))
				res.setContentType("application/pdf");
			else if (docAttachment.getAttachmentType().equalsIgnoreCase("doc"))
				res.setContentType("application/msword");
			else if (docAttachment.getAttachmentType().equalsIgnoreCase("xls"))
				res.setContentType("application/vnd.ms-excel");
			else if (docAttachment.getAttachmentType().equalsIgnoreCase("tiff")
					|| docAttachment.getAttachmentType()
							.equalsIgnoreCase("gif")
					|| docAttachment.getAttachmentType().equalsIgnoreCase(
							"jpeg")
					|| (docAttachment.getAttachmentType()
							.equalsIgnoreCase("png")))
				res.setContentType("image/" + docAttachment.getAttachmentType());
			else
				res.setContentType("application/octet-stream");
			res.setContentLength(byteArray.length);
			res.setHeader("Content-Disposition", "attachment; filename="
					+ docAttachment.getFileName());

			res.getOutputStream().write(byteArray);

			return new Status(1, "Document downloaded Successfully !");
		} catch (Exception e) {
			e.printStackTrace();
			return new Status(0, e.toString());
		}

	}

	/*
	 * @RequestMapping(value = "/downloadDoc", method = RequestMethod.GET)
	 * public @ResponseBody Status getDocument(@RequestParam("p_Attachment_Id")
	 * int p_Attachment_Id) { DocAttachment docAttachment = new DocAttachment();
	 * try { System.out.println("p_Attachment_Id"+p_Attachment_Id);
	 * docAttachment = documentService.getEntityObjByDocAccId(p_Attachment_Id);
	 * return new Status(1, "Document downloaded Successfully !"); } catch
	 * (Exception e) { e.printStackTrace(); return new Status(0, e.toString());
	 * }
	 * 
	 * }
	 */

	@RequestMapping("/documentService/getRPNumbers")
	public @ResponseBody ArrayList<RPNumber> getRPNumbers() {
		ArrayList<RPNumber> rpnum = documentService.getRPNumbers();
		return rpnum;
	}

	@RequestMapping("/documentService/getUserByDocIdAndDocRole")
	public @ResponseBody DocUserMaster getUserByDocIdAndDocRole(
			@RequestParam(value = "documentId") Integer documentId) {

		return documentService.getUserBydocumentIdAndDocRole(documentId);
	}


	/**
	 * @throws DocumentException
	 * @throws IOException
	 */
	@RequestMapping(value = "/documentService/writePDF")
	public synchronized @ResponseBody Status writeRPASPDF(
			HttpServletRequest request) throws IOException {
		// TODO Auto-generated method stub
		String rpNum= request.getParameter("rpNum");
		String specialChars2 = "[$&+,.:;=?@#|'<>^*()%!-/]";
		rpNum = rpNum.replaceAll(specialChars2, "_");
		String isReferredInitiator=request.getParameter("isReferredInitiator");
		System.out.println("<<< rpNum >>>>"+rpNum);
		docAttachmentUtil.writePdf(request,rpNum,null,null,"true",null,false,isReferredInitiator);
		return null;
	}
	
	

	/**
	 * @param dmdc
	 * @return
	 * @throws NotSignedInException
	 * @throws Exception
	 */
	@RequestMapping(value = "/documentService/releaseRP", method = RequestMethod.POST)
	public @ResponseBody/* String */Status releaseRP(
			@RequestBody DocumentMetaDataCreate dmdc) {
		documentService.updateReleaseRP(dmdc);
		System.out.println("<<<< Value Posted >>>>");
		return new Status(1, "Success");
	}

	
	/**
	 * @param dmdc
	 * @return
	 * @throws NotSignedInException
	 * @throws Exception
	 */
	@RequestMapping(value = "/documentService/createFinalRP", method = RequestMethod.POST)
	public @ResponseBody/* String */Status createFinalRP(
			@RequestBody DocumentMetaDataCreate dmdc) {
		documentService.updateFinalRP(dmdc);
		System.out.println("<<<< Value Posted >>>>");
		return new Status(1, "Success");
	}
	
	
	/**
	 * @param request
	 * @param response
	 * @param mimeType
	 * @throws Exception
	 */
	@RequestMapping(value = "/documentService/downloadPdf")
	public void downloadPdf(HttpServletRequest request,
			HttpServletResponse response, String mimeType) throws Exception {
		// localInfo("entering into showFile()");
		FileInputStream fileInputStream = null;
		// PrintWriter out = null;
		File fileToDownload = null;
		boolean removeAfterDownload=true;
		try {
			String userId = request.getParameter("userId");
			String rpNum= request.getParameter("rpNum");
			String specialChars2 = "[$&+,.:;=?@#|'<>^*()%!-/]";
			rpNum = rpNum.replaceAll(specialChars2, "_");
			String fileName = rpNum + ".pdf";

			String filePath = PropsUtil.getValue("temp.path");

			WFUtil.makeFilePath(filePath);

			filePath = filePath + "\\pdf" + userId + "\\" + fileName;
			System.out.println("<<<< fileName >>>>" + fileName);
			System.out.println("<<<< filePath >>>>" + filePath);
			// String filePath = request.getParameter("filePath");
			// String fileName = new File(filePath).getName();
			if (mimeType == null || mimeType.trim().length() == 0) {
				// mimeType = "application/x-unknown";
				mimeType = "application/download";
			}
			response.setContentType(mimeType);

			response.setHeader("Content-disposition", "attachment; filename=\""
					+ fileName + "\"");

			/*
			 * response.setHeader("Pragma", "public");
			 * response.setHeader("Cache-Control", "max-age=0");
			 */

			// transfer the file byte-by-byte to the response object
			fileToDownload = new File(filePath);

			fileInputStream = new FileInputStream(fileToDownload);
			// out = response.getWriter();
			int i;
			while ((i = fileInputStream.read()) != -1) {
				response.getOutputStream().write(i);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				if (fileInputStream != null) {
					fileInputStream.close();
				}
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				// e1.printStackTrace();
			}
			try {
				if (removeAfterDownload) {
					if (fileToDownload != null && fileToDownload.exists()) {
						fileToDownload.delete();
					}
				}
			} catch (Exception e) {
				//
			}
		}
	}

	@RequestMapping(value = "/documentService/checkFileIsUploaded", method = RequestMethod.GET)
	public @ResponseBody Status checkFileIsUploaded(HttpServletRequest request)
			throws Exception {
		// localInfo("entering into showFile()");
		System.out.println("checkFileIsUploaded");
		boolean isLocked = false;
		String fileName = request.getParameter("fileName");

		String specialChars2 = "[$&+,:;=?@#|'<>^*()%!-]";
		fileName = fileName.replaceAll(specialChars2, "_");

		String filePath = PropsUtil.getValue("temp.path");
		String userId = request.getParameter("userId");
		filePath = filePath + "\\vwatt" + userId + "\\" + fileName;
		// filePath=filePath+"\\pdf1\\viewRBPDF1.pdf";
		File file = new File(filePath);
		isLocked = isCompletelyWritten(file);
		String fileStatus = "error";
		if (isLocked) {
			fileStatus = "success";
		}

		System.out.println("fileStatus" + fileStatus);

		return new Status(0, fileStatus);
	}

	/**
	 * @param file
	 * @return
	 */
	private boolean isCompletelyWritten(File file) {
		RandomAccessFile stream = null;
		try {
			stream = new RandomAccessFile(file, "rw");
			return true;
		} catch (Exception e) {
			System.out.println("Skipping file " + file.getName()
					+ " for this iteration due it's not completely written");
		} finally {
			if (stream != null) {
				try {
					stream.close();
				} catch (IOException e) {
					System.out.println("Exception during closing file "
							+ file.getName());
				}
			}
		}
		return false;
	}

	@RequestMapping("/documentService/getDocumentUserActionDocumentAllMainLineUsersInQueue")
	public ArrayList<DocumentUserActionBO> getDocumentUserActionDocumentAllMainLineUsersInQueue(
			@RequestParam(value = "documentId") Integer documentId) {
		return docUserActionService.geUsersInQueue(documentId, null, false, false, true);
		/*return docUserActionService.getDocumentUserActionDocumentAllUsersInQueue(
				documentId, false,false);*/
	}

	@RequestMapping("/documentService/getDocumentUserActionDocumentAllAdvisoryLineUsersInQueue")
	public ArrayList<DocumentUserActionBO> getDocumentUserActionDocumentAllAdvisoryLineUsersInQueue(
			@RequestParam(value = "documentId") Integer documentId) {
		return docUserActionService.geUsersInQueue(documentId, null, true, false, true);
		/*return docUserActionService.getDocumentUserActionDocumentAllUsersInQueue(
				documentId, true,false);*/
	}
	
	
	@RequestMapping(value = "/documentService/cancelRP", method = RequestMethod.POST)
	public synchronized @ResponseBody Status cancelRP(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		logger.info("<<<< Inside cancelRP >>>>");
		
		Integer documentId =Integer.valueOf(request.getParameter("documentId"));
		
		Integer userId =Integer.valueOf(request.getParameter("userId"));
		
		logger.info("<<<< documentId >>>>"+documentId);
	    
		if(documentId!=null && userId!=null){
			updateComments(request, response,true);
			docUserActionService.saveCancelledData(userId,documentId);
			
		}
		
		Status status = null;
		DocumentMaster dm = documentService
				.getDocumentMasterByDocumentId(documentId);
		String documentType=dm.getDocumentType();
		if(documentType == null || documentType.isEmpty()){
			documentType="RP";
		}
		dm.setIsAmendment("No");
		documentService.saveDocumentMaster(dm);
		List<RevenueProposal> revenueProposalList = documentService
				.getRevenueProposalByDocumentId(documentId);
		String rpNumber = null;
		String message = "";
		if (revenueProposalList != null && revenueProposalList.size() > 0) {
			rpNumber = revenueProposalList.get(0).getRpNumber();
			message = "This is to inform you that a new RP with RP No. <b>'"
					+ rpNumber + "'</b> has been Cancelled, no action is needed";
		}
		String subject = "";
		String templateName = "/com/gt/templates/mailTemplate.html";
		templateName = WFUtil.getFileContent(templateName, true);
		
		String plantName = null;
		String material = null;
	
//		Integer delegatedTo= null;
		String isCancelled = "true";
		Map<String, List<String>> getMailIds = documentService
				.getMailIdsToSend(dm, null,null, null, null,
						null, null, userId,null, null, rpNumber,
						revenueProposalList, null, null,
						null,isCancelled);
		if (getMailIds != null) {
			String mailHighPriority;
			if (dm.getIsExpress() != null && dm.getIsExpress()) {
				mailHighPriority = "1";
			} else {
				mailHighPriority = "3";
			}
			List<String> toMailIds = getMailIds.get("TO");
			List<String> ccMailIds = getMailIds.get("CC");
			List<String> subjectList = getMailIds.get("SUBJECT");
			List<String> plantNameList = getMailIds.get("PLANTNAME");
			List<String> materialList = getMailIds.get("MATERIAL");
			if (subjectList != null && subjectList.size() > 0) {
				subject = subjectList.get(0);
			}
			if (plantNameList != null && plantNameList.size() > 0) {
				plantName = plantNameList.get(0);
			}
			if (materialList != null && materialList.size() > 0) {
				material = materialList.get(0);
			}
			String[] toMailIdsArr = toMailIds.toArray(new String[toMailIds
					.size()]);
			String[] ccMailIdsArr = ccMailIds.toArray(new String[ccMailIds
					.size()]);
			String toAddress = null;
			String ccAddress = null;

			if (toMailIdsArr != null && toMailIdsArr.length > 0
					/*&& ccMailIdsArr != null && ccMailIdsArr.length > 0*/) {
				toAddress = Arrays.toString(toMailIdsArr);

				StringTokenizer removeBraces = new StringTokenizer(toAddress,
						"[]");
				toAddress = removeBraces.nextToken();
				if( ccMailIdsArr != null && ccMailIdsArr.length > 0){
					ccAddress = Arrays.toString(ccMailIdsArr);
					removeBraces = new StringTokenizer(ccAddress, "[]");
					ccAddress = removeBraces.nextToken();
				}
				System.out.println("toAddress "+toAddress);
				System.out.println("ccAddress "+ccAddress);
				System.out.println("subject "+subject);
				
				String appURL = PropsUtil.getValue("app.domain")+"/login";
				
				Map<String, Object> nameValueMap = new HashMap<String, Object>();
				
				nameValueMap.put("message", message);
				nameValueMap.put("plantName", plantName);
				nameValueMap.put("materials", material);
				nameValueMap.put("hostName", appURL);
				nameValueMap.put("delegatedByName", "null");
				if(documentType.equalsIgnoreCase("RP")){
					nameValueMap.put("labelName", "Materials");
				}else{
					nameValueMap.put("labelName", "Category");
				}

				String mailContent = VelocityStringUtil.getFinalContent("SM",
						templateName, nameValueMap);
				 System.out.println("mailContent >>>>>"+mailContent);

				status = mailMail.sendHtmlMail(null, toAddress, ccAddress, subject,
						mailContent, mailHighPriority);
		
				}
			}
		return status;
		}

	
	@RequestMapping(value = "/documentService/downloadMisReport")
	public void downloadMisReport(HttpServletRequest request,
			HttpServletResponse response, String mimeType) throws Exception {
		// localInfo("entering into showFile()");
		FileInputStream fileInputStream = null;
		// PrintWriter out = null;
		File fileToDownload = null;
		boolean removeAfterDownload=true;
		try {
			String filePath = PropsUtil.getValue("temp.path");
			String currentTime = request.getParameter("time");
			
			String fileName="e-TrackRev-MIS_"+currentTime+".xls";

			filePath = filePath + "\\e-TrackRev-MIS\\" + fileName;
			
			// String fileName = new File(filePath).getName();
			if (mimeType == null || mimeType.trim().length() == 0) {
				// mimeType = "application/x-unknown";
				mimeType = "application/download";
			}
			response.setContentType(mimeType);

			response.setHeader("Content-disposition", "attachment; filename=\""
					+ fileName + "\"");

		
			fileToDownload = new File(filePath);

			fileInputStream = new FileInputStream(fileToDownload);
			// out = response.getWriter();
			int i;
			while ((i = fileInputStream.read()) != -1) {
				response.getOutputStream().write(i);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				if (fileInputStream != null) {
					fileInputStream.close();
				}
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				// e1.printStackTrace();
			}
			try {
				if (removeAfterDownload) {
					if (fileToDownload != null && fileToDownload.exists()) {
						fileToDownload.delete();
					}
				}
			} catch (Exception e) {
				//
			}
		}
	}
	
	@RequestMapping(value = "/documentService/writeExcelForMIS")
	public synchronized  Status writeExcelForMIS(
			@RequestBody DocumentMetaDataCreate dmdc) throws IOException {
		// TODO Auto-generated method stub
		Integer companyIdInt=dmdc.getCompanyId();
		Integer businessIdInt=dmdc.getBusinessId();
		Integer sbuIdInt=dmdc.getSbuId();
		Integer plantIdInt = dmdc.getPlantId();
		Integer materialIdInt = dmdc.getMaterialId();
		String isRelParty = dmdc.getIsRelParty();
		Integer userId=dmdc.getUserId();
		String isBudgeted= dmdc.getIsBudgeted();
		String fromDate=dmdc.getFromDate();
		String toDate=dmdc.getToDate();
		Double upperLimit=dmdc.getUpperLimit();
		Double lowerLimit=dmdc.getLowerLimit();
		String currentDateTime=dmdc.getCurrentDateTime();
		Boolean isplantReport=dmdc.getIsPlantReport();
		Integer rtnValue=0;
		Date fromDateVal=null;
		Date toDateVal=null;
		System.out.println("fromDate >>>>>>"+fromDate+"toDate >>>>>"+toDate);
	if(fromDate!=null && !fromDate.isEmpty()){
		fromDateVal=WFUtil.getStringToDate(fromDate, "dd/MM/yyyy");
	}
	
	if(toDate!=null && !toDate.isEmpty()){
		toDateVal=WFUtil.getStringToDate(toDate, "dd/MM/yyyy");
		LocalDate local = toDateVal.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		if(toDateVal!=null){
			 Calendar c = new GregorianCalendar();
			 c.set(Calendar.DATE,local.getDayOfMonth());
			 c.set(Calendar.MONTH,local.getMonthValue()-1);
			 c.set(Calendar.YEAR,local.getYear());
			 c.set(Calendar.HOUR_OF_DAY, 23); 
			    c.set(Calendar.MINUTE, 59);
			    c.set(Calendar.SECOND, 59);
			    toDateVal=c.getTime();
		}
		
	}
	System.out.println("fromDateVal >>>>>>"+fromDateVal+"toDateVal >>>>>"+toDateVal);
		
		System.out.println("companyIdInt, businessIdInt, sbuIdInt"+companyIdInt+ businessIdInt+ sbuIdInt+userId+"isRelParty"+isRelParty);
		HashMap<String,Object> misReport = documentService.getMisReport(companyIdInt, businessIdInt, sbuIdInt,plantIdInt,materialIdInt,isRelParty,userId,isBudgeted,
				fromDateVal,toDateVal,upperLimit,lowerLimit,isplantReport);
		
		
		String filePath = PropsUtil.getValue("temp.path");

		WFUtil.makeFilePath(filePath);

		filePath = filePath + "\\e-TrackRev-MIS";
		WFUtil.makeFilePath(filePath);
		
		filePath = filePath + "\\e-TrackRev-MIS_"+currentDateTime+".xls";
		List<List<String>> rowList = (List<List<String>>) misReport.get("rowList");
		List<String> columnList=(List<String>) misReport.get("columnList");;
		
		if(rowList!=null && rowList.size()>0){
			ExcelUtils.wirteCommonExcelReport(columnList, rowList, filePath, "misreport");
			 rtnValue=1;
		}
		return new Status(rtnValue, null);
	}

}
