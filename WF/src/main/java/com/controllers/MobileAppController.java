package com.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gt.bo.MobileAppBo;
import com.gt.entity.RevenueProposal;
import com.gt.services.DocumentService;
import com.gt.utils.DocAttachmentUtil;
import com.gt.utils.PropsUtil;

@RestController
public class MobileAppController {

	public static final Logger logger = Logger.getLogger(MobileAppController.class.getName());
	@Autowired
	private DocumentService documentservice;

	@Autowired
	private DocAttachmentUtil docAttachmentUtil;

	@RequestMapping(value = "/documentService/getPDFDocument")
	public synchronized @ResponseBody MobileAppResponse getAppPDF(MobileAppBo mobileApp) throws IOException {

		String docId = mobileApp.getDocumentId();
		String userId = mobileApp.getUserId();
		String rpNumber = null;
		logger.info("DocumentID: " + docId);
		logger.info("UserID: " + userId);

		ArrayList<RevenueProposal> list = documentservice.getRevenueProposalByDocumentId(Integer.parseInt(docId));
		if (list != null && list.size() > 0) {
			rpNumber = list.get(0).getRpNumber();
		}
		logger.info("RPNumber :" + rpNumber);

		docAttachmentUtil.writePdf(null, rpNumber, userId, docId, "true", null, false,null);
		String path = PropsUtil.getValue("temp.path");
		String specialChars2 = "[$&+,:;=?@#|'<>^*()%!-/]";
		rpNumber = rpNumber.replaceAll(specialChars2, "_");

		String filePath = path + "\\pdf" + userId + "\\" + rpNumber + ".pdf";
		File file = new File(filePath);
		FileInputStream pdfInFile = new FileInputStream(file);
		byte[] pdfArr = new byte[(int) file.length()];
		pdfInFile.read(pdfArr);
		String result = javax.xml.bind.DatatypeConverter.printBase64Binary(pdfArr);
		MobileAppResponse appResponse = new MobileAppResponse();
		appResponse.setPdfCode(result);

		return appResponse;
	}

}
