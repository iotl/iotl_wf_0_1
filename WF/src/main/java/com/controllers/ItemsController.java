package com.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gt.bo.Items;
import com.gt.bo.SearchBO;
import com.gt.services.ItemsService;
import com.gt.services.UserService;

@RestController
public class ItemsController {
	
	@Autowired
	private UserService userservice;
	
	@Autowired
	private ItemsService itemsService;
	
	@RequestMapping("/items/getPendingActions")
	public @ResponseBody ArrayList<Items> getPendingActions(@RequestParam(value="userId") Integer userId) {
		
		return itemsService.getPendingActions(userId);
	}
	
	@RequestMapping("/items/getDrafts")
	public @ResponseBody ArrayList<Items> getDrafts(@RequestParam(value="userId") Integer userId,@RequestParam(value="role") String roleName) {
		
		return itemsService.getDrafts(userId,roleName);
	}
	
	@RequestMapping("/items/isUserDelegated")
	public @ResponseBody Status isUserDelegated(@RequestParam(value="userId") Integer userId) {
		
		return itemsService.isUserDelegated(userId);
	}
	
	@RequestMapping("/items/searchRPNumber")
	 public @ResponseBody ArrayList<Items> searchRPNumber(@RequestBody SearchBO searchBO) {
	  Integer userId=searchBO.getUserId();
	  String rpNum=searchBO.getRpNum();
	  System.out.println("userId"+userId+"rpNum"+rpNum);
	  return itemsService.searchRPNumber(searchBO);
	 }
	
	@RequestMapping("/items/getInProcessRPs")
	public @ResponseBody ArrayList<Items> getInProcessRPs(@RequestParam(value="userId") Integer userId,@RequestParam(value="role") String roleName) {
		
		return itemsService.getInProcessRPs(userId,roleName);
	}
	
	@RequestMapping("/items/getReferedRPs")
	public @ResponseBody ArrayList<Items> getRefered(@RequestParam(value="userId") Integer userId) {
		
		return itemsService.getReferedRPs(userId);
	}
	
	@RequestMapping("/items/getEndorsedRPs")
	public @ResponseBody ArrayList<Items> getEndorsedRPs(@RequestParam(value="userId") Integer userId) {
		
		return itemsService.getEndorsedRPs(userId);
	}
	
	@RequestMapping("/items/getEndorsedByApprover")
	public @ResponseBody ArrayList<Items> getEndorsedByApprover(@RequestParam(value="userId") Integer userId) {
		
		return itemsService.getEndorsedByApprover(userId);
	}

	@RequestMapping("/items/getDelegateRPs")
	public @ResponseBody ArrayList<Items> getDelegateRPs(@RequestParam(value="userId") Integer userId) {
		System.out.println(userId);
		return itemsService.getDelegateRPs(userId);
	}
	
	@RequestMapping("/items/getApprovedRPs")
	public @ResponseBody ArrayList<Items> getApprovedRPs(@RequestParam(value="userId") Integer userId,@RequestParam(value="role") String roleName,@RequestParam(value="plantName") String plantName) {
		//System.out.println(userId);
		return itemsService.getApprovedRPs(userId,roleName,plantName);
	}
	
	@RequestMapping("/items/getReleasedRPs")
	public @ResponseBody ArrayList<Items> getReleasedRPs(@RequestParam(value="userId") Integer userId,@RequestParam(value="role") String roleName,@RequestParam(value="plantName") String plantName) {
		//System.out.println(userId);
		return itemsService.getReleasedRPs(userId,roleName,plantName);
	}
	
	@RequestMapping("/items/getRPsApprovedByMe")
	public @ResponseBody ArrayList<Items> getRPsApprovedByMe(@RequestParam(value="userId") Integer userId) {
		
		return itemsService.getRPsApprovedByMe(userId);
	}
	
	@RequestMapping("/items/getRPsReleasedByMe")
	public @ResponseBody ArrayList<Items> getRPsReleasedByMe(@RequestParam(value="userId") Integer userId) {
		
		return itemsService.getRPsReleasedByMe(userId);
	}
	
	@RequestMapping("/items/getRejectedRPs")
	public @ResponseBody ArrayList<Items> getRejectedRPs(@RequestParam(value="userId") Integer userId) {
		
		return itemsService.getRejectedRPs(userId);
	}
	
	@RequestMapping("/items/getCancelledRPs")
	public @ResponseBody ArrayList<Items> getCancelledRPs(@RequestParam(value="userId") Integer userId) {
		
		return itemsService.getCancelledRPs(userId);
	}

	

}
