package com.controllers;

import com.gt.entity.Companies;
import com.gt.entity.Material;
import com.gt.entity.Users;

public class Status {

	private int code;
	private String message;
	private int userId;
	private String roleName;
	private int documentId;
	private String authorizationKey;
	private int commentId;
	private int actionId;
	private String plantName;
	private String dashBoardType;
	private Integer deptId;
	private Integer seqId;
	private Material material;
  private Companies company;
  private Users user;
  private String rpNumber;
  
	public String getDashBoardType() {
		return dashBoardType;
	}

	public void setDashBoardType(String dashBoardType) {
		this.dashBoardType = dashBoardType;
	}

	/**
	 * added dashboardtype for user management on 7-12-15
	 * 
	 * @param code
	 * @param message
	 * @param userId
	 * @param roleName
	 * @param authorizationKey
	 * @param dashBoardType
	 */
	public Status(int code, String message, int userId, String roleName,
			String authorizationKey, String dashBoardType,Integer deptId,Users user) {
		super();
		this.code = code;
		this.message = message;
		this.userId = userId;
		this.roleName = roleName;
		this.authorizationKey = authorizationKey;
		this.dashBoardType = dashBoardType;
		this.deptId=deptId;
		this.user =user;
	}

	public Status(int code, String message, int documentId) {
		super();
		this.code = code;
		this.message = message;
		this.documentId = documentId;
	}

	public Status(int code, String message, int documentId, int commentId,
			int actionId) {
		super();
		this.code = code;
		this.message = message;
		this.documentId = documentId;
		this.commentId = commentId;
		this.actionId = actionId;

	}
	
	public Status(int code, String message, int documentId, int commentId,
			int actionId,int seqId) {
		super();
		this.code = code;
		this.message = message;
		this.documentId = documentId;
		this.commentId = commentId;
		this.actionId = actionId;
		this.seqId = seqId;

	}

	/**
	 * added dashboardtype for user management on 7-12-15
	 * 
	 * @param code
	 * @param message
	 * @param documentId
	 * @param commentId
	 * @param actionId
	 * @param dashBoardType
	 */
	public Status(int code, String message, int documentId, int commentId,
			int actionId, String dashBoardType) {
		super();
		this.code = code;
		this.message = message;
		this.documentId = documentId;
		this.commentId = commentId;
		this.actionId = actionId;
		this.dashBoardType = dashBoardType;

	}

	/**
	 * 
	 * added on 301015 for plantname added dashboardtype for user management on
	 * 7-12-15
	 */
	public Status(int code, String message, int userId, String plantName,
			String roleName, String authorizationKey, String dashBoardType,Integer deptId,Users user)  {
		super();
		this.code = code;
		this.message = message;
		this.userId = userId;
		this.roleName = roleName;
		this.authorizationKey = authorizationKey;
		this.plantName = plantName;
		this.dashBoardType = dashBoardType;
		this.deptId=deptId;
		this.user= user;

	}

	public String getPlantName() {
		return plantName;
	}

	public void setPlantName(String plantName) {
		this.plantName = plantName;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Status() {
	}

	public Status(int code, String message) {
		this.code = code;
		this.message = message;
	}
	public Status(int code, String message,Material material) {
		this.code = code;
		this.message = message;
		this.material = material;
	}
	public Status(int code, String message, Companies company) {
		this.code = code;
		this.message = message;
		this.company = company;
		// TODO Auto-generated constructor stub
	}
	
	public Status(int code, String message, int documentId,String rpNumber) {
		this.code = code;
		this.message = message;
		this.rpNumber = rpNumber;
		this.documentId=documentId;
		// TODO Auto-generated constructor stub
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the documentId
	 */
	public int getDocumentId() {
		return documentId;
	}

	/**
	 * @param documentId
	 *            the documentId to set
	 */
	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}

	/**
	 * @return the authorizationKey
	 */
	public String getAuthorizationKey() {
		return authorizationKey;
	}

	/**
	 * @param authorizationKey
	 *            the authorizationKey to set
	 */
	public void setAuthorizationKey(String authorizationKey) {
		this.authorizationKey = authorizationKey;
	}

	public int getCommentId() {
		return commentId;
	}

	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}

	public int getActionId() {
		return actionId;
	}

	public void setActionId(int actionId) {
		this.actionId = actionId;
	}

	public Integer getDeptId() {
		return deptId;
	}

	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public String getRpNumber() {
		return rpNumber;
	}

	public void setRpNumber(String rpNumber) {
		this.rpNumber = rpNumber;
	}

	public Integer getSeqId() {
		return seqId;
	}

	public void setSeqId(Integer seqId) {
		this.seqId = seqId;
	}

	
}
