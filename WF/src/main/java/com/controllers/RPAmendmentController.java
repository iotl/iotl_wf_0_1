package com.controllers;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gt.bo.AmendmentBO;
import com.gt.entity.Comments;
import com.gt.entity.DocUserAction;
import com.gt.entity.DocUserMaster;
import com.gt.entity.DocumentMaster;
import com.gt.entity.RevenueProposal;
import com.gt.entity.Users;
import com.gt.services.DocUserActionService;
import com.gt.services.DocumentService;
import com.gt.services.UserService;
import com.gt.utils.ActionStatus;
import com.gt.utils.DocRole;

@RestController
public class RPAmendmentController {
	
	@Autowired
	private DocumentService documentService;
	
	@Autowired
	private DocUserActionService docUserActionService;
	
	@Autowired
	private UserService userService;
	
	public static final Logger logger = Logger
			.getLogger(RPAmendmentController.class.getName());
	
	/**
	 * @param amendmentBO
	 * @return
	 */
	@RequestMapping(value = "/amendment/searchRpAmendment", method = RequestMethod.POST)
	public @ResponseBody Status searchRpAmendment(AmendmentBO amendmentBO) {
		String rpNumber=amendmentBO.getRpNumber();
		if(rpNumber == null || rpNumber.isEmpty()){
			return new Status(0,"Invalid Rp Number");
		}
		rpNumber=rpNumber.trim();
		if(rpNumber.indexOf("Amendment")!=-1){
			rpNumber=rpNumber.substring(0,rpNumber.indexOf("Amendment")).trim();	
		}
		rpNumber=rpNumber.trim();
		System.out.println("<<<< rpNumber <<<<"+rpNumber);
		List<RevenueProposal> rpList=documentService.findByRpNumber(rpNumber);
		
		if(rpList!=null && rpList.size()>0){
			int documentId= rpList.get(0).getDocumentId();
			System.out.println("<<<< documentId <<<<"+documentId);
			DocumentMaster dm= documentService.getDocumentMasterByDocumentId(documentId);
			Users loggedInusers=userService.findByUserid(amendmentBO.getCreatedBy());
			Users documentUser=userService.findByUserid(dm.getCreatedBy());
			System.out.println("<<<< loggedInusers.getPlantId().getPlantId()mentId <<<<"+documentUser.getPlantId());
			System.out.println("<<<< loggedInusers.getPlantId() <<<<"+loggedInusers.getPlantId());
			System.out.println("<<<< loggedInusers.getDeptId().getDeptId()getDeptId <<<<"+documentUser.getDeptId());
			System.out.println("<<<< loggedInusers.getDeptId() <<<<"+loggedInusers.getDeptId());
			if(dm!=null && loggedInusers!=null && documentUser!=null && (documentUser.getPlantId() != loggedInusers.getPlantId() || documentUser.getDeptId() != loggedInusers.getDeptId())){
				return new Status(4,"You can not create an Amendment for Revenue Proposal initiated by a Unit.");
			}else
			if(dm.getStatus().equalsIgnoreCase(ActionStatus.APPROVED) || dm.getStatus().equalsIgnoreCase(ActionStatus.RELEASED)){
			List<DocumentMaster> isAmendmentExists= documentService.getDocumentMasterByOriginalDocId(documentId);
			
			for (DocumentMaster obj:isAmendmentExists){
				documentId=obj.getDocumentId();
				if(obj!=null && "Yes".equalsIgnoreCase(obj.getIsAmendment())){
					return new Status(3,"Amendment is initiated for  this Rp Number");
				}
			}
			
			rpList=documentService.findRPByDocumentId(documentId);
			
			System.out.println("<<<< documentId <<<<"+documentId);
				DocumentMaster docMaster=new DocumentMaster();
				/*docMaster.setBusinessId(dm.getBusinessId());
				docMaster.setCompanyId(dm.getCompanyId());
				docMaster.setDocumentType(dm.getDocumentType());
				docMaster.setSbuId(dm.getSbuId());
				docMaster.setPlantId(dm.getPlantId());
				docMaster.setMaterialId(dm.getMaterialId());
				docMaster.setTitle(dm.getTitle());
				docMaster.setIsExpress(dm.getIsExpress());
				docMaster.setIsRelatedParty(dm.getIsRelatedParty());*/
				BeanUtils.copyProperties(dm, docMaster);
				docMaster.setStatus(ActionStatus.INITIATED);
				docMaster.setIsAmendment("Yes");
				docMaster.setOriginalDocId(dm.getDocumentId());
				docMaster.setSeqNo(documentService.getMaxSeqNo(dm.getDocumentId()));
				Date date = new Date();
				docMaster.setCreatedTimeStamp(date);
				docMaster.setCreatedBy(amendmentBO.getCreatedBy());
				docMaster.setDocumentId(0);
				docMaster.setCurrentlyWith(amendmentBO.getCreatedBy());
				//DocumentMaster docMaster=new DocumentMaster();
				System.out.println("<<<< docMaster.getDocumentId() <<<<"+docMaster.getDocumentId());
				/*BeanUtils.copyProperties(dm, docMaster, new String[]{"DocumentId"});
				System.out.println("<<<< dm <<<<"+dm.getDocumentId());
				if(docMaster!=null){
					System.out.println("<<<< docMaster.getDocumentId() <<<<"+docMaster.getDocumentId());
					return null;	
				}*/
				//dm.setDocumentId(i);
				DocumentMaster tempDm=documentService.saveDocumentMaster(docMaster);
				System.out.println("tempDm.getDocumentId()"+tempDm.getDocumentId());
				String rpNum=" Amendment "+tempDm.getSeqNo();
				for(RevenueProposal rp : rpList){
					RevenueProposal rpTemp= new RevenueProposal();
					BeanUtils.copyProperties(rp, rpTemp);
					rpTemp.setDocumentId(tempDm.getDocumentId());
					rpTemp.setRpNumber(rpNumber+rpNum);
					rpTemp.setApprovedRate(null);
					rpTemp.setApprovedQuantity(null);
					rpTemp.setAmountApproved(null);
					documentService.saveRP(rpTemp);
				}
//				List<DocUserMaster> docUserMasterList= documentService.findByDocumentIdAndSerialNumberIsNotNullOrderBySerialNumberAsc(documentId);
				List<DocUserMaster> docUserMasterListNoAdvisory = documentService.findByDocumentIdAndNoAdvisory(documentId);
				for(DocUserMaster dua : docUserMasterListNoAdvisory){
					DocUserMaster dumTemp= new DocUserMaster();
					BeanUtils.copyProperties(dua, dumTemp);
					dumTemp.setDocumentId(tempDm.getDocumentId());
					if(dumTemp.getDocRole().equalsIgnoreCase(DocRole.INITIATOR)){
						if(dumTemp.getUserId()!=amendmentBO.getCreatedBy()){
							dumTemp.setUserId(amendmentBO.getCreatedBy());
						}
						
					}
					DocUserMaster usermaster=documentService.findByDocumentIdAndDocRole(tempDm.getDocumentId(), DocRole.INITIATOR);
					if(usermaster!=null && usermaster.getUserId()== dumTemp.getUserId() && !dumTemp.getDocRole().equalsIgnoreCase(DocRole.INITIATOR)){
						continue;
					}
					documentService.saveDocUserMaster(dumTemp);
				}
				List<DocUserAction> duaList=docUserActionService.findByDocumentIdAndActionStatus(documentId, ActionStatus.PENDING);
				for(DocUserAction docUserAction:duaList){
					System.out.println("docUserAction.getDocRole()"+docUserAction.getDocRole());
					System.out.println("docUserAction.getDocRole()"+docUserAction.getActionStatus());
					if(docUserAction.getDocRole().equalsIgnoreCase(DocRole.APPROVER) && docUserAction.getActionStatus().equalsIgnoreCase(ActionStatus.PENDING)){
						docUserAction.setActionStatus(ActionStatus.PENDING_INVALID);
						docUserAction.setIsDraft("No");						
						docUserActionService.updateDocUserAction(docUserAction);
						logger.info("docUserAction.getIsDraft() in search Ammendment>>>"+docUserAction.getIsDraft());
						System.out.println("docuserAction search RP Ammendment draft NO>>>>> "+docUserAction.getIsDraft());
						System.out.println("inside if"+docUserAction.getActionStatus());
					}else if(docUserAction.getDocRole().equalsIgnoreCase(DocRole.CONTROLLER) && docUserAction.getActionStatus().equalsIgnoreCase(ActionStatus.PENDING)){
						docUserAction.setActionStatus(ActionStatus.PENDING_INVALID);
						docUserAction.setIsDraft("No");
						docUserActionService.updateDocUserAction(docUserAction);
						logger.info("docUserAction.getIsDraft()in search Ammendment>>>"+docUserAction.getIsDraft());
						System.out.println("docuserAction search RP Ammendment draft NO>>>>> "+docUserAction.getIsDraft());
						System.out.println("inside elseif"+docUserAction.getActionStatus());
					}
				}
				DocUserAction dua=docUserActionService.saveDocUserActionForDraft(tempDm.getDocumentId());
				Comments commentEty = new Comments();
				commentEty.setDocumentId(tempDm.getDocumentId());

				commentEty.setActionId(dua.getActionId());
				
				String comments="";
						
				//Comments commentsEntity = 
						docUserActionService.saveComments(commentEty,comments);
				//DocUserMaster dum1 = docUserMasterRepo.findByDocumentIdAndUserId(documentId,userId);
				return new Status(2,"Valid Rp Number");
			}else{
				return new Status(1,"Valid Rp Number, it is in-process");
			}
		}
		
		
		return new Status(0,"Invalid Rp Number");
	}
}
