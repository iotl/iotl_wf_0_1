package com.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gt.bo.AmendmentBO;
import com.gt.bo.DocumentMetaDataCreate;
import com.gt.entity.Comments;
import com.gt.entity.DocUserAction;
import com.gt.entity.DocumentMaster;
import com.gt.entity.RevenueProposal;
import com.gt.entity.RpTransportation;
import com.gt.repository.UserRepository;
import com.gt.services.DocUserActionService;
import com.gt.services.DocumentService;
import com.gt.services.UserService;
import com.gt.utility.MailMail;
import com.gt.utils.ActionStatus;
import com.gt.utils.DocAttachmentUtil;
import com.gt.utils.RPDetailsGeneration;
import com.itextpdf.text.DocumentException;

@RestController
public class TransportationController {

	public static final Logger logger = Logger
			.getLogger(DocumentController.class.getName());

	/**
	 * 
	 */
	@Autowired
	private DocumentService documentService;
	
	/**
	 * 
	 */
	@Autowired
	private DocUserActionService docUserActionService;

	
	@Autowired
	private DocAttachmentUtil docAttachmentUtil;
	
	@Autowired
	private UserService userservice;

	@Autowired
	MailMail mailMail;
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private RPDetailsGeneration rpDetailsGeneration;
	

	@Autowired
	private UserService userService;
	
	
	
	@RequestMapping("/items/getMnMApprovedReleasedList")
	public @ResponseBody ArrayList<DocUserAction> getMnMApprovedReleasedList() {
		
		return docUserActionService.getMnMApprovedReleasedList();
	}

	

	@RequestMapping(value = "/documentService/createRPTransportationData", method = RequestMethod.POST)
	public @ResponseBody/* String */Status createRPData(
			@RequestBody DocumentMetaDataCreate dmdc) {
		
		System.out.println("sucess");
		/*DocumentMaster tempDm = documentService.getDocumentMasterByDocumentId(dmdc.getDocumentId());
//		Boolean isRulePresent=documentService.isRulePresent(dmdc);
		if(tempDm==null || tempDm.getIsAmendment() == null || !tempDm.getIsAmendment().equalsIgnoreCase("Yes")){
		Boolean isRulePresent=	documentService.addBusinessRuleUsers(dmdc, null, null,null);
		System.out.println("isRulePresent"+isRulePresent);
		if(isRulePresent!=null && !isRulePresent){
			return new Status(-1, "No Rules");
		}
		}*/
	//	DocumentMaster dm = documentService.saveDocumentMetaDataXportation(dmdc);
	//	ArrayList<RpTransportation> rps = documentService.saveRpTransportation(dmdc,
	//			dm);
		// documentService.creatingRPNumber(dmdc);
		
		

	//	documentService.addBusinessRuleUsers(dmdc, dm, rps);

		// to show rpnumber in the create rp screen after the save & continue
		// button is clicked.
		/*String rpNum = null;
		if (rps != null && rps.size() > 0) {
			rpNum = rps.get(0).getRpNumber();
		}

		//

		System.out.println("posted on server: success " + dmdc.getTitle() + " "
				+ dmdc.getCompany().getCompanyName() + " "
				+ dmdc.getBusiness().getBusinessName() + " "
				+ dmdc.getSbu().getSbuName() + " "
												 * +dmdc.getPlants().getPlantName
												 * ()
												 + " "
				+ dmdc.getMaterial().getMaterialName() + " " );
		
		DocUserAction dua=docUserActionService.saveDocUserActionForDraft(dm.getDocumentId());
		Comments commentEty = new Comments();
		commentEty.setDocumentId(dm.getDocumentId());

		commentEty.setActionId(dua.getActionId());
		
		String comments="";
				
		Comments commentsEntity = docUserActionService.saveComments(commentEty,comments);


		return new Status(1, rpNum, dm.getDocumentId(),commentsEntity.getComment_id(),commentEty.getActionId());*/
		

		
		System.out.println("sucess");
		DocumentMaster tempDm = documentService.getDocumentMasterByDocumentId(dmdc.getDocumentId());
//		Boolean isRulePresent=documentService.isRulePresent(dmdc);
		if(tempDm==null || tempDm.getIsAmendment() == null || !tempDm.getIsAmendment().equalsIgnoreCase("Yes")){
		Boolean isRulePresent=	documentService.addBusinessRuleUsers(dmdc, null, null,null);
		System.out.println("isRulePresent"+isRulePresent);
		if(isRulePresent!=null && !isRulePresent){
			return new Status(-1, "No Rules");
		}
		}
		DocumentMaster dm = documentService.saveDocumentMetaData(dmdc);
		ArrayList<RevenueProposal> rps = documentService.saveDocumentData(dmdc,
				dm);
		
		ArrayList<RpTransportation> rpTrans=documentService.saveRpTransportation(dmdc,
							dm);
		// documentService.creatingRPNumber(dmdc);
		
		

		documentService.addBusinessRuleUsers(dmdc, dm, rps,null);

		// to show rpnumber in the create rp screen after the save & continue
		// button is clicked.
		String rpNum = null;
		if (rps != null && rps.size() > 0) {
			rpNum = rps.get(0).getRpNumber();
		}

		//

		System.out.println("posted on server: success " + dmdc.getTitle() + " "
				+ dmdc.getCompany().getCompanyName() + " "
				+ dmdc.getBusiness().getBusinessName() + " "
				+ dmdc.getSbu().getSbuName() + " "/*
												 * +dmdc.getPlants().getPlantName
												 * ()
												 */+ " "
				+ dmdc.getMaterial().getMaterialName() + " " + dmdc.getIsExp()
				+ " " + dmdc.getIsRelPar());
		
		DocUserAction dua=docUserActionService.saveDocUserActionForDraft(dm.getDocumentId());
		Comments commentEty = new Comments();
		commentEty.setDocumentId(dm.getDocumentId());

		commentEty.setActionId(dua.getActionId());
		
		String comments="";
				
		Comments commentsEntity = docUserActionService.saveComments(commentEty,comments);

		return new Status(1, rpNum, dm.getDocumentId(),commentsEntity.getComment_id(),commentEty.getActionId(),rpTrans.get(0).getTransporterSeqId());

	}
	
	@RequestMapping(value = "/documentService/updateRPTransportationData", method = RequestMethod.POST)
	public @ResponseBody/* String */Status updateRPTransportationData(
			@RequestBody DocumentMetaDataCreate dmdc) throws  DocumentException,
			IOException  {
		Boolean isRulePresent=true;
		Boolean isReply = dmdc.getIsReferredToInitiator();

		System.out.println("sucess"+isReply);
		DocumentMaster tempDm = documentService.getDocumentMasterByDocumentId(dmdc.getDocumentId());
//		Boolean isRulePresent=documentService.isRulePresent(dmdc);
		if(tempDm==null || tempDm.getIsAmendment() == null || !tempDm.getIsAmendment().equalsIgnoreCase("Yes")){
		isRulePresent=	documentService.addBusinessRuleUsers(dmdc, null, null,isReply);
		System.out.println("isRulePresent"+isRulePresent);
		if(isRulePresent!=null && !isRulePresent){
			return new Status(-1, "No Rules");
		}
		}
		DocumentMaster dm = documentService.saveDocumentMetaData(dmdc);
		ArrayList<RevenueProposal> rps = documentService.saveDocumentData(dmdc,
				dm);
		
		ArrayList<RpTransportation> rpTrans=documentService.saveRpTransportation(dmdc,
				dm);

		// documentService.deleteDouUserMaster(dm);
		if(isRulePresent!=null){
		documentService.addBusinessRuleUsers(dmdc, dm, rps,isReply);
		}

		// to show rpnumber in the create rp screen after the save & continue
		// button is clicked.
		String rpNum = null;
		if (rps != null && rps.size() > 0) {
			rpNum = rps.get(0).getRpNumber();
		}
		System.out.println("posted on server: success " + dmdc.getTitle() + " "
				+ dmdc.getCompany().getCompanyName() + " "
				+ dmdc.getBusiness().getBusinessName() + " "
				+ dmdc.getSbu().getSbuName() + " "/*
												 * +dmdc.getPlants().getPlantName
												 * ()
												 */+ " "
				+ dmdc.getMaterial().getMaterialName() + " " + dmdc.getIsExp()
				+ " " + dmdc.getIsRelPar());

		int commentId=0;
		int actionId=0;
		ArrayList<DocUserAction> duaList=docUserActionService.getDocUserActionByDocumentId(dm.getDocumentId());
		if(duaList== null || (duaList!=null && duaList.size()==0)){
		DocUserAction dua=docUserActionService.saveDocUserActionForDraft(dm.getDocumentId());
		actionId =dua.getActionId();
		Comments commentEty = new Comments();
		commentEty.setDocumentId(dm.getDocumentId());

		commentEty.setActionId(dua.getActionId());
		
		String comments="";
				
		Comments commentEntity = docUserActionService.saveComments(commentEty,comments);
		commentId=commentEntity.getComment_id();
		}else{
			actionId =duaList.get(duaList.size()-1).getActionId();
			Comments commentEntity=documentService.getCommentByAction_id(actionId);
			if(commentEntity!=null){
				commentId=commentEntity.getComment_id();
			}
		}
		System.out.println("<<<< commentId >>>>"+commentId);
		System.out.println("<<<< actionId >>>>"+actionId);
		
		
		return new Status(1, rpNum, dm.getDocumentId(),commentId,actionId,rpTrans.get(0).getTransporterSeqId());
	}
	
	/**
	 * @param amendmentBO
	 * @return
	 */
	@RequestMapping(value = "/xportation/searchRPXportation", method = RequestMethod.POST)
	public @ResponseBody Status searchRpAmendment(AmendmentBO amendmentBO) {
		String rpNumber=amendmentBO.getRpNumber();
		if(rpNumber == null || rpNumber.isEmpty()){
			return new Status(0,"Invalid Rp Number");
		}
		rpNumber=rpNumber.trim();
		/*if(rpNumber.indexOf("xportation")!=-1){
			rpNumber=rpNumber.substring(0,rpNumber.indexOf("Amendment")).trim();	
		}
		rpNumber=rpNumber.trim();*/
		System.out.println("<<<< rpNumber <<<<"+rpNumber);
		
		List<RevenueProposal> rpList=documentService.findByRpNumber(rpNumber);
		
		if(rpList!=null && rpList.size()>0){
			int documentId= rpList.get(0).getDocumentId();
			List<DocumentMaster> dMaster= documentService.getDocumentMasterByTransportationJustification(rpNumber);
//			System.out.println("<<<< dMaster.getTransportationJustification() <<<<"+dMaster.getTransportationJustification());
//			System.out.println("<<<< dMaster.getOriginalProposalDocId() <<<<"+dMaster.getOriginalProposalDocId());
			
			if(dMaster!=null && dMaster.size() >0 && dMaster.get(0).getTransportationJustification()!=null && !dMaster.get(0).getTransportationJustification().isEmpty() && 
					dMaster.get(0).getTransportationJustification().equalsIgnoreCase(rpNumber)){
				if(dMaster.get(0).getOriginalProposalDocId() == null || dMaster.get(0).getOriginalProposalDocId().intValue()== 0){
					dMaster.get(0).setOriginalProposalDocId(documentId);
					System.out.println("<<<< documentId <<<<"+documentId);
					documentService.saveDocumentMaster(dMaster.get(0));
				}
			
			}
			return new Status(2,"Valid Rp Number",documentId,rpNumber);
		/*	System.out.println("<<<< documentId <<<<"+documentId);
			DocumentMaster dm= documentService.getDocumentMasterByDocumentId(documentId);
			if(dm.getStatus().equalsIgnoreCase(ActionStatus.APPROVED) || dm.getStatus().equalsIgnoreCase(ActionStatus.RELEASED)){
			List<DocumentMaster> isXportationExists= documentService.getDocumentMasterByOriginalProposalDocId(documentId);
			if(isXportationExists!=null && isXportationExists.size()>0){
				return new Status(3,"Xportation is initiated for  this Rp Number");
			}
			for (DocumentMaster obj:isAmendmentExists){
				documentId=obj.getDocumentId();
				if(obj!=null && documentId!=0){
					return new Status(3,"Xportation is initiated for  this Rp Number");
				}
			}
				return new Status(2,"Valid Rp Number",documentId,rpNumber);
			}else{
				return new Status(1,"Valid Rp Number, it is in-process");
			}*/
		}
		
		
		return new Status(0,"Invalid Rp Number");
	}
}


