/**
 * 
 */
package com.controllers;

/**
 * @author
 *
 */
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gt.bo.Items;
import com.gt.entity.Plant;
import com.gt.entity.Role;
import com.gt.entity.UserRoles;
import com.gt.entity.Users;
import com.gt.services.ItemsService;
import com.gt.services.MasterData;
import com.gt.services.RoleService;
import com.gt.services.UserRoleService;
import com.gt.services.UserService;
import com.gt.services.Wf_UserService;
import com.gt.utility.MailMail;
import com.gt.utils.PassCode;
import com.gt.utils.PropsUtil;
import com.gt.utils.VelocityStringUtil;
import com.gt.utils.WFUtil;

@Controller
@RequestMapping("/loginC")
public class LoginController {
	@Autowired
	Wf_UserService wf_UserService;
	
	@Autowired
	UserRoleService userRoleService;
	
	@Autowired
	RoleService roleService;

	@Autowired
	MailMail mailMail;

	@Autowired
	MasterData masterdata;
	
	@Autowired
	private UserService userservice;
	
	
	
	
	/**
	 * @param wfUser
	 * @return
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public @ResponseBody Status loginPage(@RequestBody Users wfUser,HttpServletRequest request,HttpServletResponse response) {
		try {
			// dataServices.addEntity(user);
			/*
			 *  To login we have used `Email_Id` column  as user name from wf_user table.
			 */
			Users dbObject = wf_UserService.getEntityByEmailId(wfUser
					.getEmailId());
			
			
			/*
			 * added on 291015 for utkal based login
			 */
			Plant plantObj=null;
			
			
			String dashBoardType=null;
			
			String defaultPassword=PropsUtil.getValue("default.password");
			System.out.println("<<<<< defaultPassword >>>>> "+getBcryptedPassword(defaultPassword));
			if (dbObject != null && dbObject.getUserId() != 0) {
				if(dbObject !=null && dbObject.getPlantId()!=null){
					plantObj=masterdata.getPlantById(dbObject.getPlantId());
					System.out.println("plantObj------>"+plantObj.getPlantName());
				}
				
				if (dbObject.getEmailId().equalsIgnoreCase(wfUser.getEmailId())) {
					if (BCrypt.checkpw(wfUser.getPassword(),
							dbObject.getPassword())){
					 ArrayList<UserRoles> userRoles = userRoleService.findByUserId(dbObject.getUserId());
						Role role=null;
						String roleName="No Role";
						if(userRoles!=null){
							if(userRoles.size()==1){
								role=roleService.findByRoleId(userRoles.get(0).getRoleId());
								roleName=role.getRoleName();
								dashBoardType=role.getDashboard_Type();
							}else if (userRoles.size()>1){
								roleName= "InitEndorser";
							}
						}else{
							return new Status(2, "Role is not assigned. Please contact admin !",
									dbObject.getUserId(), roleName,null,dashBoardType,dbObject.getDeptId(),dbObject);
						}
						String authorizationKey = PassCode.generate(6, false);
						dbObject.setAuthorizationKey(authorizationKey);
						System.out.println("<<<<< defaultPassword >>>>> "+defaultPassword);
						System.out.println("<<<<< defaultPassword >>>>> "+getBcryptedPassword(defaultPassword));
						System.out.println("<<<<< defaultPassword >>>>> "+BCrypt.checkpw(defaultPassword,
								dbObject.getPassword()));
						wf_UserService.editEntityById(dbObject);
						if (BCrypt.checkpw(defaultPassword,
								dbObject.getPassword())){
							
							/*return new Status(4, "Default Password !",
							dbObject.getUserId(), roleName,authorizationKey);*/
							
							
							/*
							 * 
							 * added on 291015 for utkal login
							 */
							if(plantObj!=null){
								return new Status(4, "Default Password !",
										dbObject.getUserId(),plantObj.getPlantName(),roleName,authorizationKey,dashBoardType,dbObject.getDeptId(),dbObject);
							}
							else{
							return new Status(4, "Default Password !",
									dbObject.getUserId(),roleName,authorizationKey,dashBoardType,dbObject.getDeptId(),dbObject);
							}
						}
						/*
						 * To check whether role is assigned or not to the user. this is checked using wf_userroles
						 */
						/*return new Status(1, "Authentication Successful !",
								dbObject.getUserId(), roleName,authorizationKey);*/
						
						//added on 291015 for utkal login
						if(plantObj!=null){
							return new Status(1,"Authentication Successful !",dbObject.getUserId(),plantObj.getPlantName(),roleName,authorizationKey,dashBoardType,dbObject.getDeptId(),dbObject);
						}
						else{
							Boolean isActivedelegation = userservice.getIsActiveDelegation(dbObject.getUserId());
							
							if(!isActivedelegation && roleName.equalsIgnoreCase("Temporary-Delegatee")){
								return new Status(10,"Authentication Successful ! Delegation Expired",dbObject.getUserId(),roleName,authorizationKey,dashBoardType,dbObject.getDeptId(),dbObject);
							}
							
							
							
							return new Status(1,"Authentication Successful !",dbObject.getUserId(),roleName,authorizationKey,dashBoardType,dbObject.getDeptId(),dbObject);
							
						}
						
						
					}else{
						 ArrayList<UserRoles> userRoles = userRoleService.findByUserId(dbObject.getUserId());
							Role role=null;
							String roleName="No Role";
							if(userRoles!=null){
								if(userRoles.size()==1){
									role=roleService.findByRoleId(userRoles.get(0).getRoleId());
									roleName=role.getRoleName();
									dashBoardType=role.getDashboard_Type();
								}else if (userRoles.size()>1){
									roleName= "InitEndorser";
								}
							}
						Boolean isActivedelegation = userservice.getIsActiveDelegation(dbObject.getUserId());
						
						if(!isActivedelegation && roleName.equalsIgnoreCase("Temporary-Delegatee")){
							return new Status(10,"Authentication Successful ! Delegation Expired");
						}
						
						return new Status(0, "Authentication Failure !");
					}

				}/*else{
					return new Status(3, "Invalid Username");
				}*/

			} else {
				return new Status(3, "Invalid Username");
//				return new Status(0, "Authentication Failure !");
			}

		} catch (Exception e) {
			e.printStackTrace();
			return new Status(0, e.toString());
		}
		return null;

	}
	
	@RequestMapping(value = "/resetPassword", method = RequestMethod.POST)
	public @ResponseBody Status resetPassword(@RequestBody Users wfUser,HttpServletRequest request) throws Exception {
		System.out.println("wfUser.getEmailId()"+wfUser
				.getEmailId());
		System.out.println("request.getEmailId()"+request.getParameter("emailId"));
		Users dbObject = wf_UserService.getEntityByEmailId(wfUser
				.getEmailId());
		if (dbObject != null) {
			if(BCrypt.checkpw(wfUser.getExistingPassword(),
					dbObject.getPassword())){
				if(wfUser.getPassword()!=null && !wfUser.getPassword().isEmpty()){
					dbObject.setPassword(getBcryptedPassword(wfUser
							.getPassword()));
					wf_UserService.editEntityById(dbObject);
					return new Status(1, "New Password Updated");
				}
			}else{
				return new Status(2, "Invalid Existing Password");
			}
			
		}
		return new Status(0, "Invalid User Name");
	}
	
	
	@RequestMapping(value = "/resetPasswordWithId", method = RequestMethod.POST)
	public @ResponseBody Status resetPasswordWithId(@RequestBody Users wfUser,HttpServletRequest request) throws Exception {
		System.out.println("request.getEmailId()"+request.getParameter("emailId"));
		Users dbObject = wf_UserService.getEntityById(wfUser
				.getUserId());
		if (dbObject != null) {
				if(wfUser.getPassword()!=null && !wfUser.getPassword().isEmpty()){
					dbObject.setPassword(getBcryptedPassword(wfUser
							.getPassword()));
					wf_UserService.editEntityById(dbObject);
					return new Status(1, "New Password Updated");
				}
			
		}
		return new Status(0, "Invalid User Name");
	}
	/**
	 * @param wfUser
	 * @return
	 */
	@RequestMapping(value = "/ForgetPasslogin", method = RequestMethod.POST)
	public @ResponseBody Status ForGetLoginPage(@RequestBody Users wfUser) {
		try {
			// dataServices.addEntity(user);
			Users dbObject = wf_UserService.getEntityByEmailId(wfUser
					.getEmailId());
			
			if (dbObject != null) {
				if (dbObject.getEmailId().equalsIgnoreCase(wfUser.getEmailId())
						&& dbObject.getAccessTokenValidity() != null) {

					long diff = dbObject.getAccessTokenValidity().getTime()
							- new Date().getTime();
					String accessToken=wfUser.getAccessToken();
					if(accessToken!=null && !accessToken.isEmpty()){
						accessToken=accessToken.indexOf("#") == -1 ? accessToken :accessToken.substring(0, accessToken.indexOf("#"));
						//System.out.println(userId);
					}
					if(!accessToken.equalsIgnoreCase(dbObject
							.getAccessToken()) && wfUser.getPassword().isEmpty())
					{
						return new Status(3, "Link expired");
					}
					//
					if(!wfUser.getPassword().isEmpty()){
					if (diff < (24 * 60 * 60 * 1000)
							&& (accessToken.equals(dbObject
									.getAccessToken()))) {
						dbObject.setPassword(getBcryptedPassword(wfUser
								.getPassword()));
						dbObject.setAccessToken("");
						dbObject.setAccessTokenValidity(null);
						wf_UserService.editEntityById(dbObject);
						return new Status(1, "New Password Updated");
					} else{
						return new Status(0, "Please Request Again!");
					}
				}

				} else {
					return new Status(2, "Please Request Again!");
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			return new Status(0, e.toString());
		}
		return null;

	}
	
	
	
	@RequestMapping(value = "/logout")
	public String logout(@RequestHeader("origin") String hostName,final RedirectAttributes redirectAttributes) {
		System.out.println(hostName);
		String redirectUrl = "/login.html";
		String rd="redirect:" +hostName+ redirectUrl;
		System.out.println(rd);
		return rd;
	}
	
	@RequestMapping(value = "/isLoggedIn")
	public @ResponseBody Status isLoggedIn(HttpServletRequest request) {
		return new Status(1, "Authentication Failure !");//();
	}
	

	/**
	 * @param wfUser
	 * @return
	 */
	@RequestMapping(value = "/ForgetPassword", method = RequestMethod.POST)
	public @ResponseBody Status ForGetPassword(@RequestBody Users wfUser) {

		try {
			// dataServices.addEntity(user);
			Users dbObject = wf_UserService.getEntityByEmailId(wfUser
					.getEmailId());
			if (dbObject != null) {
				if (dbObject.getEmailId().equalsIgnoreCase(wfUser.getEmailId())) {

					Random t = new Random();

					String accessCode = getBcryptedPassword(""
							+ t.nextInt(10000));
					dbObject.setAccessToken((accessCode));
					Date dt = new Date();
					Calendar c = Calendar.getInstance();
					c.setTime(dt);
					c.add(Calendar.DATE, 1);
					dt = c.getTime();
					dbObject.setAccessTokenValidity(dt);
					wf_UserService.editEntityById(dbObject);
					String domainName=PropsUtil.getValue("app.domain");
					String url=domainName
							+ "/resetPasswordByUsr.html?&userName="
							+ wfUser.getEmailId() + "&accessCode="
							+ accessCode;

					/*System.out
							.println(url);*/
					
					String templateName = "/com/gt/templates/forgetPasswordTemplate.html";
					templateName = WFUtil.getFileContent(templateName, true);
					String defaultPassword=PropsUtil.getValue("default.password");
					String hostName=PropsUtil.getValue("app.domain")+"/login";
//							dbObject.setPassword(getBcryptedPassword(defaultPassword));
//							wf_UserService.editEntityById(dbObject);
					Map<String, Object> valueParams= new HashMap<String, Object>();
					valueParams.put("hostName", hostName);
					valueParams.put("firstName", dbObject.getFirstName());
					valueParams.put("lastName", dbObject.getLastName());
					valueParams.put("password", defaultPassword);
					valueParams.put("url", url);
					//valueParams.put("url", url);
					String mailContent = VelocityStringUtil.getFinalContent("FP", templateName, valueParams);
					String subject="Reset Password ";

					// SEnd Email
					
					mailMail.sendHtmlMail("", wfUser.getEmailId(), null, subject, mailContent, "1");

					/*mailMail.sendMail("Admin", dbObject.getEmailId(),
							"Testing123", hostName
									+ "/forgetPassword.html?&userName="
									+ wfUser.getEmailId() + "&accessCode="
									+ accessCode);*/

					return new Status(1, "Please Check Email");

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			return new Status(0, e.toString());
		}
		return new Status(0, "Invalid User Id");

	}

	/* user */
	@RequestMapping(value = "/createUser", method = RequestMethod.POST)
	public @ResponseBody Status addUser(@RequestBody Users user) {
		try {
			user.setPassword(getBcryptedPassword(user.getPassword()));
			wf_UserService.addEntity(user);
			return new Status(1, "user added Successfully !");
		} catch (Exception e) {
			// e.printStackTrace();
			return new Status(0, e.toString());
		}

	}

	@RequestMapping(value = "/userList", method = RequestMethod.GET)
	public @ResponseBody List<Users> getUser() {

		List<Users> userList = null;
		try {
			userList = wf_UserService.getEntityList();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return userList;
	}

	@RequestMapping(value = "/editUser", method = RequestMethod.POST)
	public @ResponseBody Status editUser(@RequestBody Users user) {
		try {
			System.out.println("insed rest");
			user.setPassword(getBcryptedPassword(user.getPassword()));
			wf_UserService.editEntityById(user);
			return new Status(1, "user edited Successfully !");

		} catch (Exception e) {
			e.printStackTrace();
			return new Status(0, e.toString());
		}
	}

	public String getBcryptedPassword(String password) {
		return BCrypt.hashpw(password, BCrypt.gensalt());
	}

}