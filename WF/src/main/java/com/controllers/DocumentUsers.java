package com.controllers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gt.bo.AdvisorCreate;
import com.gt.bo.DelegationBO;
import com.gt.bo.UsersBO;
import com.gt.entity.Delegation;
import com.gt.entity.DocUserMaster;
import com.gt.entity.DocumentMaster;
import com.gt.entity.Role;
import com.gt.entity.UserRoles;
import com.gt.entity.Users;
import com.gt.services.DocumentService;
import com.gt.services.ItemsService;
import com.gt.services.RoleService;
import com.gt.services.UserRoleService;
import com.gt.services.UserService;
import com.gt.utility.MailMail;
import com.gt.utils.DocRole;
import com.gt.utils.PropsUtil;
import com.gt.utils.VelocityStringUtil;
import com.gt.utils.WFUtil;

@RestController
public class DocumentUsers {
	
	@Autowired
	private UserService userservice;
	
	@Autowired
	private DocumentService documentService;
	
	@Autowired
	MailMail mailMail;
	
	@Autowired
	UserRoleService	userRoleService;
	
	@Autowired
	private RoleService roleService;
	
	
	@Autowired
	private ItemsService itemsService;
	
	
	@RequestMapping("/UserService/findAllSupportingEndorsers")
	public List<UsersBO> findAllSupportingEndorsers(@RequestParam(value="docID") Integer documentId) {
		//DocUserMaster dum = new DocUserMaster();
				return (List<UsersBO>) userservice.findAllSupportingEndorsers(documentId);
	}
	
/*	@RequestMapping("/UserService/findAllCPSupportingEndorsers")
	public List<UsersBO> findAllCPSupportingEndorsers(@RequestParam(value="docID") Integer documentId) {
		//DocUserMaster dum = new DocUserMaster();
				return (List<UsersBO>) userservice.findAllCPSupportingEndorsers(documentId);
	}*/
	
	@RequestMapping("/usersData/getAllUsers")
    public @ResponseBody ArrayList<Users> getAllUsers() {
			ArrayList<Users> users = userservice.getAllUsers(); 
			return users;
	}
	
	@RequestMapping("/UserService/deleteDocUserMaster")
	public Integer deleteDocUserMaster(@RequestParam(value="docID") Integer documentId,@RequestParam(value="userid") Integer userId) {
		//DocUserMaster dum = new DocUserMaster();
				return (Integer) userservice.deleteDocUserMaster(userId, documentId);
	}
	
	
	@RequestMapping(value = "/UserService/cancelDelegation", method = RequestMethod.POST)	
	public  @ResponseBody Status cancelDelegation( HttpServletRequest req)   {	
		//itemsService.
		Integer userId=Integer.parseInt(req.getParameter("userId"));
		List<Delegation> delegationList=itemsService.getIsActiveUserIdOrderByToDate(userId);
		
		System.out.println("delegationList >>>"+delegationList.size());
		if(delegationList!=null && delegationList.size()>0){
			Delegation delegation =delegationList.get(0);
			if(delegation!=null){
				System.out.println("delegationList >>>"+delegation.getDelegationId());
				delegation.setIsActive(false);
				itemsService.saveDelegation(delegation);
				String subject = "Temporary Delegation Withdrawal in e-TrackRev";
				String templateName = "/com/gt/templates/delegationMailTemplate.html";
				templateName = WFUtil.getFileContent(templateName, true);
				// System.out.println("templateName >>>"+templateName);
				Integer delegatedBy = delegation.getUserId();
				Integer delegatedTo = delegation.getDelegateUserId();
				Users usr= userservice.getUsersById(delegatedTo);
				String toAddress=usr.getEmailId();
				
				
				usr= userservice.getUsersById(delegatedBy);
				String message = usr.getFirstName() +" "+usr.getLastName();
				String ccAddress=usr.getEmailId();
				String mailHighPriority = "3";
				String appURL = PropsUtil.getValue("app.domain")+"/login";
				Map<String, Object> nameValueMap = new HashMap<String, Object>();				
				
				nameValueMap.put("message", message);
				nameValueMap.put("delegatedByName", message);
				nameValueMap.put("fromDate", WFUtil.getFormattedDisplayDate(delegation.getFromDate(), "dd-MMM-yyyy", "IST"));
				nameValueMap.put("toDate", WFUtil.getFormattedDisplayDate(delegation.getToDate(), "dd-MMM-yyyy", "IST"));
				nameValueMap.put("hostName", appURL);
				String mailContent = VelocityStringUtil.getFinalContent("SM",
						templateName, nameValueMap);		
				System.out.println("mailContent >>>>>" + mailContent);
				Status status = mailMail.sendHtmlMail(null, toAddress, ccAddress, subject,
							mailContent, mailHighPriority);
				return status;
			}
		}
		return new Status(0,"");
	}

	
	@RequestMapping(value = "/UserService/delegation", method = RequestMethod.POST)	
	public  @ResponseBody /*String*/ Status saveUser_JSON( @RequestBody DelegationBO delegations)   {		
		Status status = null;
		System.out.println("sucess");
		status = userservice.saveDelegationData(delegations);
		System.out.println(status.getCode());
		String delegatedByName=null;
		String delegatedToName=null;
		if(status.getCode() != -2 && status.getCode()!= -3 && status.getCode()!= -4){
			
		String subject = "";
		String templateName = "/com/gt/templates/mailTemplate.html";
		templateName = WFUtil.getFileContent(templateName, true);
		// System.out.println("templateName >>>"+templateName);
		String message = "You have been assigned Temporary Delegation in e-TrackRev";
		Integer delegatedBy = delegations.getUserId();
		String mailId= delegations.getMailId();
		Users usrDelegated=userservice.getEntityByEmailId(mailId);
		Integer delegatedTo = usrDelegated.getUserId();
//		Integer delegatedTo = delegations.getUsers().getUserid();
		DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
		String toDate=df.format(delegations.getToDate());
		String fromDate=df.format(delegations.getFromDate());
		Users usr= userservice.getUsersById(delegatedBy);
		 delegatedByName= usr.getFirstName()+' '+usr.getLastName();
		 delegatedToName= usrDelegated.getFirstName()+' '+usrDelegated.getLastName();
		Map<String, List<String>> getMailIds = documentService
				.getMailIdsToSend(null,null, null, null, null,
						null, null,null ,delegatedTo,delegatedBy, null,
						null, null, null,
						null,null);
		if (getMailIds != null) {
			String mailHighPriority;
			
//				mailHighPriority = "1";
			
				mailHighPriority = "3";
			
			List<String> toMailIds = getMailIds.get("TO");
			List<String> ccMailIds = getMailIds.get("CC");
			List<String> subjectList = getMailIds.get("SUBJECT");
	
			if (subjectList != null && subjectList.size() > 0) {
				subject = subjectList.get(0);
			}
		
			String[] toMailIdsArr = toMailIds.toArray(new String[toMailIds
					.size()]);
			String[] ccMailIdsArr = ccMailIds.toArray(new String[ccMailIds
					.size()]);
			String toAddress = null;
			String ccAddress = null;

			if (toMailIdsArr != null && toMailIdsArr.length > 0
					&& ccMailIdsArr != null && ccMailIdsArr.length > 0) {
				toAddress = Arrays.toString(toMailIdsArr);

				StringTokenizer removeBraces = new StringTokenizer(toAddress,
						"[]");
				toAddress = removeBraces.nextToken();
				ccAddress = Arrays.toString(ccMailIdsArr);
				removeBraces = new StringTokenizer(ccAddress, "[]");
				ccAddress = removeBraces.nextToken();
				System.out.println(toAddress);
				System.out.println(ccAddress);
				System.out.println(subject);
				
				String appURL = PropsUtil.getValue("app.domain")+"/login";
				
				Map<String, Object> nameValueMap = new HashMap<String, Object>();				
		
				nameValueMap.put("message", message);
				nameValueMap.put("plantName", "null");
				nameValueMap.put("materials", "null");
				nameValueMap.put("hostName", appURL);
				nameValueMap.put("delegatedByName", delegatedByName);
				nameValueMap.put("fromDate", fromDate);
				nameValueMap.put("toDate", toDate);

				String mailContent = VelocityStringUtil.getFinalContent("SM",
						templateName, nameValueMap);		
				System.out.println("mailContent >>>>>" + mailContent);
				status = mailMail.sendHtmlMail(null, toAddress, ccAddress, subject,
							mailContent, mailHighPriority);
				
			}
		  }
		}
		if(status!=null && status.getCode()!= -4){
			status.setMessage(delegatedToName);
		}
		return status;
		
		
//		System.out.println("posted on server: success " + delegations.getDelegation().getDelegationId()+" " + delegations.getUsers().getUserId()+" ");
		
	}
	
	   @RequestMapping(value = "/UserService/addEndorsers", method = RequestMethod.POST)     
	    public void save_SupportingEndorsers(@RequestBody AdvisorCreate ac) {
	    	
	               System.out.println("Sucess");
	               userservice.saveSupportingEndorser(ac);
	               
	    }
	   
	   @RequestMapping("/UserService/findByUserid")
		public Users findByUserid(@RequestParam(value ="userId") Integer userId){
			return userservice.findByUserid(userId); 
		}
	   
	   @RequestMapping("/UserService/getPersonToBeRefered")
			public ArrayList<UsersBO> getPersonToBeRefered(@RequestParam(value ="documentId") Integer documentId, @RequestParam(value ="userId") Integer userId  ){
		   
		   		DocumentMaster dm = documentService.getDocumentMasterByDocumentId(documentId);
				int currentSerialNo= 0;
				int parallelAddedBy= 0;
				
				int userIdCurrentlyWith = dm.getCurrentlyWith() == null ? 0 : dm.getCurrentlyWith();
				
				DocUserMaster docCurrentlyWith =	documentService.getDocUserMasterByDocumentIdAndUserId(documentId, userIdCurrentlyWith);	
				if(docCurrentlyWith!=null){
					currentSerialNo= docCurrentlyWith.getSerialNumber();
				}
				
			    List<DocUserMaster> dums= documentService.getDocUserMasterByDocumentIdAscending(documentId);
			    ArrayList<UsersBO> usrs= new ArrayList<UsersBO>();
			    UsersBO usr=null;
//				List<DocUserAction> duas = documentService.getDocUserActionByDocumentIdAndActionStatus(documentId, "Pending_Parallel");
		  
			   		for(DocUserMaster dum:dums){
			   			usr= new UsersBO();
			   			
			   			if( dum.getSerialNumber()==null && userId.equals(dum.getUserId())){
			   				for(DocUserMaster dum1:dums){
				   				if(dum1.getDocRole().equalsIgnoreCase("Controller")){
					   				parallelAddedBy= dum1.getUserId();
					   			}else if(dum1.getDocRole().equalsIgnoreCase("Initiator")){
					   				parallelAddedBy= dum1.getUserId();
					   			}
			   				}
			   				Users user =	userservice.getUsersById(parallelAddedBy);
				    		usr.setUserid(user.getUserId());
				    		usr.setName(user.getFirstName() +" "+ user.getLastName());
				    		usr.setDocRole(dum.getDocRole());
				    		usrs.add(usr);
				    		return usrs; 
			   			}
			   		/*}
		   		
			   		for(DocUserMaster dum:dums){*/
				    	
				    	if((dum.getSerialNumber()!=null && !(userId.equals(dum.getUserId()))) && dum.getSerialNumber()< currentSerialNo){
				    	
				    	Users user =	userservice.getUsersById(dum.getUserId());
				    		usr.setUserid(dum.getUserId());
				    		usr.setName(user.getFirstName() +" "+ user.getLastName());		
				    		usr.setDocRole(dum.getDocRole());
				    		usrs.add(usr);
				    	
	   				}
				   }
	  
				return usrs; 
		   
			}
	   
	 /*  @RequestMapping("/UserService/findUserDelegatesByUserId")
	   public List<UserDelegates> findUserDelegatesByUserId(@RequestParam(value ="userId") Integer userId){
	 		return userservice.findUserDelegatesBasedOnUserId(userId); 
	   }
	   
	   @RequestMapping("/UserService/findUsersToDelegate")
	   public ArrayList<UsersBO> findUsersToDelegate(@RequestParam(value ="userId") Integer userId){
		   List<UserDelegates> userDelegates =  userservice.findUserDelegatesBasedOnUserId(userId);
		   Date currentDate = new Date();
		   Boolean delegationPresent;
		 
		   
		   ArrayList<UsersBO> userBOs = new ArrayList<UsersBO>();
		   UsersBO userBo;
		   for(UserDelegates userdelegate :userDelegates){
			   
			   Users user = userservice.getUsersById(userdelegate.getDelegateId());
				userBo = new UsersBO();
				userBo.setName(user.getFirstName()+" "+ user.getLastName());
				userBo.setUserid(user.getUserId());
				userBOs.add(userBo);
		   }	 
//		   for(UserDelegates userdelegate :userDelegates){
//			   List<Delegation> delegatedUsers = userservice.findUserDelegated(userdelegate.getDelegateId());
//			   for(Delegation dele : delegatedUsers){
//				   Date fromDate = dele.getFromDate();
//				   Date toDate = dele.getToDate();
//				   delegationPresent =((currentDate.after(fromDate)||currentDate.equals(fromDate))&& (currentDate.before(toDate) ||currentDate.equals(toDate)));
//				   if(delegationPresent){
//					   Integer delegatedUser = dele.getUserId();
//					   for(int i = 0; i< userBOs.size(); i++){
//						   Integer usrId = userBOs.get(i).getUserid();
//							 if(usrId.equals(delegatedUser)){
//								 userBOs.remove(i);
//							 }
//						 }
//					   
//				   }
//			   }
//		   }
	 		return userBOs; 
	   }
	   */
	//--------Methods for Testing-----------
/*	@RequestMapping("/users/all")
    public @ResponseBody List<Users> getAllUsers() {
		return userservice.findAllUsers();
	}
	
	@RequestMapping("/docusermaster/all")
    public @ResponseBody List<DocUserMaster> getAllDocUserMaster() {
		return userservice.findAllDocUserMasters();
	}
	
	@RequestMapping("/documentService/getUserRoles")
	public ArrayList<UserRoles> getUserRoles(){
		return documentService.getUserRoles(); 
	}
	@RequestMapping("/documentService/getUserRolesByUserId")
	public UserRoles getUserRolesByUserId(@RequestParam(value ="userId") Integer userId){
		return documentService.getUserRolesByUserId(userId); 
	}
	@RequestMapping("/documentService/getUserRolesByRoleId")
	public UserRoles getUserRolesByRoleId(@RequestParam(value ="roleId") Integer roleId){
		return documentService.getUserRolesByUserId(roleId); 
	}
	
	@RequestMapping("/documentService/getRole")
	public ArrayList<Role> getRole(){
		return documentService.getRole(); 
	}
	
	@RequestMapping("/documentService/getRoleId")
	public Role getRoleId(@RequestParam(value ="roleId") Integer roleId){
		return documentService.getRoleId(roleId); 
	}*/
	   
	@RequestMapping("/documentService/getUserByEmailId")
	public Status getUserByEmailId(@RequestParam("emailId") String emailId,@RequestParam("currentUserId") int currentUserId ) {
		
		 Status outputObj = null ;
	
		 System.out.println("email Id is " +emailId);
		
		Users user = userservice.getEntityByEmailId(emailId);
		if(user == null){
			outputObj = new Status(1,"User does not exist");
		}
		else if(user.getUserId()==currentUserId){
			outputObj = new Status(2,"User can't delegate to himself");
		}/*else if(user.getPlantId()!=null && user.getPlantId()>0){
			outputObj = new Status(1,"Raw material User should not delegate to other plant Users");
		}else if(user.getDeptId()!=null && user.getDeptId()>0){
			outputObj = new Status(1,"Raw material User should not delegate to Corporate it users");
		}*/
		else{
			System.out.println("loggeed in User Id is " + currentUserId);
			ArrayList<UserRoles> currentuserRole = userRoleService.findByUserId(currentUserId);
			ArrayList<String> currentRoles= new ArrayList<String>();
			Role role= null;
			
			for(UserRoles usrRole: currentuserRole){
				role = roleService.findByRoleId(usrRole.getRoleId());
				currentRoles.add(role.getRoleName());
			}
			
//			int currentUserRoleId = currentuserRole.get(0).getRoleId();
//			System.out.println("logged in role id is "+ currentUserRoleId);
			
			ArrayList<String> searchRoles= new ArrayList<String>();
			Role searchRole= null;
			Integer searchUserId =user.getUserId();
			ArrayList<UserRoles> searchuserRole = userRoleService.findByUserId(searchUserId);
			System.out.println("searchuserRole >>>>"+searchuserRole.size());
			if (searchuserRole != null && searchuserRole.size()>1){
				return new Status(14,"User can't delegate to InitEndorser");
			}else if(searchuserRole != null && (searchuserRole.get(0).getRoleId() == 4 || searchuserRole.get(0).getRoleId() == 10)){
				return new Status(15,"User can't delegate to Controller");
			}else if(searchuserRole != null && (searchuserRole.get(0).getRoleId() == 1)){
				return new Status(16,"User can't delegate to Initiator");
			}
			for(UserRoles usrRole:searchuserRole){
				searchRole = roleService.findByRoleId(usrRole.getRoleId());
				searchRoles.add(searchRole.getRoleName());
			}
			Boolean delegation = false;
			Users currentUser = userservice.findByUserid(currentUserId);
			Users searchUser = userservice.findByUserid(searchUserId);
			for(int i =0 ;i<currentRoles.size();i++){
				String currentRole=currentRoles.get(i);
				if(!delegation){
				switch (currentRole) {
				case DocRole.INITIATOR:
					for(int j= 0;j<searchRoles.size();j++){
						String searchRole1=searchRoles.get(j);
						if(DocRole.INITIATOR.equalsIgnoreCase(searchRole1)){
//							delegation= true;
							if(currentUser.getDeptId()!=null && searchUser.getDeptId()!=null && currentUser.getDeptId().equals(searchUser.getDeptId())){
								delegation= true;
								outputObj = new Status(3,"Initiator Delegated To Initiator within same Dept");
							}else if(currentUser.getPlantId()!=null && searchUser.getPlantId()!=null && currentUser.getPlantId().equals(searchUser.getPlantId())){
								delegation= true;
								outputObj = new Status(4,"Initiator Delegated To Initiator in same plant");
							}else if(currentUser.getDeptId()==null && searchUser.getDeptId()==null && currentUser.getPlantId()==null && searchUser.getPlantId()==null){
								delegation= true;
								outputObj = new Status(5,"Initiator Delegated To Initiator in Corporate");
							}
//							outputObj = new Status(3,"Initiator Delegated To Initiator");
						}
					}
					break;
				case DocRole.CONTROLLER:
					for(int j= 0;j<searchRoles.size();j++){
						String searchRole1=searchRoles.get(j);
						if(DocRole.ASSISTANT_CONTROLLER.equalsIgnoreCase(searchRole1)){
//							delegation= true;
//							outputObj = new Status(4,"Controller Delegated To Asst- Controller");
							if(currentUser.getDeptId()!=null && searchUser.getDeptId()!=null && currentUser.getDeptId().equals(searchUser.getDeptId())){
								delegation= true;
								outputObj = new Status(6,"Controller Delegated To Asst- Controller within same Dept");
							}else if(currentUser.getPlantId()!=null && searchUser.getPlantId()!=null && currentUser.getPlantId().equals(searchUser.getPlantId())){
								delegation= true;
								outputObj = new Status(7,"Controller Delegated To Asst- Controller in same plant");
							}else if(currentUser.getDeptId()==null && searchUser.getDeptId()==null && currentUser.getPlantId()==null && searchUser.getPlantId()==null){
								delegation= true;
								outputObj = new Status(8,"Controller Delegated To Asst- Controller in Corporate");
							}
						}
					}
					break;	
				default:
					for(int j= 0;j<searchRoles.size();j++){
					if(i==currentRoles.size()-1 && !(currentRole.equalsIgnoreCase(DocRole.CONTROLLER)) && !(currentRole.equalsIgnoreCase(DocRole.INITIATOR)) && !(searchRoles.get(j).equalsIgnoreCase(DocRole.INITIATOR))){
						delegation= true;
						outputObj = new Status(9,"Other to Other");
						/*if(currentUser.getDeptId()!=null && searchUser.getDeptId()!=null && currentUser.getDeptId().equals(searchUser.getDeptId())){
							delegation= true;
							outputObj = new Status(5,"Other to Other with same Dept");
						}else if(currentUser.getPlantId()!=null && searchUser.getPlantId()!=null && currentUser.getPlantId().equals(searchUser.getPlantId())){
							delegation= true;
							outputObj = new Status(6,"Other to other with same plant");
						}else if(currentUser.getDeptId()==null && searchUser.getDeptId()==null && currentUser.getPlantId()==null && searchUser.getPlantId()==null){
							delegation= true;
							outputObj = new Status(6,"Other to other in Corporate");
						}*/
					}	
					}
					break;
				}
				}
//				for(int j= 0;j<currentRoles.size();j++){
//					if(SearchRole.equals(currentRoles.get(j))){
//						//Init -> Init, Endorser -> Endorser, Approver -> Approver
//						if(DocRole.INITIATOR.equalsIgnoreCase(currentRoles.get(j))){
//							// Delegate Initiator to Initiator
//							
//						}
//					}
//				}
			}
			if(!delegation){
				outputObj = new Status(10,"Not An Appropriate Delegatee");
			}
			
//			int searchUserRoleId = searchuserRole.get(0).getRoleId();
//			System.out.println("search user role id is "+searchUserRoleId);
		/*	if(currentUserRoleId == searchUserRoleId ){                                              //intiator can send to initiators
				outputObj = new Status(3,"user delegation allowed");
			}
			else{
				outputObj = new Status(4,"User delegation Not allowed");
			}*/
		}
		
		return outputObj;
		
	}
	
	@RequestMapping("/RoleService/findRoleByUserid")
	public Role findRoleByUserid(@RequestParam(value = "userId") Integer userId) {

		UserRoles userRole = userRoleService.findByUserId(userId).get(0);
		Role role = roleService.findByRoleId(userRole.getRoleId());

		return role;
	}

}
