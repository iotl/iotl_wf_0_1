package com.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.map.MultiValueMap;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gt.bo.BusinessesBO;
import com.gt.bo.CompaniesBO;
import com.gt.bo.InsertNewUserBO;
import com.gt.bo.MaterialBO;
import com.gt.bo.PlantsBO;
import com.gt.bo.SBUsBO;
import com.gt.bo.UsersBO;
import com.gt.entity.Businesses;
import com.gt.entity.Companies;
import com.gt.entity.Material;
import com.gt.entity.Plant;
import com.gt.entity.Sbus;
import com.gt.services.AdminService;
import com.gt.utils.PropsUtil;

@RestController
public class AdminController {
	
	public static final Logger logger = Logger
			.getLogger(AdminController.class.getName());
	
	@Autowired
	private AdminService adminService;
	
	@RequestMapping(value = "/admin/updateMaterial", method = RequestMethod.POST)
	public @ResponseBody/* String */Status updateMaterial(
			@RequestBody MaterialBO matBO) {
		List<Material> materialList = matBO.getMaterialList();
		List<Material> deleteList=matBO.getMaterialListToDelete();
		for(Material material:materialList){
			Boolean duplicate = null;
			System.out.println("assa"+material.getBusinessId()+"matId"+material.getMaterialId());
			if(material.getMaterialId()==0){
				List<Material> getMaxMaterialId=adminService.findAllMaterials();
				if(getMaxMaterialId!=null && getMaxMaterialId.size()>0){
					int size=getMaxMaterialId.size()-1;
					Material materialtemp=(Material) getMaxMaterialId.get(size);
					material.setMaterialId(materialtemp.getMaterialId()+1);
					material.setIsActive(true);
					material.setDeptId(0);
				}else{
					material.setMaterialId(1);
					material.setIsActive(true);
					material.setDeptId(0);
				}
			}else{
				duplicate = adminService.isMaterialDuplicated(material);			
//				material.setClusterId(0);
			}
			if(duplicate!= null && duplicate){
				System.out.println("Material name or  its abbreviation already exists");
				return new Status(1, "Material name or  its abbreviation already exists",material);
			}
			adminService.saveMaterial(material);
		}
		
		for(Material deleteMaterial:deleteList){
			adminService.deleteByMaterialId(deleteMaterial.getMaterialId());
		}
		
			/////
		
		/*List<Material> matListBybusinessId=adminService.findByBusinessIdAndDeptId(1,0);
		  JSONObject obj = new JSONObject();
		  
		  JSONArray list = new JSONArray();
		  Map<Object,Object> map=null;
		  for(Material material:matListBybusinessId){
		   map=new HashMap<Object,Object>();
		   //obj.put("", material);
		   map.put("materialId", material.getMaterialId());
		   map.put("materialName", material.getMaterialName());
		   map.put("abbreviation", material.getAbbreviation());
		   map.put("businessId", material.getBusinessId());
		   map.put("deptId", material.getDeptId());
		   map.put("clusterId", material.getClusterId());
		   map.put("doa", material.getDoa());
		  
		   if((material.getIsActive()==null || material.getIsActive()) && (material.getClusterId() == null || material.getClusterId()==0)){
		    list.add(map);
		   }
		  }
		  
		  obj.put("mat1", list);
		  matListBybusinessId=adminService.findByBusinessIdAndDeptId(2,0);
		  list = new JSONArray();
		  for(Material material:matListBybusinessId){
		   map=new HashMap<Object,Object>();
		   //obj.put("", material);
		   map.put("materialId", material.getMaterialId());
		   map.put("materialName", material.getMaterialName());
		   map.put("abbreviation", material.getAbbreviation());
		   map.put("businessId", material.getBusinessId());
		   map.put("deptId", material.getDeptId());
		   map.put("clusterId", material.getClusterId());
		   map.put("doa", material.getDoa());
		  
		   if((material.getIsActive()==null || material.getIsActive()) && (material.getClusterId() == null || material.getClusterId()==0)){
		    list.add(map);
		   }
		  }
		  
		  obj.put("mat2", list);
		  matListBybusinessId=adminService.findByBusinessIdAndDeptId(3,0);
		  list = new JSONArray();
		  for(Material material:matListBybusinessId){
		   map=new HashMap<Object,Object>();
		   //obj.put("", material);
		   map.put("materialId", material.getMaterialId());
		   map.put("materialName", material.getMaterialName());
		   map.put("abbreviation", material.getAbbreviation());
		   map.put("businessId", material.getBusinessId());
		   map.put("deptId", material.getDeptId());
		   map.put("clusterId", material.getClusterId());
		   map.put("doa", material.getDoa()); 
		   if((material.getIsActive()==null || material.getIsActive()) && (material.getClusterId() == null || material.getClusterId()==0)){
		    list.add(map);
		   }
		  }
		  
		  obj.put("mat3", list);
		  
		JSONObject objTmp = new JSONObject();
		  
		  JSONArray listTmp = new JSONArray();
		  
		  matListBybusinessId=adminService.findByBusinessIdIsNull();
		  //list = new JSONArray();
		  for(Material material:matListBybusinessId){
		   map=new HashMap<Object,Object>();
		   //obj.put("", material);
		   map.put("materialId", material.getMaterialId());
		   map.put("materialName", material.getMaterialName());
		   map.put("abbreviation", material.getAbbreviation());
		   map.put("businessId", material.getBusinessId());
		   map.put("deptId", material.getDeptId());
		   map.put("clusterId", material.getClusterId());
		   map.put("doa", material.getDoa());
		   if((material.getIsActive()==null || material.getIsActive()) && (material.getClusterId() == null || material.getClusterId()==5)){
		    listTmp.add(map);
		   }
		  }
		  listTmp.addAll((JSONArray) obj.get("mat1"));
		  //listTmp=;
		  objTmp.put("mat1", listTmp);
		
		//////
		List<Material> matListBybusinessId=adminService.findByBusinessIdAndDeptId(1,null);
		JSONObject obj = new JSONObject();
		
		JSONArray list = new JSONArray();
		Map<Object,Object> map=null;
		for(Material material:matListBybusinessId){
			map=new HashMap<Object,Object>();
			//obj.put("", material);
			map.put("materialId", material.getMaterialId());
			map.put("materialName", material.getMaterialName());
			map.put("abbreviation", material.getAbbreviation());
			map.put("businessId", material.getBusinessId());
			if((material.getIsActive()==null || material.getIsActive()) && (material.getClusterId() == null || material.getClusterId()==0)){
				list.add(map);
			}
		}
		
		obj.put("mat1", list);
		matListBybusinessId=adminService.findByBusinessIdAndDeptId(2,null);
		list = new JSONArray();
		for(Material material:matListBybusinessId){
			map=new HashMap<Object,Object>();
			//obj.put("", material);
			map.put("materialId", material.getMaterialId());
			map.put("materialName", material.getMaterialName());
			map.put("abbreviation", material.getAbbreviation());
			map.put("businessId", material.getBusinessId());
			if((material.getIsActive()==null || material.getIsActive()) && (material.getClusterId() == null || material.getClusterId()==0)){
				list.add(map);
			}
		}
		
		obj.put("mat2", list);
		matListBybusinessId=adminService.findByBusinessIdAndDeptId(3,null);
		list = new JSONArray();
		for(Material material:matListBybusinessId){
			map=new HashMap<Object,Object>();
			//obj.put("", material);
			map.put("materialId", material.getMaterialId());
			map.put("materialName", material.getMaterialName());
			map.put("abbreviation", material.getAbbreviation());
			map.put("businessId", material.getBusinessId());
			if((material.getIsActive()==null || material.getIsActive()) && (material.getClusterId() == null || material.getClusterId()==0)){
				list.add(map);
			}
		}
		
		obj.put("mat3", list);
		
JSONObject objTmp = new JSONObject();
		
		JSONArray listTmp = new JSONArray();
		
		matListBybusinessId=adminService.findByBusinessIdIsNull();
		//list = new JSONArray();
		for(Material material:matListBybusinessId){
			map=new HashMap<Object,Object>();
			//obj.put("", material);
			map.put("materialId", material.getMaterialId());
			map.put("materialName", material.getMaterialName());
			map.put("abbreviation", material.getAbbreviation());
			map.put("businessId", material.getBusinessId());
			if((material.getIsActive()==null || material.getIsActive()) && (material.getClusterId() == null || material.getClusterId()==5)){
				listTmp.add(map);
			}
		}
		listTmp.addAll((JSONArray) obj.get("mat1"));
		//listTmp=;
		objTmp.put("mat1", listTmp);

//		JSONObject objtemp = new JSONObject();
//		objtemp.put("MaterialData", obj);
		try {
			 String path=System.getProperty("user.dir");
			 String appMode=PropsUtil.getValue("app.mode"); 
			 if(appMode.equals("debug")){
			 if(path!=null && path.lastIndexOf("WF")!=-1){
				 path=path.substring(0,path.lastIndexOf("WF"));
			 }
			 }
			 String test="MaterialData="+obj.toJSONString().replace("\\", "");
			FileWriter file = new FileWriter(path+"\\UI\\material.json");
			file.write(test);
			file.flush();
			file.close();

			test="MaterialData="+objTmp.toJSONString().replace("\\", "");
			 file = new FileWriter(path+"\\UI\\material_utkal.json");
				file.write(test);
				file.flush();
				file.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.print(obj);*/

		return null;
	}
	
	
	
	
	
	/**
	 * added for user management on 4-12-15
	 * @param insertNewUserBO
	 * @return
	 */
	
	@RequestMapping(value = "/admin/insertNewUser", method = RequestMethod.POST)
	public @ResponseBody Status insertNewUser(@RequestBody InsertNewUserBO insertNewUserBO) {
		
		try{
		
			return adminService.saveUser(insertNewUserBO);
		}
		catch(Exception e){
			
		}
		return null;
	}
	
	@RequestMapping("/admin/getAllImmediateEndorsers")
	public ArrayList<UsersBO> getAllImmediateEndorsers()  {

		return adminService.getAllEndorsers();
	}	
	
	
	@RequestMapping("/admin/updateSampleDAO")
	public String updateSampleDAO(@RequestParam("fileName") String fileName){
		
		return adminService.updateSampleDAO(fileName);
		
	}
	
	
	@RequestMapping("/admin/getAllMasterData")
	public  @ResponseBody Map getAllMasterData(@RequestParam("MISReportData") Boolean MISReportData){

		
		 String plantReportEnabled=PropsUtil.getValue("coporate.plant.misreport"); 
		
		MultiValueMap subMap = new MultiValueMap();
		
		
		List<Companies> companyList = adminService.findAllCompany();
		List<Businesses> businessList1 = adminService.findAllBusiness();
		List<Sbus> sbuList1 = adminService.findAllSbus();
		List<Businesses> businessList;
		List<Sbus> sbuList;
		List<Plant> plantList;
		int companyId;
		int businessId;
		int sbuId;
		Companies comp = new Companies();
		Businesses busi1 =new Businesses();
		Sbus sbu1 =new Sbus();
		
		if(MISReportData.equals(true)){
		
		comp.setCompanyId(-1);
		comp.setAbbreviation("ALC");
		comp.setCompanyName(" All-Company");
		
		
		busi1.setBusinessId(-1);
		busi1.setAbbreviation("ALB");
		busi1.setBusinessName(" All-Business");
		
		
		sbu1.setSbuId(-1);
		sbu1.setAbbreviation("ALS");
		sbu1.setSbuName(" All-SBU");
		
		companyList.add(comp);
		businessList1.add(busi1);
		sbuList1.add(sbu1);
		}
		/*for(Businesses busi:adminService.findAllBusiness()){
			subMap.put("allBusiness", busi);
		}*/
		
		for(Businesses busi:businessList1){
		subMap.put("allBusiness",busi);
		}
		for(Sbus sbu:sbuList1){
			subMap.put("allSbu",sbu);
			}
		for(Plant plant:adminService.findByisCorp()){
			subMap.put("allPlants",plant);
			}
				
		for(Companies companies:companyList){
			companyId=companies.getCompanyId();
			//find all businnes by companyId;
			businessList = adminService.findByCompanyId(companyId);
			if(MISReportData.equals(true)){
				businessList.add(busi1);
			}
			for(Businesses businesses:businessList)
			{
			subMap.put("a"+companyId, businesses);
			businessId = businesses.getBusinessId();
			sbuList = adminService.findByCompanyIdAndBusinessId(companyId, businessId);
			if(MISReportData.equals(true)){
				sbuList.add(sbu1);
			}
				for(Sbus sbu:sbuList){
					
					subMap.put("ab"+companyId+"busi"+businessId, sbu);
					sbuId=sbu.getSbuId();
					plantList = adminService.findByCompanyIdAndBusinessIdAndSbuIdAndisCorp(companyId, businessId, sbuId);
					for(Plant plant : plantList){
						subMap.put("as"+companyId +"busi"+ businessId +"sbu"+ sbuId, plant);
						
					}
					
					
				}			
			
			
			}
		
		}
		if(plantReportEnabled.equalsIgnoreCase("enable")){
			
			plantList = adminService.findByIsPlant();
			subMap.put("plantLoginList",plantList);
			
		}
				
		
		
		return subMap; 
		
	}
	
	@RequestMapping("/admin/getMaterialData")
	public  @ResponseBody List<Material> getMaterialData(@RequestParam("businessId") Integer businessId,@RequestParam("sbuId") Integer sbuId,@RequestParam("plantId") Integer plantId){
		List<Material> matList=new ArrayList<Material> ();
		if(businessId==-1){
			if(sbuId==-1){
				if(plantId==-1){
					matList.addAll(adminService.findByBusinessIdAndDeptId(businessId, 0));
					
				}else{
					Plant plantSelect =adminService.findByPlantId(plantId);
					Integer plantBusinessId= plantSelect.getBusinessId();
					Integer plantClusterId = plantSelect.getClusterId();
					matList.addAll(adminService.findByBusinessIdAndDeptId(plantBusinessId, 0));
					
					matList.addAll(adminService.findMaterialByClusterId(plantClusterId));	
					
					
					}
			}else{
				if(plantId==-1){
					Integer sbuBusinessId = adminService.findBySbuId(sbuId).getBusinessId();
					matList.addAll(adminService.findByBusinessIdAndDeptId(sbuBusinessId, 0));
					
				}else{
					Plant plantSelect =adminService.findByPlantId(plantId);
					Integer plantBusinessId= plantSelect.getBusinessId();
					Integer plantClusterId = plantSelect.getClusterId();
					matList.addAll(adminService.findByBusinessIdAndDeptId(plantBusinessId, 0));
					
					matList.addAll(adminService.findMaterialByClusterId(plantClusterId));	
					
				}
				
			}
			return matList;
			
		}else{
			return adminService.findByBusinessIdAndDeptId(businessId,0);
		}
//		return null;
		
		
		
		
	}
	
	@RequestMapping(value = "/admin/addCompanyDetails", method = RequestMethod.POST)
	public @ResponseBody Companies addCompanyDetails(
			@RequestBody CompaniesBO companyBO) {
		Companies company=new Companies();
		int companyId=adminService.findMaxCompanyId()+1;
		company.setCompanyId(companyId);
		company.setCompanyName(companyBO.getCompanyName());
		company.setAbbreviation(companyBO.getAbbreviation());
		Companies companies=	adminService.saveCompany(company);
	
		return companies;
	}
	
	
	@RequestMapping(value="/admin/updateSbuDetails", method = RequestMethod.POST)
	public @ResponseBody Status saveUpdateSbuDetails(@RequestBody SBUsBO sbuBO){
	
	int sbuId = sbuBO.getSbuId();
	int count = adminService.findDocumentMasterCountBySbuId(sbuId);
	Status status = new Status();
	if(count!=0){
		status.setCode(1);
		status.setMessage("Cannot update Sbu ,since Proposals created with that Sbu ");
	}
	else{
		adminService.updateSbuBySbuId(sbuBO.getName(), sbuBO.getAbbreviation(), sbuId);
		status.setCode(2);
		status.setMessage("Sbu updated Successfully");
		
	}
	return status;
	}
	
	
	@RequestMapping(value="/admin/addSbuDetails", method = RequestMethod.POST)
	public @ResponseBody Sbus saveNewSbuDetails(@RequestBody SBUsBO sbuBO){
	
	Sbus sbu  = new Sbus();
 
	sbu.setCompanyId(sbuBO.getCompanyId());
	sbu.setBusinessId(sbuBO.getBusinessId());
	sbu.setSbuName(sbuBO.getName());
	sbu.setAbbreviation(sbuBO.getAbbreviation());
	
	Sbus sbuPersisted = adminService.saveSbu(sbu);
	 
	sbu =null; 
	logger.info("Sbu added successfully with Sbu Id"+sbuPersisted.getSbuId()); 
	return sbuPersisted;
	}
	
	@RequestMapping(value = "/admin/addBusinessDetails", method = RequestMethod.POST)
	public @ResponseBody Businesses addBusinessDetails(
			@RequestBody BusinessesBO businessBO) {
		Businesses  duplicateObj=adminService.findByBusinessNameAndAbbreviation(businessBO.getBusinessName(),businessBO.getAbbreviation());
		Businesses businessObj=new Businesses();
		int businessId=adminService.findMaxBusinessId()+1;
		if(duplicateObj!=null){
			businessObj.setBusinessId(duplicateObj.getBusinessId());
		}else{
			businessObj.setBusinessId(businessId);
		}
		businessObj.setBusinessName(businessBO.getBusinessName());
		businessObj.setAbbreviation(businessBO.getAbbreviation());
		businessObj.setCompanyId(businessBO.getCompanyId());
		Businesses businesses=adminService.saveBusiness(businessObj);
		return businesses;
	}
	
	
	@RequestMapping(value = "/admin/updateBusinessDetails", method = RequestMethod.POST)
	public @ResponseBody Status updateBusinessDetails(
			@RequestBody BusinessesBO businessBO) {
		
	    int businessId;
	    int count;
	    Status status = new Status();
	    businessId = Integer.valueOf(businessBO.getBusinessId());
	    count  = adminService.findDocumentMasterCountByBusinessId(businessId);
	    if(count!=0){
	    	status.setCode(1);
	    	status.setMessage("Cannot update Business,since Proposals created with that business ");
	    }
	    else{
	    	adminService.updateBusinessByBusinessId(businessBO.getBusinessName(), businessBO.getAbbreviation(), businessId);
	    	status.setCode(2);
	    	status.setMessage("Business updated Successfully");
	    }
		return status;
		
	}
	
	@RequestMapping(value = "/admin/updateCompanyDetails", method = RequestMethod.POST)
	public @ResponseBody Status updateCompanyDetails(
			@RequestBody CompaniesBO companyBO) {
		
		int companyId = Integer.valueOf(companyBO.getCompanyId());
		Status status = new Status();
		int count = adminService.findDocumentMasterCountByCompanyId(companyId);
		if(count !=0){
			status.setCode(1);
			status.setMessage("Cannot update Company,since Proposals created with that company ");
		}
		else{
		Companies company=new Companies();
		company.setCompanyId(companyId);
		company.setCompanyName(companyBO.getCompanyName());
		company.setAbbreviation(companyBO.getAbbreviation());
		adminService.saveCompany(company);
		status.setCode(2);
		status.setMessage("Company updated successfully");
		}
		return status;
	}
	
	@RequestMapping(value="/admin/updatePlantDetails", method = RequestMethod.POST)
	public @ResponseBody Status saveUpdatePlantDetails(@RequestBody PlantsBO plantBO){
		
	Plant plant = 	plantBO.getPlant();
	int plantId = plant.getPlantId();
	int count = adminService.findDocumentMasterCountByPlantId(plantId);
	Status status = new Status();
	if(count!=0){
		status.setCode(1);
		status.setMessage("Cannot update Plant ,since Proposals created with that Plant");
	}
	else{
		status.setCode(2);	
		status.setMessage("Updated Plant Details Succesfully");
	 adminService.updatePlantByPlantId(plant.getPlantName(), plant.getAbbreviation(), plant.getPlantId());
     logger.info("Plant is update with plant Id = "+plant.getPlantId());
	}
	return  status ;
	}
	
	
	@RequestMapping(value="/admin/saveNewPlantDetails", method = RequestMethod.POST)
	public @ResponseBody Plant saveNewPlantDetails(@RequestBody PlantsBO plantBO){
		
	Plant plant = 	plantBO.getPlant();
		
    Plant persistedPlant = adminService.savePlant(plant);
    logger.info("Plant is added with plant Id = "+persistedPlant.getPlantId()); 
	return persistedPlant;
	}
	

	/**
	 * @param emailId
	 * @return
	 */
	@RequestMapping(value = "/admin/getUserDetailsByEmailId", method = RequestMethod.POST)
	public  InsertNewUserBO getUserDetailsByEmailId(@RequestBody InsertNewUserBO insertNewUserBO) {
		
		try{
			System.out.println("emailId >>>+"+insertNewUserBO.getEmailId());
		//System.out.println("emailId >>>+emailId"+request.getParameter("email"));
			return adminService.getUserDetailsByEmailId(insertNewUserBO.getEmailId());
		}
		catch(Exception e){
			
		}
		return new InsertNewUserBO();
	}
	
	@RequestMapping(value = "/admin/updateUser", method = RequestMethod.POST)
	public @ResponseBody Status updateUser(@RequestBody InsertNewUserBO insertNewUserBO) {
		
		try{
		
			return adminService.updateUser(insertNewUserBO);
		}
		catch(Exception e){
			
		}
		return null;
	}
	
	
	@RequestMapping(value="/admin/addExistingBusinessToCompanies",method=RequestMethod.POST)
	public @ResponseBody Status addExistingBusinessToCompanies(@RequestBody BusinessesBO businessBO) {
		
		try{
			
			int companyId = businessBO.getCompanyId();
			System.out.println("companyId is "+companyId);
			List<Businesses> previousMappedBusinesses = businessBO.getPreviousBusinessList();
			System.out.println("previous list size is " + previousMappedBusinesses.size());
			
			
			List<Businesses> newMappedBusinesses = businessBO.getNewBusinessList();
			System.out.println("new  list size is " + newMappedBusinesses.size());
			
			if(previousMappedBusinesses.size() ==0){
				for(Businesses newBusiness:newMappedBusinesses){
					System.out.println("business to be Added "+newBusiness.getBusinessId());
					//sent for Addition
					newBusiness.setCompanyId(companyId);
					adminService.saveBusiness(newBusiness);
				}
				return null;
			}
			
			int count=0;
			 for(Businesses previousBusiness:previousMappedBusinesses){
				count=0;
				loop2:for(Businesses newBusiness:newMappedBusinesses){
					
					if(previousBusiness.getBusinessId()==newBusiness.getBusinessId()){
						//dont send previousbusiness to addition
						break loop2;
					}
					else{
						count=count+1;
						if(count == newMappedBusinesses.size()){
							//sent for deletion
							System.out.println("business to be deleted "+previousBusiness.getBusinessId());
							System.out.println("companyId is "+previousBusiness.getCompanyId() + previousBusiness.getBusinessName()+previousBusiness.getAbbreviation());
							adminService.deleteBusiness(previousBusiness);
							
						}
						
					}
					
					
				}
				
				
			}
			
			int count1=0; 
			for(Businesses newBusiness:newMappedBusinesses) {
				count1=0;
			 loop2:	for(Businesses previousBusiness:previousMappedBusinesses){
					if(newBusiness.getBusinessId()==previousBusiness.getBusinessId()){
						break loop2;
						
					}
					else{
						count1=count1+1;
						if(count1 == previousMappedBusinesses.size() ){
							
							System.out.println("business to be Added "+newBusiness.getBusinessId());
							//sent for Addition
							newBusiness.setCompanyId(companyId);
							adminService.saveBusiness(newBusiness);
						}
						
					}
					
					
				}
				
			}
			
			
			//return adminService.updateUser(insertNewUserBO);
		}
		catch(Exception e){
			
		}
		return null;
	}
	
	
	@RequestMapping(value="/admin/getSbusByNotInCompanyIdAndNotInBusinessId",method=RequestMethod.POST)
	public  @ResponseBody List<Sbus> getSbusByNotInCompanyIdAndNotInBusinessId(@RequestBody SBUsBO sbusBO) {
		
		List<Integer> sbuIdList = new ArrayList<Integer>();
		List<Sbus> sbusList = sbusBO.getPreviousSBUsList();
		List<Sbus> sbusListRet = new ArrayList<>();
		if(sbusList.size() == 0){
			sbusListRet = adminService.findAllSbus();
		}
		else{
		for(Sbus sbus :sbusList){
			sbuIdList.add(sbus.getSbuId());
		}
		sbusListRet = adminService.findByNotCompanyIdAndNotBusinessId(sbusBO.getCompanyId(),sbusBO.getBusinessId(),sbuIdList);
		}
		
				return sbusListRet;
	}
	
	@RequestMapping(value="/admin/addExistingSBUs",method=RequestMethod.POST)
	public @ResponseBody Status addExistingSBUs(@RequestBody SBUsBO sbusBO) {
		
		try{
			
			int companyId = sbusBO.getCompanyId();
			int businessId=sbusBO.getBusinessId();
			List<Sbus> previousMappedSBUs = sbusBO.getPreviousSBUsList();
			List<Sbus> newMappedSbus= sbusBO.getNewSBUsList();
			if (previousMappedSBUs.size() == 0) {

				for (Sbus newSbus : newMappedSbus) {
					newSbus.setCompanyId(companyId);
					newSbus.setBusinessId(businessId);
					adminService.saveSbus(newSbus);

				}
				return null;

			}
			
			int count=0;
			 for(Sbus previousSbus:previousMappedSBUs){
				count=0;
				loop2:for(Sbus newSbus:newMappedSbus){
					
					if(previousSbus.getSbuId()==newSbus.getSbuId()){
						break loop2;
					}
					else{
						count=count+1;
						if(count == newMappedSbus.size()){
							adminService.deleteSbus(previousSbus);
						}
					}
				}
				
				
				
			}
			
			int count1=0; 
			for(Sbus newSbus:newMappedSbus) {
				count1=0;
			 loop2:	for(Sbus previousSbus:previousMappedSBUs){
					if(newSbus.getSbuId()==previousSbus.getSbuId()){
						break loop2;
					}
					else{
						count1=count1+1;
						if(count1 == previousMappedSBUs.size() ){
							newSbus.setCompanyId(companyId);
							newSbus.setBusinessId(businessId);
							adminService.saveSbus(newSbus);
						}
						
					}
					
					
				}
				
			}
			
		}
		catch(Exception e){
			
		}
		return null;
	}
	
	@RequestMapping(value="/admin/addExistingPlantsToSbuBusinessCompany",method=RequestMethod.POST)
	public @ResponseBody Status addExistingPlantsToSbuBusinessCompany(@RequestBody PlantsBO plantsBO) {
		
		try{
			
			int companyId = plantsBO.getCompanyId();
			int businessId = plantsBO.getBusinessId();
			int sbuId = plantsBO.getSbuId(); 
			System.out.println("companyId is "+companyId +"business Id is "+businessId+"sbu Id id "+sbuId);
			
			List<Plant> previousPlantList = plantsBO.getPreviousPlantList();
			System.out.println("previous list size is " + previousPlantList.size());
			
			
			List<Plant> newPlantList = plantsBO.getNewPlantList();
			System.out.println("new  list size is " + newPlantList.size());
			
			if(previousPlantList.size()==0){
				for(Plant newPlant:newPlantList) {
					System.out.println("Plant  to be Added "+newPlant.getPlantId());
					//sent for Addition
					newPlant.setCompanyId(companyId);
					newPlant.setBusinessId(businessId);
					newPlant.setSbuId(sbuId);
					int pkId = adminService.findMaxPrimaryKeyofPlant();
					newPlant.setPkId(pkId+1);
					adminService.savePlant(newPlant);
					
				}
				return null;
				
			}
			
			int count=0;
			 for(Plant previousPlant:previousPlantList){
				count=0;
				loop2:for(Plant newPlant:newPlantList){
					
					if(previousPlant.getPlantId()==newPlant.getPlantId()){
						//dont send previousbusiness to addition
						break loop2;
					}
					else{
						count=count+1;
						if(count == newPlantList.size()){
							//sent for deletion
							System.out.println("plant Id is "+previousPlant.getPlantId()+"pk Id id "+previousPlant.getPkId());
							adminService.deletePlant(previousPlant);
							
						}
						
					}
					
					
				}
				
				
			}
			
			int count1=0; 
			for(Plant newPlant:newPlantList) {
				count1=0;
			 loop2:	for(Plant previousPlant:previousPlantList){
					if(newPlant.getPlantId()== previousPlant.getPlantId()){
						break loop2;
						
					}
					else{
						count1=count1+1;
						if(count1 == previousPlantList.size() ){
							
							System.out.println("Plant  to be Added "+newPlant.getPlantId());
							//sent for Addition
							newPlant.setCompanyId(companyId);
							newPlant.setBusinessId(businessId);
							newPlant.setSbuId(sbuId);
							int pkId = adminService.findMaxPrimaryKeyofPlant();
							newPlant.setPkId(pkId+1);
							adminService.savePlant(newPlant);
						}
						
					}
					
					
				}
				
			}
			
		}
		catch(Exception e){
			
		}
		return null;
	}
	
	
}
