package com.controllers;

public class DocumentData {
	private String plantName;
	private Boolean inBudget;
	private String annualQuantity;
	private String orderQuantity;
	private String pNBRate;
	private String poleStarRate;
	private String negotiatedRate;
	private String negotiatedAmt;
	private String expectedSavingLossCmpTo;
	
	public DocumentData(){}
	
	public DocumentData(String docNumber) {
		// TODO Auto-generated constructor stub
	}
	
	public String getPlantName() {
		return plantName;
	}



	public void setPlantName(String plantName) {
		this.plantName = plantName;
	}

	

	public String getAnnualQuantity() {
		return annualQuantity;
	}

	public void setAnnualQuantity(String annualQuantity) {
		this.annualQuantity = annualQuantity;
	}

	public Boolean getInBudget() {
		return inBudget;
	}

	public void setInBudget(Boolean inBudget) {
		this.inBudget = inBudget;
	}


	
	public String getOrderQuantity() {
		return orderQuantity;
	}
	public void setOrderQuantity(String orderQuantity) {
		this.orderQuantity = orderQuantity;
	}
	
	public String getpNBRate() {
		return pNBRate;
	}
	public void setpNBRate(String pNBRate) {
		this.pNBRate = pNBRate;
	}
	
	public String getPoleStarRate() {
		return poleStarRate;
	}
	public void setPoleStarRate(String poleStarRate) {
		this.poleStarRate = poleStarRate;
	}
	
	public String getNegotiatedRate() {
		return negotiatedRate;
	}
	public void setNegotiatedRate(String negogiatedRate) {
		this.negotiatedRate = negogiatedRate;
	}
	
	public String getNegotiatedAmt() {
		return negotiatedAmt;
	}
	public void setNegotiatedAmt(String negogiatedAmt) {
		this.negotiatedAmt = negogiatedAmt;
	}
	
	public String getExpectedSavingLossCmpTo() {
		return expectedSavingLossCmpTo;
	}
	public void setExpectedSavingLossCmpTo(String expectedSavingLossCmpTo) {
		this.expectedSavingLossCmpTo = expectedSavingLossCmpTo;
	}



	
	

}
